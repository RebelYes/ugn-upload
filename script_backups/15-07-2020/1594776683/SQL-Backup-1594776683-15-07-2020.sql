-- phpMyAdmin SQL Dump
-- http://www.phpmyadmin.net
--
-- Host Connection Info: Localhost via UNIX socket
-- Generation Time: July 15, 2020 at 01:31 AM ( UTC )
-- Server version: 5.7.31
-- PHP Version: 7.3.19
--

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


-- ---------------------------------------------------------
--
-- Table structure for table : `activities`
--
-- ---------------------------------------------------------

CREATE TABLE `activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `track_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `track_id` (`track_id`),
  KEY `time` (`time`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities`
--

INSERT INTO activities VALUES
("2","3","2","uploaded_track","1592701542"),
("3","3","3","uploaded_track","1592708827"),
("4","3","4","uploaded_track","1592709834"),
("5","3","5","uploaded_track","1592710074"),
("6","8","6","uploaded_track","1592714416"),
("7","8","7","uploaded_track","1592714416"),
("8","8","8","uploaded_track","1592714417"),
("9","8","9","uploaded_track","1592714417"),
("11","8","7","liked_track","1592716475"),
("12","8","6","liked_track","1592716736"),
("15","8","9","liked_track","1592748894"),
("16","10","20","uploaded_track","1592780788"),
("17","10","21","uploaded_track","1592782769"),
("18","10","22","uploaded_track","1592783184"),
("19","10","23","uploaded_track","1592783505"),
("20","10","24","uploaded_track","1592783669"),
("21","10","25","uploaded_track","1592784306"),
("22","10","26","uploaded_track","1592784719"),
("23","14","27","uploaded_track","1592845227"),
("39","16","43","uploaded_track","1592938838"),
("40","16","44","uploaded_track","1592939387"),
("41","3","45","uploaded_track","1592939479"),
("42","16","46","uploaded_track","1592940148"),
("43","16","47","uploaded_track","1592940338"),
("44","16","48","uploaded_track","1592940631"),
("45","16","49","uploaded_track","1592941004"),
("50","19","54","uploaded_track","1592968547"),
("51","20","58","uploaded_track","1593026357"),
("52","20","59","uploaded_track","1593027076"),
("53","20","60","uploaded_track","1593027883"),
("54","14","61","uploaded_track","1593054608"),
("55","14","62","uploaded_track","1593054759"),
("56","14","63","uploaded_track","1593055273"),
("57","14","64","uploaded_track","1593055649"),
("58","14","65","uploaded_track","1593055664"),
("59","14","66","uploaded_track","1593056151"),
("60","14","67","uploaded_track","1593056173"),
("61","14","68","uploaded_track","1593056299"),
("62","14","69","uploaded_track","1593056445"),
("63","14","70","uploaded_track","1593056750"),
("64","24","71","uploaded_track","1593059781"),
("65","14","60","liked_track","1593098038"),
("66","14","72","uploaded_track","1593098531"),
("68","27","74","uploaded_track","1593114464"),
("69","24","71","commented_track","1593114953"),
("70","3","74","liked_track","1593125557"),
("71","31","75","uploaded_track","1593131010"),
("72","30","76","uploaded_track","1593131540"),
("73","30","77","uploaded_track","1593131541"),
("74","30","78","uploaded_track","1593132207"),
("75","30","79","uploaded_track","1593132207"),
("76","30","80","uploaded_track","1593132207"),
("77","30","81","uploaded_track","1593132207"),
("78","30","82","uploaded_track","1593132207"),
("79","30","83","uploaded_track","1593132207"),
("80","30","84","uploaded_track","1593132208"),
("81","30","85","uploaded_track","1593137998"),
("82","30","86","uploaded_track","1593138045"),
("83","30","87","uploaded_track","1593138045"),
("84","30","88","uploaded_track","1593138045"),
("85","30","89","uploaded_track","1593138091"),
("86","30","90","uploaded_track","1593138091"),
("87","30","87","liked_track","1593138371"),
("88","30","91","uploaded_track","1593141075"),
("89","30","92","uploaded_track","1593141075"),
("90","30","93","uploaded_track","1593141076"),
("91","30","94","uploaded_track","1593141076"),
("92","30","95","uploaded_track","1593141661"),
("93","30","96","uploaded_track","1593146052"),
("94","32","97","uploaded_track","1593149548"),
("95","32","98","uploaded_track","1593149813"),
("96","32","99","uploaded_track","1593150072"),
("97","32","100","uploaded_track","1593150280"),
("98","32","101","uploaded_track","1593150782"),
("99","32","102","uploaded_track","1593151137"),
("100","32","103","uploaded_track","1593151458"),
("101","32","104","uploaded_track","1593152242"),
("102","32","105","uploaded_track","1593152460"),
("103","32","106","uploaded_track","1593153191"),
("104","32","107","uploaded_track","1593153614"),
("105","27","74","liked_track","1593154807"),
("106","32","108","uploaded_track","1593155033"),
("107","32","107","liked_track","1593156268"),
("108","21","109","uploaded_track","1593201175"),
("109","21","110","uploaded_track","1593201712"),
("110","34","111","uploaded_track","1593234274"),
("111","34","112","uploaded_track","1593234275"),
("112","34","113","uploaded_track","1593234275"),
("113","34","114","uploaded_track","1593235538"),
("114","34","115","uploaded_track","1593235984"),
("115","25","116","uploaded_track","1593577304"),
("116","8","8","liked_track","1593577698"),
("117","25","116","liked_track","1593578621"),
("118","17","117","uploaded_track","1593719657"),
("119","41","118","uploaded_track","1593745630"),
("120","38","3","liked_track","1593748651"),
("121","43","119","uploaded_track","1593823289"),
("123","43","4","liked_track","1593826803"),
("124","43","74","liked_track","1593826898"),
("125","43","119","liked_track","1593856879");

--
-- Dumping data for table `activities`
--

INSERT INTO activities VALUES
("127","10","20","shared_track","1594015000"),
("128","43","120","uploaded_track","1594073399"),
("129","43","121","uploaded_track","1594078908");

-- ---------------------------------------------------------
--
-- Table structure for table : `admin_invitations`
--
-- ---------------------------------------------------------

CREATE TABLE `admin_invitations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(300) NOT NULL DEFAULT '0',
  `posted` varchar(50) NOT NULL DEFAULT '0',
  `status` varchar(50) CHARACTER SET utf8mb4 DEFAULT 'Pending',
  PRIMARY KEY (`id`),
  KEY `code` (`code`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ---------------------------------------------------------
--
-- Table structure for table : `ads_transactions`
--
-- ---------------------------------------------------------

CREATE TABLE `ads_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ad_id` int(11) NOT NULL DEFAULT '0',
  `track_owner` int(11) NOT NULL DEFAULT '0',
  `amount` varchar(11) NOT NULL DEFAULT '0',
  `type` varchar(50) NOT NULL DEFAULT '',
  `time` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=203 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ads_transactions`
--

INSERT INTO ads_transactions VALUES
("1","2","3","0.1","spent","1592951577"),
("2","2","0","0","view","1592951577"),
("3","2","3","0.1","spent","1592962280"),
("4","2","0","0","view","1592962280"),
("5","2","3","0.1","spent","1592962282"),
("6","2","0","0","view","1592962282"),
("7","2","3","0.1","spent","1592962284"),
("8","2","0","0","view","1592962284"),
("9","2","3","0.1","spent","1592962286"),
("10","2","0","0","view","1592962286"),
("11","2","3","0.1","spent","1592962288"),
("12","2","0","0","view","1592962288"),
("13","2","3","0.1","spent","1592962290"),
("14","2","0","0","view","1592962290"),
("15","2","3","0.1","spent","1592962292"),
("16","2","0","0","view","1592962293"),
("17","2","3","0.1","spent","1592962295"),
("18","2","0","0","view","1592962295"),
("19","2","3","0.1","spent","1592962297"),
("20","2","0","0","view","1592962297"),
("21","2","3","0.1","spent","1592962299"),
("22","2","0","0","view","1592962299"),
("23","2","3","0.1","spent","1592962301"),
("24","2","0","0","view","1592962301"),
("25","2","3","0.1","spent","1592962303"),
("26","2","0","0","view","1592962303"),
("27","2","3","0.1","spent","1592962305"),
("28","2","0","0","view","1592962305"),
("29","2","3","0.1","spent","1592962308"),
("30","2","0","0","view","1592962308"),
("31","2","3","0.1","spent","1592962310"),
("32","2","0","0","view","1592962310"),
("33","2","3","0.1","spent","1592962316"),
("34","2","0","0","view","1592962316"),
("35","2","3","0.1","spent","1592962318"),
("36","2","0","0","view","1592962318"),
("37","2","3","0.1","spent","1592968547"),
("38","2","0","0","view","1592968547"),
("39","2","3","0.1","spent","1592973394"),
("40","2","0","0","view","1592973394"),
("41","2","3","0.1","spent","1592986457"),
("42","2","0","0","view","1592986457"),
("43","2","3","0.1","spent","1592992705"),
("44","2","0","0","view","1592992705"),
("45","2","3","0.1","spent","1592994977"),
("46","2","0","0","view","1592994978"),
("47","2","3","0.1","spent","1592995545"),
("48","2","0","0","view","1592995545"),
("49","2","3","0.1","spent","1592997817"),
("50","2","0","0","view","1592997817"),
("51","2","3","0.1","spent","1593000094"),
("52","2","0","0","view","1593000094"),
("53","2","3","0.1","spent","1593001793"),
("54","2","0","0","view","1593001793"),
("55","2","3","0.1","spent","1593003997"),
("56","2","0","0","view","1593003997"),
("57","2","3","0.1","spent","1593003999"),
("58","2","0","0","view","1593003999"),
("59","2","3","0.1","spent","1593004001"),
("60","2","0","0","view","1593004001"),
("61","2","3","0.1","spent","1593004003"),
("62","2","0","0","view","1593004003"),
("63","2","3","0.1","spent","1593005699"),
("64","2","0","0","view","1593005700"),
("65","2","3","0.1","spent","1593005724"),
("66","2","0","0","view","1593005724"),
("67","2","3","0.1","spent","1593005758"),
("68","2","0","0","view","1593005758"),
("69","2","3","0.1","spent","1593005785"),
("70","2","0","0","view","1593005785"),
("71","2","3","0.1","spent","1593005803"),
("72","2","0","0","view","1593005804"),
("73","2","3","0.1","spent","1593005814"),
("74","2","0","0","view","1593005814"),
("75","2","3","0.1","spent","1593005833"),
("76","2","0","0","view","1593005833"),
("77","2","3","0.1","spent","1593005850"),
("78","2","0","0","view","1593005850"),
("79","2","3","0.1","spent","1593005862"),
("80","2","0","0","view","1593005862"),
("81","2","3","0.1","spent","1593005875"),
("82","2","0","0","view","1593005875"),
("83","2","3","0.1","spent","1593005888"),
("84","2","0","0","view","1593005888"),
("85","2","3","0.1","spent","1593005902"),
("86","2","0","0","view","1593005902"),
("87","2","3","0.1","spent","1593005922"),
("88","2","0","0","view","1593005922"),
("89","2","3","0.1","spent","1593006360"),
("90","2","0","0","view","1593006360"),
("91","2","3","0.1","spent","1593006372"),
("92","2","0","0","view","1593006372"),
("93","2","3","0.1","spent","1593006377"),
("94","2","0","0","view","1593006377"),
("95","2","3","0.1","spent","1593006385"),
("96","2","0","0","view","1593006385"),
("97","2","3","0.1","spent","1593006395"),
("98","2","0","0","view","1593006396"),
("99","2","3","0.1","spent","1593006407"),
("100","2","0","0","view","1593006407");

--
-- Dumping data for table `ads_transactions`
--

INSERT INTO ads_transactions VALUES
("101","2","3","0.1","spent","1593006422"),
("102","2","0","0","view","1593006422"),
("103","2","3","0.1","spent","1593493120"),
("104","2","0","0","view","1593493120"),
("105","2","3","0.1","spent","1593494026"),
("106","2","0","0","view","1593494026"),
("107","2","3","0.1","spent","1593494587"),
("108","2","0","0","view","1593494587"),
("109","2","3","0.1","spent","1593495604"),
("110","2","0","0","view","1593495604"),
("111","2","3","0.1","spent","1593496026"),
("112","2","0","0","view","1593496026"),
("113","2","3","0.1","spent","1593496029"),
("114","2","0","0","view","1593496030"),
("115","2","3","0.1","spent","1593496032"),
("116","2","0","0","view","1593496032"),
("117","2","3","0.1","spent","1593496037"),
("118","2","0","0","view","1593496037"),
("119","2","3","0.1","spent","1593496041"),
("120","2","0","0","view","1593496041"),
("121","2","3","0.1","spent","1593496046"),
("122","2","0","0","view","1593496046"),
("123","2","3","0.1","spent","1593496050"),
("124","2","0","0","view","1593496050"),
("125","2","3","0.1","spent","1593496055"),
("126","2","0","0","view","1593496055"),
("127","2","3","0.1","spent","1593496060"),
("128","2","0","0","view","1593496060"),
("129","2","3","0.1","spent","1593496065"),
("130","2","0","0","view","1593496065"),
("131","2","3","0.1","spent","1593496068"),
("132","2","0","0","view","1593496068"),
("133","2","3","0.1","spent","1593496072"),
("134","2","0","0","view","1593496072"),
("135","2","3","0.1","spent","1593496078"),
("136","2","0","0","view","1593496079"),
("137","2","3","0.1","spent","1593496083"),
("138","2","0","0","view","1593496083"),
("139","2","3","0.1","spent","1593496088"),
("140","2","0","0","view","1593496088"),
("141","2","3","0.1","spent","1593496093"),
("142","2","0","0","view","1593496093"),
("143","2","3","0.1","spent","1593496100"),
("144","2","0","0","view","1593496100"),
("145","2","3","0.1","spent","1593496104"),
("146","2","0","0","view","1593496104"),
("147","2","3","0.1","spent","1593496111"),
("148","2","0","0","view","1593496111"),
("149","2","3","0.1","spent","1593496115"),
("150","2","0","0","view","1593496115"),
("151","2","3","0.1","spent","1593496123"),
("152","2","0","0","view","1593496123"),
("153","2","3","0.1","spent","1593496128"),
("154","2","0","0","view","1593496128"),
("155","2","3","0.1","spent","1593496133"),
("156","2","0","0","view","1593496133"),
("157","2","3","0.1","spent","1593496140"),
("158","2","0","0","view","1593496140"),
("159","2","3","0.1","spent","1593496146"),
("160","2","0","0","view","1593496146"),
("161","2","3","0.1","spent","1593496152"),
("162","2","0","0","view","1593496152"),
("163","2","3","0.1","spent","1593496158"),
("164","2","0","0","view","1593496159"),
("165","2","3","0.1","spent","1593496164"),
("166","2","0","0","view","1593496164"),
("167","2","3","0.1","spent","1593496170"),
("168","2","0","0","view","1593496170"),
("169","2","3","0.1","spent","1593496177"),
("170","2","0","0","view","1593496177"),
("171","2","3","0.1","spent","1593496185"),
("172","2","0","0","view","1593496186"),
("173","2","3","0.1","spent","1593496193"),
("174","2","0","0","view","1593496193"),
("175","2","3","0.1","spent","1593496198"),
("176","2","0","0","view","1593496198"),
("177","2","3","0.1","spent","1593496207"),
("178","2","0","0","view","1593496207"),
("179","2","3","0.1","spent","1593496215"),
("180","2","0","0","view","1593496215"),
("181","2","3","0.1","spent","1593496221"),
("182","2","0","0","view","1593496221"),
("183","2","3","0.1","spent","1593496227"),
("184","2","0","0","view","1593496227"),
("185","2","3","0.1","spent","1593496234"),
("186","2","0","0","view","1593496234"),
("187","2","3","0.1","spent","1593496241"),
("188","2","0","0","view","1593496241"),
("189","2","3","0.1","spent","1593496248"),
("190","2","0","0","view","1593496248"),
("191","2","3","0.1","spent","1593496256"),
("192","2","0","0","view","1593496256"),
("193","2","3","0.1","spent","1593496261"),
("194","2","0","0","view","1593496261"),
("195","2","3","0.1","spent","1593496267"),
("196","2","0","0","view","1593496267"),
("197","2","3","0.1","spent","1593496275"),
("198","2","0","0","view","1593496275"),
("199","2","3","0.1","spent","1593496280"),
("200","2","0","0","view","1593496281");

--
-- Dumping data for table `ads_transactions`
--

INSERT INTO ads_transactions VALUES
("201","2","3","0.1","spent","1593496287"),
("202","2","0","0","view","1593496287");

-- ---------------------------------------------------------
--
-- Table structure for table : `albums`
--
-- ---------------------------------------------------------

CREATE TABLE `albums` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` varchar(16) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `thumbnail` varchar(200) NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT '0',
  `registered` varchar(15) NOT NULL DEFAULT '00/0000',
  `price` float NOT NULL DEFAULT '0',
  `purchases` int(11) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `album_id` (`album_id`),
  KEY `user_id` (`user_id`),
  KEY `title` (`title`),
  KEY `price` (`price`),
  KEY `time` (`time`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `albums`
--

INSERT INTO albums VALUES
("2","QbhOawZ5XySXTUV","14","20/20 VISION","Looking for something new? Officially LadyJ&#039;s, 20/20 VISION&#039;s sophomore release delivers.  Much like her first album, Tales of a Congregation, its lyrics are intentional and unapologetic.  This producer, singer and songwriter continues to expose the trials and struggles of christianity over a backdrop of heavy baselines and syncopated rhythms.  Her unique sound is a fusion of R&amp;B, Funk &amp; Hip Hop.  Officially LadyJ&#039;s hauntingly smooth voice encourages the reflection and repentance.  Try a new sound and give your listeners a new worshipping experience.","12","upload/photos/2020/06/mm4iFuNcUGp6cRCb2WbJ_25_e9414f0ec9f0ac3296749cd111b49eec_image.jpg","1593056750","2020/6","0","0"),
("3","FY7FoBPu7ikrtf6","30","Be Made Whole","healing cd by songwriter Louise Smith, sung by Group Seasoned featuring Louise Smith","12","upload/photos/2020/06/nETjr6Bl9en4HHpDGEaW_25_c581152cd7131acec6f2e9aa700ff290_image.jpg","1593132208","2020/6","0","0"),
("4","3ryWzhg4DhCFbPI","30","Fragrance of a Worshipper","Romantic God Songs <br>Praise &amp; Worship <br>words and music Louise Smith <br>ASCAP <br>LOUJAM MUSIC PUBLISHING <br>Beautyhill Music Studio  <br>Musician : Superior Edwards <br>Praise and worship <br>Jazzpel <br>Gospel Jazz","8","upload/photos/2020/06/aS6tdhA98v2XiNkD8aa4_26_3fd3c69a5cf9c59f97688097758e320a_image.jpg","1593138091","2020/6","0","0");

-- ---------------------------------------------------------
--
-- Table structure for table : `announcement`
--
-- ---------------------------------------------------------

CREATE TABLE `announcement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text,
  `time` int(32) NOT NULL DEFAULT '0',
  `active` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `active` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ---------------------------------------------------------
--
-- Table structure for table : `announcement_views`
--
-- ---------------------------------------------------------

CREATE TABLE `announcement_views` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `announcement_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `announcement_id` (`announcement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ---------------------------------------------------------
--
-- Table structure for table : `apps_sessions`
--
-- ---------------------------------------------------------

CREATE TABLE `apps_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `session_id` varchar(120) NOT NULL DEFAULT '',
  `platform` varchar(32) NOT NULL DEFAULT '',
  `platform_details` text,
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_id` (`session_id`),
  KEY `user_id` (`user_id`),
  KEY `platform` (`platform`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `apps_sessions`
--

INSERT INTO apps_sessions VALUES
("4","3","bc206ed0b4bc3f56fe5d10e86becde42d37fb7d51592636350ec55a6e472e04f2af82ff703117fb0c3","web","a:6:{s:9:\"userAgent\";s:112:\"Mozilla/5.0 (Linux; Android 7.1.1; K88) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.67 Safari/537.36\";s:4:\"name\";s:13:\"Google Chrome\";s:7:\"version\";s:12:\"75.0.3770.67\";s:8:\"platform\";s:11:\"Android Web\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Chrome|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:12:\"24.32.89.221\";}","1592972130"),
("12","10","cef103a59d70d38b7a6fdc5e645164ce61f345751592714557dfaefd7c9c7d4b044bdb666c2a083c02","web","a:6:{s:9:\"userAgent\";s:116:\"Mozilla/5.0 (X11; CrOS x86_64 12871.102.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.141 Safari/537.36\";s:4:\"name\";s:13:\"Google Chrome\";s:7:\"version\";s:13:\"81.0.4044.141\";s:8:\"platform\";s:7:\"Unknown\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Chrome|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:13:\"76.176.17.171\";}","1594027873"),
("13","13","3b023ea3ee68e017a1bbb5b3765ddffd977d7da41592717469dcf22301bd21606b0788d2bbaa67595a","web","a:6:{s:9:\"userAgent\";s:131:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36 Edg/83.0.478.54\";s:4:\"name\";s:13:\"Google Chrome\";s:7:\"version\";s:13:\"83.0.4103.106\";s:8:\"platform\";s:7:\"windows\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Chrome|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:12:\"12.37.34.162\";}","1592731785"),
("16","3","85201ebe6dc079494076a54784511a1a76840ae11592755077ebded11de9e8f98095b3a5fa4f2220a1","web","a:6:{s:9:\"userAgent\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36\";s:4:\"name\";s:13:\"Google Chrome\";s:7:\"version\";s:13:\"83.0.4103.106\";s:8:\"platform\";s:3:\"mac\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Chrome|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:12:\"24.32.89.221\";}","1593543162"),
("20","1","91a08f755e8bc9a3fe677fc528f98473e8b07cb11592875333b93cc92ace097787dda8c6b617461f9a","web","a:6:{s:9:\"userAgent\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36\";s:4:\"name\";s:13:\"Google Chrome\";s:7:\"version\";s:13:\"83.0.4103.106\";s:8:\"platform\";s:7:\"windows\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Chrome|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:14:\"75.187.104.162\";}","1593033736"),
("24","16","32790154a439f4eef96ea1085cf7ebb47a382a3815929322000cc10e1606934650809ccc65cbb381fc","web","a:6:{s:9:\"userAgent\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36\";s:4:\"name\";s:13:\"Google Chrome\";s:7:\"version\";s:13:\"83.0.4103.116\";s:8:\"platform\";s:7:\"windows\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Chrome|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:11:\"50.4.240.10\";}","1593790718"),
("25","16","f070dc325b7f8d528eb23c3249392571986127691592937868383621fb4ef922c274151788803717d7","web","a:6:{s:9:\"userAgent\";s:131:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36 Edg/83.0.478.54\";s:4:\"name\";s:13:\"Google Chrome\";s:7:\"version\";s:13:\"83.0.4103.106\";s:8:\"platform\";s:7:\"windows\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Chrome|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:11:\"50.4.240.10\";}","1592952174"),
("37","24","e79b0f0a57de319d849ee9e70d2c46046ba2703e15930568794325ef3b2b0ce7cb864f4ed7c0120098","web","a:6:{s:9:\"userAgent\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36\";s:4:\"name\";s:13:\"Google Chrome\";s:7:\"version\";s:12:\"83.0.4103.97\";s:8:\"platform\";s:7:\"windows\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Chrome|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:12:\"71.71.96.117\";}","1593065220"),
("38","25","8d7e1d2789f8503eb208b94c67cc0199ba6424a215930602453fb61225d1796ea79f26185d09d5d79d","web","a:6:{s:9:\"userAgent\";s:209:\"Mozilla/5.0 (iPhone; CPU iPhone OS 13_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 [FBAN/FBIOS;FBDV/iPhone10,2;FBMD/iPhone;FBSN/iOS;FBSV/13.5.1;FBSS/3;FBID/phone;FBLC/en_US;FBOP/5]\";s:4:\"name\";s:7:\"Unknown\";s:7:\"version\";s:1:\"?\";s:8:\"platform\";s:3:\"mac\";s:7:\"pattern\";s:60:\"#(?<browser>Version||other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:12:\"45.20.139.57\";}","1593060983"),
("44","27","0041ced22a99bdca0b5a7692f50c699774b764701593114225441996947dcc939f82aa247ceaa59dda","web","a:6:{s:9:\"userAgent\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36\";s:4:\"name\";s:13:\"Google Chrome\";s:7:\"version\";s:13:\"83.0.4103.116\";s:8:\"platform\";s:7:\"windows\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Chrome|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:12:\"73.252.63.23\";}","1593119693"),
("45","31","e9b3b62fe86b387f206b10f5452fec270c6968d8159313066866d25cc174e90ec8c300322a017ec75b","web","a:6:{s:9:\"userAgent\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36\";s:4:\"name\";s:13:\"Google Chrome\";s:7:\"version\";s:13:\"83.0.4103.116\";s:8:\"platform\";s:7:\"windows\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Chrome|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:13:\"139.138.83.37\";}","1593131250"),
("46","21","31c98982eeca529bb05e17402e656d82caf131a61593138869d22d7e194dd395714660ad3b0729dc57","web","a:6:{s:9:\"userAgent\";s:194:\"Mozilla/5.0 (Linux; Android 9; moto e6 Build/PCB29.73-109-6-3; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/83.0.4103.106 Mobile Safari/537.36 [FB_IAB/FB4A;FBAV/275.0.0.49.127;]\";s:4:\"name\";s:13:\"Google Chrome\";s:7:\"version\";s:3:\"4.0\";s:8:\"platform\";s:11:\"Android Web\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Chrome|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:13:\"98.196.84.109\";}","1593138869"),
("47","33","3f83a2ffe8495b2322a7f35e6d97509d6b28232f1593151880a836ca857dae31860ba4c47cf96f23a4","web","a:6:{s:9:\"userAgent\";s:127:\"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36 OPR/68.0.3618.173\";s:4:\"name\";s:13:\"Google Chrome\";s:7:\"version\";s:13:\"81.0.4044.138\";s:8:\"platform\";s:7:\"windows\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Chrome|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:13:\"207.47.175.34\";}","1593152161"),
("49","32","9bb91f40ac07017e41b10f65606330d713ce35591593182028b8c34767a51fe1caaf1c9dfaad3de5e2","web","a:6:{s:9:\"userAgent\";s:78:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0\";s:4:\"name\";s:15:\"Mozilla Firefox\";s:7:\"version\";s:4:\"77.0\";s:8:\"platform\";s:7:\"windows\";s:7:\"pattern\";s:67:\"#(?<browser>Version|Firefox|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:13:\"65.27.140.238\";}","1593214823"),
("54","18","2d2da2e52273fba63e56ac37e7a68d78197053a21593267532f569230d6c973760860ea8e1a3d1aa5a","web","a:6:{s:9:\"userAgent\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36\";s:4:\"name\";s:13:\"Google Chrome\";s:7:\"version\";s:13:\"83.0.4103.116\";s:8:\"platform\";s:7:\"windows\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Chrome|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:14:\"64.132.141.220\";}","1593268763"),
("55","38","d2d18502d2c07e2cb0572b203cf7d183afe78c4b1593392058e56442e93a0ddb8734d86c33751d971e","web","a:6:{s:9:\"userAgent\";s:139:\"Mozilla/5.0 (iPhone; CPU iPhone OS 13_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Mobile/15E148 Safari/604.1\";s:4:\"name\";s:12:\"Apple Safari\";s:7:\"version\";s:6:\"13.1.1\";s:8:\"platform\";s:3:\"mac\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Safari|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:12:\"67.10.36.102\";}","1593392408"),
("59","25","0572603784190b91d4e6d0eb5ea4b7d5309acbaa1593576492000c56e238716f7bb26563f71beb0d07","web","a:6:{s:9:\"userAgent\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36\";s:4:\"name\";s:13:\"Google Chrome\";s:7:\"version\";s:13:\"83.0.4103.106\";s:8:\"platform\";s:3:\"mac\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Chrome|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:12:\"45.20.139.57\";}","1593925611"),
("60","41","b2dac0d5c4e9d4ff62907744587d5efa37185e5915935804571edc5dffaf47cd8ae367651ae2a7deab","web","a:6:{s:9:\"userAgent\";s:129:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18362\";s:4:\"name\";s:13:\"Google Chrome\";s:7:\"version\";s:13:\"70.0.3538.102\";s:8:\"platform\";s:7:\"windows\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Chrome|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:11:\"50.4.240.10\";}","1593791420"),
("61","20","c060a3edc7e22913d22d420c20b6afd94588a0ff1593583691b3864ef7792d9e8fa893ce83381b60b5","web","a:6:{s:9:\"userAgent\";s:119:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15\";s:4:\"name\";s:12:\"Apple Safari\";s:7:\"version\";s:6:\"13.1.1\";s:8:\"platform\";s:3:\"mac\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Safari|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:14:\"107.77.198.131\";}","1593584964"),
("62","3","04dac9076fec23c0ca2fb041fd81a751568215d01593593817a66f16a484ff4790f9499fc12dc9d53b","web","a:6:{s:9:\"userAgent\";s:119:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15\";s:4:\"name\";s:12:\"Apple Safari\";s:7:\"version\";s:6:\"13.1.1\";s:8:\"platform\";s:3:\"mac\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Safari|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:12:\"208.86.136.9\";}","1593828427"),
("64","14","0d3954ac1dcc56d75672701cd71434d6e749d59c1593641695c9188c8bdc7279a944ca83fe8b5ac205","web","a:6:{s:9:\"userAgent\";s:139:\"Mozilla/5.0 (iPhone; CPU iPhone OS 13_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Mobile/15E148 Safari/604.1\";s:4:\"name\";s:12:\"Apple Safari\";s:7:\"version\";s:6:\"13.1.1\";s:8:\"platform\";s:3:\"mac\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Safari|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:12:\"165.225.0.98\";}","1593644052"),
("65","3","3120fcc0d5eb1fc864985dcab414d056bd7183e41593643734e1cf91b3522f9dc4e0657d539c9a5f24","web","a:6:{s:9:\"userAgent\";s:105:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36\";s:4:\"name\";s:13:\"Google Chrome\";s:7:\"version\";s:13:\"83.0.4103.106\";s:8:\"platform\";s:5:\"linux\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Chrome|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:12:\"208.86.136.9\";}","1594147844"),
("67","42","e9241142e34544aab955a88e85d18e0a04d0f05215937948263dc6c40c807f4d2a54a2bf1e76200704","web","a:6:{s:9:\"userAgent\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36\";s:4:\"name\";s:13:\"Google Chrome\";s:7:\"version\";s:13:\"83.0.4103.116\";s:8:\"platform\";s:7:\"windows\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Chrome|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:12:\"39.41.133.82\";}","1593794826"),
("71","46","b66f7ed9e08c915758d4ddd440aadf2b4a5579aa1593966110c69fa3a0aff15c8c718db0831f225f5e","web","a:6:{s:9:\"userAgent\";s:274:\"Mozilla/5.0 (iPhone; CPU iPhone OS 13_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 LightSpeed [FBAN/MessengerLiteForiOS;FBAV/271.0.0.54.116;FBBV/227057669;FBDV/iPhone8,2;FBMD/iPhone;FBSN/iOS;FBSV/13.5.1;FBSS/3;FBCR/;FBID/phone;FBLC/en_US;FBOP/0]\";s:4:\"name\";s:7:\"Unknown\";s:7:\"version\";s:1:\"?\";s:8:\"platform\";s:3:\"mac\";s:7:\"pattern\";s:60:\"#(?<browser>Version||other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:14:\"76.107.162.243\";}","1593968472"),
("78","43","9549dadc40ba14dbf268939179ec259d493613e71594273219eebb24c0fbd70c1e5f938d5e7f6cf78b","web","a:6:{s:9:\"userAgent\";s:139:\"Mozilla/5.0 (iPhone; CPU iPhone OS 13_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Mobile/15E148 Safari/604.1\";s:4:\"name\";s:12:\"Apple Safari\";s:7:\"version\";s:6:\"13.1.1\";s:8:\"platform\";s:3:\"mac\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Safari|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:13:\"100.34.162.49\";}","1594273365"),
("83","3","4d45669d2c270b81dffb320ee3cbccf12c8ac1001594270015981fbb8d486b9f73e460144f6563c0af","web","a:6:{s:9:\"userAgent\";s:114:\"Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36\";s:4:\"name\";s:13:\"Google Chrome\";s:7:\"version\";s:13:\"83.0.4103.116\";s:8:\"platform\";s:7:\"windows\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Chrome|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:12:\"208.86.136.9\";}","1594305994"),
("88","3","5ed7b17a967087fc9041a629e894187eadc1ebb41594404947fd3431b430877838914a1f951c4fd841","web","a:6:{s:9:\"userAgent\";s:120:\"Mozilla/5.0 (Linux; Android 9; LM-Q720) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Mobile Safari/537.36\";s:4:\"name\";s:13:\"Google Chrome\";s:7:\"version\";s:13:\"83.0.4103.106\";s:8:\"platform\";s:11:\"Android Web\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Chrome|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:12:\"208.86.136.9\";}","1594767679"),
("89","17","610460f1ad0f787cc29d32cebb08aca2e30b323915946767189a3dedecde20564a602c8e13ae634fb9","web","a:6:{s:9:\"userAgent\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36\";s:4:\"name\";s:13:\"Google Chrome\";s:7:\"version\";s:13:\"83.0.4103.116\";s:8:\"platform\";s:7:\"windows\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Chrome|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:13:\"208.83.85.121\";}","1594691262"),
("90","1","e08fa6da291f1b6374e660482b1170b7bc5a92381594773824884312a3780e33a127635c8ae0f6131e","web","a:6:{s:9:\"userAgent\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36\";s:4:\"name\";s:13:\"Google Chrome\";s:7:\"version\";s:13:\"83.0.4103.116\";s:8:\"platform\";s:7:\"windows\";s:7:\"pattern\";s:66:\"#(?<browser>Version|Chrome|other)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#\";s:10:\"ip_address\";s:14:\"75.187.104.162\";}","1594776617");

-- ---------------------------------------------------------
--
-- Table structure for table : `artist_requests`
--
-- ---------------------------------------------------------

CREATE TABLE `artist_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `website` varchar(100) NOT NULL DEFAULT '',
  `details` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `photo` varchar(150) NOT NULL DEFAULT '',
  `passport` varchar(150) NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ---------------------------------------------------------
--
-- Table structure for table : `bank_receipts`
--
-- ---------------------------------------------------------

CREATE TABLE `bank_receipts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `description` tinytext,
  `price` varchar(50) NOT NULL DEFAULT '0',
  `mode` varchar(50) NOT NULL DEFAULT '',
  `track_id` varchar(50) CHARACTER SET utf8mb4 DEFAULT '',
  `approved` int(11) unsigned NOT NULL DEFAULT '0',
  `receipt_file` varchar(250) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `approved_at` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ---------------------------------------------------------
--
-- Table structure for table : `banned_ip`
--
-- ---------------------------------------------------------

CREATE TABLE `banned_ip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(32) NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ip_address` (`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ---------------------------------------------------------
--
-- Table structure for table : `blocks`
--
-- ---------------------------------------------------------

CREATE TABLE `blocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `blocked_id` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `blocked_id` (`blocked_id`),
  KEY `time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ---------------------------------------------------------
--
-- Table structure for table : `blog`
--
-- ---------------------------------------------------------

CREATE TABLE `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(120) NOT NULL DEFAULT '',
  `content` text,
  `description` text,
  `posted` varchar(300) DEFAULT '0',
  `category` int(255) DEFAULT '0',
  `thumbnail` varchar(100) DEFAULT 'upload/photos/d-blog.jpg',
  `view` int(11) DEFAULT '0',
  `shared` int(11) DEFAULT '0',
  `tags` varchar(300) DEFAULT '',
  `created_at` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `category` (`category`),
  KEY `tags` (`tags`(255))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog`
--

INSERT INTO blog VALUES
("1","The First Ever Global Urban Gospel Top 100 Chart","","Be Apart Of History as The first ever Gospel Global Urban Chart is Launched in America  Powered By Digital Radio Tracket.","0","1313","upload/photos/2020/06/TgofX5bYzlnAbRZSCXOV_20_45f450093d5ee25a9e01314308dd5838_image.jpg","12","0","","1592627362");

-- ---------------------------------------------------------
--
-- Table structure for table : `blog_comments`
--
-- ---------------------------------------------------------

CREATE TABLE `blog_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `track_id` (`article_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ---------------------------------------------------------
--
-- Table structure for table : `blog_likes`
--
-- ---------------------------------------------------------

CREATE TABLE `blog_likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `comment_id` int(11) unsigned NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `article_id` (`article_id`),
  KEY `user_id` (`user_id`),
  KEY `time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ---------------------------------------------------------
--
-- Table structure for table : `categories`
--
-- ---------------------------------------------------------

CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cateogry_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tracks` int(11) NOT NULL DEFAULT '0',
  `color` varchar(20) NOT NULL DEFAULT '#333',
  `background_thumb` varchar(120) NOT NULL DEFAULT '',
  `time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cateogry_name` (`cateogry_name`),
  KEY `tracks` (`tracks`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO categories VALUES
("1","Other","0","#000000","upload/photos/2019/04/FaS2oOegTOBm5OpFJiCK_17_6ad5d4edf1fb542961a2a64a8d0768e7_image.jpg","0"),
("2","Traditional","0","#000000","upload/photos/2020/06/Jf5IP4SPgfn3WtlmP3SF_20_bd140c721ebf9f3f06b036de91362d1e_image.jpg","1592622257"),
("3","Jazz","0","#000000","upload/photos/2020/06/JUUludsig1Kr1G89XAuR_20_0855a3d0b491fe9f085afc0fb1b4ac16_image.jpg","1592622309"),
("4","Contemporary","0","#000000","upload/photos/2020/06/XjV5ZeiFL8eH565ASrJ5_20_0d5d68a9fcded352f2a36736f98f4238_image.jpg","1592623623"),
("5","Inspirational","0","#000000","upload/photos/2020/06/soxUTqGKRzHEHTOBYmPn_20_90daf08d90971850e4216577747b506b_image.jpg","1592624999"),
("6","CHH","0","#000000","upload/photos/2020/06/HWFagyilNAGGRgmWLXTi_20_0c0ba50ef439c386d6d70d99b8f1b9ec_image.jpg","1592625030"),
("7","Quartet","0","#000000","upload/photos/2020/06/ndIcer675LcHaJu65S2G_20_72406ef13e058dc8858efdfa16d0dbf4_image.jpg","1592625106"),
("8","Praise And Worship","0","#000000","upload/photos/2020/06/r5ppvKZXTQbfHijDWERv_20_95d0a15e1bee46e9ce2413fc62ef4074_image.jpg","1592625147"),
("9","Gospel Mixx","0","#000000","upload/photos/2020/06/FrJbFosQj2kEYONaBBmG_20_c2ee26c682c4337b416e23851208c315_image.jpg","1592625586"),
("10","EDM Gospel","0","#000000","upload/photos/2020/06/9I5ePwsmkf4Nt8M4SQ1P_20_308c67924373d993a3d835e5786331d4_image.jpg","1592625627"),
("12","Radio","0","#000000","upload/photos/2020/06/JnXR1lLTe8IKLjotiF4h_20_e09499ab7528667755b89061242f561d_image.jpg","1592625819"),
("13","Carribean Gospel","0","#000000","","1594008826"),
("14","African Gospel Artists","0","#000000","","1594008885");

-- ---------------------------------------------------------
--
-- Table structure for table : `comments`
--
-- ---------------------------------------------------------

CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `track_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `songseconds` float NOT NULL DEFAULT '0',
  `songpercentage` float NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `track_id` (`track_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO comments VALUES
("1","71","24","Choose To Be &quot;GRATEFUL&quot;","0","0","1593114953");

-- ---------------------------------------------------------
--
-- Table structure for table : `config`
--
-- ---------------------------------------------------------

CREATE TABLE `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `value` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `value` (`value`(255))
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config`
--

INSERT INTO config VALUES
("1","theme","volcano"),
("2","censored_words",""),
("3","title","Urban Praises"),
("4","name","Urban Praises"),
("5","keyword","Urban Gospel Music, Contemporary, Traditional, Sharing, Streaming, Caribbean Gospel"),
("6","email","office@urbanpraises.com"),
("7","description","Bringing Together Radio/Artists in the Urban Global Gospel Music World for The purpose of Networking."),
("8","validation","on"),
("9","recaptcha","off"),
("10","recaptcha_key",""),
("11","language","english"),
("12","google_app_ID","1033037130181-f2gf5pqnt3ldhcj8vnnmqickh6d06mgb.apps.googleusercontent.com"),
("13","google_app_key","0lQZjzx2ciQ6d2wGh1tLvn7x"),
("14","facebook_app_ID","307061943000840"),
("15","facebook_app_key","d959bcf644e88badf0f3c2e4b05e40e8"),
("16","twitter_app_ID",""),
("17","twitter_app_key",""),
("21","smtp_or_mail","mail"),
("22","smtp_host",""),
("23","smtp_username",""),
("24","smtp_password",""),
("25","smtp_encryption","ssl"),
("26","smtp_port",""),
("27","delete_account","off"),
("36","last_admin_collection","1594774577"),
("37","user_statics","[{\"month\":\"January\",\"new_users\":0},{\"month\":\"February\",\"new_users\":0},{\"month\":\"March\",\"new_users\":0},{\"month\":\"April\",\"new_users\":0},{\"month\":\"May\",\"new_users\":0},{\"month\":\"June\",\"new_users\":29},{\"month\":\"July\",\"new_users\":10},{\"month\":\"August\",\"new_users\":0},{\"month\":\"September\",\"new_users\":0},{\"month\":\"October\",\"new_users\":0},{\"month\":\"November\",\"new_users\":0},{\"month\":\"December\",\"new_users\":0}]"),
("38","audio_statics","[{&quot;month&quot;:&quot;January&quot;,&quot;new_videos&quot;:0},{&quot;month&quot;:&quot;February&quot;,&quot;new_videos&quot;:0},{&quot;month&quot;:&quot;March&quot;,&quot;new_videos&quot;:0},{&quot;month&quot;:&quot;April&quot;,&quot;new_videos&quot;:0},{&quot;month&quot;:&quot;May&quot;,&quot;new_videos&quot;:0},{&quot;month&quot;:&quot;June&quot;,&quot;new_videos&quot;:0},{&quot;month&quot;:&quot;July&quot;,&quot;new_videos&quot;:15},{&quot;month&quot;:&quot;August&quot;,&quot;new_videos&quot;:18},{&quot;month&quot;:&quot;September&quot;,&quot;new_videos&quot;:0},{&quot;month&quot;:&quot;October&quot;,&quot;new_videos&quot;:0},{&quot;month&quot;:&quot;November&quot;,&quot;new_videos&quot;:0},{&quot;month&quot;:&quot;December&quot;,&quot;new_videos&quot;:0}]"),
("45","user_registration","on"),
("46","verification_badge","on"),
("49","fb_login","on"),
("50","tw_login","off"),
("51","plus_login","off"),
("52","wowonder_app_ID",""),
("53","wowonder_app_key",""),
("54","wowonder_domain_uri",""),
("56","wowonder_login","off"),
("57","wowonder_img",""),
("58","google",""),
("59","last_created_sitemap","22-03-2019"),
("60","is_ok","1"),
("63","go_pro","on"),
("64","paypal_id","AQ-BIU8NWo_2RPgJe7sFgfusqa8dOn1SkKo23J3Z2BVYuXFvZFclt0Ti50fcVrOAukND0hmdvlZCQDad"),
("65","paypal_secret","EPvn0vBrUYYm1PusJzR5uT9FLxF6cYU84bGwvbCU2efACAj6chetDapeiJym_0svA5GCP8ob6L-7oXi8"),
("66","paypal_mode","live"),
("67","last_backup","08-07-2020"),
("68","user_ads","on"),
("70","max_upload","512000000"),
("71","s3_upload","off"),
("72","s3_bucket_name",""),
("73","amazone_s3_key",""),
("74","amazone_s3_s_key",""),
("75","region","us-east-1"),
("81","apps_api_key","1fcdbad1dd4561ae379c17bed456999f"),
("82","ffmpeg_system","off"),
("83","ffmpeg_binary_file","./ffmpeg/ffmpeg"),
("84","user_max_upload","96000000"),
("86","convert_speed","faster"),
("87","night_mode","night"),
("90","ftp_host","localhost"),
("91","ftp_port","21"),
("92","ftp_username",""),
("93","ftp_password",""),
("94","ftp_upload","off"),
("95","ftp_endpoint","storage.wowonder.com"),
("96","ftp_path","./"),
("111","currency","USD"),
("112","commission","50"),
("113","pro_upload_limit","24"),
("114","pro_price",".99"),
("115","server_key","1fcdbad1dd4561ae379c17bed456999f"),
("116","facebook_url",""),
("117","twitter_url",""),
("118","google_url",""),
("119","currency_symbol","$"),
("120","maintenance_mode","off"),
("121","auto_friend_users","admin"),
("122","waves_color","#ff0000"),
("123","total_songs","87"),
("124","total_albums","3"),
("125","total_plays","427"),
("126","total_sales","1.00"),
("127","total_users","41"),
("128","total_artists","8"),
("129","total_playlists","5"),
("130","total_unactive_users","6"),
("131","user_statics","[{\"month\":\"January\",\"new_users\":0},{\"month\":\"February\",\"new_users\":0},{\"month\":\"March\",\"new_users\":0},{\"month\":\"April\",\"new_users\":0},{\"month\":\"May\",\"new_users\":0},{\"month\":\"June\",\"new_users\":29},{\"month\":\"July\",\"new_users\":10},{\"month\":\"August\",\"new_users\":0},{\"month\":\"September\",\"new_users\":0},{\"month\":\"October\",\"new_users\":0},{\"month\":\"November\",\"new_users\":0},{\"month\":\"December\",\"new_users\":0}]"),
("132","songs_statics","[{\"month\":\"January\",\"new_songs\":0},{\"month\":\"February\",\"new_songs\":0},{\"month\":\"March\",\"new_songs\":0},{\"month\":\"April\",\"new_songs\":0},{\"month\":\"May\",\"new_songs\":0},{\"month\":\"June\",\"new_songs\":82},{\"month\":\"July\",\"new_songs\":5},{\"month\":\"August\",\"new_songs\":0},{\"month\":\"September\",\"new_songs\":0},{\"month\":\"October\",\"new_songs\":0},{\"month\":\"November\",\"new_songs\":0},{\"month\":\"December\",\"new_songs\":0}]"),
("133","version","1.3.2"),
("134","artist_sell","on"),
("135","stripe_version",""),
("136","stripe_secret",""),
("137","bank_payment","off"),
("138","bank_transfer_note","In order to confirm the bank transfer, you will need to upload a receipt or take a screenshot of your transfer within 1 day from your payment date. If a bank transfer is made but no receipt is uploaded within this period, your order will be cancelled. We will verify and confirm your receipt within 3 working days from the date you upload it."),
("139","who_can_download","pro"),
("140","stripe_currency","USD"),
("141","paypal_currency","USD"),
("142","push","0"),
("143","android_push_native","0"),
("144","ios_push_native","0"),
("145","android_m_push_id","");

--
-- Dumping data for table `config`
--

INSERT INTO config VALUES
("146","android_m_push_key",""),
("147","ios_m_push_id",""),
("148","ios_m_push_key",""),
("149","displaymode","night"),
("150","bank_description","&lt;div class=&quot;dt_settings_header bg_gradient&quot;&gt;    &lt;div class=&quot;bank_info_innr&quot;&gt;        &lt;h4 class=&quot;bank_name&quot;&gt;Garanti Bank&lt;/h4&gt;        &lt;div class=&quot;row&quot;&gt;            &lt;div class=&quot;col-md-12&quot;&gt;                &lt;div class=&quot;bank_account&quot;&gt;                    &lt;p&gt;4796824372433055&lt;/p&gt;                    &lt;span class=&quot;help-block&quot;&gt;Account number / IBAN&lt;/span&gt;                &lt;/div&gt;            &lt;/div&gt;            &lt;div class=&quot;col-md-12&quot;&gt;                &lt;div class=&quot;bank_account_holder&quot;&gt;                    &lt;p&gt;Antoian Kordiyal&lt;/p&gt;                    &lt;span class=&quot;help-block&quot;&gt;Account name&lt;/span&gt;                &lt;/div&gt;            &lt;/div&gt;            &lt;div class=&quot;col-md-6&quot;&gt;                &lt;div class=&quot;bank_account_code&quot;&gt;                    &lt;p&gt;TGBATRISXXX&lt;/p&gt;                    &lt;span class=&quot;help-block&quot;&gt;Routing code&lt;/span&gt;                &lt;/div&gt;            &lt;/div&gt;            &lt;div class=&quot;col-md-6&quot;&gt;                &lt;div class=&quot;bank_account_country&quot;&gt;                    &lt;p&gt;United States&lt;/p&gt;                    &lt;span class=&quot;help-block&quot;&gt;Country&lt;/span&gt;                &lt;/div&gt;            &lt;/div&gt;        &lt;/div&gt;    &lt;/div&gt;&lt;/div&gt;"),
("151","stripe_payment","off"),
("152","paypal_payment","on"),
("153","multiple_upload","on"),
("154","usr_v_mon","on"),
("155","stripe_id",""),
("156","ad_v_price","0.1"),
("157","pub_price","0.02"),
("158","ad_c_price","0.5"),
("159","sound_cloud_client_id","842017135"),
("160","soundcloud_login","off"),
("161","sound_cloud_client_secret",""),
("162","emailNotification","on"),
("163","login_auth","0"),
("164","two_factor","0"),
("165","two_factor_type","email"),
("166","sms_twilio_username",""),
("167","sms_twilio_password",""),
("168","sms_t_phone_number",""),
("169","sms_phone_number",""),
("170","rapidapi_key","61c143715cmshcf064f1b7dbf687p1fdeabjsne6077f5e66f9"),
("171","soundcloud_go","off"),
("172","soundcloud_import","on"),
("173","radio_station_import","off"),
("174","affiliate_system","1"),
("175","affiliate_type","1"),
("176","amount_ref","0.10"),
("177","amount_percent_ref","5"),
("178","discover_land","0"),
("179","itunes_import","on"),
("180","itunes_affiliate","admin"),
("181","itunes_partner_token",""),
("182","deezer_import","on"),
("183","audio_ads","on"),
("184","who_audio_ads","users"),
("185","who_can_upload","all"),
("186","android_n_push_id",""),
("187","android_n_push_key",""),
("188","ios_n_push_id",""),
("189","ios_n_push_key",""),
("190","web_push_id",""),
("191","web_push_key","");

-- ---------------------------------------------------------
--
-- Table structure for table : `conversations`
--
-- ---------------------------------------------------------

CREATE TABLE `conversations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_one` int(11) NOT NULL DEFAULT '0',
  `user_two` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_one` (`user_one`),
  KEY `user_two` (`user_two`),
  KEY `time` (`time`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `conversations`
--

INSERT INTO conversations VALUES
("1","3","1","1592633027"),
("2","1","3","1592633027"),
("3","3","14","1592866921"),
("4","14","3","1592866921"),
("5","3","10","1594078099"),
("6","10","3","1594078099"),
("8","3","24","1593063038"),
("9","3","46","1594425956"),
("10","46","3","1594425956");

-- ---------------------------------------------------------
--
-- Table structure for table : `copyrights`
--
-- ---------------------------------------------------------

CREATE TABLE `copyrights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `track_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `track_id` (`track_id`),
  KEY `user_id` (`user_id`),
  KEY `time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ---------------------------------------------------------
--
-- Table structure for table : `custompages`
--
-- ---------------------------------------------------------

CREATE TABLE `custompages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `page_title` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `page_content` text COLLATE utf8_unicode_ci,
  `page_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `custompages`
--

INSERT INTO custompages VALUES
("1","UGNRadioStations","UGN Radio Stations","&lt;p align=&quot;center&quot; style=&quot;margin-top: 0; margin-bottom: 0&quot;&gt;    <br>    &lt;a href=&quot;https://urbanpraises.com/site_pages/UBNSTATION&quot;&gt;    <br>    &lt;img border=&quot;0&quot; src=&quot;https://ugnbroadcasting.com/images/ss/ubn21.png&quot; width=&quot;239&quot; height=&quot;240&quot;&gt;&lt;/a&gt;&lt;a href=&quot;https://urbanpraises.com/site_pages/XtremePraiseRadio&quot;&gt;&lt;img border=&quot;0&quot; src=&quot;https://ugnbroadcasting.com/images/ss/XtremePraiseRadio.png&quot; width=&quot;385&quot; height=&quot;263&quot;&gt;&lt;/a&gt;&lt;/p&gt;    <br>    &lt;p align=&quot;center&quot; style=&quot;margin-top: 0; margin-bottom: 0&quot;&gt;    <br>    &lt;a href=&quot;https://urbanpraises.com/site_pages/RightNowPraizzRadio&quot;&gt;    <br>    &lt;img border=&quot;0&quot; src=&quot;https://ugnbroadcasting.com/images/ss/RightNowPraizzRadioLogo.jpg&quot; width=&quot;368&quot; height=&quot;246&quot;&gt;&lt;/a&gt;&lt;a href=&quot;https://urbanpraises.com/site_pages/SoArise&quot;&gt;&lt;img border=&quot;0&quot; src=&quot;https://ugnbroadcasting.com/images/stations/SoArise.jpg&quot; width=&quot;367&quot; height=&quot;247&quot;&gt;&lt;/a&gt;&lt;/p&gt;    <br>    &lt;p align=&quot;center&quot; style=&quot;margin-top: 0; margin-bottom: 0&quot;&gt;    <br>    &amp;nbsp;&lt;/p&gt;    <br>    &lt;p align=&quot;center&quot; style=&quot;margin-top: 0; margin-bottom: 0&quot;&gt;    <br>    &lt;a href=&quot;https://urbanpraises.com/site_pages/LCJWRADIO&quot;&gt;    <br>    &lt;img border=&quot;0&quot; src=&quot;https://ugnbroadcasting.com/images/stations/lcjw.jpg&quot; width=&quot;364&quot; height=&quot;351&quot;&gt;&lt;/a&gt;&lt;a href=&quot;https://urbanpraises.com/site_pages/UPR&quot;&gt;&lt;img border=&quot;0&quot; src=&quot;https://urbanpraises.com/streaming/upr/img_0039.jpg&quot; width=&quot;374&quot; height=&quot;348&quot;&gt;&lt;/a&gt;&lt;/p&gt;    <br>    &lt;p align=&quot;center&quot; style=&quot;margin-top: 0; margin-bottom: 0&quot;&gt;    <br>    &amp;nbsp;&lt;/p&gt; <br>&lt;p align=&quot;center&quot; style=&quot;margin-top: 0; margin-bottom: 0&quot;&gt;    <br>    &lt;a href=&quot;https://urbanpraises.com/site_pages/MGW&quot;&gt; <br>    &lt;img border=&quot;0&quot; src=&quot;https://urbanpraises.com/streaming/mgw/MGWRADIOLOGO.jpg&quot; width=&quot;389&quot; height=&quot;249&quot;&gt;&lt;/a&gt;&lt;a href=&quot;https://urbanpraises.com/site_pages/GHS&quot;&gt;&lt;img border=&quot;0&quot; src=&quot;https://urbanpraises.com/streaming/ghs/high-resghsradiologo.jpg&quot; width=&quot;254&quot; height=&quot;251&quot;&gt;&lt;/a&gt;&lt;/p&gt;","1"),
("2","UBNSTATION","UBN GOSPEL RADIO STATION","&lt;p&gt;  <br>&lt;iframe name=&quot;I1&quot; src=&quot;https://urbanpraises.com/streaming/deetraradio/ubn.html&quot; marginwidth=&quot;5&quot; marginheight=&quot;5&quot; height=&quot;600&quot; width=&quot;100%&quot; scrolling=&quot;no&quot; border=&quot;0&quot; frameborder=&quot;0&quot;&gt;  <br>Your browser does not support inline frames or is currently configured not to display inline frames.  <br>&lt;/iframe&gt;&lt;/p&gt;  <br>&lt;img border=&quot;0&quot; src=&quot;https://ugnbroadcasting.com/images/ss/1200x200.jpg&quot; width=&quot;100%&quot; height=&quot;200&quot;&gt;&lt;/p&gt;","1"),
("3","XtremePraiseRadio","Xtreme Praise Radio","&lt;p&gt;      <br>&lt;iframe name=&quot;I1&quot; src=&quot;https://urbanpraises.com/streaming/XtremePraiseRadio/etr.html&quot; marginwidth=&quot;5&quot; marginheight=&quot;5&quot; width=&quot;100%&quot; scrolling=&quot;no&quot; border=&quot;0&quot; frameborder=&quot;0&quot; height=&quot;500&quot;&gt;  <br>Your browser does not support inline frames or is currently configured not to display inline frames.&lt;/iframe&gt;&lt;/p&gt;    <br>&lt;p align=&quot;center&quot;&gt;    <br>      &lt;img border=&quot;0&quot; src=&quot;https://ugnbroadcasting.com/images/ss/1200x200.jpg&quot; width=&quot;1195&quot; height=&quot;200&quot;&gt;&lt;/p&gt;","1"),
("4","RightNowPraizzRadio","Right Now Praizz Radio","&lt;p&gt;   <br>&lt;iframe name=&quot;I1&quot; src=&quot;https://urbanpraises.com/streaming/rightnowpraize/rnp.html&quot; marginwidth=&quot;5&quot; marginheight=&quot;5&quot; height=&quot;600&quot; width=&quot;100%&quot; scrolling=&quot;no&quot; border=&quot;0&quot; frameborder=&quot;0&quot;&gt;   <br>Your browser does not support inline frames or is currently configured not to display inline frames.   <br>&lt;/iframe&gt;&lt;/p&gt;","1"),
("5","SoArise","So Arise Radio","&lt;p&gt;&lt;iframe name=&quot;I1&quot; src=&quot;https://urbanpraises.com/streaming/SoArise/sar.html&quot; marginwidth=&quot;5&quot; marginheight=&quot;5&quot; width=&quot;100%&quot; scrolling=&quot;no&quot; border=&quot;0&quot; frameborder=&quot;0&quot; height=&quot;600&quot;&gt;Your browser does not support inline frames or is currently configured not to display inline frames.&lt;/iframe&gt;&lt;/p&gt;","1"),
("6","LCJWRADIO","LCJW RADIO","&lt;p&gt;&lt;iframe name=&quot;I1&quot; src=&quot;https://urbanpraises.com/streaming/lcjwradio/lcjw.html&quot; marginwidth=&quot;5&quot; marginheight=&quot;5&quot; height=&quot;600&quot; width=&quot;100%&quot; scrolling=&quot;no&quot; border=&quot;0&quot; frameborder=&quot;0&quot;&gt;Your browser does not support inline frames or is currently configured not to display inline frames.&lt;/iframe&gt;&lt;/p&gt;","1"),
("7","UPR","UPR","&lt;p&gt;&lt;iframe name=&quot;I1&quot; src=&quot;https://urbanpraises.com/streaming/upr/upr.html&quot; width=&quot;100%&quot; height=&quot;600&quot; marginwidth=&quot;5&quot; marginheight=&quot;5&quot; scrolling=&quot;no&quot; border=&quot;0&quot; frameborder=&quot;0&quot;&gt;Your browser does not support inline frames or is currently configured not to display inline frames.&lt;/iframe&gt;&lt;/p&gt;","1"),
("8","MGW","MGW","&lt;p&gt;&lt;iframe name=&quot;I1&quot; src=&quot;https://urbanpraises.com/streaming/mgw/mgw.html&quot; marginwidth=&quot;5&quot; marginheight=&quot;5&quot; width=&quot;100%&quot; scrolling=&quot;no&quot; border=&quot;0&quot; frameborder=&quot;0&quot; height=&quot;600&quot;&gt;Your browser does not support inline frames or is currently configured not to display inline frames.&lt;/iframe&gt;&lt;/p&gt;","1"),
("9","GHS","GHS","&lt;p&gt;&lt;iframe name=&quot;I1&quot; src=&quot;https://urbanpraises.com/streaming/ghs/ghs.html&quot; marginwidth=&quot;5&quot; marginheight=&quot;5&quot; height=&quot;600&quot; width=&quot;100%&quot; scrolling=&quot;no&quot; border=&quot;0&quot; frameborder=&quot;0&quot;&gt;Your browser does not support inline frames or is currently configured not to display inline frames.&lt;/iframe&gt;&lt;/p&gt;","1");

-- ---------------------------------------------------------
--
-- Table structure for table : `dislikes`
--
-- ---------------------------------------------------------

CREATE TABLE `dislikes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `track_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `comment_id` int(11) unsigned NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `track_id` (`track_id`),
  KEY `user_id` (`user_id`),
  KEY `time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ---------------------------------------------------------
--
-- Table structure for table : `downloads`
--
-- ---------------------------------------------------------

CREATE TABLE `downloads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `fingerprint` varchar(120) NOT NULL DEFAULT '',
  `track_id` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`user_id`),
  KEY `track_id` (`track_id`),
  KEY `fingerprint` (`fingerprint`),
  KEY `time` (`time`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `downloads`
--

INSERT INTO downloads VALUES
("1","3","0d7af6a6b2d14903c42999b3903a8ed31adb49a3","45","1592939542"),
("2","11","0d7af6a6b2d14903c42999b3903a8ed31adb49a3","45","1592940458");

-- ---------------------------------------------------------
--
-- Table structure for table : `favorites`
--
-- ---------------------------------------------------------

CREATE TABLE `favorites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `track_id` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `track_id` (`track_id`),
  KEY `time` (`time`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `favorites`
--

INSERT INTO favorites VALUES
("1","8","9","1592717077"),
("4","24","71","1593060887"),
("6","30","84","1593132471"),
("7","30","82","1593132474"),
("8","30","81","1593132475"),
("9","30","80","1593132477"),
("10","30","79","1593132479"),
("11","30","78","1593132480"),
("12","30","77","1593132482"),
("13","30","76","1593132483"),
("14","34","113","1593235277"),
("15","8","8","1593577693"),
("16","41","118","1593786828"),
("17","16","118","1593787040");

-- ---------------------------------------------------------
--
-- Table structure for table : `followers`
--
-- ---------------------------------------------------------

CREATE TABLE `followers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `follower_id` int(11) NOT NULL DEFAULT '0',
  `following_id` int(11) NOT NULL DEFAULT '0',
  `artist_id` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `follower_id` (`follower_id`),
  KEY `following_id` (`following_id`),
  KEY `artist_id` (`artist_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `followers`
--

INSERT INTO followers VALUES
("1","8","3","0","1592715640"),
("2","10","1","0","1592725269"),
("3","10","3","0","1592726250"),
("4","3","14","0","1592856634"),
("5","17","3","0","1592949362"),
("6","25","3","0","1593060258"),
("7","24","3","0","1593061028"),
("8","3","24","0","1593063685"),
("9","14","20","0","1593098043"),
("10","3","31","0","1593142779"),
("11","21","3","0","1593166171"),
("12","32","1","0","1593182056"),
("13","32","3","0","1593182607"),
("15","14","1","0","1593641710"),
("16","3","41","0","1593768156"),
("17","43","1","0","1593825580"),
("18","43","25","0","1593825600"),
("19","43","3","0","1593826704"),
("20","3","17","0","1593828188"),
("21","3","25","0","1593828223"),
("22","43","34","0","1593906302"),
("23","3","43","0","1593913647");

-- ---------------------------------------------------------
--
-- Table structure for table : `langs`
--
-- ---------------------------------------------------------

CREATE TABLE `langs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref` varchar(250) DEFAULT '',
  `options` text NOT NULL,
  `lang_key` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `english` text,
  `spanish` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `lang_key` (`lang_key`(255)),
  KEY `ref` (`ref`)
) ENGINE=InnoDB AUTO_INCREMENT=830 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `langs`
--

INSERT INTO langs VALUES
("1","","","signup","Signup","Regístrate"),
("2","","","get_access_to_your_music__playlists_and_account","Get access to your music, playlists and account","Accede a tu música, listas de reproducción y cuenta."),
("3","","","full_name","Full Name","Nombre completo"),
("4","","","username","Username","Nombre de usuario"),
("5","","","email_address","Email address","Dirección de correo electrónico"),
("6","","","password","Password","Contraseña"),
("7","","","confirm_password","Confirm Password","Confirmar contraseña"),
("8","","","already_have_an_account_","Already have an account?","¿Ya tienes una cuenta?"),
("9","","","login","Login","Iniciar sesión"),
("10","","","by_signing_up__you_agree_to_our","By signing up, you agree to our","Al registrarte, aceptas nuestra"),
("11","","","terms","Terms","Condiciones"),
("12","","","and","and","y"),
("13","","","privacy_policy","Privacy Policy","Política de privacidad"),
("14","","","please_wait..","Please wait..","Por favor espera.."),
("15","","","search_for_songs__artists__playlists_and_more","Search for songs, artists, playlists and more..","Busca canciones, artistas, listas de reproducción y más."),
("16","","","trending_now","Trending Now","Siendo tendencia ahora"),
("17","","","advanced_search","Advanced Search","Búsqueda Avanzada"),
("18","","","feed","Feed","Alimentar"),
("19","","","upload","Upload","Subir"),
("20","","","dashboard","Dashboard","Tablero"),
("21","","","settings","Settings","Ajustes"),
("22","","","recently_played","Recently Played","Recientemente jugado"),
("23","","","my_playlists","My Playlists","Mis playlists"),
("24","","","favourites","Favourites","Favoritos"),
("25","","","logout","Logout","Cerrar sesión"),
("26","","","register","Register","Registro"),
("27","","","forgot_your_password_","Forgot your password?","¿Olvidaste tu contraseña?"),
("28","","","don_t_have_an_account_","Don&#039;t have an account?","¿No tienes una cuenta?"),
("29","","","sign_up","Sign Up","Regístrate"),
("30","","","or","OR","O"),
("31","","","login_with_facebook","Login with Facebook","Iniciar sesión con Facebook"),
("32","","","login_with_twitter","Login with Twitter","Inicia sesión con Twitter"),
("33","","","login_with_google","Login with Google","Inicia sesión con Google"),
("34","","","login_with_vk","Login with VK","Iniciar sesión con VK"),
("35","","","this_e-mail_is_already_taken","This e-mail is already taken","este correo electrónico ya está en uso"),
("36","","","incorrect_username_or_password","Incorrect username or password","Nombre de usuario o contraseña incorrecta"),
("37","","","registration_successful__we_have_sent_you_an_email__please_check_your_inbox_spam_to_verify_your_account.","Registration successful! We have sent you an email, Please check your inbox/spam to verify your account.","¡Registro exitoso! Le hemos enviado un correo electrónico. Verifique su bandeja de entrada / correo no deseado para verificar su cuenta."),
("38","","","this_username_is_already_taken","This username is already taken","Este nombre de usuario ya está en uso"),
("39","","","your_account_is_not_activated_yet__please_check_your_inbox_for_the_activation_link","Your account is not activated yet, please check your inbox for the activation link","Su cuenta aún no está activada, por favor revise su bandeja de entrada para el enlace de activación"),
("40","","","enter_your_email_to_get_password_reset_link.","Enter your email to get password reset link.","Ingrese su correo electrónico para obtener el enlace de restablecimiento de contraseña."),
("41","","","send_link","Send Link","Enviar un enlace"),
("42","","","this_e-mail_not_found","This E-mail not found","Este correo electrónico no encontrado"),
("43","","","this_e-mail_is_not_found","This e-mail is not found","Este correo electrónico no se encuentra"),
("44","","","reset_password","Reset Password","Restablecer la contraseña"),
("45","","","error_found_while_sending_the_reset_link__please_try_again_later.","Error found while sending the reset link, please try again later.","Se ha encontrado un error al enviar el enlace de restablecimiento. Inténtalo de nuevo más tarde."),
("46","","","please_check_your_inbox___spam_folder_for_the_reset_email.","Please check your inbox / spam folder for the reset email.","Por favor, revise su carpeta de bandeja de entrada / correo no deseado para el correo electrónico de restablecimiento."),
("47","","","please_check_your_details","Please check your details","Por favor comprueba tus detalles"),
("48","","","reset_your_password","Reset your password","Restablecer su contraseña"),
("49","","","enter_new_password_to_proceed.","Enter new password to proceed.","Introduzca la nueva contraseña para continuar."),
("50","","","new_password","New Password","Nueva contraseña"),
("51","","","reset","Reset","Reiniciar"),
("52","","","passwords_don_t_match","Passwords don&#039;t match","Las contraseñas no coinciden"),
("58","","","about_us","About Us","Sobre nosotros"),
("59","","","contact","Contact","Contacto"),
("60","","","copyright_____date___name_.","Copyright © |DATE| |NAME|.","Copyright © |DATE| |NAME|."),
("61","","","contact_us","Contact Us","Contáctenos"),
("62","","","let_us_help_you.","Let us help you.","Dejanos ayudarte."),
("63","","","write_here_your_message","Write here your message","Escribe aquí tu mensaje"),
("64","","","send","Send","Enviar"),
("65","","","e-mail_sent_successfully","E-mail sent successfully","E-mail enviado correctamente"),
("66","","","followers","Followers","Seguidores"),
("67","","","following","Following","Siguiendo"),
("68","","","all","All","Todos"),
("69","","","songs","Songs","Canciones"),
("70","","","albums","Albums","Los álbumes"),
("71","","","playlists","Playlists","Listas de reproducción"),
("72","","","follow","Follow","Seguir"),
("73","","","edit_profile","Edit Profile","Editar perfil"),
("74","","","confirm_your_account","Confirm your account","Confirme su cuenta"),
("75","","","general_settings","General Settings","Configuración general"),
("76","","","email","Email","Email"),
("77","","","country","Country","País"),
("78","","","age","Age","Años"),
("79","","","gender","Gender","Género"),
("80","","","save","Save","Salvar"),
("81","","","delete_account","Delete Account","Borrar cuenta"),
("82","","","are_you_sure_you_want_to_delete_your_account__all_content_including_published_songs__will_be_permanetly_removed_","Are you sure you want to delete your account? All content including published songs, will be permanetly removed!","¿Estás seguro de que quieres eliminar tu cuenta? Todo el contenido, incluidas las canciones publicadas, se eliminará permanentemente!"),
("83","","","current_password","Current Password","contraseña actual"),
("84","","","delete","Delete","Borrar"),
("85","","","change_password","Change Password","Cambia la contraseña"),
("86","","","repeat_new_password","Repeat New Password","Repita la nueva contraseña"),
("87","","","change","Change","Cambio"),
("88","","","profile_settings","Profile Settings","Configuración de perfil"),
("89","","","about_me","About Me","Sobre mi"),
("90","","","facebook_username","Facebook Username","Nombre de usuario de Facebook"),
("91","","","website","Website","Sitio web"),
("94","","","male","Male","Masculino"),
("95","","","female","Female","Hembra"),
("96","","","settings_successfully_updated_","Settings successfully updated!","Configuraciones exitosamente actualizadas!"),
("97","","","no_notifications_found","No notifications found","No se encontraron notificaciones"),
("98","","","year","year","año"),
("99","","","month","month","mes"),
("100","","","day","day","día"),
("101","","","hour","hour","hora"),
("102","","","minute","minute","minuto"),
("103","","","second","second","segundo"),
("104","","","years","years","años"),
("105","","","months","months","meses"),
("106","","","days","days","dias"),
("107","","","hours","hours","horas");

--
-- Dumping data for table `langs`
--

INSERT INTO langs VALUES
("108","","","minutes","minutes","minutos"),
("109","","","seconds","seconds","segundos"),
("110","","","ago","ago","hace"),
("111","","","started_following_you.","started following you.","empecé a seguirte."),
("112","","","profile_successfully_updated_","Profile successfully updated!","Perfil actualizado con éxito!"),
("113","","","invalid_website_url__format_allowed__http_s_____.___","Invalid website url, format allowed: http(s)://*.*/*","URL del sitio web no válida, formato permitido: http (s): //*.*/*"),
("114","","","invalid_facebook_username__urls_are_not_allowed","Invalid facebook username, urls are not allowed","Nombre de usuario de Facebook no válido, las URL no están permitidas"),
("115","","","new_password_is_too_short","New password is too short","La nueva contraseña es demasiado corta"),
("116","","","your_current_password_is_invalid","Your current password is invalid","Tu contraseña actual no es válida"),
("117","","","your_password_was_successfully_updated_","Your password was successfully updated!","Su contraseña fue actualizada con éxito!"),
("118","","","your_account_was_successfully_deleted__please_wait..","Your account was successfully deleted, please wait..","Su cuenta fue eliminada exitosamente, por favor espere .."),
("119","","","select_files_to_upload","Select files to upload","Seleccionar archivos para subir"),
("120","","","or_drag___drop_files_here","or drag &amp; drop files here","o arrastrar &amp; soltar archivos aquí"),
("121","","","title","Title","Título"),
("122","","","your_song_title__2_-_55_characters","Your song title, 2 - 55 characters","El título de tu canción, 2 - 55 caracteres."),
("123","","","description","Description","Descripción"),
("124","","","tags","Tags","Etiquetas"),
("125","","","add_tags_to_describe_more_about_your_track","Add tags to describe more about your track","Agrega etiquetas para describir más sobre tu pista"),
("126","","","genre","Genre","Género"),
("127","","","availability","Availability","Disponibilidad"),
("128","","","public","Public","Público"),
("129","","","private","Private","Privado"),
("130","","","age_restriction","Age Restriction","Restricción de edad"),
("131","","","all_ages_can_listen_this_song","All ages can listen this song","Todas las edades pueden escuchar esta canción."),
("132","","","only__18","Only +18","Solo +18"),
("133","","","price","Price","Precio"),
("134","","","publish","Publish","Publicar"),
("135","","","audio_file_not_found__please_refresh_the_page_and_try_again.","Audio file not found, please refresh the page and try again.","No se encontró el archivo de audio, por favor actualice la página y vuelva a intentarlo."),
("136","","","something_went_wrong_please_try_again_later_","Something went wrong Please try again later!","Algo salió mal Por favor, intente de nuevo más tarde!"),
("138","","","please_wait__your_track_is_being_coverted_to_mp3_audio_file._this_might_take_a_few_minutes.","Please wait, your track is being coverted to mp3 audio file. This might take a few minutes.","Por favor espere, su pista está siendo cubierta a un archivo de audio mp3. Esto puede tardar unos minutos."),
("139","","","invalid_file_format__only_mp3_is_allowed","Invalid file format, only mp3 is allowed","Formato de archivo inválido, solo se permite mp3"),
("140","","","invalid_file_format__only_jpg__jpeg__png_are_allowed","Invalid file format, only jpg, jpeg, png are allowed","Formato de archivo no válido, solo se permiten jpg, jpeg, png"),
("141","","","error_found_while_uploading_your_image__please_try_again_later.","Error found while uploading your image, please try again later.","Se ha encontrado un error al cargar tu imagen, inténtalo de nuevo más tarde."),
("142","","","error_found_while_uploading_your_track__please_try_again_later.","Error found while uploading your track, please try again later.","Se ha encontrado un error al cargar la pista. Inténtalo de nuevo más tarde."),
("143","","","invalid_file_format__only_mp3__ogg__wav__and_mpeg_is_allowed","Invalid file format, only mp3, ogg, wav, and mpeg is allowed","Formato de archivo no válido, solo se permite mp3, ogg, wav y mpeg"),
("144","","","sorry__page_not_found_","Sorry, page not found!","Lo sentimos, la página no se encuentra!"),
("145","","","the_page_you_are_looking_for_could_not_be_found._please_check_the_link_you_followed_to_get_here_and_try_again.","The page you are looking for could not be found. Please check the link you followed to get here and try again.","La página que estás buscando no se pudo encontrar. Por favor revise el enlace que siguió para llegar aquí e intente nuevamente."),
("146","","","home","Home","Casa"),
("147","","","become_an_artist","Become an artist","Conviértete en un artista"),
("148","","","info","Info","Información"),
("149","","","located_in","Located in","Situado en"),
("150","","","bio","Bio","Bio"),
("151","","","social_links","Social Links","vínculos sociales"),
("152","","","__user_gender","{{USER gender}}","{{USER gender}}"),
("153","","","release_date","Release date","Fecha de lanzamiento"),
("154","","","uploaded_new_song","Uploaded new song","Subido nueva canción"),
("155","","","report","Report","Informe"),
("156","","","delete_track","Delete Track","Eliminar pista"),
("157","","","edit_info","Edit Info","Editar información"),
("158","","","pin","Pin","Alfiler"),
("159","","","no_tracks_found","No tracks found","No se encontraron pistas"),
("160","","","load_more","Load More","Carga más"),
("161","","","no_more_tracks_found","No more tracks found","No se encontraron más pistas"),
("162","","","like","Like","Me gusta"),
("163","","","share","Share","Compartir"),
("164","","","more","More","Más"),
("165","","","add_to_playlist","Add to Playlist","Agregar a la lista de reproducción"),
("166","","","add_to_queue","Add to Queue","Añadir a la cola"),
("167","","","edit","Edit","Editar"),
("168","","","download","Download","Descargar"),
("169","","","purchase","Purchase","Compra"),
("170","","","save_track","Save Track","Guardar track"),
("171","","","the_new_track_details_are_updated__please_wait..","The new track details are updated, please wait..","Los nuevos detalles de la pista se actualizan, por favor espere ..."),
("172","","","liked_your_song.","liked your song.","Me gustó tu canción."),
("173","","","liked","Liked","Gustó"),
("174","","","write_a_comment_and_press_enter","Write a comment and press enter","Escribe un comentario y presiona enter"),
("175","","","delete_your_track","Delete your track","Borra tu pista"),
("176","","","are_you_sure_you_want_to_delete_this_track_","Are you sure you want to delete this track?","¿Seguro que quieres borrar esta pista?"),
("177","","","cancel","Cancel","Cancelar"),
("178","","","share_this_song","Share this Song","Comparte esta canción"),
("179","","","close","Close","Cerrar"),
("180","","","tracks","Tracks","Pistas"),
("181","","","recently_played_music","Recently Played Music","Música recientemente reproducida"),
("182","","","no_tracks_found__try_to_listen_more____","No tracks found, try to listen more? ;)","No se han encontrado pistas, intenta escuchar más? ;)"),
("183","","","repeat","Repeat","Repetir"),
("184","","","shuffle","Shuffle","Barajar"),
("185","","","queue","Queue","Cola"),
("186","","","clear","Clear","Claro"),
("187","","","just_now","Just now","Justo ahora"),
("188","","","no_comments_found","No comments found","No se encontraron comentarios"),
("189","","","delete_comment","Delete comment","Eliminar comentario"),
("190","","","are_you_sure_you_want_to_delete_this_comment_","Are you sure you want to delete this comment?","¿Estás seguro de que quieres eliminar este comentario?"),
("191","","","report_comment","Report Comment","Reportar comentario"),
("192","","","no_more_comments_found","No more comments found","No se han encontrado más comentarios."),
("213","","","cateogry_1","Alternative ","Other"),
("215","","","in","in","en"),
("216","","","other","Other","Otro"),
("217","","","more_tracks","More Tracks","Más pistas"),
("218","","","purchase_track","Purchase track","Pista de compra"),
("219","","","error_found_while_creating_the_payment__please_try_again_later.","Error found while creating the payment, please try again later.","Se ha encontrado un error al crear el pago, inténtalo de nuevo más tarde."),
("220","","","purchase_required","Purchase Required","Compra Requerida"),
("221","","","to_continue_listening_to_this_track__you_need_to_purchase_the_song.","To continue listening to this track, you need to purchase the song.","Para continuar escuchando esta pista, necesitas comprar la canción."),
("222","","","purchased","Purchased","Comprado"),
("223","","","no_purchased_tracks_found","No purchased tracks found","No se encontraron pistas compradas"),
("224","","","purchased_songs","Purchased Songs","Canciones compradas"),
("225","","","my_purchases","My Purchases","Mis compras"),
("226","","","purchased_on","Purchased on","Comprado en"),
("227","","","purchased_your_song.","purchased your song.","compré tu canción."),
("229","","","go_pro_to_download","Go PRO To Download","Ir PRO para descargar"),
("230","","","generating_waves..","Generating waves..","Generando olas ..");

--
-- Dumping data for table `langs`
--

INSERT INTO langs VALUES
("231","","","name","Name","Nombre"),
("232","","","your_full_name_as_showing_on_your_id","Your full name as showing on your ID","Su nombre completo como se muestra en su identificación"),
("233","","","upload_documents","Upload documents","Subir documentos"),
("234","","","please_upload_a_photo_with_your_passport___id___your_distinct_photo.","Please upload a photo with your passport / ID &amp; your distinct photo.","Suba una foto con su pasaporte / ID &amp; tu foto distinta"),
("235","","","your_personal_photo","Your Personal Photo","Tu foto personal"),
("236","","","passport___id_card","Passport / ID card","Pasaporte / DNI"),
("237","","","additional_details","Additional details","Detalles adicionales"),
("238","","","we_will_review_your_request_within_24_hours__you_ll_be_informed_shourtly.","We will review your request within 24 hours, you&#039;ll be informed shourtly.","Revisaremos su solicitud dentro de las 24 horas, se le informará brevemente."),
("240","","","additional_details_about_your_self__optinal_","Additional details about your self (Optinal)","Detalles adicionales sobre tu auto (Optinal)"),
("241","","","website__optional_","Website (Optional)","Sitio web (opcional)"),
("242","","","thank_you._your_request_has_been_sent__we_will_get_back_to_you_shourtly.","Thank you. Your request has been sent, we will get back to you shourtly.","Gracias. Su solicitud ha sido enviada, nos pondremos en contacto con usted en breve."),
("243","","","error_found_while_processing_your_request__please_try_again_later.","Error found while processing your request, please try again later.","Se ha encontrado un error al procesar su solicitud, inténtelo de nuevo más tarde."),
("244","","","your_request_has_been_already_sent__we_will_get_back_to_you_shourtly.","Your request has been already sent, we will get back to you shourtly.","Su solicitud ya ha sido enviada, nos pondremos en contacto con usted en breve."),
("245","","","get_verified__upload_more_songs__get_more_space__sell_your_songs__get_a_special_looking_profile_and_get_famous_on_our_platform_","Get verified, upload more songs, get more space, sell your songs, get a special looking profile and get famous on our platform!","¡Verifíquese, cargue más canciones, obtenga más espacio, venda sus canciones, obtenga un perfil de aspecto especial y sea famoso en nuestra plataforma!"),
("246","","","play_all","Play All","Jugar todo"),
("247","","","latest_songs","Latest Songs","Canciones más recientes"),
("248","","","special_songs","Special Songs","Canciones especiales"),
("249","","","top_songs","Top Songs","Mejores canciones"),
("250","","","similar_artists","Similar Artists","Artistas similares"),
("251","","","artists","artists","artistas"),
("252","","","artist","artist","artista"),
("253","","","store","Store","Almacenar"),
("254","","","congratulations__your_request_to_become_an_artist_was_approved.","Congratulations! Your request to become an artist was approved.","¡Felicidades! Su solicitud para convertirse en un artista fue aprobada."),
("255","","","sadly__your_request_to_become_an_artist_was_declined.","Sadly, Your request to become an artist was declined.","Lamentablemente, su solicitud para convertirse en un artista fue rechazada."),
("256","","","activities","Activities","Ocupaciones"),
("259","","","re_post","Re Post","volver a publicar"),
("260","","","the_song_was_successfully_shared_on_your_timeline.","The song was successfully shared on your timeline.","La canción fue compartida con éxito en su línea de tiempo."),
("261","","","no_activties_found","No activties found","No se han encontrado actividades."),
("272","","","delete_post","Delete Post","Eliminar mensaje"),
("273","","","no_more_activities_found","No more activities found","No se han encontrado más actividades."),
("274","","","weekly_top_tracks","Weekly Top Tracks","Top pistas semanales"),
("275","","","delete_your_post","Delete your post","Borra tu publicación"),
("276","","","are_you_sure_you_want_to_delete_this_post_","Are you sure you want to delete this post?","¿Estás seguro de que quieres eliminar esta publicación?"),
("277","","","uploaded_a_new_song.","Uploaded a new song.","Subido una nueva canción."),
("278","","","artists_to_follow","Artists to Follow","Artistas a seguir"),
("279","","","likes","Likes","Gustos"),
("280","","","plays","Plays","Obras de teatro"),
("281","","","no_favourite_tracks_found","No favourite tracks found","No se encontraron pistas favoritas"),
("282","","","my_favourites","My Favourites","Mis favoritos"),
("283","","","you_currently_have__c__favourite_songs","You currently have |c| favourite songs","Actualmente tienes |c| canciones favoritas"),
("285","","","you_currently_have__c__playlists.","You currently have |c| playlists.","Actualmente tienes |c| playlists"),
("286","","","create","Create","Crear"),
("287","","","create_playlist","Create Playlist","Crear lista de reproducción"),
("288","","","playlist_name","Playlist name","Nombre de la lista de reproducción"),
("289","","","error_found_while_uploading_the_playlist_avatar__please_try_again_later.","Error found while uploading the playlist avatar, Please try again later.","Se ha encontrado un error al cargar el avatar de la lista de reproducción. Inténtalo de nuevo más tarde."),
("291","","","edit_playlist","Edit Playlist","Editar lista de reproducción"),
("292","","","delete_playlist","Delete Playlist","Eliminar lista de reproducción"),
("293","","","delete_your_playlist","Delete your playlist","Eliminar tu lista de reproducción"),
("294","","","are_you_sure_you_want_to_delete_this_playlist_","Are you sure you want to delete this playlist?","¿Estás seguro de que deseas eliminar esta lista de reproducción?"),
("295","","","share_this_playlist","Share this Playlist","Comparte esta lista de reproducción"),
("296","","","play","Play","Jugar"),
("297","","","no_songs_on_this_playlist.","No songs on this playlist.","No hay canciones en esta lista de reproducción."),
("298","","","no_songs_on_this_playlist_yet.","No songs on this playlist yet.","No hay canciones en esta lista de reproducción todavía."),
("299","","","select_playlists","Select playlists","Seleccionar listas de reproducción"),
("300","","","add","Add","Añadir"),
("301","","","please_select_which_playlist_you_want_to_add_this_song_to.","Please select which playlist you want to add this song to.","Seleccione la lista de reproducción a la que desea agregar esta canción."),
("302","","","no_playlists_found","No playlists found","No se encontraron listas de reproducción"),
("303","","","new","New","Nuevo"),
("304","","","no_more_playlists_found","No more playlists found","No se encontraron más listas de reproducción"),
("305","","","discover","Discover","Descubrir"),
("306","","","show_all","Show All","Mostrar todo"),
("307","","","new_releases","New Releases","Nuevos lanzamientos"),
("308","","","most_popular_this_week","Most Popular This Week","Más popular esta semana"),
("309","","","most_recommended","Most Recommended","Más recomendado"),
("310","","","recommended","Recommended","Recomendado"),
("311","","","new_music","New Music","Música nueva"),
("312","","","best_new_releases","Best New Releases","Mejores lanzamientos nuevos"),
("313","","","latest_music","Latest Music","La ultima musica"),
("315","","","top_music","Top Music","La mejor música"),
("316","","","see_all","See All","Ver todo"),
("317","","","top_albums","Top Albums","Top albumes"),
("318","","","top","Top","Parte superior"),
("319","","","top_50","Top 50","Top 50"),
("320","","","browse_music","Browse Music","Buscar música"),
("321","","","genres","Genres","Géneros"),
("322","","","your_music","Your Music","Tu musica"),
("323","","","latest_songs_in","Latest Songs In","Últimas canciones en"),
("324","","","age_restricted_track","Age restricted track","Pista de edad restringida"),
("325","","","this_track_is_age_restricted_for_viewers_under__18","This track is age restricted for viewers under +18","Esta pista tiene restricciones de edad para los espectadores menores de 18 años."),
("326","","","create_an_account_or_login_to_confirm_your_age.","Create an account or login to confirm your age.","Crea una cuenta o inicia sesión para confirmar tu edad."),
("327","","","this_track_is_age_restricted_for_viewers_under_18","This track is age restricted for viewers under 18","Esta pista tiene restricciones de edad para los espectadores menores de 18 años."),
("328","","","upgrade_to_pro","Upgrade To PRO","Actualizar a PRO"),
("329","","","go_pro_","Go Pro!","¡Vaya Pro!"),
("330","","","discover_more_features_with_our_premium_package_","Discover more features with our Premium package!","¡Descubre más características con nuestro paquete Premium!"),
("331","","","free_plan","Free Plan","Plan gratis"),
("332","","","upload_songs_up_to","Upload songs up to","Sube canciones hasta"),
("333","","","pro_badge","Pro badge","Insignia pro"),
("334","","","download_songs","Download songs","Descargar canciones"),
("335","","","turn_off_comments_download","Turn off comments/download","Desactivar comentarios / descargar"),
("336","","","current_plan","Current Plan","Plan actual"),
("337","","","pro_plan","Pro Plan","Pro Plan"),
("338","","","per_month","per month","por mes"),
("339","","","p_month","p/month","p / mes"),
("340","","","monthly","monthly","mensual"),
("341","","","upload_unlimited_songs","Upload unlimited songs","Sube canciones ilimitadas"),
("342","","","upgrade","Upgrade","Mejorar"),
("343","","","secured_payment_transaction","Secured payment transaction","Transacción de pago garantizado"),
("344","","","redirecting..","Redirecting..","Redireccionando .."),
("345","","","oops__an_error_found.","Oops, an error found.","Vaya, se ha encontrado un error."),
("346","","","you_are_a_pro_","You are a pro!","Eres un profesional!");

--
-- Dumping data for table `langs`
--

INSERT INTO langs VALUES
("347","","","unexpected_error_found_while_processing_your_payment__please_try_again_later.","Unexpected error found while processing your payment, please try again later.","Se ha encontrado un error inesperado al procesar su pago. Inténtelo de nuevo más tarde."),
("348","","","payment_error","Payment Error","Error en el pago"),
("353","","","you_have_reached_your_upload_limit___link__to_upload_unlimited_songs.","You have reached your upload limit, |link| to upload unlimited songs.","Has alcanzado tu límite de subida, |link| para subir canciones ilimitadas."),
("354","","","get_verified__sell_your_songs__get_a_special_looking_profile_and_get_famous_on_our_platform_","Get verified, sell your songs, get a special looking profile and get famous on our platform!","¡Verifíquese, venda sus canciones, obtenga un perfil especial y vuélvase famoso en nuestra plataforma!"),
("355","","","pro_memeber","PRO Member","Miembro PRO"),
("356","","","manage_my_songs","Manage My Songs","Manejar mis canciones"),
("357","","","published","Published","Publicado"),
("358","","","total_songs","Total Songs","Canciones totales"),
("359","","","total_plays","Total Plays","Jugadas totales"),
("360","","","total_downloads","Total Downloads","Descargas totales"),
("361","","","total_sales","Total Sales","Ventas totales"),
("362","","","total_sales_this_month","Total Sales This Month","Total de ventas este mes"),
("363","","","total_sales_this_today","Total Sales This Today","Ventas Totales Este Hoy"),
("364","","","total_sales_today","Total Sales Today","Ventas totales hoy"),
("365","","","downloads","Downloads","Descargas"),
("366","","","sales","Sales","Ventas"),
("367","","","most_played_songs","Most played songs","Las canciones más jugadas"),
("368","","","no_songs_found","No songs found","No se encontraron canciones"),
("369","","","most_commented_songs","Most commented songs","Canciones más comentadas"),
("370","","","most_liked_songs","Most liked songs","Las canciones mas gustadas"),
("371","","","most_downloaded_songs","Most downloaded songs","Las canciones más descargadas"),
("372","","","recent_sales","Recent sales","Ventas recientes"),
("373","","","no_sales_found","No sales found","No se encontraron ventas"),
("374","","","listened_by","Listened by","Escuchado por"),
("375","","","recently_listened_by","Recently Listened by","Recientemente escuchado por"),
("376","","","songs_i_liked","Songs I Liked","Canciones que me gustaron"),
("377","","","block","Block","Bloquear"),
("378","","","are_you_sure_you_want_to_block_this_user","Are you sure you want to block this user","Estás seguro de que quieres bloquear a este usuario"),
("379","","","unblock","Unblock","Desatascar"),
("380","","","blocked_users","Blocked Users","Usuarios bloqueados"),
("381","","","no_blocked_users_found","No blocked users found","No se encontraron usuarios bloqueados"),
("382","","","album_title","Album Title","Título del álbum"),
("383","","","your_album_title__2_-_55_characters","Your album title, 2 - 55 characters","El título de tu álbum, 2 - 55 caracteres."),
("384","","","album_description","Album Description","Descripción del Album"),
("385","","","album_price","Album Price","Precio del album"),
("386","","","add_song","Add Song","Añadir canción"),
("387","","","successfully_uploaded","Successfully uploaded","Cargado con éxito"),
("388","","","album_thumbnail_is_required.","Album thumbnail is required.","Se requiere la miniatura del álbum."),
("389","","","your_album_was_successfully_created__please_wait..","Your album was successfully created, please wait..","Su álbum fue creado exitosamente, por favor espere .."),
("390","","","album_title_is_required.","Album title is required.","El título del álbum es obligatorio."),
("391","","","album_description_is_required.","Album description is required.","Se requiere la descripción del álbum."),
("392","","","are_you_sure_you_want_to_delete_this_song_","Are you sure you want to delete this song?","¿Seguro que quieres borrar esta canción?"),
("393","","","top_50_albums","Top 50 Albums","Top 50 de los álbumes"),
("394","","","no_songs_on_this_album_yet.","No songs on this album yet.","No hay canciones en este álbum todavía."),
("395","","","you_may_also_like","You may also like","También te puede interesar"),
("396","","","your_album_was_successfully_updated__please_wait..","Your album was successfully updated, please wait..","Su álbum fue actualizado con éxito, por favor espere ..."),
("397","","","in_album_","in album:","en el álbum:"),
("398","","","delete_your_album","Delete your album","Borra tu álbum"),
("399","","","are_you_sure_you_want_to_delete_this_album_","Are you sure you want to delete this album?","¿Estás seguro de que quieres eliminar este álbum?"),
("400","","","yes__but_keep_the_songs","Yes, But Keep The Songs","Sí, pero conserva las canciones"),
("401","","","yes__delete_everything","Yes, Delete Everything","Si, eliminar todo"),
("402","","","my_songs","My Songs","Mis canciones"),
("403","","","my_albums","My Albums","Mis albumes"),
("404","","","create_copyright_dmca_take_down_notice","Create copyright DMCA take down notice","Crear derechos de autor DMCA quitar aviso"),
("405","","","report_coopyright","Report Copyright","Informe de derechos de autor"),
("406","","","report_copyright","Report Copyright","Informe de derechos de autor"),
("407","","","create_dmca_take_down_notice","Create DMCA take down notice","Crear aviso de eliminación de DMCA"),
("408","","","i_have_a_good_faith_belief_that_use_of_the_copyrighted_work_described_above_is_not_authorized_by_the_copyright_owner__its_agent_or_the_law","I have a good faith belief that use of the copyrighted work described above is not authorized by the copyright owner, its agent or the law","Creo de buena fe que el uso del trabajo con derechos de autor descrito anteriormente no está autorizado por el propietario de los derechos de autor, su agente o la ley"),
("409","","","i_confirm_that_i_am_the_copyright_owner_or_am_authorised_to_act_on_behalf_of_the_owner_of_an_exclusive_right_that_is_allegedly_infringed.","I confirm that I am the copyright owner or am authorised to act on behalf of the owner of an exclusive right that is allegedly infringed.","Confirmo que soy el propietario de los derechos de autor o que estoy autorizado para actuar en nombre del propietario de un derecho exclusivo que presuntamente se ha infringido."),
("410","","","submit","Submit","Enviar"),
("411","","","please_describe_your_request_carefully_and_as_much_as_you_can__note_that_false_dmca_requests_can_lead_to_account_termination.","Please describe your request carefully and as much as you can, note that false DMCA requests can lead to account termination.","Describa su solicitud con cuidado y tanto como pueda, tenga en cuenta que las solicitudes falsas de DMCA pueden llevar a la cancelación de la cuenta."),
("412","","","please_describe_your_request.","Please describe your request.","Por favor describa su solicitud."),
("413","","","please_select_the_checkboxs_below_if_you_own_the_copyright.","Please select the checkboxs below if you own the copyright.","Por favor, seleccione las casillas de verificación a continuación si posee los derechos de autor."),
("414","","","spotlight","Spotlight","Destacar"),
("415","","","no_spotlight_tracks_found","No spotlight tracks found","No se encontraron pistas de foco"),
("416","","","spotlight_your_songs","Spotlight your songs","Destaca tus canciones"),
("417","","","spotlight_your_songs__feature_","Spotlight your songs (feature)","Destacar tus canciones (característica)"),
("418","","","spotlight_your_songs__featured_","Spotlight your songs (featured)","Destaca tus canciones (destacadas)"),
("419","","","embed","Embed","Empotrar"),
("420","","","browse","Browse","Vistazo"),
("421","","","no_songs_found_on_this_store.","No songs found on this store.","No se encontraron canciones en esta tienda."),
("422","","","top_seller","Top Seller","Mejor vendedor"),
("423","","","no_more_followers_found","No more followers found","No se han encontrado más seguidores."),
("424","","","no_followers_found","No followers found","No se encontraron seguidores"),
("425","","","no_more_following_found","No more following found","No se han encontrado más seguidores."),
("426","","","no_following_found","No following found","No se han encontrado los siguientes"),
("427","","","is_pro_user","Is Pro user","Es usuario pro"),
("428","","","verified","Verified","Verificado"),
("429","","","pro_user","Pro user","Usuario pro"),
("430","","","normal_user","Normal user","Usuario normal"),
("431","","","unverified","Unverified","Inconfirmado"),
("432","","","featured","Featured","Destacados"),
("433","","","yes","Yes","Sí"),
("434","","","no","No","No"),
("435","","","like_comment","Like Comment","Me gusta comentar"),
("436","","","liked_your_comment.","liked your comment.","Me gustó tu comentario."),
("437","","","unlike_comment","UnLike Comment","No te gusta comentar"),
("438","","","report_comment.","Report comment.","Reportar comentario."),
("439","","","please_describe_whey_you_want_to_report_this_comment.","Please describe whey you want to report this comment.","Por favor describa si quiere reportar este comentario."),
("440","","","unreport_comment","UnReport Comment","UnReport Comment"),
("441","","","the_comment_report_was_successfully_deleted.","The comment report was successfully deleted.","El informe de comentarios fue eliminado con éxito."),
("442","","","unreport","Un Report","Un informe"),
("443","","","the_track_report_was_successfully_deleted.","The track report was successfully deleted.","El informe de seguimiento se ha eliminado correctamente."),
("444","","","track_comment.","Track comment.","Seguir el comentario."),
("445","","","please_describe_whey_you_want_to_report_this_track.","Please describe whey you want to report this track.","Describa por qué quiere informar de esta pista."),
("446","","","report_track.","Report track.","Informe de seguimiento."),
("447","","","results_for_","results for:","resultados para:"),
("448","","","what_are_looking_for__","What are looking for ?","Lo que están buscando ?"),
("449","","","no_more_artists_found","No more artists found","No se han encontrado más artistas."),
("450","","","no_more_albums_found","No more albums found","No se han encontrado más álbumes.");

--
-- Dumping data for table `langs`
--

INSERT INTO langs VALUES
("452","","","please_wait","please_wait","por favor espera"),
("454","","","admin_panel","Admin Panel","Panel de administrador"),
("455","","","no_messages_found_channel","no messages found channel","canal no encontrado"),
("456","","","search","search","buscar"),
("457","","","write_message","Write message","Escribe un mensaje"),
("458","","","are_you_sure_you_want_delete_chat","Are you sure you want delete chat","¿Estás seguro de que quieres eliminar el chat?"),
("459","","","messages","messages","mensajes"),
("460","","","no_messages_were_found__please_choose_a_channel_to_chat.","No messages were found, please choose a channel to chat.","No se encontraron mensajes, por favor elige un canal para chatear."),
("461","","","no_messages_were_found__say_hi_","No messages were found, say Hi!","No se encontraron mensajes, saludos!"),
("462","","","load_more_messages","Load more messages","Cargar mas mensajes"),
("463","","","message","message","mensaje"),
("464","","","no_match_found","No match found","No se encontraron coincidencias"),
("465","","","buy","Buy","Comprar"),
("466","","","you_have_bought_this_album.","You have bought this album.","Usted ha comprado este álbum."),
("467","","","price_range","Price range","Rango de precios"),
("468","","","you_have_bought_this_track.","You have bought this track.","Has comprado esta canción."),
("469","","","no_more_albums","No more albums","No mas discos"),
("470","","","no_more_songs","No more songs","No mas canciones"),
("471","","","no_results_found","No results found","No se han encontrado resultados"),
("472","","","no_albums_found","No albums found","No se encontraron álbumes"),
("473","","","send_as_message","Send as message","Enviar como mensaje"),
("474","","","add_a_maximum_of_10_friends_and_send_them_this_track","Add a maximum of 10 friends and send them this track","Agrega un máximo de 10 amigos y envíales esta pista"),
("475","","","get_started","Get Started","Empezar"),
("476","","","message_sent_successfully","Message sent successfully","Mensaje enviado con éxito"),
("477","","","password_is_too_short","Password is too short","La contraseña es demasiado corta"),
("478","","","username_length_must_be_between_5___32","Username length must be between 5 / 32","La longitud del nombre de usuario debe estar entre 5/32"),
("479","","","invalid_username_characters","Invalid username characters","Caracteres de usuario inválidos"),
("480","","","this_e-mail_is_invalid","This e-mail is invalid","Este email es invalido"),
("481","","","you_ain_t_logged_in_","You ain&#039;t logged in!","Usted no ha iniciado sesión!"),
("482","","","invalid_user_id","Invalid user ID","Identificación de usuario inválida"),
("483","","","no_new_releases_found","No new releases found","No se han encontrado nuevos lanzamientos."),
("484","","","light_mode","Light mode","Modo de luz"),
("485","","","____date___name_","© |DATE| |NAME|","© |DATE| |NAME|"),
("486","","","chat","Chat","Charla"),
("487","","","from_now","from now","desde ahora"),
("488","","","any_moment_now","any moment now","cualquier momento ahora"),
("489","","","about_a_minute_ago","about a minute ago","hace alrededor de un minuto"),
("490","","","_d_minutes_ago","%d minutes ago","Hace %d minutos"),
("491","","","about_an_hour_ago","about an hour ago","Hace aproximadamente una hora"),
("492","","","_d_hours_ago","%d hours ago","Hace %d horas"),
("493","","","a_day_ago","a day ago","Hace un día"),
("494","","","_d_days_ago","%d days ago","Hace %d días"),
("495","","","about_a_month_ago","about a month ago","Hace más o menos un mes"),
("496","","","_d_months_ago","%d months ago","Hace %d meses"),
("497","","","about_a_year_ago","about a year ago","Hace un año"),
("498","","","_d_years_ago","%d years ago","hace %d años"),
("504","","","no_data_to_show","No data to show","No hay datos para mostrar"),
("505","","","listen_to_songs","Listen to Songs","Escuchar canciones"),
("506","","","discover__stream__and_share_a_constantly_expanding_mix_of_music_from_emerging_and_major_artists_around_the_world.","Discover, stream, and share a constantly expanding mix of music from emerging and major artists around the world.","Descubra, transmita y comparta una mezcla de música en constante expansión de artistas emergentes e importantes de todo el mundo."),
("507","","","signup_now","Signup Now","Regístrate ahora"),
("508","","","explore","Explore","Explorar"),
("509","","","listen_music_everywhere__anytime","Listen Music Everywhere, Anytime","Escuchar música en cualquier lugar, en cualquier momento"),
("510","","","lorem_ipsum_dolor_sit_amet__consectetur_adipiscing_elit__sed_do_eiusmod_tempor_incididunt_ut_labore_et_dolore_magna_aliqua.","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."),
("511","","","create_playlists_with_any_song__on-the-go","Create Playlists with any song, On-The-Go","Crea listas de reproducción con cualquier canción, On-The-Go"),
("512","","","top_trending_artists","Top Trending Artists","Los mejores artistas de tendencias"),
("513","","","calling_all_creators","Calling all creators","Llamando a todos los creadores"),
("514","","","get_on__0__to_connect_with_fans__share_your_sounds__and_grow_your_audience.","Get on {0} to connect with fans, share your sounds, and grow your audience.","Súbete a {0} para conectarte con los fanáticos, compartir tus sonidos y aumentar tu audiencia."),
("515","","","upload_songs","Upload Songs","Subir canciones"),
("516","","","check_stats","Check Stats","Comprobar estadísticas"),
("517","","","ready_to_rock_your_world.","Ready to rock your world.","Listo para sacudir tu mundo."),
("518","","","search_for_artists__tracks","Search for artists, tracks","Búsqueda de artistas, pistas"),
("520","","","day_mode","Day mode","Modo día"),
("521","","","night_mode","Night mode","Modo nocturno"),
("522","","","interest","Interest","Interesar"),
("523","","","select_your_music_preference","Select your music preference","Seleccione su preferencia musical"),
("524","","","choose_below_to_start","Choose below to start","Elija a continuación para comenzar"),
("525","","","next","Next","Siguiente"),
("526","","","you_have_to_choose_your_favorites_genres_below","You have to choose your favorites genres below","Debes elegir tus géneros favoritos a continuación"),
("527","","","maintenance","Maintenance","Mantenimiento"),
("528","","","website_maintenance_mode_is_active__login_for_user_is_forbidden","Website maintenance mode is active, Login for user is forbidden","El modo de mantenimiento del sitio web está activo, el inicio de sesión para usuarios está prohibido"),
("529","","","website_maintenance_mode_is_active","Website maintenance mode is active","El modo de mantenimiento del sitio web está activo"),
("530","","","we___ll_be_back_soon_","We’ll be back soon!","¡Estaremos de vuelta pronto!"),
("531","","","sorry_for_the_inconvenience_but_we_rsquo_re_performing_some_maintenance_at_the_moment._if_you_need_help_you_can_always","Sorry for the inconvenience but we&amp;rsquo;re performing some maintenance at the moment. If you need help you can always","Disculpe las molestias, pero estamos realizando algunas tareas de mantenimiento en este momento. Si necesitas ayuda siempre puedes"),
("532","","","otherwise_we_rsquo_ll_be_back_online_shortly_","otherwise we&amp;rsquo;ll be back online shortly!","De lo contrario, estaremos de nuevo en línea próximamente."),
("533","","","sorry_for_the_inconvenience_but_we_performing_some_maintenance_at_the_moment._if_you_need_help_you_can_always","Sorry for the inconvenience but we performing some maintenance at the moment. If you need help you can always","Disculpe las molestias pero estamos realizando algunas tareas de mantenimiento en este momento. Si necesitas ayuda siempre puedes"),
("534","","","otherwise_we_will_be_back_online_shortly_","otherwise we will be back online shortly!","De lo contrario, volveremos a estar en línea pronto."),
("535","","","views","views","puntos de vista"),
("536","","","hide","hide","esconder"),
("538","","","your_selection_saved_successfully.","Your selection has been updated successfully.","Su selección ha sido actualizada con éxito."),
("539","","","please_wait...","Please wait...","Por favor espera..."),
("540","","","liked__auser__song_","liked |auser| song,","gustado |auser| canción,"),
("541","","","shared__auser__song_","shared |auser| song,","compartido |auser| canción,"),
("542","","","commented_on__auser__song_","commented on |auser| song,","ha comentado en |auser| canción,"),
("543","","","uploaded_a_new_song_","Uploaded a new song,","Subido una nueva canción,"),
("544","","","comments","comments","comentarios"),
("545","","","upload_single_song","Upload single song","Subir una sola canción"),
("546","","","upload_an_album","Upload an album","Subir un álbum"),
("547","","","thanks_for_your_submission__we_will_review_your_request_shortly.","Thanks for your submission, we will review your request shortly.","Gracias por su envío, revisaremos su solicitud en breve."),
("548","","","user_type","User Type","Tipo de usuario"),
("549","","","years_old","years old","años"),
("550","","","login_with_wowonder","Login with WoWonder","Iniciar sesión con Wowonder\n"),
("551","","","balance","Balance","Equilibrar"),
("552","","","available_balance","Available balance","Saldo disponible"),
("553","","","withdrawals","Withdrawals","Retiros"),
("554","","","paypal_e-mail","PayPal E-mail","E-mail de Paypal"),
("555","","","amount","Amount","Cantidad"),
("556","","","min","Min","Min"),
("557","","","submit_withdrawal_request","Submit withdrawal request","Enviar solicitud de retiro"),
("558","","","status","Status","Estado"),
("559","","","your_withdrawal_request_has_been_successfully_sent_","Your withdrawal request has been successfully sent!","Su solicitud de retiro ha sido enviada con éxito!");

--
-- Dumping data for table `langs`
--

INSERT INTO langs VALUES
("560","","","requested_on","Requested on","Solicitado en"),
("561","","","accepted","Completed","Terminado"),
("562","","","rejected","Rejected","Rechazado"),
("563","","","pending","Pending","Pendiente"),
("564","","","the_amount_exceeded_your_current_balance.","The amount exceeded your current balance.","La cantidad superó su saldo actual."),
("565","","","minimum_amount_required_is_50.","Minimum amount required is 50.","La cantidad mínima requerida es de 50."),
("566","","","second_lorem_ipsum_dolor_sit_amet__consectetur_adipiscing_elit__sed_do_eiusmod_tempor_incididunt_ut_labore_et_dolore_magna_aliqua__2","Second Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua, 2","Segundo Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua, 2"),
("567","","","you_can_not_submit_withdrawal_request_until_the_previous_requests_has_been_approved___rejected","You can not submit withdrawal request until the previous requests has been approved / rejected","No puede enviar una solicitud de retiro hasta que las solicitudes anteriores hayan sido aprobadas / rechazadas"),
("568","","","get_verified__get_a_special_looking_profile_and_get_famous_on_our_platform_","Get verified, get a special looking profile and get famous on our platform!","¡Verifíquese, obtenga un perfil especial y vuélvase famoso en nuestra plataforma!"),
("569","","","purchases","Purchases","Compras"),
("570","","","select_payment_method.","Select a payment method","Seleccione un método de pago"),
("571","","","choose_a_payment_method.","Choose a payment method","Elija un método de pago"),
("572","","","track_purchase","Purchase Track ","Pista de compra"),
("573","","","credit_card","Credit Card","Tarjeta de crédito"),
("574","","","bank_transfer","Bank Transfer","Transferencia bancaria"),
("575","","","note","Note","Nota"),
("576","","","please_transfer_the_amount_of","Please transfer the amount of","Por favor transfiera la cantidad de"),
("577","","","to_this_bank_account_to_buy","to this bank account to buy","a esta cuenta bancaria para comprar"),
("578","","","upload_receipt","Upload Receipt","Cargar Recibo"),
("579","","","confirm","Confirm","Confirmar"),
("580","","","your_receipt_uploaded_successfully.","Your receipt  has been uploaded successfully.","Su recibo ha sido cargado con éxito."),
("581","","","we_approved_your_bank_transfer_of__d_","We approved your bank transfer of %d!","¡Aprobamos su transferencia bancaria de% d!"),
("582","","","we_have_rejected_your_bank_transfer__please_contact_us_for_more_details.","We have rejected your bank transfer, please contact us for more details.","Hemos rechazado su transferencia bancaria, póngase en contacto con nosotros para obtener más detalles."),
("583","","","dislike","Dislike","Disgusto"),
("584","","","disliked","Disliked","No me gustó"),
("585","","","paypal","PayPal","PayPal"),
("586","","","read_more","Read more","Lee mas"),
("587","","","read_less","Read less","Leer menos"),
("588","","","this_username_is_disallowed","This username is not allowed","Este nombre de usuario no está permitido"),
("589","","","disliked__auser__song_","disliked |auser| song,","no me gustó |auser| canción,"),
("590","","","statistics","Statistics","Estadística"),
("591","","","total_views","Total Views","Vistas totales"),
("592","","","total_likes","Total Likes","Me gusta en total"),
("593","","","total_dislikes","Total Dislikes","Aversiones totales"),
("594","","","today","Today",""),
("595","","","this_week","This week","Esta semana"),
("596","","","this_month","This month","Este mes"),
("597","","","dislikes","dislikes","aversiones"),
("598","","","allow_downloads","Allow downloads",""),
("599","","","display_embed_code","Display embed code","Mostrar código de inserción"),
("600","","","lyrics","Lyrics","Letra"),
("601","","","show_more","Show more","Mostrar más"),
("602","","","show_less","Show less","Muestra menos"),
("603","","","no_payment_method_available.","No payment method available.","No hay método de pago disponible."),
("604","","","upload_multiple_songs","Upload songs",""),
("605","","","add_multiple_songs","Add Songs","Añadir canciones"),
("606","","","no_users_found","No users found","No se encontraron usuarios"),
("607","","","show_only_in_track_page","Show only in track page","Mostrar solo en la página de seguimiento"),
("608","","","show_on_all_pages","Show on all pages","Mostrar en todas las páginas"),
("609","","","edit_spotlight","Edit Spotlight","Editar Spotlight"),
("610","","","blog","Blog","Blog"),
("611","blog_categories","","1309","Comedy","Comedia"),
("612","blog_categories","","1310","Cars and Vehicles","Autos y vehiculos"),
("613","blog_categories","","1311","Economics and Trade","Economía y comercio"),
("614","blog_categories","","1312","Education","Educación"),
("615","blog_categories","","1313","Entertainment","Entretenimiento"),
("616","blog_categories","","1314","Movies & Animation","Películas"),
("617","blog_categories","","1315","Gaming","Juego de azar"),
("618","blog_categories","","1316","History and Facts","Historia y hechos"),
("619","blog_categories","","1317","Live Style","Estilo de vida"),
("620","blog_categories","","1318","Natural","Natural"),
("621","blog_categories","","1319","News and Politics","Noticias y politica"),
("622","blog_categories","","1320","People and Nations","Pueblos y naciones"),
("623","blog_categories","","1321","Pets and Animals","Mascotas y animales"),
("624","blog_categories","","1322","Places and Regions","Lugares y Regiones"),
("625","blog_categories","","1323","Science and Technology","Ciencia y Tecnología"),
("626","blog_categories","","1324","Sport","Deporte"),
("627","blog_categories","","1325","Travel and Events","Viajes y eventos"),
("628","blog_categories","","1326","Other","Otro"),
("629","","","read_more","Read more","Lee mas"),
("630","","","categories","Categories","Categorias"),
("631","","","no_more_articles_to_show.","No more articles to show.","No hay más artículos para mostrar."),
("632","","","article","Article","Artículo"),
("633","","","share_to","Share to","Compartir a"),
("634","","","blogs","Blogs","Blogs"),
("635","","","no_more_articles","No more articles to show","No hay más artículos para mostrar."),
("636","","","no_more_article_found","No more articles found","No se encontraron más artículos."),
("637","","","delete_song","Delete Song","Eliminar canción"),
("638","","","delete_this_song_from_playlist","Remove from playlist","Eliminar de la lista de reproducción"),
("639","","","please_enter_song_description","Please enter the song description","Por favor ingrese la descripción de la canción"),
("640","","","please_enter_song_tags","Please enter song\'s tags","Por favor, introduzca las etiquetas de la canción"),
("641","","","please_upload_song_thumbnail","Please upload song thumbnail","Sube la miniatura de la canción"),
("642","","","you_have_reached_your_upload_limit__upgrade_to_upload_unlimited_songs.","You have reached your upload limit, upgrade to upload unlimited songs.","Ha alcanzado su límite de carga, actualice para cargar canciones ilimitadas."),
("643","","","advertising","Advertising","Publicidad"),
("644","","","wallet","Wallet","Billetera"),
("645","","","create_ad","New Campaign","Nueva campaña"),
("646","","","category","Category","Categoría"),
("647","","","results","Results","Resultados"),
("648","","","spent","Spent","Gastado"),
("649","","","action","Action","Acción"),
("650","","","2checkout","2Checkout","2Caja"),
("651","","","address","Address","Dirección"),
("652","","","city","City","Ciudad"),
("653","","","state","State","Estado"),
("654","","","zip","Zip","Cremallera"),
("655","","","phone_number","Phone number","Número de teléfono"),
("656","","","card_number","Card Number","Número de tarjeta"),
("657","","","pay","Pay","Paga"),
("658","","","replenish","Replenish","Reponer"),
("659","","","please_check_the_details","Please check the details","Por favor revise los detalles");

--
-- Dumping data for table `langs`
--

INSERT INTO langs VALUES
("660","","","confirmation","Confirmation","Confirmación"),
("661","","","are_you_sure_you_want_to_delete_this_ad_","Are you sure you want to delete this campaign?","¿Estás seguro de que deseas eliminar esta campaña?"),
("662","","","deleted","deleted","eliminado"),
("663","","","please_check_details","please_check_details","please_check_details"),
("664","","","confirming_your_payment__please_wait..","Confirming your payment, please wait..","Confirmando su pago, por favor espere ..."),
("665","","","payment_declined__please_try_again_later.","Payment declined, please try again later.","Pago rechazado, intente nuevamente más tarde."),
("666","","","my_balance","My Balance","Mi balance"),
("667","","","replenish_my_balance","Replenish My Balance","Reponer mi saldo"),
("668","","","browse_to_upload","Browse To Upload","Navegar para cargar"),
("669","","","create_advertising","New campaign ","Nueva campaña"),
("670","","","create_new_ad","Create new campaign","Crear nueva campaña"),
("671","","","your_current_wallet_balance_is__0__please_top_up_your_wallet_to_continue.","Your current wallet balance is 0, please top up your wallet to continue.","El saldo actual de su billetera es 0, recargue su billetera para continuar."),
("672","","","top_up","Top Up","Completar"),
("673","","","url","URL","URL"),
("674","","","select_media","Select Media","Select Media"),
("675","","","target_audience","Target Audience","Público objetivo"),
("676","","","placement","Placement","Colocación"),
("677","","","pricing","Pricing","Precios"),
("678","","","pay_per_click","Pay Per Click","Pago por clic"),
("679","","","pay_per_impression","Pay Per Impression","Pago por impresión"),
("680","","","spending_limit_per_day","Spending limit per day","Límite de gasto por día"),
("681","","","media_file_is_invalid._please_select_a_valid_image","Media file is invalid. Please select a valid image","El archivo multimedia no es válido. Por favor seleccione una imagen válida"),
("682","","","error_","Error!","Error!"),
("683","","","file_is_too_big__max_upload_size_is","File is too big, Max upload size is","El archivo es demasiado grande, el tamaño máximo de carga es"),
("684","","","media_file_is_invalid._please_select_a_valid_image___video","Media file is invalid. Please select a valid image / video","El archivo multimedia no es válido. Selecciona una imagen / video válido"),
("685","","","your_ad_has_been_published_successfully","Your campaign has been published successfully.","Su campaña ha sido publicada con éxito."),
("686","","","the_url_is_invalid._please_enter_a_valid_url","The URL is invalid. Please enter a valid URL.","La URL no es válida. Por favor introduzca un URL válido."),
("687","","","ad_title_must_be_between_5_100","Campaign title must be between 5/100.","El título de la campaña debe estar entre 5/100."),
("688","","","edit_ad","Edit campaign","Editar campaña"),
("689","","","your_changes_to_the_ad_were_successfully_saved","Your changes to the campaign were successfully saved.","Sus cambios en la campaña se guardaron correctamente."),
("690","","","name_must_be_between_5_32","Name must be between 5/32","El nombre debe estar entre 5/32"),
("691","","","clicks","Clicks","Clics"),
("692","","","ads_analytics","Campaign analytics","Análisis de campaña"),
("693","","","this_year","This year","Este año"),
("694","","","view_report","View report","Vista del informe"),
("695","","","ad_analytics","Campaign analytics","Análisis de campaña"),
("696","","","sponsor_ads","SPONSOR","PATROCINADOR"),
("697","","","by","By","Por"),
("698","","","import","Import","Importar"),
("699","","","import_from_soundcloud.","Import From SoundCloud.","Importar desde SoundCloud."),
("700","","","enter_the_soundcloud_track_link_and_click_the_button_below.","Paste your SoundCloud URL above.","Pegue su URL de SoundCloud arriba."),
("701","","","error_found_while_importing_your_track__please_try_again_later.","Error found while importing your track, please try again later.","Se encontró un error al importar su pista. Vuelva a intentarlo más tarde."),
("702","","","please_enter_valid_soundcloud_track_url.","Please enter a valid SoundCloud track URL.","Ingrese una URL de pista válida de SoundCloud."),
("703","","","please_enter_soundcloud_track_link_to_import.","Please enter SoundCloud track link to import.","Ingrese el enlace de la pista de SoundCloud para importar."),
("704","","","error_found_while_importing_your_track__please_check_soundcloud_client_id.","Error found while importing your track, please check SoundCloud client ID.","Se encontró un error al importar su pista, verifique la ID del cliente de SoundCloud."),
("705","","","dmca","DMCA","DMCA"),
("706","","","login_with_soundcloud","Login with SoundCloud","Inicie sesión con SoundCloud"),
("707","","","dcma","DCMA","DCMA"),
("708","","","move_to_album","Move to an album","Moverse a un álbum"),
("709","","","select_albums","Select albums","Seleccionar álbumes"),
("710","","","please_select_which_album_you_want_to_add_this_song_to.","Please select which album you want to add this song to.","Seleccione a qué álbum desea agregar esta canción."),
("711","","","review_track","Review Track","Pista de revisión"),
("712","","","review","Review","revisión"),
("713","","","review_track.","Review track.","Revisar pista."),
("714","","","please_enter_your_review.","Please enter your review.","Por favor, introduzca su opinión."),
("715","","","thanks_for_your_submission.","Thanks for your submission.","Gracias por tu presentación."),
("716","","","reviews","Reviews","Comentarios"),
("717","","","no_reviews_on_this_track_yet.","No reviews on this track yet.","Aún no hay reseñas sobre esta pista."),
("718","","","upload_new_track.","upload new track.","subir nueva pista"),
("719","","","notification","Notification","Notificación"),
("720","","","notification_settings","Notification Settings","Configuración de las notificaciones"),
("721","","","someone_followed_me","Someone followed me","Alguien me siguió"),
("722","","","someone_liked_one_of_my_tracks","Someone liked one of my tracks","A alguien le gustó una de mis canciones."),
("723","","","someone_liked_one_of_my_comments","Someone liked one of my comments","A alguien le gustó uno de mis comentarios."),
("724","","","approve_disapprove_artist_request","Approve/Disapprove artist request(s)","Aprobar / Rechazar solicitud (es) de artista"),
("725","","","approve_disapprove_bank_payment_request","Approve/Disapprove bank payment request(s)","Aprobar / rechazar solicitudes de pago bancario"),
("726","","","one_of_my_following_upload_new_track","One of my following artists uploaded a new track","Uno de mis siguientes artistas subió una nueva canción."),
("727","","","one_of_my_following_users_upload_new_track","One of my following artists uploaded a new track","Uno de mis siguientes artistas subió una nueva canción."),
("728","","","notify_me_when","Notify me when","Notifícame cuando"),
("729","","","new_notification","New notification","Nueva notificación"),
("730","","","you_can_not_import_this_track_because_this_track_is_imported_before.","This track is already imported, please choose another track.","Esta pista ya está importada, elija otra pista."),
("731","","","manage_sessions","Manage Sessions","Administrar sesiones"),
("732","","","ip_address","IP Address","Dirección IP"),
("733","","","platform","Platform","Plataforma"),
("734","","","browser","Browser","Navegador"),
("735","","","last_seen","Last Seen","Ultima vez visto"),
("736","","","actions","Actions","Comportamiento"),
("737","","","session_expired","Session Expired","Sesión expirada"),
("738","","","your_session_has_been_expired__please_login_again.","Your Session has been expired, please login again.","Su sesión ha caducado, vuelva a iniciar sesión."),
("739","","","two-factor_authentication","Two-factor authentication","Autenticación de dos factores"),
("740","","","phone","Phone","Teléfono"),
("741","","","enable","Enable","Habilitar"),
("742","","","disable","Disable","Inhabilitar"),
("743","","","turn_on_2-step_login_to_level-up_your_account_s_security__once_turned_on__you_ll_use_both_your_password_and_a_6-digit_security_code_sent_to_your_phone_or_email_to_log_in.","Turn on 2-step login to level-up your account\'s security, Once turned on, you\'ll use both your password and a 6-digit security code sent to your phone or email to log in.","Active el inicio de sesión en 2 pasos para subir de nivel su cuenta"),
("744","","","a_confirmation_email_has_been_sent.","A confirmation email has been sent.","Un correo electrónico de confirmación ha sido enviado."),
("745","","","we_have_sent_an_email_that_contains_the_confirmation_code_to_enable_two-factor_authentication.","We have sent an email that contains the confirmation code to enable Two-factor authentication.","Hemos enviado un correo electrónico que contiene el código de confirmación para habilitar la autenticación de dos factores."),
("746","","","confirmation_code","Confirmation code","Código de confirmación"),
("747","","","a_confirmation_message_and_email_were_sent.","A confirmation message and email were sent.","Se envió un mensaje de confirmación y un correo electrónico."),
("748","","","we_have_sent_a_message_and_an_email_that_contain_the_confirmation_code_to_enable_two-factor_authentication","We have sent a message and an email that contain the confirmation code to enable two-factor authentication","Hemos enviado un mensaje y un correo electrónico que contienen el código de confirmación para permitir la autenticación de dos factores."),
("749","","","a_confirmation_message_was_sent.","A confirmation message was sent.","Se envió un mensaje de confirmación."),
("750","","","we_have_sent_a_message_that_contains_the_confirmation_code_to_enable_two-factor_authentication.","We have sent a message that contains the confirmation code to enable Two-factor authentication.","Hemos enviado un mensaje que contiene el código de confirmación para habilitar la autenticación de dos factores."),
("751","","","we_have_sent_you_an_email_with_the_confirmation_code.","We have sent you an email with the confirmation code.","Le hemos enviado un correo electrónico con el código de confirmación."),
("752","","","wrong_confirmation_code.","Wrong confirmation code.","Código de confirmación incorrecto."),
("753","","","your_e-mail_has_been_successfully_verified.","Your E-mail has been successfully verified.","Su correo electrónico ha sido verificado con éxito."),
("754","","","unusual_login","Unusual login","Inicio de sesión inusual"),
("755","","","we_have_sent_you_the_confirmation_code_to_your_email_address.","We have sent you the confirmation code to your email address.","Le hemos enviado el código de confirmación a su dirección de correo electrónico."),
("756","","","to_log_in__you_need_to_verify_your_identity.","To log in, you need to verify your identity.","Para iniciar sesión, debe verificar su identidad."),
("757","","","welcome...","Welcome Back!","¡Dar una buena acogida!"),
("758","","","stations","Stations","Estaciones"),
("759","","","no_stations_found","No stations found","No se encontraron estaciones");

--
-- Dumping data for table `langs`
--

INSERT INTO langs VALUES
("760","","","add_station","Add Station","Agregar estación"),
("761","","","search_for_stations","Search for stations","Busca estaciones"),
("762","","","station_search.","Station Search.","Búsqueda de estación."),
("763","","","please_enter_more_than_3_characters.","Please enter more than 3 characters.","Por favor, introduzca más de 3 caracteres."),
("764","","","error_found_while_search_stations__please_try_again_later.","Error found while search stations, please try again later.","Se encontró un error al buscar estaciones, por favor intente más tarde."),
("765","","","you_already_add_this_station.","You already add this station.","Ya agregaste esta estación."),
("766","","","delete_station","Delete Station","Eliminar estación"),
("767","","","no_more_stations_found","No more stations found","No se encontraron más estaciones"),
("768","","","the_track_has_been_moved_to_this_album_successfully.","The track has been moved to following album.","La pista se ha movido al siguiente álbum."),
("769","","","someone_liked_disliked_one_of_my_tracks","Someone liked/disliked one of my tracks","A alguien le gustó / no le gustó una de mis canciones"),
("770","","","disliked_your_song.","disliked your song.","No me gustó tu canción."),
("771","","","reviewed_your_song.","reviewed your song.","revisó tu canción"),
("772","","","someone_reviewed_one_of_my_tracks","Someone reviewed one of my tracks","Alguien revisó una de mis canciones."),
("773","","","you_can_not_import_this_track_because_this_track_is_one_of_soundcloud_go__tracks.","You can not import this track because this track is one of SoundCloud Go+ tracks.","No puede importar esta pista porque esta es una de las pistas de SoundCloud Go."),
("774","","","remove_song","Remove Song","Eliminar canción"),
("775","","","invalid_file_format__only_mp3__ogg__opus__oga__wav__and_mpeg_is_allowed","Invalid file format, only mp3, ogg, opus, oga, wav, and mpeg is allowed","Formato de archivo no válido, solo se permiten mp3, ogg, opus, oga, wav y mpeg"),
("776","","","remove_song","Remove Song","Eliminar canción"),
("777","","","invalid_file_format__only_mp3__ogg__opus__oga__wav__and_mpeg_is_allowed","Invalid file format, only mp3, ogg, opus, oga, wav, and mpeg is allowed","Formato de archivo no válido, solo se permiten mp3, ogg, opus, oga, wav y mpeg"),
("778","","","my_affiliates","My Affiliates","Mis afiliados"),
("779","","","your_affiliate_link","Your affiliate link","Su enlace de afiliado"),
("780","","","import_from","Import From","Importar de"),
("781","","","paste_your_url_above.","Paste your URL above.","Pega tu URL arriba."),
("782","","","please_enter_a_valid_link_to_import.","Please enter a valid link to import.","Ingrese un enlace válido para importar."),
("783","","","import_sounds","Import Music","Importar música"),
("784","","","link_must_be_like_ex__https___music.apple.com_us_album_wolves_1445055015_i_1445055017","Link must be like EX: https://music.apple.com/us/album/wolves/1445055015?i=1445055017","El enlace debe ser como EX: https://music.apple.com/us/album/wolves/1445055015?i=1445055017"),
("785","","","itunes_partner_token","Itunes Partner Token","Token de socio de iTunes"),
("786","","","listen_in_deezer","Listen in Deezer","Escucha en Deezer"),
("787","","","type","Type","Tipo"),
("788","","","banners_ads","Banners Ads","Anuncios de Banners"),
("789","","","audio_ads","Audio Ads","Anuncios de audio"),
("790","","","select_audio","Select Audio","Seleccionar audio"),
("791","","","error_500_internal_server_error_","Error 500 internal server error!","¡Error 500 - Error Interno del Servidor!"),
("792","","","no_songs_found__add_new_songs.","No songs found, add new songs.","No se encontraron canciones, agregue nuevas canciones."),
("793","","","add_upload_folder","Add Upload Folder","Agregar carpeta de carga"),
("794","","","upload_folder","Upload Folder","Subir carpeta"),
("795","","","playlist_single","Playlist","Lista de reproducción"),
("796","","","drop_your_files","Drop your files to upload","Suelta tus archivos para subir"),
("797","","","invite_new_users","Invite new Users.","Invita a nuevos usuarios."),
("798","","","get_credits","Get Credits.","Obtener créditos."),
("799","","","to_skip_ad","to Skip Ad","Saltar anuncio"),
("800","","","campaign_deleted_succ","Campaign deleted successfully.","Campaña eliminada con éxito."),
("801","","","earn_up","Earn up to {{PRICE}} for each user your refer to us!","¡Gane hasta {{PRECIO}} por cada usuario que nos recomiende!"),
("802","","","buy_album","Buy Album",""),
("803","","","first_name","first_name",""),
("804","","","cateogry_2","Traditional",""),
("805","","","cateogry_3"," Jazz",""),
("806","","","cateogry_4","Contemporary",""),
("807","","","cateogry_5","Inspirational",""),
("808","","","cateogry_6","CHH",""),
("809","","","cateogry_7","Quartet",""),
("810","","","cateogry_8","Praise And Worship",""),
("811","","","cateogry_9","Gospel Mixx",""),
("812","","","cateogry_10","EDM Gospel",""),
("813","","","cateogry_11","EDM Gospel",""),
("814","","","cateogry_12","Radio",""),
("815","","","invalid_request","Invalid Request",""),
("816","","","","",""),
("817","","","discover1","Discover1",""),
("818","","","ugn_stations","UGN Stations",""),
("819","","","radiostations","RadioStations",""),
("820","","","radio_stations","Radio Stations",""),
("821","","","please_enter_song_title","Please enter song title",""),
("822","","","cateogry_13","Carribean Gospel",""),
("823","","","cateogry_14","African Gospel Artists",""),
("824","","","listen_to_gospel_everywhere__anytime.","Listen To Gospel Everywhere, Anytime.",""),
("825","","","listen_to_gospel_everywhere__anytime","Listen To Gospel Everywhere, Anytime",""),
("826","","","discover_music_from_every_genre_of_urban_gospel_music_from_up_and_coming_to_national_and_established_along_with_over_20_plus_global_gospel_stations...","Discover music from every Genre of Urban Gospel Music from up and coming to National and established along with over 20 plus Global Gospel Stations...",""),
("827","","","enjoy_your_selection_of_music_with_your_very_own_playlist_of_selected_music__or_one_of_our_ugn_charting_stations......","Enjoy your selection of Music with your very own playlist of selected Music, or one of our UGN Charting Stations......",""),
("828","","","urban_praises__come_listen_to_the_best_indie_and_main_stream_artists_in_the_world_","Urban Praises, Come listen to the best Indie and Main Stream artists in the world!",""),
("829","","","put_some_praise_in_your_world.","Put Some Praise In Your World.","");

-- ---------------------------------------------------------
--
-- Table structure for table : `likes`
--
-- ---------------------------------------------------------

CREATE TABLE `likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `track_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `comment_id` int(11) unsigned NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `track_id` (`track_id`),
  KEY `user_id` (`user_id`),
  KEY `time` (`time`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `likes`
--

INSERT INTO likes VALUES
("2","7","8","0","1592716475"),
("3","6","8","0","1592716736"),
("4","9","8","0","1592748894"),
("5","60","14","0","1593098038"),
("6","74","3","0","1593125556"),
("7","87","30","0","1593138371"),
("8","74","27","0","1593154807"),
("9","107","32","0","1593156268"),
("10","8","8","0","1593577698"),
("11","116","25","0","1593578621"),
("12","3","38","0","1593748651"),
("14","4","43","0","1593826803"),
("15","74","43","0","1593826898"),
("16","119","43","0","1593856879");

-- ---------------------------------------------------------
--
-- Table structure for table : `messages`
--
-- ---------------------------------------------------------

CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_id` int(11) NOT NULL DEFAULT '0',
  `to_id` int(11) NOT NULL DEFAULT '0',
  `text` text,
  `seen` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  `from_deleted` int(11) NOT NULL DEFAULT '0',
  `to_deleted` int(11) NOT NULL DEFAULT '0',
  `sent_push` int(11) unsigned NOT NULL DEFAULT '0',
  `notification_id` varchar(50) NOT NULL DEFAULT '',
  `type_two` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `from_id` (`from_id`),
  KEY `to_id` (`to_id`),
  KEY `seen` (`seen`),
  KEY `time` (`time`),
  KEY `from_deleted` (`from_deleted`),
  KEY `to_deleted` (`to_deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `messages`
--

INSERT INTO messages VALUES
("1","3","1","Hello","1592871441","1592633027","0","0","0","",""),
("2","3","14","Welcome to UGN Radio and Artists Network","1592866787","1592856683","0","0","0","",""),
("3","3","10","Welcome Eturnal","1594012799","1592856958","0","0","0","",""),
("4","14","3","Thank you!","1592866885","1592866799","0","0","0","",""),
("5","3","14","The site is now secure also","1592921299","1592866921","0","0","0","",""),
("6","24","3","Hello. Is there a link or cash app to pay you?","1593062446","1593061128","1","0","0","",""),
("7","3","24","Go up to the little circle at the top where your picture is and press it and a drop-down menu will come and it will say upgrade to Pro and when you do that you&#039;ll be able to pay it it&#039;ll be through PayPal but you can use credit card or whatever","1593062687","1593062526","0","1","0","",""),
("8","24","3","thanks","1593063015","1593062712","1","0","0","",""),
("9","3","24","Your welcome","1593114249","1593063038","0","1","0","",""),
("10","3","46","Welcome","0","1594007178","0","0","0","",""),
("11","10","3","Hello How are you? Im just seeing this. Dont forget its Eturnul no A.","1594078070","1594012850","0","0","0","",""),
("12","3","10","Ok","0","1594078099","0","0","0","",""),
("13","3","46","Glad to have you upload some music","0","1594425956","0","0","0","","");

-- ---------------------------------------------------------
--
-- Table structure for table : `notifications`
--
-- ---------------------------------------------------------

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notifier_id` int(11) NOT NULL DEFAULT '0',
  `recipient_id` int(11) NOT NULL DEFAULT '0',
  `track_id` int(11) NOT NULL DEFAULT '0',
  `comment_id` int(11) unsigned NOT NULL DEFAULT '0',
  `type` varchar(20) NOT NULL DEFAULT '',
  `text` text,
  `url` varchar(3000) NOT NULL DEFAULT '',
  `seen` varchar(50) NOT NULL DEFAULT '0',
  `sent_push` int(11) unsigned DEFAULT '0',
  `time` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `recipient_id` (`recipient_id`),
  KEY `type` (`type`),
  KEY `seen` (`seen`),
  KEY `notifier_id` (`notifier_id`),
  KEY `time` (`time`),
  KEY `music_id` (`track_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notifications`
--

INSERT INTO notifications VALUES
("2","3","27","74","0","liked_track","","track/47J7UgiHL2bXXHZ","1593154773","0","1593125556"),
("5","3","14","0","0","approved_artist","","user/GospelUnderground","1594029729","0","1593644058"),
("6","3","28","0","0","approved_artist","","user/gharris817","0","0","1593644058"),
("7","3","25","0","0","approved_artist","","user/Chosen","1593923776","0","1593644059"),
("8","3","41","0","0","approved_artist","","user/Soundsofimani","1593740892","0","1593644061"),
("11","3","27","0","0","approved_artist","","user/hawkinsbhu","0","0","1593644162"),
("12","43","27","74","0","liked_track","","track/47J7UgiHL2bXXHZ","0","0","1593826898"),
("13","43","34","0","0","follow_user","","","0","0","1593906302"),
("14","3","46","0","0","approved_artist","","user/513db937d","0","0","1594408955");

-- ---------------------------------------------------------
--
-- Table structure for table : `payments`
--
-- ---------------------------------------------------------

CREATE TABLE `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `amount` float NOT NULL DEFAULT '0',
  `type` varchar(15) NOT NULL DEFAULT '',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pro_plan` varchar(100) DEFAULT '',
  `info` varchar(100) DEFAULT '0',
  `via` varchar(100) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payments`
--

INSERT INTO payments VALUES
("1","11","0.5","TRACK","2020-06-23 12:27:21","0","cavNvguPALIVySg","PayPal"),
("2","14","0.5","TRACK","2020-06-24 09:40:17","0","cavNvguPALIVySg","PayPal");

-- ---------------------------------------------------------
--
-- Table structure for table : `playlist_songs`
--
-- ---------------------------------------------------------

CREATE TABLE `playlist_songs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playlist_id` int(11) NOT NULL DEFAULT '0',
  `track_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `playlist_id` (`playlist_id`),
  KEY `song_id` (`track_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `playlist_songs`
--

INSERT INTO playlist_songs VALUES
("1","1","3","3","1592715771"),
("2","1","5","3","1592715829"),
("3","1","2","3","1592715845"),
("4","1","4","3","1592715872"),
("5","2","9","8","1592716068"),
("6","2","8","8","1592716111"),
("7","2","6","8","1592716985"),
("8","1","27","3","1592874910"),
("11","5","49","16","1592960005"),
("12","5","48","16","1592960021"),
("13","5","47","16","1592960035"),
("14","5","46","16","1592960048"),
("15","5","44","16","1592960063"),
("16","5","43","16","1592960074"),
("17","1","60","3","1593037254"),
("18","1","8","3","1593063547"),
("19","1","74","3","1593125567"),
("20","6","97","32","1593182473"),
("21","6","98","32","1593182483"),
("22","6","99","32","1593182494"),
("23","6","100","32","1593182505"),
("24","6","101","32","1593182518"),
("25","6","102","32","1593182525"),
("26","6","103","32","1593182533"),
("27","6","104","32","1593182544"),
("28","6","105","32","1593182559"),
("29","6","106","32","1593182568"),
("30","6","107","32","1593182575"),
("31","6","108","32","1593182586"),
("32","5","97","16","1593578817"),
("33","5","107","16","1593578927"),
("34","5","3","16","1593579097"),
("35","5","118","16","1593787034");

-- ---------------------------------------------------------
--
-- Table structure for table : `playlists`
--
-- ---------------------------------------------------------

CREATE TABLE `playlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `privacy` int(11) NOT NULL DEFAULT '0',
  `thumbnail` varchar(120) NOT NULL,
  `uid` varchar(12) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `user_id` (`user_id`),
  KEY `privacy` (`privacy`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `playlists`
--

INSERT INTO playlists VALUES
("1","UBN Gospel Radio Playlist","3","0","upload/photos/2020/06/JoOyGIGNxFZNQGYpqpvK_23_0398354290d180e2e3ec8f31bf7681dd_image.jpg","x3JLOIvIPJMK","1592715658"),
("2","The Chapman Project","8","0","upload/photos/2020/06/kmiZShmYmF6nZItvgJ1e_21_de9943ea656bd7845c0d184afbde0e7d_image.jpg","4F2zWzosdWrS","1592716011"),
("5","Right Now Praizz Top 30 Playlist","16","0","upload/photos/2020/06/IZMAvp29TR2TKwrv42oj_24_7fed274606333cc8286ceb2e7e490c47_image.png","NUMaUtEBJMMj","1592959921"),
("6","Push-It! Artist","32","0","upload/photos/2020/06/jOleKKgGEoYP43XyP8SD_26_6ce3c2f820e168025c5ac3f102565156_image.jpg","QYMnU3Gz1smz","1593157205"),
("8","CARIBBEAN HITS","17","0","upload/photos/2020/07/xhUMTSnTnTek9XevdA5u_02_eaf36f2bd7ffe2aaa413a3261890f852_image.png","BDXJ5m6UOfM6","1593719251");

-- ---------------------------------------------------------
--
-- Table structure for table : `profile_fields`
--
-- ---------------------------------------------------------

CREATE TABLE `profile_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci,
  `type` text COLLATE utf8_unicode_ci,
  `length` int(11) NOT NULL DEFAULT '0',
  `placement` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'profile',
  `registration_page` int(11) NOT NULL DEFAULT '0',
  `profile_page` int(11) NOT NULL DEFAULT '0',
  `select_type` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'none',
  `active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `registration_page` (`registration_page`),
  KEY `active` (`active`),
  KEY `profile_page` (`profile_page`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ---------------------------------------------------------
--
-- Table structure for table : `purchases`
--
-- ---------------------------------------------------------

CREATE TABLE `purchases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `track_id` int(11) NOT NULL DEFAULT '0',
  `track_owner_id` int(11) NOT NULL DEFAULT '0',
  `final_price` float NOT NULL DEFAULT '0',
  `commission` float NOT NULL DEFAULT '0',
  `price` float NOT NULL DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `track_id` (`track_id`),
  KEY `timestamp` (`timestamp`),
  KEY `time` (`time`),
  KEY `track_owner_id` (`track_owner_id`),
  KEY `final_price` (`final_price`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchases`
--

INSERT INTO purchases VALUES
("1","11","45","3","0.5","50","0.99","2020-06-23 12:27:21","1592940441"),
("2","14","45","3","0.5","50","0.99","2020-06-24 09:40:17","1593016817");

-- ---------------------------------------------------------
--
-- Table structure for table : `reports`
--
-- ---------------------------------------------------------

CREATE TABLE `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `track_id` int(11) NOT NULL DEFAULT '0',
  `comment_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `time` int(11) NOT NULL DEFAULT '0',
  `seen` int(11) unsigned DEFAULT '0',
  `ignored` int(11) unsigned DEFAULT '0',
  `mode` varchar(11) CHARACTER SET utf8mb4 DEFAULT 'track',
  PRIMARY KEY (`id`),
  KEY `track_id` (`track_id`),
  KEY `comment_id` (`comment_id`),
  KEY `user_id` (`user_id`),
  KEY `time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ---------------------------------------------------------
--
-- Table structure for table : `reviews`
--
-- ---------------------------------------------------------

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `track_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `rate` int(11) unsigned NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `track_id` (`track_id`),
  KEY `user_id` (`user_id`),
  KEY `rate` (`rate`),
  KEY `time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ---------------------------------------------------------
--
-- Table structure for table : `searches`
--
-- ---------------------------------------------------------

CREATE TABLE `searches` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `keyword` varchar(250) NOT NULL DEFAULT '',
  `hits` int(11) unsigned NOT NULL DEFAULT '0',
  `created_at` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `searches`
--

INSERT INTO searches VALUES
("1","Bud","37","1592632675"),
("2","Tendeep","49","1592634063"),
("3","eturnul","59","1592713690"),
("4","Officially LadyJ","86","1592733785"),
("5","Officially LadY J","41","1592821828"),
("6","officially lady","59","1592845238"),
("7","rowan","41","1592878746"),
("8","Favourites","44","1592881077"),
("9","Right Now","41","1592932930"),
("10","Right Now Praizz Radio","9","1592932953"),
("11","So Arise","40","1592937683"),
("12","Recently Played","68","1592944829"),
("13","Xtreme praise","2","1592970244"),
("14","Alba","62","1592991001"),
("15","R and B","45","1593003466"),
("16","Rowan  Chapman","37","1593003544"),
("17","Rowan Chapman   LordILoveYou","37","1593003568"),
("18","chill","41","1593003806"),
("19","contemporary gospel","63","1593003827"),
("20","Another Day","72","1593003891"),
("21","Chh","73","1593003919"),
("22","Committed","43","1593003922"),
("23","Contemporary","50","1593003924"),
("24","Gospel","123","1593003948"),
("25","Gospel Hip Hop","44","1593003950"),
("26","Hip Hop","441","1593003958"),
("27","Hip Hp","478","1593003961"),
("28","Inspirational","47","1593003967"),
("29","My Playlists","50","1593003970"),
("30","r","53","1593004141"),
("31","radio","53","1593004162"),
("32","Andraye Speed","58","1593026087"),
("33","Chad Howard","5","1593031283"),
("34","Neva Ford","1","1593032602"),
("35","En Son Oynanan","46","1593033966"),
("36","Favoriler","46","1593033968"),
("37","Favoris","47","1593033969"),
("38","Favoriten","46","1593033971"),
("39","Favoritos","47","1593033973"),
("40","Joué récemment","118","1593034065"),
("41","Kürzlich gespielt","118","1593034067"),
("42","Meine Wiedergabelisten","38","1593034068"),
("43","Mes playlists","39","1593034070"),
("44","Mijn afspeellijsten","40","1593034072"),
("45","Mis playlists","44","1593034074"),
("46","Oynatma Listelerim","39","1593034090"),
("47","Recent gespeeld","40","1593034092"),
("48","Recientemente jugado","45","1593034094"),
("49","favorieten","46","1593034096"),
("50","Избранные","102","1593034098"),
("51","Мои плейлисты","81","1593034099"),
("52","Недавно играл","97","1593034101"),
("53","المفضلة","95","1593034103"),
("54","قوائم التشغيل الخاصة بي","126","1593034105"),
("55","لعبت مؤخرا","140","1593034107"),
("56","What You Gonna Do About Jesus","4","1593038740"),
("57","Highly Favoured","1","1593044145"),
("58","Pastor ricky","3","1593044173"),
("59","Pastor rickey","2","1593044187"),
("60","Since we repent","1","1593044203"),
("61","Roy","1","1593044303"),
("62","Gosopel","41","1593048381"),
("63","HoldOn  JazzGospel","35","1593048385"),
("64","Jazz  Praise","36","1593048399"),
("65","Praise","47","1593048407"),
("66","Testimonial","32","1593048436"),
("67","Rap","38","1593048516"),
("68","duo","34","1593048752"),
("69","female vocalist","33","1593048756"),
("70","group","41","1593048786"),
("71","male vocalist","41","1593048790"),
("72","praise and worship","44","1593048793"),
("73","traditional","32","1593048927"),
("74","traditional gospel","34","1593048936"),
("75","vocalist","36","1593048943"),
("76","worship","35","1593048952"),
("77","Chosen","1","1593063213"),
("78","You Are Holy by Lady Cassaundra Webb","30","1593075345"),
("79","lord be with","1","1593106504"),
("80","lord by my side","1","1593106517"),
("81","Alternative","33","1593108011"),
("82","Slow","32","1593111820"),
("83","Southern","42","1593111821"),
("84","Tesimonial","27","1593111823"),
("85","Holiday","35","1593112109"),
("86","Kara Nichole","7","1593113621"),
("87","louise smith","4","1593123774"),
("88","Christmas","16","1593132336"),
("89","Fast","18","1593132344"),
("90","Frozene Hayes","3","1593227280"),
("91","Lets Help One Another","4","1593230108"),
("92","Reflections of Dave Shirley","2","1593230144"),
("93","Hope","1","1593258690"),
("94","Bemeche","9","1593286365"),
("95","I&#039;mDetermined","4","1593343166"),
("96","Lead","3","1593392582"),
("97","Healing CD","8","1593393718"),
("98","jazzpel","2","1593399398"),
("99","gospel jazz","2","1593402806"),
("100","romantic god song","2","1593427128");

--
-- Dumping data for table `searches`
--

INSERT INTO searches VALUES
("101","Amanda Lynn","6","1593439322"),
("102","Gospel Inspiration","2","1593439360"),
("103","women&#039;s anthem","2","1593440862"),
("104","Quartet","1","1593462448"),
("105","toe-tapping","2","1593463582"),
("106","Lift Him Up","2","1593466422"),
("107","BishopJDMeansSr","2","1593467183"),
("108","old school","5","1593467759"),
("109","Dave Shirley","4","1593468694"),
("110","Church medley","1","1593470061"),
("111","daveshirleymusic","1","1593471181"),
("112","HealTheBrokenness","2","1593471752"),
("113","SundayMorningGospel","5","1593473238"),
("114","MiettaStancil-Farrar","5","1593474572"),
("115","Give Him the Glory","2","1593479494"),
("116","Tiffany","2","1593558716"),
("117","Urban Contemporary Gospel","8","1593580022"),
("118","Minister R L Taylor","1","1593580084"),
("119","sounds of imani","23","1593639506"),
("120","I&#039;llDoItForYou","1","1593707790"),
("121","Jr","4","1593707796"),
("122","LeaveItAlone","2","1593707802"),
("123","leader","1","1593707806"),
("124","rightnowpraizzradio","4","1593786570"),
("125","i live to worship","2","1593822645"),
("126","so&#039;lo","2","1593822682"),
("127","POST","3","1593863006"),
("128","AlvinDarling","2","1593937152"),
("129","BillMoss","3","1593937165"),
("130","ChangeMyHeart","3","1593937173"),
("131","Ensemble","1","1593937180"),
("132","I&#039;ll Praise You In Advance I&#039;ll Praise You in A Dance","3","1593937196"),
("133","Modern","2","1593937224"),
("134","P","2","1593937230"),
("135","Soulful","1","1593937242"),
("136","TheFantasticBibletones","1","1593937247"),
("137","Urban","1","1593937255"),
("138","healing song","1","1593937265"),
("139","solo","2","1593937277"),
("140","Mz tiffany","1","1594007065"),
("141","Mid Tempo","3","1594010479"),
("142","indie","4","1594029927"),
("143","Lisa Harris","18","1594030120"),
("144","I&#039;m Alive","3","1594030271"),
("145","Caribbean Gospel","4","1594030690"),
("146","Gospel cross-over","1","1594030694"),
("147","Antigua Barbuda","3","1594032585"),
("148","So Arise Radio","18","1594033529"),
("149","mantic God song","1","1594055986"),
("150","Matthew 5:41 Jesus will go the extra mile","5","1594079415"),
("151","you will be free indeed.","3","1594126120"),
("152","neo-soul","1","1594151798"),
("153","neva","2","1594269977"),
("154","So’lo","1","1594328214"),
("155","Darrell McFadden","9","1594374752"),
("156","minister john taylor jr","4","1594381774"),
("157","shelia moore piper","1","1594423929"),
("158","shanrae cheree price","1","1594426422"),
("159","shanrae price","2","1594426449"),
("160","John 8:36 So if the Son sets you free","2","1594431482"),
("161","sounds of imai","1","1594490279"),
("162","calandra gantt","1","1594515548"),
("163","ShondaEnglish","1","1594654469"),
("164","WhileTravelingThrough","1","1594655887"),
("165","ClapYourHands","1","1594683290"),
("166","AlfonzoUdell","1","1594686338"),
("167","Beams of Heaven","1","1594687359"),
("168","EvangelistLorrainePunchBaldwin","1","1594688346"),
("169","I&#039;llMakeIt","1","1594689351"),
("170","IHaveSoMuchToThankGodFor","1","1594690346"),
("171","TellTheWorld","1","1594710683");

-- ---------------------------------------------------------
--
-- Table structure for table : `sessions`
--
-- ---------------------------------------------------------

CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(100) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `platform` varchar(30) NOT NULL DEFAULT 'web',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`),
  KEY `user_id` (`user_id`),
  KEY `platform` (`platform`),
  KEY `time` (`time`)
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sessions`
--

INSERT INTO sessions VALUES
("4","bc206ed0b4bc3f56fe5d10e86becde42d37fb7d51592636350ec55a6e472e04f2af82ff703117fb0c3","3","web","1592636350"),
("16","7d7f12624fe4a103defc9a9fb1ce62dec02c4b5a15927143902321e83e9368745d948819bf2a7aac07","10","web","1592714390"),
("17","cef103a59d70d38b7a6fdc5e645164ce61f345751592714557dfaefd7c9c7d4b044bdb666c2a083c02","10","web","1592714557"),
("18","f79a05726696a83453020ee8857cdf58050151ee159271618296493b0fadf14f73e7525e192d1feb66","11","web","1592716182"),
("19","ceba0227dc0dadf23ec364c1044936b828c9389c15927173974fac66c623e6dd261e17e1c72ca49d96","13","web","1592717397"),
("20","3b023ea3ee68e017a1bbb5b3765ddffd977d7da41592717469dcf22301bd21606b0788d2bbaa67595a","13","web","1592717469"),
("23","85201ebe6dc079494076a54784511a1a76840ae11592755077ebded11de9e8f98095b3a5fa4f2220a1","3","web","1592755077"),
("24","f96529e9866e81360e6d6ec511ae7e0143bac1d7159283699864f890e50fc9a82d3ffc35382f5195ff","14","web","1592836998"),
("28","91a08f755e8bc9a3fe677fc528f98473e8b07cb11592875333b93cc92ace097787dda8c6b617461f9a","1","web","1592875333"),
("34","ef863c6ef2f0e1ffd357f50fd07fe5d130b322e915929319248334ee876f3add01c2fb9fb741e5ad52","17","web","1592931924"),
("35","32790154a439f4eef96ea1085cf7ebb47a382a3815929322000cc10e1606934650809ccc65cbb381fc","16","web","1592932200"),
("36","f070dc325b7f8d528eb23c3249392571986127691592937868383621fb4ef922c274151788803717d7","16","web","1592937868"),
("42","b760ab554ac40604ec8a13ad287de0b14ade946f1592966807f3c48cae507c8489a4d3c460ea01d21c","18","web","1592966807"),
("46","0999dc46d02618985c6aef15f6545a8fe4cfa6001593026043d8c24022461bfe808be545a21e71cfde","20","web","1593026043"),
("48","e3d06547c5f22b97cd41fff112df679f823aae7215930308188c32bc66ad1e06a15ce2508da44e1b2b","21","web","1593030818"),
("52","5374c621f43d11784f4a83a0c7d90cf014737f601593039412d607fd020ead75dc0ec6211a4c8f69a6","24","web","1593039412"),
("55","e79b0f0a57de319d849ee9e70d2c46046ba2703e15930568794325ef3b2b0ce7cb864f4ed7c0120098","24","web","1593056879"),
("56","e430859a821d039e59626bf1f784acffdd96325d1593059678b1863dc66c890eeb03c3bd49724dbe1d","25","web","1593059678"),
("57","8d7e1d2789f8503eb208b94c67cc0199ba6424a215930602453fb61225d1796ea79f26185d09d5d79d","25","web","1593060245"),
("61","e3f6f4e18b7df932b731f49acea1c3985f3e24ca1593112153b050f7b4c6ffc3046bc09b934dc0e324","27","web","1593112153"),
("62","b99b34c299c39ea4ed20c89720bb1c2506099ec41593112511302f087580de8a039b08d7e1b39c6f8a","27","web","1593112511"),
("63","8e4f9e298a106aa28da2fa8546e83299483669661593113905db4dfe8025bd44cadda2f13c237863cd","29","web","1593113905"),
("65","0041ced22a99bdca0b5a7692f50c699774b764701593114225441996947dcc939f82aa247ceaa59dda","27","web","1593114225"),
("66","8add240dd22943341e7e75e75ae781671e8cb3e11593114625c8d2c09494fd02632b199c898148208c","28","web","1593114625"),
("67","de778640906c4f4531068b1796c3c167c20a47581593123670b8ba238a20492fc7312f6ff66e1f6195","30","web","1593123670"),
("68","9622efa359363b356a2d0b91df0880d2720f18fb159313064330503818fb884afa063c74fffb823327","31","web","1593130643"),
("69","e9b3b62fe86b387f206b10f5452fec270c6968d8159313066866d25cc174e90ec8c300322a017ec75b","31","web","1593130668"),
("70","31c98982eeca529bb05e17402e656d82caf131a61593138869d22d7e194dd395714660ad3b0729dc57","21","web","1593138869"),
("71","a400890e1d6398fc5085f1af439f09ed2f8457fc1593147048e2915db131f9104db86f0fff3c9ae2f6","32","web","1593147048"),
("72","a2e3cd94d1e41bbf3704a5f19680e540105eaf0b15931518520cb55e53e315b8b6183e13755e3748bc","33","web","1593151852"),
("73","3f83a2ffe8495b2322a7f35e6d97509d6b28232f1593151880a836ca857dae31860ba4c47cf96f23a4","33","web","1593151880"),
("74","564d5326aff57b6063a9eaee50a88e6c0ae8595715931562309e84c9a0a8586a8526e195bfea2542e6","32","web","1593156230"),
("75","9bb91f40ac07017e41b10f65606330d713ce35591593182028b8c34767a51fe1caaf1c9dfaad3de5e2","32","web","1593182028"),
("76","658f96d771f375fe98e2b2b7bbeffd3ac340521515932256169f722275760bde69436b2fd1106f614d","34","web","1593225616"),
("78","e6e591bac92c206bc2f5390b5ef4486b9c739b491593226225682241bfcda1336e5461651fb9ad6c10","35","web","1593226225"),
("80","2eb8727206b8235529b24c233402ff488ca81730159326753201fc3a8c4c5a919bf1e204bc808ae21a","36","web","1593267532"),
("81","1d8bd57770fce8c0402726f16013ea1cff4de7d11593267532c7d1964673c25478bb7cabd74a96ea28","18","web","1593267532"),
("82","8b8cb0c72426f84d8d60a0408d8ad4ccac7545281593267532cddb0fd4443536eeabcc06ca7aa55d9c","18","web","1593267532"),
("83","2d2da2e52273fba63e56ac37e7a68d78197053a21593267532f569230d6c973760860ea8e1a3d1aa5a","18","web","1593267532"),
("84","6db5f9338f9fc74889816f1494968155e4622eeb1593294244050a6f072250f605971d0853c3227c21","37","web","1593294244"),
("85","76b7a729e45ab8a12e4ef259e10ddc5268e9fa21159339201688645a12897e35e8a5720a52cbb31b01","38","web","1593392016"),
("86","d2d18502d2c07e2cb0572b203cf7d183afe78c4b1593392058e56442e93a0ddb8734d86c33751d971e","38","web","1593392058"),
("90","0572603784190b91d4e6d0eb5ea4b7d5309acbaa1593576492000c56e238716f7bb26563f71beb0d07","25","web","1593576492"),
("92","405c20089f43ede7ea0fdc766d23582b31b85c0d1593580195a50f90182562f4a0d690a95f7bfe5b3c","41","web","1593580195"),
("93","b2dac0d5c4e9d4ff62907744587d5efa37185e5915935804571edc5dffaf47cd8ae367651ae2a7deab","41","web","1593580457"),
("94","c060a3edc7e22913d22d420c20b6afd94588a0ff1593583691b3864ef7792d9e8fa893ce83381b60b5","20","web","1593583691"),
("95","04dac9076fec23c0ca2fb041fd81a751568215d01593593817a66f16a484ff4790f9499fc12dc9d53b","3","web","1593593817"),
("96","28539c201ee87df9a2dea77c4c5246160493495015936314047e47035ad53f8afd1c1be6372a77122d","1","web","1593631404"),
("97","0d3954ac1dcc56d75672701cd71434d6e749d59c1593641695c9188c8bdc7279a944ca83fe8b5ac205","14","web","1593641695"),
("98","3120fcc0d5eb1fc864985dcab414d056bd7183e41593643734e1cf91b3522f9dc4e0657d539c9a5f24","3","web","1593643734"),
("99","f9474176ea03aff426fc50405ab8a058490294c815937259308a737a91b8db227577bc00d81f73fc89","1","web","1593725930"),
("100","e9241142e34544aab955a88e85d18e0a04d0f05215937948263dc6c40c807f4d2a54a2bf1e76200704","42","web","1593794826"),
("101","9daffe92098d63c360cc72ff6fcb5a7a35fffc361593822299e72ae2bd5b5efad8ec375f91a31e7e5a","43","web","1593822299"),
("103","673662405f0355ca141dd44aa0ecf37321c15bed1593825260e64c76b0020a6e41a80610322561a794","43","web","1593825260"),
("104","709b909962f728754582319d110d4db03c4a8e861593897806cca0167adbf436e0a6cd709d05514f38","44","web","1593897806"),
("106","30cfb97e6b670c3f3cd892b568a0798c620791f615939039338ca247edbaad2c3d2e7f8a9e9e9017e2","45","web","1593903933"),
("107","b66f7ed9e08c915758d4ddd440aadf2b4a5579aa1593966110c69fa3a0aff15c8c718db0831f225f5e","46","web","1593966110"),
("114","9549dadc40ba14dbf268939179ec259d493613e71594273219eebb24c0fbd70c1e5f938d5e7f6cf78b","43","web","1594273219"),
("116","b64fee47440e4afe2d9cee37810b2ad57070273b15942665551d166ea1d8a466ee11fc0709c46ecd52","3","web","1594266555"),
("118","46e44795500ade2a4fbf98c19d7e3132d3bbf5441594270012e4d00b73df0bb6e474e0244102abd2b2","3","web","1594270012"),
("119","4d45669d2c270b81dffb320ee3cbccf12c8ac1001594270015981fbb8d486b9f73e460144f6563c0af","3","web","1594270015"),
("124","5ed7b17a967087fc9041a629e894187eadc1ebb41594404947fd3431b430877838914a1f951c4fd841","3","web","1594404947"),
("125","610460f1ad0f787cc29d32cebb08aca2e30b323915946767189a3dedecde20564a602c8e13ae634fb9","17","web","1594676718"),
("126","e08fa6da291f1b6374e660482b1170b7bc5a92381594773824884312a3780e33a127635c8ae0f6131e","1","web","1594773824");

-- ---------------------------------------------------------
--
-- Table structure for table : `site_ads`
--
-- ---------------------------------------------------------

CREATE TABLE `site_ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `placement` varchar(50) NOT NULL DEFAULT '',
  `code` text,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `placement` (`placement`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `site_ads`
--

INSERT INTO site_ads VALUES
("1","header","&lt;p align=&quot;center&quot;&gt;\n&lt;a href=&quot;https://urbanpraises.com/site_pages/UGNRadioStations&quot;&gt;\n\n    &lt;img src=&quot;https://ugnbroadcasting.com/images/ss/ugnradio.jpg&quot; \n        style=&quot;height:auto;\n        max-width:100%;\n        border:none;\n        display:block;&quot; /&gt;&lt;/a&gt;&lt;/p&gt;\n","1"),
("2","footer","","0"),
("3","side_bar","","0");

-- ---------------------------------------------------------
--
-- Table structure for table : `song_price`
--
-- ---------------------------------------------------------

CREATE TABLE `song_price` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `price` decimal(20,2) unsigned NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `song_price`
--

INSERT INTO song_price VALUES
("1","1.99"),
("2","2.99"),
("3","4.99"),
("4","9.99"),
("5","19.99"),
("6","0.99");

-- ---------------------------------------------------------
--
-- Table structure for table : `songs`
--
-- ---------------------------------------------------------

CREATE TABLE `songs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `audio_id` varchar(20) NOT NULL DEFAULT '',
  `title` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `tags` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `thumbnail` varchar(150) NOT NULL DEFAULT 'default',
  `availability` int(11) NOT NULL DEFAULT '0',
  `age_restriction` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  `views` int(11) NOT NULL DEFAULT '0',
  `artist_id` int(11) NOT NULL DEFAULT '0',
  `album_id` int(11) NOT NULL DEFAULT '0',
  `price` float NOT NULL DEFAULT '0',
  `duration` varchar(12) NOT NULL,
  `demo_duration` varchar(10) NOT NULL DEFAULT '0:0',
  `audio_location` varchar(120) NOT NULL DEFAULT '',
  `demo_track` varchar(200) NOT NULL DEFAULT '',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `registered` varchar(12) NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `dark_wave` varchar(120) NOT NULL DEFAULT '',
  `light_wave` varchar(120) NOT NULL DEFAULT '',
  `shares` int(11) NOT NULL DEFAULT '0',
  `spotlight` int(11) NOT NULL DEFAULT '0',
  `ffmpeg` int(11) unsigned NOT NULL DEFAULT '1',
  `lyrics` text CHARACTER SET utf8mb4,
  `allow_downloads` int(11) unsigned NOT NULL DEFAULT '1',
  `display_embed` int(11) unsigned NOT NULL DEFAULT '1',
  `sort_order` int(11) unsigned DEFAULT '0',
  `src` varchar(50) CHARACTER SET utf8mb4 DEFAULT '',
  `itunes_token` varchar(150) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `itunes_affiliate_url` varchar(300) CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `title` (`title`),
  KEY `views` (`views`),
  KEY `artist_id` (`artist_id`),
  KEY `album_id` (`album_id`),
  KEY `price` (`price`),
  KEY `audio_id` (`audio_id`),
  KEY `registered` (`registered`),
  KEY `spotlight` (`spotlight`),
  KEY `category_id` (`category_id`),
  KEY `ffmpeg` (`ffmpeg`),
  KEY `age_restriction` (`age_restriction`),
  KEY `time` (`time`),
  KEY `itunes_token` (`itunes_token`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `tags` (`tags`)
) ENGINE=MyISAM AUTO_INCREMENT=122 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `songs`
--

INSERT INTO songs VALUES
("44","16","rvJfkEl7kuFi7yX","Because of Who You Are","Smooth Contemporary gospel w/ jazz - contemporary gospel duo/group","contemporary gospel,duo,group","upload/photos/2020/06/Ir6vxOOOCufn9VoCGhhh_23_701a6fcdb9585cf6518b6b5f607e8f84_image.jpg","0","0","1592939387","0","0","0","0","00:00","0:0","upload/audio/2020/06/s4BwVa1fMbuAXTFRJFdj_23_03ca77e03e7785ad3ebeab8a7719123b_audio.mp3","","4","2020/6","10949455","upload/waves/2020/06/11WxxEb39XjJ7pPh7uaCCVFujpVBfxZYbImd6CxB_dark.png","upload/waves/2020/06/nOpL5H5T7W16PJvV2BiR5PLfH5CmH3czw7ovVE6C_light.png","0","1","0","","0","0","0","","",""),
("45","3","cavNvguPALIVySg","01 Love Covers All 1.mp3","Gospel Record from 2018 The Voice Artists Deandre Nico","Inspirational,Gosopel,Contemporary","upload/photos/2020/06/7Nl8KNcCYtMbGGp4kIKQ_23_a979dafdf193f763507570c14c8b03a6_image.jpg","0","0","1592939479","0","0","0","0.99","5:35","0:0","upload/audio/2020/06/Lm4ROh9EzBKK1r8cQJCS_23_701a6fcdb9585cf6518b6b5f607e8f84_audio.mp3","","5","2020/6","7911788","upload/waves/2020/06/rjpqhsgNZwch5QgxmOdpGdPIAScEYLhtoFONt6Od_dark.png","upload/waves/2020/06/MGaijPKjP1SO9C8fnOgLunnBuGtdbKXe3sJu58qt_light.png","0","1","0","","0","1","0","","",""),
("2","3","1GMvUrrBb7zXAjh","IF I WAS YOUR BOYFRIEND-2.mp3","I started Making music at 17, I&#039;m 33 now. Tried this in a ministry at the age of 19 but My flesh wasn&#039;t ready. Joined the Army at 22 years old and Continued to do Secular music unti age of 27. While in the Army, I did shows for my music while stationed in korea and Afghanistan.  It was a hobby. A way to express my thoughts. I went through a medical discharge into full retirement in 2013, went to jail for 14months (2 moths after honorable discharge). Got out of jail, tried ministry again...failed again. Then last September started working for House of Hope, St Petersburg FL. My Pastor Tim Marshick,  encouraged me to write daily letters to God after getting diagnosed with multiple sclerosis in January of 2020. I started turning those letters into songs. Then my friend from church Johnny k a 66 y.o bass player...and a friend of mine from a Christian Band I follow from the area named James Cory. All 3 of us are military vets living in Pinellas County FL, loving Jesus, and spreading the Word!","Gospel Inspiration,Gospel Hip Hop","upload/photos/2020/06/piVgTu517OStuhoHfior_21_5a356faeb6d9fce0249ad3e3565ea0ff_image.jpg","0","0","1592701542","0","0","0","0","3:43","0:0","upload/audio/2020/06/e3DaKF2ZNLx8DNyiDgzX_21_100142b9485720664817683bf36ba3ab_audio.mp3","","4","2020/6","8917203","upload/waves/2020/06/4REbrNlLCjkea8TQ2bwTMyP2U5SJvGuXpJHJjHdC_dark.png","upload/waves/2020/06/LeOwXCsRP2FZVtfxZb6NEnny4sdS3emVjfI4IfxS_light.png","2","1","0","","0","0","0","","",""),
("3","3","1FqjmJhOs8AhEU6","NEVA FORD NATION HOLDING ON 2.0.mp3","Nevah Foed Nation","Contemporary,Gospel","upload/photos/2020/06/dZ5j95aNs5RqNcPuUssc_21_6f8d42ae3c08b03be0ebcb4eefaa23c0_image.jpg","0","0","1592708827","0","0","0","0","4:12","0:0","upload/audio/2020/06/RmN1Nj2D5of2NcH646Q2_21_7d79b2dff158437ea1af78fdda7718c4_audio.mp3","","4","2020/6","8710565","upload/waves/2020/06/2hWsjaUMIAaGpL1HC6kSpHTTlw4jBnE24NQZGeiW_dark.png","upload/waves/2020/06/VcvbY8HTKejOg6jJxO5xfoTNpBA8y4f2pVi8JfoK_light.png","2","1","0","","0","0","0","","",""),
("4","3","23Zgw8wavBix8J8","Deitrick Haddon - Open Door Season","Deitrick Vaughn Haddon (born May 17, 1973) is an American gospel singer, songwriter, music ... 4 Personal life; 5 External links; 6 References ... review of Revealed · Deitrick Haddon&#039;s Biography · Official site to the Church On the Moon album ...","Gospel,Contemporary","upload/photos/2020/06/1KdP5WBjyhTFny7pWNmP_21_1484261a3b7c47956ad2994a3248b9bf_image.jpg","0","0","1592709833","0","0","0","0","3:09","0:0","upload/audio/2020/06/QccYP2seO8Fv6ptOud4O_21_fb8238c0fe741fe787fbecb0a8775fb6_audio.mp3","","4","2020/6","7559934","upload/waves/2020/06/x75O8FAkUILOcClCcgFvyvQ8FjC7m7haRbU68hFG_dark.png","upload/waves/2020/06/DFUt3GeooammjA3VjjsIeHrTMCgRGRvvKYHRsQof_light.png","2","1","0","","1","1","0","","",""),
("5","3","2lY58AQlXmqbFBh","Joy","JOY","Gospel,Contemporary","upload/photos/2020/06/AmLjjgX3pavRI4CjJEYi_21_58b64b1e0ed0e0119d7b0aa639c3612c_image.jpg","0","0","1592710074","0","0","0","0","4:27","0:0","upload/audio/2020/06/9QHZ6mFztfF3yxHY3PdW_21_83019a53baed4b93ae2584f1423ad178_audio.mp3","","4","2020/6","6453224","upload/waves/2020/06/lkqPzvIWrj5a2eZ9YR7jis4s9fGcTVjbF29HzaZl_dark.png","upload/waves/2020/06/NiulhjYIAyOOUxyb7ikpRIUZUdI4o3NPyFt1Q4dE_light.png","0","1","0","","0","0","0","","",""),
("6","8","7XdBnIoFZ7fJHJv","Lord I&#039;m Committed.mp3","Lord I &#039;m Committed is a song of determination and praise.","Committed","upload/photos/2020/06/63fJP26L7uMVYthMlm5U_21_13603504e6730b745b8e37863ec5cc68_image.jpg","0","0","1592714416","0","0","0","0","4:12","0:0","upload/audio/2020/06/WXHXQkahUxAstRisXDOc_21_2f6115fabc6282c33a19b4ad2c294f22_audio.mp3","","8","2020/6","6059853","upload/waves/2020/06/WvfHNbSggtMxVyGK1dZhReU142vaSLkRF8ttBr5I_dark.png","upload/waves/2020/06/TtMzunrBXXsmt8aDhHvd6jENEoKPcajXkcqCLgwE_light.png","0","1","0","","1","1","0","","",""),
("7","8","uMlLTwJ2kkF58Lw","Another Day.mp3","Another Day is a song of worship with a jazzy approach.  It is a song you must hear..","Another Day","upload/photos/2020/06/63fJP26L7uMVYthMlm5U_21_13603504e6730b745b8e37863ec5cc68_image.jpg","0","0","1592714416","0","0","0","0","4:33","0:0","upload/audio/2020/06/woJ3Sb4hWanoseyYLDOj_21_77999e524858fcdf888302d80aac8aad_audio.mp3","","8","2020/6","6557008","upload/waves/2020/06/iAzBGGUJ3lJqfb5OxG8Qqb1yi9wpDq9SAD2ncmrX_dark.png","upload/waves/2020/06/6McSdItsIE83V3M4eesKhzVpA8PHVqOTvxprqVJh_light.png","0","1","0","","1","1","0","","",""),
("8","8","XNJvQ1COuHQyYZ5","Lord I Love YOu.mp3","This soon to be classic is soothing to the ears.  This worshiper is singing praises to God from whom all blessing flow.","Rowan Chapman   LordILoveYou","upload/photos/2020/06/63fJP26L7uMVYthMlm5U_21_13603504e6730b745b8e37863ec5cc68_image.jpg","0","0","1592714417","0","0","0","0","3:48","0:0","upload/audio/2020/06/JEc4Kti3lwZklp4ceaJi_21_9dad3adbc50a8a4b57e9e634a17f5ebc_audio.mp3","","8","2020/6","5463631","upload/waves/2020/06/7ZeP7YSbw9pMka7pxv25zVml3UtuvBVipGaYtAlK_dark.png","upload/waves/2020/06/Yjk6BX5k3uQ85vCJ7MZ1EpnRCFr7EOPLdpKX5TEP_light.png","0","1","0","","1","1","0","","",""),
("9","8","69wqzGKaWIeZ3jm","Hold On.mp3","The singer provides encouragement to those who are down and out.","Rowan  Chapman","upload/photos/2020/06/63fJP26L7uMVYthMlm5U_21_13603504e6730b745b8e37863ec5cc68_image.jpg","0","0","1592714417","0","0","0","0","5:27","0:0","upload/audio/2020/06/2Mlz82yXuPSTQORg1kPX_21_9095e41e92953143159ae738d885e960_audio.mp3","","8","2020/6","7844738","upload/waves/2020/06/vIgfwzECC2Cwh9H8CB8ywua6bTud2XH8sfBzQ8xu_dark.png","upload/waves/2020/06/ZuyflVFHIqh53pJuaY1UlxsihCAvGIS75NVrur1w_light.png","0","1","0","","1","1","0","","",""),
("48","16","a5zOaOfmLUoYHeL","For The Rest Of My Life","Gospel vocalist Frozene Hayes with the traditional sound of gospel.  Latest release.","traditional gospel,female vocalist,vocalist","upload/photos/2020/06/fUZYzHrqZA2z64resIsZ_23_a003f2c4c6d13caa4d5331ec95b2b3bd_image.jpg","0","0","1592940631","0","0","0","0","4:00","0:0","upload/audio/2020/06/nYkAkyLA5HLEEWEs3DVL_23_f4018aee95b438cccbf06cea20396e25_audio.mp3","","2","2020/6","5762681","upload/waves/2020/06/KdwMqyWKUwNNPszRVjq5TcZFQTVnVyelwxPBIHDD_dark.png","upload/waves/2020/06/enWrpYDulbf6ylVW6SlfaNWguvpgv8YfCd2z2CSN_light.png","1","1","0","","0","0","0","","",""),
("49","16","Efi3wq4UT7FPveq","Change My Heart","Praise and worship song by Bill Moss Jr. from the reknowned family of Bill Moss and the Celestials and the Clark Sisters.  He is the worship and fine arts director at Third New Hope Baptist Church in Detroit, MI","praise and worship,worship,male vocalist","upload/photos/2020/06/CUPnk4JtGlEonnjxuJ7e_23_674e16da1debed2d09d7482238a8688d_image.jpg","0","0","1592941004","0","0","0","0","4:31","0:0","upload/audio/2020/06/KpPl24AIWATORn1Zuu1n_23_759d82f1c7304637980fddac23237494_audio.mp3","","8","2020/6","10858811","upload/waves/2020/06/TJT4nCtlgBgDqVeLcIGh3SxKxMPMz3NnBuddiWNi_dark.png","upload/waves/2020/06/nWwz8kRjGcVpyFtOzZRb5GbWpl5nTzm5VmLSaISS_light.png","1","1","0","","0","0","0","","",""),
("60","20","vyw8cxGM1NECknn","Be Alright .mp3","Keep the faith, God is on your side, al you gotta do is pray","Hip Hop,Southern,Mid Tempo,Testimonial","upload/photos/2020/06/otzWmo4viI5ni9Q3nsHx_24_8abbf5c9c7ae02cdd33b19b400489d75_image.jpeg","0","0","1593027883","0","0","0","0","3:23","0:0","upload/audio/2020/06/232TxtiNhbKS65w3mdIg_24_e2d2ec2de4785c08d4908a0045bfe338_audio.mp3","","6","2020/6","4061957","upload/waves/2020/06/1IvqftegOUb9zOOZsvD4v9t6dUxl8CGgWBcvxYG4_dark.png","upload/waves/2020/06/L4rCTwLIy8zzlctLspYUFOtdeTo8sUa96RRtWoHJ_light.png","4","0","0","","1","1","0","","",""),
("58","20","wq6zYcKjrH9ipVD","GO HARD .mp3","Leaving the world behind and following GOD, Going hard for the Kingdom","Hip Hop,Fast,Inspirational","upload/photos/2020/06/v7Fg5fYSicv2Yqbz8ERk_24_2e8628730c782ed520bf55a185a5f711_image.jpeg","0","0","1593026357","0","0","0","0","4:31","0:0","upload/audio/2020/06/nfeO7OErvEcEDpQzpsKa_24_3e76f3e8d880b040eb2fb5c2795a6b02_audio.mp3","","6","2020/6","6504446","upload/waves/2020/06/sqv8kK19gJpm9vVtpEop131lP66jkS7bJXHU3rq2_dark.png","upload/waves/2020/06/ouufgaL6KRsTWLMwtfbdwqcgEWmrDMFtbrQscd74_light.png","0","0","0","","1","1","0","","",""),
("59","20","1QcNItx7RFliqjm","2 Live N Die .mp3","Living in this world or physical death, knowing I’m covered by the blood of Christ is enough for me. In HIS Grace.","Hip Hop,Slow,Tesimonial","upload/photos/2020/06/vOoocEupeRoYE4pTiaGY_24_0291d68d13d625dd8f4529929cab386f_image.jpeg","0","0","1593027076","0","0","0","0","4:04","0:0","upload/audio/2020/06/OAOeDxoegGI9Q9j5E6lq_24_97081dc1202451a932a24fad8bd0496c_audio.mp3","","6","2020/6","5866918","upload/waves/2020/06/ZCbJKsabZSYHs94Osev4OfwM29IeUoBjMvBnEbla_dark.png","upload/waves/2020/06/DzgAyCZNbNhyAPmRnJRvsbXv4rpHfNkfeRj6rrKy_light.png","0","0","0","","1","1","0","","",""),
("54","19","jdLYaz6SCs6zIRN","You Are Holy Radio","This song was birth in  my  prayer closet , praying for my family hope this blesses you as it has blessed me writing it and sharing it with you.","You Are Holy by Lady Cassaundra Webb","upload/photos/2020/06/5mugruNyI6Vjel243uov_24_aabdbf23826554b3d6fb183aa7f1e09c_image.jpg","0","0","1592968547","0","0","0","0","5:02","0:0","upload/audio/2020/06/YzVfMtq2GfnL2ltFprHR_24_089bec0c8f82093b98694acce6a7ce26_audio.mp3","","12","2020/6","4833444","upload/waves/2020/06/QzDj6SzaCuXQG7XlAPX7UqiutykRTozzn4Sm2N4P_dark.png","upload/waves/2020/06/O5e2rzyMj3CElXKrEdRlAaeeE7KeIy9kUTq98D7z_light.png","0","0","0","“You Are Holy”   <br>   <br>Lead: You are Holy, You are Holy, You Are Holy   <br>	You are God    <br>Repeat w/backgrounds   <br>   <br>Lead Verse: God requires A holy and Sanctified life, My god requires a Holy and sanctified life, my God requires a holy and sanctified life so I just live according to your word.   <br>   <br>Repeat w/backgrounds: You are Holy, You are Holy, You Are Holy   <br>	You are God (2x)   <br>   <br>Lead Verse: God requires A holy and Sanctified life, My god requires a Holy and sanctified life, my God requires a holy and sanctified life so I just live according to your word.   <br>   <br>Repeat w/backgrounds: You are Holy, You are Holy, You Are Holy   <br>	You are God     <br>   <br>Vamp:  You are Holy, You are Holy, You are God   <br>(continue backgrounds as lead adlibs)   <br>   <br>You Are God!!!!","1","1","0","","",""),
("27","14","Ql715H86CKKUzz8","Rain On Us","Officially LadyJ shares her worshipping experience with listeners  to the contemporary classic, Rain On US.","gospel,r&amp;b,radio,contemporary gospel,chill,officially lady","upload/photos/2020/06/KW1F14I2Xd3B7YyKiG4W_22_4cc9790daf06e75b35d2505d0e9614db_image.jpg","0","0","1592845227","0","0","0","0","5:31","0:0","upload/audio/2020/06/3ILg26NAwAb8BztDHEVg_22_43de94af823923adff8f0e8270a563be_audio.mp3","","12","2020/6","13324935","upload/waves/2020/06/q1sD8M5oiDmiijqa236Vyu6YN69Joiml9hOoY3W8_dark.png","upload/waves/2020/06/NVumOjNOjIBfu8jDtYocDFWudfbpVvuWYaB8p7Ht_light.png","0","1","0","","1","1","0","","",""),
("23","10","PKmUDT7CkeXb9Ua","805 South (Remix) [feat. May&#039;oun &amp; Anglelique Jonelle]","By Eturnul  [feat. May&#039;oun &amp; Anglelique Jonelle] <br>Music By: Jaz Williams (Hook Masters Studio)","Chh,Hip Hp/Rap,Inspirational","upload/photos/2020/06/VbqHZduvDqCsPxbe8VRs_21_f9ac249fb97c5fb657f7b037807da3cb_image.jpg","0","0","1592783505","0","0","0","0","4:10","0:0","upload/audio/2020/06/yj92y8fugtfwxaw6MShu_21_aba11fed51b44768df6d8a7139ad80f1_audio.mp3","","6","2020/6","8449942","upload/waves/2020/06/yOsf9mxWgmIWx9EKwqSA5jjcJJZSfxcBeOVHknfB_dark.png","upload/waves/2020/06/UNIq8rv9g5ru8PbTUKvzGJKYTFGOsB7TvF64rCzo_light.png","0","0","0","","0","1","0","","",""),
("24","10","EcdaquJdlKip8rA","My Cup","By Eturnul <br>Music By: D Salas","Chh,Hip Hop/Rap,Inspirational","upload/photos/2020/06/b2M6IOYuY37HeY2usTfI_21_285d773d17c461f5f2bbd96812bb2d29_image.jpg","0","0","1592783669","0","0","0","0","2:50","0:0","upload/audio/2020/06/eoSBQ4JJXe7fyFPhLhli_21_61e8b9e52b709aa514150f17f97cd241_audio.mp3","","6","2020/6","5632934","upload/waves/2020/06/OoHPURCzK1zyIeBJI4XuWEzoomPyyq2gPc2QKeRv_dark.png","upload/waves/2020/06/mcGn6OzpoFNoNXGZCRAgLkh9PZOjhoJtL8ypZ8CY_light.png","0","0","0","","0","1","0","","",""),
("25","10","yAMicqSFgOc14p7","Radical: Light in a Dark World (feat. Angelque Jonelle)","By Eturnul (feat. Angelque Jonelle) <br>Music By: David Salas","Chh,Inspirational,Hip Hop/Rap,R and B","upload/photos/2020/06/gQ5tbioctecp9ffIDlh9_22_6d2f4c7346cb187187d70b1e29a2d063_image.jpg","0","0","1592784306","0","0","0","0","3:04","0:0","upload/audio/2020/06/etI1unOWVr1XTBAbBTTo_21_9353ad2f6bb12cfb20386c1a7110d027_audio.mp3","","6","2020/6","7693228","upload/waves/2020/06/eOW9MAosVJWToTaxoy1ANJDjXBRMTAinHkDeka2d_dark.png","upload/waves/2020/06/SjhntLqM14SQ8jQREEuBw618ovEnuFXpfjncDr9Z_light.png","0","0","0","","0","1","0","","",""),
("26","10","APAdBp5DsHMzM8Q","So Much Better","By Eturnul","Chh,Inspirational,Hip Hop/Rap,R and B","upload/photos/2020/06/JMHF8IATfjd3skRa6xit_22_fe22f795bb4561e965cf94bf904b8a36_image.jpg","0","0","1592784719","0","0","0","0","4:30","0:0","upload/audio/2020/06/6MoUtiKE8KWfZBikzzQY_22_1b5e915cfa901a7ac73d8c9471a67496_audio.mp3","","6","2020/6","8708042","upload/waves/2020/06/vQZE7DVrsyqakfmcBsCCyzEJxlft3uTETow7CRFX_dark.png","upload/waves/2020/06/V7DpEoE1nXvawZG3F1aXwQiibdpK6xnFpuQxKdgJ_light.png","0","0","0","","0","1","0","","",""),
("43","16","ZiTYutf5GDydW54","GROW","Upbeat hip-hop gospel","Hip Hop,Rap","upload/photos/2020/06/Yiy8UxWHbUKjjrhlqNNp_23_a3538dcd067ae33af60e60a5d8213486_image.jpg","0","0","1592938838","0","0","0","0","3:45","0:0","upload/audio/2020/06/oq8CAopwLT83Ngbd87cc_23_c3f977244f515828dcfc6db6a365e775_audio.mp3","","6","2020/6","6770293","upload/waves/2020/06/ZNkQECLLbiaYhcYqml9eceXU6rjNCuZtbpUVmHaG_dark.png","upload/waves/2020/06/13fArcTMROoIxAjLk9NapgmylAh6ddFtCpFEmMy4_light.png","0","1","0","","0","0","0","","",""),
("46","16","VCQFbttRXYhNCAl","How Many Times","Contemporary gospel male vocalist","contemporary gospel,vocalist,male vocalist","upload/photos/2020/06/H2p3bZYuL5eYnx7634FN_23_4399e8a24f0a64b4d3c47c13f6533c36_image.jpg","0","0","1592940148","0","0","0","0","3:49","0:0","upload/audio/2020/06/DJQOdQ7H8CbrrJOKC2h1_23_21a8484aee41ec1f8d11d3faf1683c31_audio.mp3","","4","2020/6","3672940","upload/waves/2020/06/RPUho14WwcvmOT7PPvVOlROx5MsyMTNk1oaSYw1Z_dark.png","upload/waves/2020/06/OwUuIz5OiIX2RMdyyZKKq9nwiaxis75LXcSGXaUR_light.png","0","1","0","","0","0","0","","",""),
("47","16","TTDXkuQtfgQEOWt","Still Say Thank You","Traditional gospel male vocalist","traditional,male vocalist,vocalist","upload/photos/2020/06/pnBWHbumpD2FzRPx4RHO_23_d5be82ce74221006ec893f53c2ccb3b2_image.jpg","0","0","1592940338","0","0","0","0","5:13","0:0","upload/audio/2020/06/3fqEvlI3nF2rhEBxUlei_23_da1a6df998d737fb77270a1a4ce6331e_audio.mp3","","2","2020/6","12507409","upload/waves/2020/06/TpHMn6sVUNvwGVZd3R1Z2mDWkB6ydszYm7MefZrB_dark.png","upload/waves/2020/06/mLtJRf1uaCk33wOdVinxuYn7omG59Q6IkTw87Rhu_light.png","0","1","0","","0","0","0","","",""),
("22","10","TdaZY1FfLKtxOQD","Streamin (feat. Angelique Jonelle &amp; LiL J2)","By Eturnul (Feat. Angelique Jonelle &amp; Lil J2) <br>Music By : Puntin","Chh,Hip Hop/Rap","upload/photos/2020/06/H9Z7HUFe7MSB7jeBt7ub_21_2034909425dcccb362896d947771a510_image.jpg","0","0","1592783184","0","0","0","0","3:28","0:0","upload/audio/2020/06/oaRHn7TZUffEkHVZ637j_21_6aa6c9453297a03734f6bd13546a3b7b_audio.mp3","","6","2020/6","6673718","upload/waves/2020/06/7QDxaQcaHBEZ5OSlGiMVWiIzPnoGOjHFQw1QYQMJ_dark.png","upload/waves/2020/06/OrLGtGEhIljzhaYG4JKW3Y6VZh5S2fEqcNTEJ93r_light.png","0","0","0","Puntanious Percussion <br>Production <br>(Haaaaa) <br>Son what are you doing with my phone <br>Mom I&#039;m Streamin <br>Boy give me my phone, let me see <br> <br>Download it, put it on repeat mode <br>Hashtag me on the gram and let me know <br>(Ah Ah) who is this  <br>Vocals Milli lit <br>Stream on the Spotify, Sound cloud playlist <br>Facebook live, Instagram Gucci <br>Hit me on that youtube subscribe to me <br>Leave a little a comment, show a little love <br>Screenshot streamin with the volume turned up <br> <br>I do it for the King Yeah <br>I do it for the King Yeah <br>I do it for the King of kings <br>Creator of all things and we streamin <br>Streamin, streamin, streamin, streamin, streamin <br>Streamin, streamin streamin <br> <br>Juiced up, trail blaze <br>Your heart looking puffy, full of haterade, <br>Looking real thirsty, you need some Gatorade <br>Full of lemons, bars turned you into lemonade <br>Juicy,  you tried to play us goofy <br>Like I love Lucy and the devil trying to do me <br>What side you on <br>Its time to pick a bone <br>Christ on throne, bring it own, bring it on (Ah) <br> <br>Download it, put it on repeat mode <br>Hashtag me on the gram and let me know <br>(Ah Ah) who is this <br>Vocals Milli lit <br>Stream on the Spotify, Sound cloud playlist <br>Facebook live, Instagram Gucci <br>Hit me on that youtube subscribe to me <br>Leave a little a comment, show a little love <br>Screenshot streamin with the volume turned up <br> <br>I do it for the King Yeah <br>I do it for the King Yeah <br>I do it for the King of kings <br>Creator of all things and we streamin <br>Streamin, streamin, streamin, streamin, streamin <br>Streamin, streamin <br> <br>Who I be, where I&#039;m from <br>Vocals dumb, dumb <br>I got my gift from the maker above the sun <br>Yah Yah, oowee hot hot streamin <br>Yeah its hot lava beaming <br>I&#039;m all in, I got all to lose <br>I&#039;m keeping it real <br>I&#039;m knocking the rules <br>That&#039;s why I bring the fire when I step in the booth <br>Come with that hating evil I&#039;m gonna give you the duce <br> <br>Download it, put it on repeat mode <br>Hashtag me on the gram and let me know <br>(Ah Ah) who is this <br>Vocals Milli lit <br>Stream on the Spotify, Sound cloud playlist <br>Facebook live, live, Instagram Gucci <br>Hit me on that you tube subscribe to me <br>Leave a little a comment, show a little love <br>Screenshot streamin with the volume turned up <br> <br>I do it for the King Yeah <br>I do it for the King Yeah <br>I do it for the King of kings <br>Creator of all things and we streamin <br>Streamin, streamin, streamin, streamin, streamin <br>Streamin, streamin <br> <br>Trail blaze <br> <br>Heart looking puffy <br> <br>Streamin (Aye)","0","1","0","","",""),
("21","10","fi7C2ECEbRNHuqO","My Cup (G-Mix) [feat. Surve]","By Eturnul (Feat. Surve) <br>Music By : D Salas","Chh,Hip Hop/Rap,Inspirational","upload/photos/2020/06/skPjSMDUu4GxrJB2j1Aa_21_178fd2f6b91a17feff80396dd5782dfe_image.jpg","0","0","1592782769","0","0","0","0","3:54","0:0","upload/audio/2020/06/Av7CfN5bQteaO6SCIvtl_21_1021c0e2f74ad9e6367a3986d955cf44_audio.mp3","","6","2020/6","7868336","upload/waves/2020/06/W4FlAkpskg5rZZ7XsuaQZ25DPd2oIkPUQo9pFTnS_dark.png","upload/waves/2020/06/g49Tex98lcN6PmRubu1oDPTgoBuEEuS9MBsOclF1_light.png","0","0","0","","0","1","0","","",""),
("20","10","cc6JT1d3yTL5JCO","Blessings (feat. Fre&#039;yonni La&#039;mari)","By Eturnul <br>Feat (Fre&#039;yonni La&#039;mari) <br>Music By: TravilleFlow","Chh,Inspirational","upload/photos/2020/06/BOUW26QZdKK9NnZ3Uf1l_21_9a8447571dff05e2520a643f46c21abf_image.jpg","0","0","1592780788","0","0","0","0","3:05","0:0","upload/audio/2020/06/Z1sJcoZWZFJ6eZQjVr24_21_00d4e5f6cf47284a9cb54ba8bd55ddd8_audio.mp3","","6","2020/6","6243592","upload/waves/2020/06/vZwzk3aLStnCtJwfoUV8oa9ODZHgYGDkpdA4HLhL_dark.png","upload/waves/2020/06/rjW928s2JgJKOqagF1YHIY7tU1p6myxiSDarKsWE_light.png","0","1","0","Blessings  <br>Blessings on blessings <br>Mommy love you babies <br> <br>I Got my baby boy and my baby girl <br>And I love um like nothing else in the world <br>And I got God on my side <br>Tell my oldest I love um till the day I die <br>I love you babies <br> <br>I love your smile <br>I love your smile <br>You bring me joy, when your around <br>You will be great, I have no doubt <br>You are my blessings, blessings <br> <br>Rainy Days and sunny Days <br>Have fun, go outside and play <br>Hit the playground or the park <br>Or we can just kick back and do some art <br> <br>You are my blessings, blessing <br>You are my blessings, blessing <br> <br>You are my blessings, blessing <br>You are my blessings, blessing <br> <br>I know you didn’t understand though <br>You never seen the struggle <br>How much moms had to hustle <br>To give you what I could though <br> <br>I’m far from perfect <br>Everyone of you is worth it <br>You were my pride and my joy <br>Before u touched earth surface <br> <br>All your personalities different <br>Children are a blessings <br>If I don’t a agree with you <br>Life teaches us lessons <br> <br>We don’t always make the right decisions <br>None of you were a mistake <br>Seeing your smile is God mercy <br>Loving u is Gods grace <br> <br>I’m thankful for when I wake <br>My oldest we don’t talk everyday <br>I think about you on a daily <br>I pray always that you safe <br> <br>Some days are better than others <br>I&#039;ll always be your mother <br>I love you like no other <br>Sister and your brothers <br> <br>If you acting out, you get tough love <br>You know what it is <br>You know wrong from right <br>And mom taught you right as a kid <br> <br>I love you no matter what <br>Through downs and ups <br>You can be anything you want to be <br>The power is in us <br> <br>I got my baby boy and my baby girl <br>And I love um like nothing else in the world <br>And I got God on my side <br>Tell my oldest I love um till the day I die <br> <br>I love your smile <br>I love your smile <br>You bring me joy, when your around <br>You will be great, I have no doubt <br>You are my blessings <br> <br>Rainy days and sunny Days <br>Have fun, go outside and play <br>Hit the playground or the park <br>Or we can just kick back and do some art <br> <br>You are my blessings, blessing <br>You are my blessings, blessing <br> <br>You are my blessings, blessing <br>You are my blessings, blessing <br> <br>I love you babies <br>God watch over my children for me <br>Through the thick and the thin <br>All my family, everyone else children <br>We gotta pray for our youth <br>I love you babies","0","1","0","","",""),
("61","14","m5gvu5dvOTks39b","Who Is Officially LadyJ","Who Is Officially LadyJ?  Listen as Officially LadyJ bares her soul and speaks candidly about her music, rejection and motivation.","Urban Contemporary Gospel,CHH,Alternative","upload/photos/2020/06/8mysui3wpLvHgME3Mqyc_25_99c4b36211a005405cccae229eac5ed8_image.jpg","0","0","1593054608","0","0","2","0","2:06","0:0","upload/audio/2020/06/a8qhwbvzmHkMmQh2bnf5_25_18f00b987e134406558f9b580a9b53c8_audio.mp3","","12","2020/6","5114310","upload/waves/2020/06/YXsVLR3A1JBaeV762tErFDAeNvXVAubZEekX5QuF_dark.png","upload/waves/2020/06/F9daIYfr7Nk3aiiENprgH8X2OM8o6ZZDkRqGmLy7_light.png","0","1","0","","1","1","0","","",""),
("62","14","DUj1GWK6wo42KCD","Burdens Down","Burdens Down is a prayer set to music, asking “Jesus help me.”  A single from Officially LadyJ&#039;s sophomore album, 20/20 Vision, it&#039;s a fresh new approach to Gospel Music.  Singer/ songwriter, Officially LadyJ is intentional. She repeatedly encourages listeners to lay their burdens down.  There are many voices in the earth and the voice of a tortured mind is unbearable. In the song Burdens Down, listeners hear a compound list of burdens accompanied by a smooth baseline.  Next, a string section is introduce signifying the tension of a racing mind. This chill groove will give your listeners a different worship experience.  Experience the new sound of Gospel Soul.","Contemporary Gospel,CHH,Alternative","upload/photos/2020/06/zPiZfgO9Z9G1hnbloO42_25_8cda7815636d50dab4ede1910fca515d_image.jpg","0","0","1593054759","0","0","2","0","4:20","0:0","upload/audio/2020/06/pbOfuCVcmxSOGAyrFtVj_25_477a042c234cc61bd39c31690cad0c27_audio.mp3","","12","2020/6","10463239","","","0","1","0","I lay my burdens down <br>Lord I lay these burdens down <br>Jesus fix me <br>Burdens down <br>Burdens down <br>Burdens down <br>Burdens down <br>Burdens <br>Fall off me <br>Burdens down <br>Burdens down <br>Burdens down <br>Burdens down <br>Jesus set me free <br>My pain <br>Church hurts <br>My man <br>These bills <br>My past <br>My present <br>Future <br>Will <br>My rent <br>My kids <br>The child support <br>My case <br>The debt <br>A day in court <br>My scripts <br>My pain <br>My son <br>Police <br>The news <br>The deaths <br>Heartbreak <br>My needs <br>My cell <br>Electric <br>Gas <br>Work <br>School <br>My Weight <br>Health <br>Lies <br>My hidden truths <br>Lies lies lies lies lies lies lies lies <br>I lay my burdens down <br>I lay these burdens down <br>I lay my burdens down <br>I lay these burdens down <br>Jesus fix me <br>Jesus fix me <br>Burdens down <br>Down <br>Burdens down <br>Down <br>Burdens down <br>Down <br>Burdens down <br>Burdens <br>Fall off me <br>Fall off <br>Fall off me <br>Burdens down <br>Down <br>Burdens down <br>Down <br>Burdens down <br>Down <br>Burdens down <br>Jesus set me free <br>Set me <br>Set me free yeah <br>My thoughts <br>My life <br>My suicide <br>Depression <br>Voices in my mind <br>My sadness <br>Isolation <br>Tears <br>My weed <br>My drink <br>My pills <br>My tears <br>My fears <br>My family pressure <br>Secrets kept <br>My demons <br>Feeling <br>Friends <br>Regret <br>My fights <br>My loses <br>Records <br>Win <br>My lust <br>My fights <br>My cheat <br>My sin <br>I lay my burdens down <br>I lay my burdens down <br>I lay these burdens yeah down <br>Lord I lay my burdens down <br>Lord I lay these burdens down <br>I lay these burdens down <br>Jesus heal me <br>Heal me yeah yeah <br>Burdens down <br>Down <br>Burdens down <br>Down <br>Burdens down <br>Down <br>Burdens down <br>Burdens <br>Fall off me <br>Fall off <br>Fall off <br>Fall off me <br>Burdens down <br>Down <br>Burdens down <br>Down <br>Burdens down <br>Down <br>Burdens down <br>Jesus set me free <br>Set me <br>Free <br>Down down down down down <br>Set me free <br>Set me free <br>Free <br>Set me free <br>Free","1","1","0","","",""),
("63","14","bjrB2dtWkXsvd3s","Lay Me Down","Lay Me Down is slow drag romantic groove about marriage.  Its lyrics celebrates the convenant of traditional marriage.  What God has put together let no man put asunder.","Contemporary Gospel,Alternative,CHH","upload/photos/2020/06/blHDKsggG5bDDtjLQBBt_25_1021eb51637dc154cdc8b2a1b3091328_image.jpg","0","0","1593055273","0","0","2","0","4:38","0:0","upload/audio/2020/06/abnmULE4epMYvkipEgBE_25_069a1c28b9a62554a15fa2c81da99a5a_audio.mp3","","12","2020/6","11188075","","","0","1","0","","1","1","0","","",""),
("64","14","FilZeF7YXKHeMkf","Bae Let&#039;s Pray","Bae Let&#039;s Pray is a christian cover song for &#039;Can We Talk&#039;.  It address the taboo of life after divorce.","Contemporary Gospel,CHH,Alternative","upload/photos/2020/06/hEtEWbkkC3ft5Jop1saX_25_a85851238f9cb2c5a9a89193689f3250_image.jpg","0","0","1593055649","0","0","2","0","4:39","0:0","upload/audio/2020/06/ruMqSiyBG2cblzjZ2zGm_25_462a81162df2e2c66fc4eb0f76b6ffc8_audio.mp3","","12","2020/6","11230965","","","0","1","0","","1","1","0","","",""),
("65","14","orggkOdQYppIvNW","Rain On Us","Officially LadyJ shares her worship experience covering this Contemporary Gospel favorite.","Contemporary Gospel,Alternative","upload/photos/2020/06/pPB4earMQ2ej8apw2UZj_25_34a096595dc09f342a7919da037a8b83_image.jpg","0","0","1593055664","0","0","2","0","5:31","0:0","upload/audio/2020/06/4fxfpFEMRdUeMMFtrdWi_25_2f3e8a51c513bfbf398339e80e3fe98a_audio.mp3","","12","2020/6","13324935","","","0","1","0","","1","1","0","","",""),
("66","14","r9aPiwLf857JZPb","Hell’s Door","Hell&#039;s Door is a moderately tempo fusion of Funk and Rock. Its lyrics address the powers and influence of social media.","Contemporary Gospel,CHH,Alternative,Radio","upload/photos/2020/06/hqZ3YhW7EbtnzfywlJzM_25_78c65d8171e95197ff85aded603a3f91_image.jpg","0","0","1593056151","0","0","2","0","5:49","0:0","upload/audio/2020/06/Rvadg1O6AYeBT5UhYToE_25_6c87093da9d8d5a36283493c0b5ff8a8_audio.mp3","","12","2020/6","14036179","","","0","1","0","","1","1","0","","",""),
("67","14","wrysEHQx5ga4WRc","Count Your Blessings","Count Your Blessing is a fast paced, upbeat, encouraging song about moving on after falling into sin.","Contemporary Gospel,CHH,Alternative,Radio","upload/photos/2020/06/JrM8Eufki1wnmpjATGl1_25_13ddaeed03cbb6c85488b813c6123c18_image.jpg","0","0","1593056173","0","0","2","0","2:18","0:0","upload/audio/2020/06/p5xgo2WpWT6jTJTpkmtk_25_8c8da2c55aeaf0f45b37dc70b68f455b_audio.mp3","","12","2020/6","5592299","","","0","1","0","","1","1","0","","",""),
("68","14","jLVlqBgCYRlDq7J","New Year","New Year is a fast up beat praise song with a strong pop vibe.","Contemporary Gospel,CHH,Alternative","upload/photos/2020/06/XdkggNHF66hKqCRXRkmz_25_6d5f66ecc2c4c80d2126a869f95f0f14_image.jpg","0","0","1593056299","0","0","2","0","2:21","0:0","upload/audio/2020/06/tApn8PWGL1IxpZTXhNu3_25_d8565a0c597fbcdcced43fbed649fa8e_audio.mp3","","12","2020/6","5703146","","","0","1","0","","1","1","0","","",""),
("69","14","dZgTagpkPDjRodu","Jesus Save This Christmas","Jesus Save This Christmas is a moderately tempo holiday song.","Holiday,Christmas,Contemporary Gospel,Gospel","upload/photos/2020/06/M2HEcKQfxI1Sl52q6ngV_25_c095420907eb138335924d2ee55d789b_image.jpg","0","0","1593056445","0","0","2","0","3:42","0:0","upload/audio/2020/06/BsYdMdW8GOctQwhE45Oj_25_dae3380233b8bafe8d8c253311e01d83_audio.mp3","","12","2020/6","8945602","upload/waves/2020/06/7vp8ND9wrbQYUT4Xs6REDqOrEaOzw2EqQtnd2grA_dark.png","upload/waves/2020/06/hu4g2FF89RiX5DnkEjUXV5cDrPik97egMuCG66gU_light.png","0","1","0","","1","1","0","","",""),
("70","14","vNYuFWSnJfgSSqx","Romans Discipleship","Roman Discipleship is spoken word. Officially LadyJ shares the plan of Salvation over the background traffic noise. No music.","spoken word,alternative,Contemporary Gospel,CHH,Radio,Gospel","upload/photos/2020/06/DVq6K41TlnHjfhPcT8uH_25_ac49dac736edc17b5616282588dae730_image.jpg","0","0","1593056750","0","0","2","0","1:37","0:0","upload/audio/2020/06/3gdxDwVNqFxTErYSquo7_25_3dea2e4f58787fa1da118245127298d6_audio.mp3","","12","2020/6","3919837","","","0","1","0","","1","1","0","","",""),
("71","24","GLFWhkZQat3Jmk5","Priscilla Garner Atwater - Grateful - Grateful.mp3","Praising God for all he has done and is doing in your life. My prayer is that someone life will be touched by  the words GOD has given me to share in this song &quot;GRATEFUL&quot;","Gospel","upload/photos/2020/06/UN3LSIGBH53D9yL2t3fl_25_764587b2b0fd30b802033241bd24d397_image.jpg","0","0","1593059781","0","0","0","0","3:37","0:0","upload/audio/2020/06/wH6VUji6UtbQYlnYqZXe_25_12035aa3e456d6ce5d3fa232656d8710_audio.mp3","","5","2020/6","8690488","upload/waves/2020/06/fzbfJZPtXSCIaxbWyxy5pi7iT5yUx1IAxG1IO5it_dark.png","upload/waves/2020/06/2QNyP6kjTnCFSLU1sKRLmUuGP2SrA4AUWMA3CF9K_light.png","3","0","0","I am grateful, grateful, grateful.  I am grateful, grateful, grateful.  <br>  <br>You woke me early this morning, and I had my right mind.  <br>Lord I thank you for my life, health  and strength and I thank you for being so kind.  <br>  <br>Lord I thank you that my family doing fine. Lord I thank you for giving me peace of mind.  <br>Lord I&#039;ll praise you each and every day, these are the words that I say.  <br>  <br>I am grateful, grateful, grateful.  I am grateful, grateful, grateful.","1","1","0","","",""),
("72","14","blNQ344pBbWeK2u","Officially LadyJ - Burdens Down","Burdens Down is a prayer set to music, asking “Jesus help me.”  A single from Officially LadyJ&#039;s sophomore album, 20/20 Vision, it&#039;s a fresh new approach to Gospel Music.  Singer/ songwriter, Officially LadyJ is intentional. She repeatedly encourages listeners to lay their burdens down.  There are many voices in the earth and the voice of a tortured mind is unbearable. In the song Burdens Down, listeners hear a compound list of burdens accompanied by a smooth baseline.  Next, a string section is introduce signifying the tension of a racing mind. This chill groove will give your listeners a different worship experience.  Experience the new sound of Gospel Soul.","CHH,Contemporary Gospel","upload/photos/2020/06/baXKUZ2bjTcHAu3qktAb_25_219e55e004e4825b30bb01c149949cc8_image.jpg","0","0","1593098531","0","0","0","0","4:20","0:0","upload/audio/2020/06/c5DRsfCrUKdPfLsd6Djv_25_d60926591665f46bed1bf67fa0fb36bb_audio.mp3","","6","2020/6","10463239","upload/waves/2020/06/zbgMzK6xVApabpx9AdubeeQBugwqUY1rbvEXs3U3_dark.png","upload/waves/2020/06/49suKDGsTJEFkvGFRePjw8tR6qPcZr3lp9mv6Q15_light.png","0","1","0","I lay my burdens down <br>Lord I lay these burdens down <br>Jesus fix me <br>Burdens down <br>Burdens down <br>Burdens down <br>Burdens down <br>Burdens <br>Fall off me <br>Burdens down <br>Burdens down <br>Burdens down <br>Burdens down <br>Jesus set me free <br>My pain <br>Church hurts <br>My man <br>These bills <br>My past <br>My present <br>Future <br>Will <br>My rent <br>My kids <br>The child support <br>My case <br>The debt <br>A day in court <br>My scripts <br>My pain <br>My son <br>Police <br>The news <br>The deaths <br>Heartbreak <br>My needs <br>My cell <br>Electric <br>Gas <br>Work <br>School <br>My Weight <br>Health <br>Lies <br>My hidden truths <br>Lies lies lies lies lies lies lies lies <br>I lay my burdens down <br>I lay these burdens down <br>I lay my burdens down <br>I lay these burdens down <br>Jesus fix me <br>Jesus fix me <br>Burdens down <br>Down <br>Burdens down <br>Down <br>Burdens down <br>Down <br>Burdens down <br>Burdens <br>Fall off me <br>Fall off <br>Fall off me <br>Burdens down <br>Down <br>Burdens down <br>Down <br>Burdens down <br>Down <br>Burdens down <br>Jesus set me free <br>Set me <br>Set me free yeah <br>My thoughts <br>My life <br>My suicide <br>Depression <br>Voices in my mind <br>My sadness <br>Isolation <br>Tears <br>My weed <br>My drink <br>My pills <br>My tears <br>My fears <br>My family pressure <br>Secrets kept <br>My demons <br>Feeling <br>Friends <br>Regret <br>My fights <br>My loses <br>Records <br>Win <br>My lust <br>My fights <br>My cheat <br>My sin <br>I lay my burdens down <br>I lay my burdens down <br>I lay these burdens yeah down <br>Lord I lay my burdens down <br>Lord I lay these burdens down <br>I lay these burdens down <br>Jesus heal me <br>Heal me yeah yeah <br>Burdens down <br>Down <br>Burdens down <br>Down <br>Burdens down <br>Down <br>Burdens down <br>Burdens <br>Fall off me <br>Fall off <br>Fall off <br>Fall off me <br>Burdens down <br>Down <br>Burdens down <br>Down <br>Burdens down <br>Down <br>Burdens down <br>Jesus set me free <br>Set me <br>Free <br>Down down down down down <br>Set me free <br>Set me free <br>Free <br>Set me free <br>Free","1","1","0","","",""),
("75","31","4igNqTfvCwZQkcI","Call On Me (Radio Edit).mp3","Increasingly popular contemporary song from EPK The Call","contemporary inspirational singer songwriter","upload/photos/2020/06/pPrH41PVFc6llr4Up1YU_26_f01960cbb969b09f4514c0aabf4477a0_image.jpg","0","0","1593131010","0","0","0","0","4:22","0:0","upload/audio/2020/06/nwdX6RkxLNyFnCe5IVXn_26_e1cb6f924f01eb99a65a07a54c73c6c4_audio.mp3","","12","2020/6","11000488","upload/waves/2020/06/3PeMGMZaPLAZTjwQaeCcuLwkEFsvXWDdwaFRYGhA_dark.png","upload/waves/2020/06/RLHPGsmQhI84HUj5K1TE37bxMjtWuQRaIMUBZGTs_light.png","1","0","0","","1","1","0","","",""),
("76","30","I7bH4vZksFwG5eL","09-Confessions, Affirmations, and Prayers.mp3","Spoken by Mary J. Bryant <br>song undertone: In the Presence of God","prayers,confessions,music,mary j bryant","upload/photos/2020/06/nETjr6Bl9en4HHpDGEaW_25_c581152cd7131acec6f2e9aa700ff290_image.jpg","0","0","1593131540","0","0","3","0","14:06","0:0","upload/audio/2020/06/XCtSHmm6RDHNy28gsFhO_25_1b58462883b02c94ab8816ef2cfa5a5c_audio.mp3","","12","2020/6","33855130","","","0","0","0","","1","1","8","","",""),
("77","30","23euaJo9RExpNqm","08-Talithacumi Spoken Word.mp3","words by Markeeta Foster <br>spoken by Markeeta Foster","spoken word,poetry","upload/photos/2020/06/nETjr6Bl9en4HHpDGEaW_25_c581152cd7131acec6f2e9aa700ff290_image.jpg","0","0","1593131541","0","0","3","0","6:08","0:0","upload/audio/2020/06/zALyb68NMWr5ckbComAU_25_845e44d64cb53d493dad82c2ee5e733b_audio.mp3","","12","2020/6","14737388","","","0","0","0","","1","1","7","","",""),
("74","27","47J7UgiHL2bXXHZ","I Believe (Live)","I Believe is an up-tempo Contemporary Gospel song that reflects on the beauty of God&#039;s promises. If you are confident in God and believe by faith, He will redirect and change you! I want to help people through the message of this song, &quot;I Believe&quot; find that inner peace and know that &quot;It&#039;s Working For You!&quot; If you confess it with your mouth and believe in your heart, God can and will Exceed your Expectations! Believe!! <br> <br>Performed by Lead Vocalist: Betty Hawkins <br>Music Written and Produced by Joey Oscar (JOMOCO MUSIC) <br>Video Produced by Roland Pollard Productions  <br> <br>To connect with Betty Hawkins visit: <br>Website: [a]https%3A%2F%2Fwww.bettyhawkinsandunited.org[/a] <br>Instagram: [a]http%3A%2F%2Finstagram.com%2Fbettyhawkinsandunited[/a] <br>Facebook: [a]https%3A%2F%2Fwww.facebook.com%2Fbettyhawkinsandunited[/a] <br>Twitter: [a]https%3A%2F%2Ftwitter.com%2Fbhawkinsunited[/a]","Gospel,Contemporary Gospel,Praise and Worship,Inspirational","upload/photos/2020/06/TVXjLurzRmfIAxrinEBU_25_f1fe1d86e87d27a977e2baf06bef0cac_image.jpg","0","0","1593114464","0","0","0","0","4:39","0:0","upload/audio/2020/06/saUjDdhNlMJYlZ5Hlu3g_25_5e5cbf8dc3c372444c517df5e565fc02_audio.mp3","","4","2020/6","4465214","upload/waves/2020/06/wMAqPUAUrSjwVe4R4M35u6bKdQ6pSU9wwVkQHaa1_dark.png","upload/waves/2020/06/WcTUAQIe2YKOPnx6uhdA3TwvFMWXNFO6TJvTXhzm_light.png","2","0","0","","0","1","0","","",""),
("78","30","bHdH7TuYoWplPaU","05-Truth.mp3","trumpeter Linton Smith <br>words and arrangement Louise Smith <br>music by Fred Cockfield","healing cd,jazzpel,gospel jazz,group,leader","upload/photos/2020/06/nETjr6Bl9en4HHpDGEaW_25_c581152cd7131acec6f2e9aa700ff290_image.jpg","0","0","1593132207","0","0","3","0","5:12","0:0","upload/audio/2020/06/2L2WtqVR4oDZ6zEnYok2_25_c12b85980b3a5032a1bd0d13ad2997b8_audio.mp3","","12","2020/6","12461482","","","0","0","0","Truth <br>Composer Louise Smith <br>© 2007   <br> <br>Unison <br>He will lead you into all truth x2 <br> <br>He is the way-ay-ay-ayee <br>He is the way, the truth, and the life <br>repeat <br> <br>He is the Truth x2 (JESUS)  <br>He is the truth x2 <br> <br>Parts <br>He will lead you into all truth x2 <br> <br>He will lead you into all truth x2 <br>(JESUS) unison <br>He will lead you into all truth","1","1","4","","",""),
("79","30","bJilPbkTJOVYiHN","06-Talitha Cumi.mp3","written by Louise Smith <br>music arrangement Louise Smith <br>Musician: Fred Cockfield, Linton Smith (trumpet) <br>background singers  Seasoned","healing song,women&#039;s anthem,gospel jazz,jazzpel","upload/photos/2020/06/nETjr6Bl9en4HHpDGEaW_25_c581152cd7131acec6f2e9aa700ff290_image.jpg","0","0","1593132207","0","0","3","0","4:50","0:0","upload/audio/2020/06/PyZ8Et7kVSE7mi32ieNu_25_0965cd2e95bc765429e8fbcd54784802_audio.mp3","","12","2020/6","11614805","","","0","0","0","Talitha Cumi <br>Composed by Louise Smith ©2016 <br>  <br>Chorus <br>Talitha Cumi  x3 <br>Arise, Damsel arise <br> <br>Hook  <br>With your stripes, I am healed x2 <br> <br>I am healed, I am healed <br>With your stripes, I am healed <br>Repeat","1","1","5","","",""),
("80","30","GoqhHWPMnbCV4je","03-Citizen.mp3","Words and arrangement by Louise Smith <br>sung by louise Smith, Lakisha Whitaker, and Seasoned <br>Musician Fred Cockfield","gospel jazz,jazzpel,group,leaders","upload/photos/2020/06/nETjr6Bl9en4HHpDGEaW_25_c581152cd7131acec6f2e9aa700ff290_image.jpg","0","0","1593132207","0","0","3","0","4:29","0:0","upload/audio/2020/06/IWpyAAKGH3bYAqonDP5u_25_bed9317f675527aa56ee9d44a1f40880_audio.mp3","","12","2020/6","10745149","upload/waves/2020/06/ttagXE3sZZmqr4v7JDYmkKwD2UDif2lXNB662ZNw_dark.png","upload/waves/2020/06/h9VWBexHhZLluNFVN5Er4xXNa5EUne5BjJoxZDvj_light.png","0","0","0","Citizen <br>Composed by Louise Smith &amp; Lakisha Whitaker © 2016 <br> <br>Chorus <br>Citizen where the healing waters flow 2x <br>I am a citizen where the healing waters flow <br>Bah-bah, bah-bah <br> <br>2nd Chorus <br>Citizen x2 <br>I am a citizen were the healing water flow <br> <br>Verse 1 <br>I am a citizen where the healing water flow <br>And with His stripes I am healed and made whole <br>Jesus went to hell and took away the key <br>I am free from sickness and all manner of disease <br> <br>Verse 2 <br>Beautiful city lit with God’s Glory <br>The waters here are crystal clear <br>The leaves of the trees are for the healing of the nations <br>Come over here, it’s for all creation <br> <br>Bridge <br>Beautiful city lit with God’s glory <br>Resident of the blessed healing story <br>He will bottle up my tears and I will shoulder up my cross <br>Run to the healer that I need (repeat)","1","1","2","","",""),
("81","30","i887tKGHzglYWnL","02-Blessed Assurance.mp3","written by Louise Smith and  Travis Stuckey <br>music arrangement Louise Smith <br>Musician: Fred Cockfield <br>background singers  Seasoned","jazzpel,gospel jazz,group,lead,healing song","upload/photos/2020/06/nETjr6Bl9en4HHpDGEaW_25_c581152cd7131acec6f2e9aa700ff290_image.jpg","0","0","1593132207","0","0","3","0","4:11","0:0","upload/audio/2020/06/egqVbQxQxcFETbjzZ6Mr_25_5dbccb8aa313df7c4dbb51054d384933_audio.mp3","","12","2020/6","10045643","","","0","0","0","Blessed Assurance  <br>Composed by Louise Smith &amp; Travis Stuckey © 2016 <br> <br>Verse 1 <br>I have a blessed assurance  <br>that you are able  <br>to keep me from falling <br>you are my strength <br>when I get weak <br>you build me up <br>when I’m down, you turn my frown upside down <br>blessed assurance you are the God that healeth me. <br> <br>Verse 2 <br>I have a blessed assurance <br>That you’re my healer <br>You keep me from sickness and disease <br>when I’m in need you answer me <br>you give me peace <br>all my nights and days I’ll give you praise <br>blessed assurance you are the God that healeth me <br> <br>Bridge <br>In spite of all the wrong I have done <br>You still love me X2 <br>In spite of what I’ve done, you gave your only son <br>To prove your unfailing love <br> <br>Chant <br>Oh 6x (You forgave me) <br>Oh 7x (Lord you saved me) <br>Oh 6x (You love me, 2nd time, You raised me) <br>You’re the God that healeth me <br>Oh 6x (You are with me) <br>Oh 7x (You’re my savior) <br>Oh 6x (You are Rapha) <br>Youre the God that healeth me <br> <br>Tag <br>You’re the God that healeth me 2x (3rd time) retard on healeth","1","1","1","","",""),
("82","30","eHFyPvmgMalKYuY","07-Sweet Perfume.mp3","written by Louise Smith <br>music arrangement Louise Smith <br>Musician: Superior Edwards <br>background singers  Seasoned","jazzpel,gospel jazz,healing CD,lead,group","upload/photos/2020/06/nETjr6Bl9en4HHpDGEaW_25_c581152cd7131acec6f2e9aa700ff290_image.jpg","0","0","1593132207","0","0","3","0","4:05","0:0","upload/audio/2020/06/dMKBzoc1r13indWz537Y_25_02a1c7d3c0e07d453c543f4115f657af_audio.mp3","","12","2020/6","9819091","","","0","0","0","Sweet Perfume <br>Composed by Louise M Smith <br>© June 12, 2008 <br> <br>Doubt, worry and gloom will dissipate <br>Just enter into his gates <br>Lift up your hands and celebrate <br>Our God, our God <br> <br>For His mighty acts we appreciate <br>Our infirmities He’ll annihilate <br>Open your mouth and heart; let’s celebrate <br>Our God, our God <br> <br>Go past the gates <br>Run into the Holy of Holies <br>Come in the room 2xs <br>There’s sweet perfume. <br> <br>Chorus","1","1","6","","",""),
("83","30","JQzRP2ePEdwJcfh","01-Be Made Whole.mp3","written by Louise Smith  <br>music arrangement Louise Smith <br>Musician: Fred Cockfield <br>background singers  Seasoned","jazzpel,gospel jazz,Healing CD,group,Lead","upload/photos/2020/06/nETjr6Bl9en4HHpDGEaW_25_c581152cd7131acec6f2e9aa700ff290_image.jpg","0","0","1593132207","0","0","3","0","3:51","0:0","upload/audio/2020/06/6lVerP7OKjsXY9GVk8mk_25_be288881ccb7794844bd1f996c5d6f08_audio.mp3","","12","2020/6","9234451","upload/waves/2020/06/4faaFFUD1tBu5lDQC6qj5an4Th1ifPiSpOjZCbBr_dark.png","upload/waves/2020/06/TkcV3GIQAIQNcyfzJEsHTnjnbWuQdOHDICjj67iy_light.png","0","0","0","Be Made Whole <br>Composed by Louise Smith© 2016 <br> <br>Verse 1 <br>When I come into your presence  <br>with singing and thanksgiving <br>I bask as the Spirit takes control <br>Lord I know you’re able and are willing <br>To take my pain and bless my soul x2 <br> <br>Chorus <br>Be made whole x4 <br> <br>Verse 2 <br>The man at the pool, the man at the gate <br>The woman with the blood disease <br>They all were touched by the healing power of God <br>He’ll touch you touch too and make you free x2 <br> <br>Chorus <br>Be made whole x4 <br> <br>Bridge <br>This very hour <br>Jehovah Rapha <br>Has come to <br>Give you power <br>Over your mind, sickness, and disease <br>So rise up and be <br> <br>Chorus <br>Be made whole 4x","1","1","0","","",""),
("84","30","XmeZ9zToSGDuecI","04-The Healing Anointing.mp3","written by Louise Smith  <br>music arrangement Louise Smith <br>Musician: Fred Cockfield <br>background singers  Seasoned","jazzpel,gospel jazz,healing CD,group,lead","upload/photos/2020/06/nETjr6Bl9en4HHpDGEaW_25_c581152cd7131acec6f2e9aa700ff290_image.jpg","0","0","1593132208","0","0","3","0","3:50","0:0","upload/audio/2020/06/wLU2znKDy66IDAJuEccM_25_ed49d79ba69dded2929f5d4b81f36006_audio.mp3","","12","2020/6","9195831","","","0","0","0","The Healing Anointing <br>Composer Louise Smith  © April 17, 2012 <br>The healing anointing is on yah x2  <br>Receive, receive <br>Receive x3 <br>Healing for your 1) body <br>2) mind 3) spirit <br> <br>Vamp <br>The Lord our God is Here <br>Moving in the atmosphere <br>He’s healing, blessing, delivering <br>Yes He is x2","1","1","3","","",""),
("85","30","pKMvQKMCEuCO1k1","2 Magnify Yah.mp3","words and music Louise Smith <br>Musician : Superior Edwards <br>Praise and worship <br>Jazzpel <br>Gospel Jazz","praise and worship,jazzpel,gospel jazz,romantic God songs,group,lead","upload/photos/2020/06/aS6tdhA98v2XiNkD8aa4_26_3fd3c69a5cf9c59f97688097758e320a_image.jpg","0","0","1593137998","0","0","4","0","4:46","0:0","upload/audio/2020/06/vLNpUNnaCFYtlHlsnAZg_26_447e79fe51af61f7d509a150fd84d6af_audio.mp3","","12","2020/6","11439614","","","0","0","0","I’ll Magnify Yah <br>September 14, 2007 <br>Composer Louise M Smith <br> <br> <br>Verse  <br>Sometimes I feel I have no life at all <br>That’s when upon the Lord’s name I call <br>Knowing without you I’d surely fall <br>When my faith is weak and I can’t stand up tall <br> <br>Chorus  <br>I’ll magnify yah, I’ll glorify yah <br>I’ll give you my all, my all in all <br>I’ll magnify yah; Give you my all <br>That’s when you become great and everything else becomes small. <br>That’s when you become great! And everything else becomes small. <br>(Repeat Verse) <br> <br>Repeat chorus <br>(end) That’s when you become great <br>You become great <br>That’s when you become great <br>And everything else.... becomes small! <br> <br>Tag <br>Ill magnify Yah, I’ll glorify Yah <br>I give you my all and all x4 <br> <br>Te magnifico, Te glorifico <br>Todo Senoir <br> <br>Ill magnify Yah, I’ll glorify Yah <br>I give you my all and all x4","1","1","1","","",""),
("86","30","8VQzOFfYM8gvoie","3 Fragrance of a Worshipper.mp3","Romantic God Songs <br>Praise &amp; Worship <br>words and music Louise Smith <br>ASCAP <br>LOUJAM MUSIC PUBLISHING <br>Musician : Superior Edwards <br>Praise and worship <br>Jazzpel <br>Gospel Jazz","jazzpel,romantic god song,gospel jazz,praise &amp; worship","upload/photos/2020/06/aS6tdhA98v2XiNkD8aa4_26_3fd3c69a5cf9c59f97688097758e320a_image.jpg","0","0","1593138045","0","0","4","0","4:16","0:0","upload/audio/2020/06/o9b3zOdoBeLWHMnVKrpq_26_f50e6bd7079fbefd7e5827f45de70b07_audio.mp3","","12","2020/6","10224398","upload/waves/2020/07/nq2OeZ1q837ARzoICdhrT7xPlSVBn8Cp9WmkJoug_dark.png","upload/waves/2020/07/Pekb4V7flCww7gjMYMUvMNHZPkq2XhZfAR1FCXcH_light.png","0","0","0","The Fragrance of a Worshiper <br> <br>Verse I <br>Mary took the spikenard from her box of alabaster <br>It was for her master; O how she loved him so. <br>Her gratitude she owe for the times He rescued her;   <br>The least she had, the most she had, to Him she would bestow <br> <br>Hook  <br>I give you the fragrance of a worshiper, fragrance of a worshiper  <br>Fragrance pure and sweet; pour oil on your feet <br>I want you to know how much I love you so <br>I give the fragrance of a worshiper <br> <br>Verse II <br>On the feet of her Majesty the oil she did pour <br>Sweet smells of lavender went up, she bowed down and adored  <br>Mary worshipped the Lord from her heart, her tears, and with her hair <br>In the midst, she gave Him everything; she gave it all right there. <br> <br>Hook  <br> <br>Verse III <br>The disciples did not know, nor did they understand <br>The value of the worship or the cost of the Lamb <br>As Mary glorified her Lord, the Great I Am <br>The fragrance began to spread and this is what He said <br> <br>Hook  <br>I smell the fragrance of a worshiper, fragrance of a worshiper  <br>Fragrance pure and sweet; when you pour the oil on my feet <br>By this I know how much you love me so, when <br>I smell the fragrance of a worshiper.","1","1","2","","",""),
("87","30","gL3mpyvpP2RuaO6","Sweet Perfume","Romantic God Songs  <br>Praise &amp; Worship  <br>words and music Louise Smith  <br>ASCAP  <br>LOUJAM MUSIC PUBLISHING  <br>Musician : Superior Edwards  <br>Praise and worship  <br>Jazzpel  <br>Gospel Jazz","romantic god song,praise and worship,gospel jazz,jazzpel,neo-soul","upload/photos/2020/06/aS6tdhA98v2XiNkD8aa4_26_3fd3c69a5cf9c59f97688097758e320a_image.jpg","0","0","1593138045","0","0","4","0","4:07","0:0","upload/audio/2020/06/VHzOFVDKWSjwsPfeNxoV_26_598e400a1805d08c92bb68f04aa19805_audio.mp3","","12","2020/6","9879582","upload/waves/2020/06/hqtNtiZNz3XzBPCJqCZpdkTSaOcShulz1fK1itMR_dark.png","upload/waves/2020/06/z71OzudbCdX88O18PrsVeZnOnvqmUPjPaXLXe3aX_light.png","0","0","0","Sweet Perfume  <br>Composed by Louise M Smith  <br>© June 12, 2008  <br>  <br>Chorus  <br>Doubt, worry and gloom will dissipate  <br>Just enter into his gates  <br>Lift up your hands and celebrate  <br>Our God, our God  <br>  <br>For His mighty acts we appreciate  <br>Infirmities He’ll annihilate  <br>Open your mouth and heart let’s celebrate Our God, our God  <br>  <br>Go past the gates  <br>Run in the Holy of Holies  <br>Come in the room 2xs  <br>There’s sweet perfume.  <br>  <br>Tag   <br>Sweet x2, sweet perfume! X4  <br>With modulations up and down","1","1","0","","",""),
("88","30","3A3sfZLZlpkBIyR","Hidden","Romantic God Songs  <br>Praise &amp; Worship  <br>words and music Louise Smith  <br>ASCAP  <br>LOUJAM MUSIC PUBLISHING  <br>Musician : Superior Edwards  <br>Praise and worship  <br>Jazzpel  <br>Gospel Jazz","romantic god song,jazzpel,gospel jazz","upload/photos/2020/06/aS6tdhA98v2XiNkD8aa4_26_3fd3c69a5cf9c59f97688097758e320a_image.jpg","0","0","1593138045","0","0","4","0","3:03","0:0","upload/audio/2020/06/lGoU4d2Qu7CgV1azzXkU_26_8e1f5274053ec7cb80d471dad0c18cc5_audio.mp3","","12","2020/6","7338390","upload/waves/2020/06/1TWDVRd5Qb75tjj3loEO9nJXEY3cadkpl9Hzo9Ur_dark.png","upload/waves/2020/06/OP5o4s8jwYLGOd8FSbbMA1a9En23ZD18sFzGw5ah_light.png","0","0","0","Hidden  <br>Song of the Spirit  <br>Composed by Louise M Smith  <br>© January 2, 2010  <br>  <br>  <br>Hook  <br>I am hidden to be found in you  <br>Oh God x2  <br>  <br>For you, my soul cannot exist  <br>And for your love I can’t resist  <br>  <br>My sin you have erased  <br>I am warm in your embrace  <br>  <br>Your kisses I desire  <br>You’re my all consuming fire  <br>  <br>Tag  <br>Oh God (sop)  <br>Oh God (tenor)  <br>	Oh God (3pt)  <br>I am hidden to be found in you  <br>Oh God x2  <br>  <br>DRIVE  <br>Hidden x2  <br>I am hidden  <br>Hidden to be found in you oh God x2","1","1","4","","",""),
("89","30","rI1o7Fg5Huok7da","Tis So Sweet","Romantic God Songs  <br>Praise &amp; Worship  <br>words and music Louise Smith  <br>ASCAP  <br>LOUJAM MUSIC PUBLISHING  <br>Musician : Superior Edwards  <br>Praise and worship  <br>Jazzpel  <br>Gospel Jazz","praise &amp; worship,jazzpel,gospel jazz,neo-soul,ro,mantic God song","upload/photos/2020/06/aS6tdhA98v2XiNkD8aa4_26_3fd3c69a5cf9c59f97688097758e320a_image.jpg","0","0","1593138091","0","0","4","0","2:36","0:0","upload/audio/2020/06/f7Gg1Kel5jUfAXpYl2Ll_26_b08802015ee42eadae75265e507496d0_audio.mp3","","12","2020/6","6251696","upload/waves/2020/06/7ieucCriZYDTjrXYhpfQY4joYJDR8sj5aAueU3So_dark.png","upload/waves/2020/06/Qh4kYn2nXdYgZ2umPHIDuR3uHuKlvWarADIDdWFc_light.png","0","0","0","Tis so sweet to trust in Jesus,  <br>Just to take Him at His Word;  <br>Just to rest upon His promise,  <br>And to know, “Thus saith the Lord!”  <br>Refrain:  <br>Jesus, Jesus, how I trust Him!  <br>How I’ve proved Him o’er and o’er;  <br>Jesus, Jesus, precious Jesus!  <br>Oh, for grace to trust Him more!  <br>  <br>I’m so glad I learned to trust Thee,  <br>Savior, Jesus, Lord, and my Friend;  <br>And I know that You&#039;ll be with me,  <br>And You&#039;ll be with me till the end.  <br>  <br>Tag  <br>I&#039;ve learned to trust Him  <br>Oh for Grace to trust Him","1","1","3","","",""),
("90","30","hwwTbFpXIUv5sdI","Honeycomb","Romantic God Songs  <br>Praise &amp; Worship  <br>words and music Louise Smith  <br>ASCAP  <br>LOUJAM MUSIC PUBLISHING  <br>Musician : Superior Edwards  <br>Praise and worship  <br>Jazzpel  <br>Gospel Jazz","Praise &amp; Worship,romantic God Song,jazzpel,Gospel Jazz,Neo-soul,louise smith","upload/photos/2020/06/aS6tdhA98v2XiNkD8aa4_26_3fd3c69a5cf9c59f97688097758e320a_image.jpg","0","0","1593138091","0","0","4","0","3:35","0:0","upload/audio/2020/06/IOh3HUwmHYikdYgoJGGP_26_cf1b4a14dfc80da08df57056590427f2_audio.mp3","","12","2020/6","3444473","upload/waves/2020/06/Mycc4z3H8xIyeO532Ffd6oIM3KxnC1FBJspU1WcJ_dark.png","upload/waves/2020/06/eeWK87UlcnTaHAtBubAKvaHZrSRLomxUYZexVzME_light.png","0","0","0","Honeycomb  <br>Composed October 24, 2009  <br>  <br>Verse  <br>I’ve tasted of the vine of your sweet wine  <br>This is no cliché; I can truly say  <br>You’re sweet like rain from the morning’s dew  <br>The taste of your Word has made me brand new  <br>  <br>Chorus  <br>You&#039;re Sweeter than the honey and the honeycomb x3  <br>  <br>Verse   <br>Meditations of my Lord is always sweet  <br>Like the oil on my Jesus Feet  <br>You’re more precious than gold; more costly than pearls  <br>There’s no greater love in this world  <br>  <br>II Chorus  <br>That is sweeter than the honey and the honeycomb x3  <br>  <br>Bridge  <br>Sweet to me (echo) x4  <br>  <br>Sweeter than the honey   <br>And the honeycomb  <br>You’re sweeter than the honey and the honeycomb  <br>Sweeter than the honey  <br>And the honeycomb  <br>SWEETER THAN THE HONEY AND THE HONEYCOMB  <br>  <br>CHORUS III  <br>Sweeter than the honey and the honeycomb x4  <br>  <br>  <br>Sweeter 4x (echo)  <br>  <br>Sweeter than the honey   <br>And the honeycomb  <br>You’re sweeter than the honey and the honeycomb  <br>Sweeter than the honey  <br>And the honeycomb","1","1","5","","",""),
("91","30","6PYidmAnkp7rin6","Great &amp; Excellent is Your Name","P &amp; W","traditional,P &amp; W,ensemble","upload/photos/2020/06/ysFwLIRNVml3J966duW5_26_b1281302a55aad911d60b763d41049cf_image.jpg","0","0","1593141075","0","0","0","0","1:52","0:0","upload/audio/2020/06/cTvPive7pkCvrLIKdgcx_26_e7e4ce09532d00d1727cffae86efa597_audio.mp3","","8","2020/6","1792993","upload/waves/2020/06/nrM43m2RtAK3YFImU64SdLHPOsRoq9tardGPaSMn_dark.png","upload/waves/2020/06/C16gyVCSrSBpwzHAsdO4z1iMXuhvJFN98FTYh8sK_light.png","0","0","0","Great and Excellent is Your Name  <br>Song of the Spirit  <br>Composed by:  <br>Louise M Merritts-James  <br>© 2004  <br>LouJams Music Publishing  <br>  <br>Chorus  <br>Great and excellent is your name  <br>Great and excellent are your ways  <br>Great God, you remain the same  <br>Great and excellent is my praise  <br>  <br>Hook   <br>For you, for you, Jesus for you!","1","1","0","","",""),
("92","30","Lcqc4nDO2Jl5NeD","Private Time Medley","Praise &amp; Worship  <br>Traditional   <br>Words and music Louise Smith  <br>ASCAP  <br>LouJam Music Publishing  <br>Musicians: Calvin Ford, Louise Smith","traditional,P &amp; W,Ensemble","upload/photos/2020/06/ysFwLIRNVml3J966duW5_26_b1281302a55aad911d60b763d41049cf_image.jpg","0","0","1593141075","0","0","0","0","6:51","0:0","upload/audio/2020/06/WUlW2HkK1HkqTMVVttc9_26_edab53ea1a435ee1ee1648bef420e72b_audio.mp3","","12","2020/6","6582238","upload/waves/2020/06/fbpgFLLENggvM4De6sQAONY7pZ81J8m4uMR2O24j_dark.png","upload/waves/2020/06/F7c21HeGQTrULRP2csNmEjhddVEO6ddgZnogLzHA_light.png","0","0","0","In the presence of God there is fullness of joy; enter in, enter in  <br>  <br>So nice here in your presence to worship at your feet; to give you everything  <br>  <br>Feels right here in your presence to worship at your feet; to give you everything  <br>  <br>Melody borrowed from Clint Brown  <br>Every day that I live   <br>I’ll give all the glory  <br>I’ll give all the glory   <br>To you  <br>  <br>I live to praise you  <br>I live to praise you  <br>To worship at your feet  <br>To give you everything","1","1","0","","",""),
("93","30","yzwxAy4yXsIchDJ","Always","p &amp; w   <br>Praise &amp; Worship  <br>Jazzpel  <br>Words and music Louise Smith  <br>ASCAP  <br>LouJam Music Publishing  <br>Musicians: Calvin Ford, Louise Smith","gospel jazz,jazzpel,P &amp; W","upload/photos/2020/06/ysFwLIRNVml3J966duW5_26_b1281302a55aad911d60b763d41049cf_image.jpg","0","0","1593141076","0","0","0","0","4:05","0:0","upload/audio/2020/06/oBI6dt55eevpofi6MHNe_26_83f10a03787a7c9a590d2acb07bb0b3c_audio.mp3","","12","2020/6","3925531","upload/waves/2020/06/D19yYQ3r9XfITn69KYDOpWjmyfh4piwn5ykqKYnu_dark.png","upload/waves/2020/06/Qim1DIZlCnWSSdy2EGJWDPPbLSRuaA7ZsIjOvx1k_light.png","0","0","0","ALWAYS  <br>Composed by Louise Marie Merritts James  <br>© October 1993 Revised 2000  <br>  <br>VERSE  <br>When the devil says yes, I will say no  <br>Believe and trust in me. I&#039;m in control  <br>When the enemy says no, I will say yes;  <br>I am your peace from the stormy blast.  <br>When the principalities try to conquer every hold   <br>The only way to survive is stand and be bold.   <br>You&#039;ve got to know that Lo! I&#039;m with you always.  <br>  <br>CHORUS  <br>Always, always; I am with you always, always.  <br>(Repeat)  <br>  <br>VERSE  <br>Don&#039;t beg and plead with me ever to leave.   <br>I told you in my Word; won&#039;t you believe.  <br>When pain gets hard to bear till your heart grieves;  <br>Remember my promise take and receive.  <br>You&#039;ve got to know that lo! I am with you always.  <br>  <br>BRIDGE  <br>You will weather the storm,  <br>Free from every harm,  <br>You will have no alarm  <br>You&#039;re safe in my arms  <br>(Repeat)  <br>  <br>I&#039;m with you always, always, always","1","1","0","","",""),
("94","30","xtqcAVH3MVvDTTe","I Feel Much Better","Praise &amp; Worship  <br>Jazzpel  <br>Words and music Louise Smith  <br>ASCAP  <br>LouJam Music Publishing  <br>Musicians: Calvin Ford, Louise Smith","jazzpel,Gospel Jazz,P &amp; W","upload/photos/2020/06/ysFwLIRNVml3J966duW5_26_b1281302a55aad911d60b763d41049cf_image.jpg","0","0","1593141076","0","0","0","0","4:04","0:0","upload/audio/2020/06/EOqyObpeQhIcgZ9QTlrZ_26_30b4b3f66d02781a284b9e6d22d78e3f_audio.mp3","","12","2020/6","3909685","upload/waves/2020/06/5MpgWzCbxdPW5Vw26q8jSty355Uky8PaH9mdw6DW_dark.png","upload/waves/2020/06/QyjYmElUpS1DCIz6ZwkcBKitdXYSFsUGK6OVu7gX_light.png","0","0","0","I Feel Much Better  <br>Composed by Louise Marie Merritts James  <br>© August 1994  <br>  <br>Chorus  <br>I feel much better   <br>I&#039;d been loosed from the fetter  <br>I feel much better  <br>My body my mind my soul was loosed from fetter.  <br>  <br>The chains that had me bound  <br>I&#039;m walking on solid ground   <br>Yes, I feel better.  <br>  <br>Verse  <br>The word of God is like a two-edge sword  <br>(It cuts through the chains that binds me)  <br>Jesus set me free and now I am on board  <br>(And whom the Son sets free is free indeed)  <br>  <br>Bridge  <br>The word of God I believe in  <br>It&#039;s more than a feeling  <br>You ought to taste and see and  <br>You&#039;ll know he&#039;s real and  <br>I&#039;m gonna take a moment right now  <br>And I&#039;m gonna praise him.  <br>  <br>take a moment right now and praise Him","1","1","0","","",""),
("95","30","L55cqXuboV9heBR","Psalm of Thanks","song by Linda Norwood  <br>Jazzpel  <br>praise and worship  <br>gospel jazz","jazzpel,solo,gospel jazz","upload/photos/2020/06/ysFwLIRNVml3J966duW5_26_b1281302a55aad911d60b763d41049cf_image.jpg","0","0","1593141661","0","0","0","0","4:02","0:0","upload/audio/2020/06/h6C3igo8hMmSBKi4jo4t_26_fcd7cbe844f86756f90da35cfd242002_audio.mp3","","8","2020/6","3871321","","","0","0","0","words unavailable","1","1","0","","",""),
("96","30","Pp4PlzkhY2u7pty","Beyond The Clouds","traditional song  <br>words and music written by Louise Smith  <br>sung by Louise Smith and Seasoned  <br>ASCAP  <br>LOUJAM MUSIC PUBLISHING","gospel,traditional","upload/photos/2020/06/f9cfQa8az4r1ttk2fk2K_26_ddeb3391f1da3aebbddce50c3ae0ec98_image.jpg","0","0","1593146052","0","0","0","0","4:48","0:0","upload/audio/2020/06/uUOS8xF2SU8HGYBUS6rJ_26_9ac88ec4be21d12b3f13d4184c62e97e_audio.mp3","","12","2020/6","11526461","upload/waves/2020/06/LcXRhf6tTFWCl6i54nuToYcc7usPtVvzq52d94dM_dark.png","upload/waves/2020/06/lv1VsHwTThXIC7dltaM5AINQLV8ehZi645mcamxz_light.png","0","0","0","BEYOND THE CLOUDS  <br>A STARTING PLACE FOR ME  <br>© March 7, 2013  <br>LYRICS Louise Smith, Carlisle James, Vernel Edwards  <br>Music - Louise Smith  <br>  <br>Verse  <br>Take me beyond what I can imagine  <br>Give me the pulse to make it happen all the more  <br>I need the drive, the hunger and the passion  <br>To accomplish what I was created for  <br>Chorus  <br>Beyond the clouds is just a starting place for me  <br>Beyond the sky is what I can have  <br>There is no limit to the image of the Creator  <br>If I can see it, I can do it  <br>If I believe it, I can have it  <br>Beyond the clouds is just a starting place for me x2  <br>  <br>Verse  <br>Before I was framed  <br>God gave me greatness and ambition  <br>Then He told me to go and do not be afraid  <br>His love, His will is a must for this position  <br>And I’ll do it until I’m in the grave.  <br>Chorus  <br>Beyond the clouds is a starting place for me  <br>Beyond the sky is what I can have  <br>There is no limit to the image of the Creator  <br>If I can see it, I can do it  <br>If I believe it, I will have it  <br>Beyond the clouds is just a starting place for me x2  <br>  <br>Vamp  <br>I say I have all that’s destined for me","1","1","0","","",""),
("97","32","DHpNVKiz7nfvb4z","Rejoice - Durmond Glanton.mp3","Get ready to move your feet, clap your hands and leap for Joy!!! It&#039;s National Recording Artist Durmond Glanton and his NEW single &quot;Rejoice&quot;! Guaranteed to put a praise in your spirit, this single will encourage you to know that it doesn&#039;t matter what you&#039;re going through, or what your situations may look like, Psalms 34:1 declares that &quot;I will Bless the Lord at ALL times. His Praise shall continually be in my mouth.&quot; In these wicked and perilous times, it&#039;s hard for one to find God. However, one way to find God is by giving Him praise because that&#039;s where He dwells. Durmond Glanton&#039;s single &quot;Rejoice&quot; is that reminder. In his own words, Durmond says &quot;It makes no sense to praise Him in Victory while IN the battle then come out of the battle and sit still....IT&#039;S TIME TO REJOICE!!&quot;  <br>  <br>Durmond Glanton was born on July 14, 1977. Since the time of his conception, the enemy has had a hit out on Durmond&#039;s life. At the age of 4, he taught himself how to play the drums, and had it mastered by the age of 9. He served faithfully as the church drummer until the age of 22. It was then when Durmond locked himself in a room and taught himself how to play the piano. <br>  <br>In 2000, while attending a local revival in Rochester, NY, a prophet named Eric Cooper from Phoenix, Arizona spoke into Durmond&#039;s life and declared that &quot;After the Storm, God is going to put your name in lights for the world to see.&quot; <br>  <br>Since then Durmond has survived 3 strokes, beginning stage lung cancer, a minor heart attack, homelessness, drug addiction and suicidal battles. If you ask him today, &quot;Durmond, how were you able to go through all of that at such a young age and STILL survive?&quot; His answer is simple, &quot;When God is fighting for you, you can&#039;t lose.&quot; <br>  <br>Durmond is currently an accomplished Organist at the Bethlehem Temple Church of A New Beginning in the Atlanta, Georgia area. He is an accomplished songwriter, vocal coach, music teacher, music producer and recording artist. His main goal in Kingdom work and in life, is changing the world &quot;One song at a time&quot;. Durmond&#039;s plans are to give back to less fortunate churches in their music departments to help build strong bible based musicians for Ministry. He is a great example of overcoming all odds, and allowing others to see that ONLY through Jesus Christ, anything is possible.","durmondglanton,rejoice,dlgmuziq","upload/photos/2020/06/HpGLZZ4WYIBbTEN74TZ2_26_46f303a8a692d7048db8936d7a3f95c5_image.jpg","0","0","1593149548","0","0","0","0","3:41","0:0","upload/audio/2020/06/PkbZP8CyZUU2KoXbzeN4_26_18a1ceb9c4dd7b8b6109d21067179efa_audio.mp3","","8","2020/6","7693689","upload/waves/2020/06/j4bFo8Fr3viMNDRXJHMHXBCcQbVxVqTqyarbdRUZ_dark.png","upload/waves/2020/06/8axGpYvDqD8uT1MJbK6lKp99edoIRDFmZ6o7R1BZ_light.png","0","0","0","","1","1","0","","",""),
("98","32","4g59NKVAIeEJXzZ","While Traveling Through Shonda English feat Lemmie Battles.mp3","&quot;While Traveling Through&quot; is the title cut from Shonda&#039;s upcoming album Travelin&#039;. This churchy tune features two of Chicago greats, guest vocalist Lemmie Battles and renowned organist Richard Gibbs. David Walker and High Praise of Atlanta, GA render their voices in the background. Shonda honors the state of South Carolina by throwing in the highly recognized low country clap in the vamp.  <br>  <br>  <br>Writer: Shonda L. English  <br>ISRC: USCGJ 2044871 <br>UPC/EAN: 195079020972 <br>Publishing: BEEA Music/BMI","ShondaEnglish,WhileTravelingThrough","upload/photos/2020/06/2Ds85Yk9ce5cARU9HKDv_26_ea0835e956929389849d52d1b8e54aad_image.jpg","0","0","1593149813","0","0","0","0","4:02","0:0","upload/audio/2020/06/wPeeFFzD845SuRgk4gAg_26_ab1184b3ec5ca148f757816212e1b421_audio.mp3","","2","2020/6","3864831","upload/waves/2020/06/TiLm7Rm3VKSUn9uNQLgPhBErMexOb9an7O3E66iZ_dark.png","upload/waves/2020/06/3xvY585wMZVv5UnKup9ArxgmN9swDysAu4eGcj3B_light.png","0","0","0","","1","1","0","","",""),
("99","32","a8KUoUOULNpwjnk","Heal The  Brokenness Mietta Stancil Farrar feat Tehillah.mp3","Father, we are your children. We are crying out to you for direction and guidance on what to do... My brothers and sisters, I know you are hurting in places that you&#039;ve never experienced, your emotions are all over the place. What our eyes have seen, and our hearts have felt, is indescribable...but there&#039;s a greater love and comfort in our Savior.  There is absolutely nothing too hard for Him, and certainly no place of despair that He cannot reach. Healing is available to you...","MiettaStancil-Farrar,HealTheBrokenness","upload/photos/2020/06/vSNQAaiqF4iNA6Kz2JKo_26_e84b1f42569c0a3708f004f48b27e35e_image.jpg","0","0","1593150072","0","0","0","0","4:48","0:0","upload/audio/2020/06/AxmraAmddhZyIHrS2b4l_26_d8cf45325bccf7c5f143996f1a41d00b_audio.mp3","","5","2020/6","11531032","upload/waves/2020/06/lXKrddi1uI5k3f9ekGhVgMCPQlqB9ttIrUfZNtIR_dark.png","upload/waves/2020/06/QULmENS9plUqw82lpMvp3BeYR2a9aT1dT9e4wZNa_light.png","1","0","0","Lyrics: Heal the hurt restore the joy so much pain and sorrow Search for answers, unanswered questions only you can make us whole Heal us Lord, make us whole heal the broken, restore our soul <br>  <br>Chorus:  Heal the brokenness in this place Lord we need You to bring peace in this place pour out your love to fill the empty space Oh Lord heal the brokenness in this place <br>  <br>Bridge: Lift the hung down head raise and strengthen us get the glory through our lives As we look to you shine your light on us Lord we need you as our guide <br>  <br>Heal the brokenness in this place Lord we need you to bring peace in this place pour out your love to fill the empty space Oh Lord heal the brokenness in this place <br> <br>VAMP: You can heal, you can heal, you can heal, you can heal, you can heal,  you can heal, Oh Lord heal the brokenness in this place <br>  <br>Written &amp; Arranged by: Yoland Kelley, Mietta Stancil Farrar &amp; Avery Moses","1","1","0","","",""),
("100","32","i3opjq4ajrScBZe","Arthur Jae God Will Make It Better","&quot;God Will Make It Better&quot; comes from the heart of Arthur Jae with the faith, confidence and trust in God. That he is ever present to help in the time of trouble. Today we find many blessings as well as challenges in this life. We all need a word of encouragement that holds truth and hope to get us through. No matter how great or small God can get us through it all. He did it before he will do it again. Be of good cheer God is standing near. He will make it better for you and I.   <br>Artist: Arthur Jae   <br>Song Title:  &quot;God Will Make It Better&quot;   <br>Label: A.S.A.P.H. Enterprises   <br>Writers: Arthur Johnson  <br>  Publishing: A.S.A.P.H. Enterprise (Independent) BMI   <br>ISRC Code: USDY 4199233  <br>  UPC Code:   1974171230301","ArthurJae,GodWillMakeItBetter","upload/photos/2020/06/uKtzcI1TxogowOB7N4xy_26_6dab8cae6032a8a2c7786c9fffeb962d_image.jpg","0","0","1593150280","0","0","0","0","3:23","0:0","upload/audio/2020/06/NwnW3QXUOKkCbbc27Qfr_26_b0a773d2f8d0020b7bbcf6284cb8388a_audio.mp3","","5","2020/6","8127353","upload/waves/2020/06/wxvRZz7x3ty5XtAISVudAOag8RuwMVKgspIrUFXv_dark.png","upload/waves/2020/06/IUqb6hsYM3t8yopxEVqPaRPnksrwaIzxc8jtajRI_light.png","0","0","0","","1","1","0","","",""),
("101","32","ycGt4GCvLG7D12U","Alfonzo Udell &amp; Chosen Chorale  I&#039;ll Make It","&quot;I&#039;ll Make It&quot; is composed by Brooklyn native Alfonzo Udell and is an up-tempo blend of traditional &amp; contemporary Gospel music that will surely have the listener dancing on their feet! This song was written, to encourage people in these perilous and trialsome times, that determination is a must to make it. In writing this, Alfonzo said he actually felt the pain of the world and wanted to encourage them through his gift of music!  Fasten your seatbelts! #Enjoy!  <br>Single Title: &quot;I&#039;ll Make It&quot;  <br>Artist: Alfonzo Udell  <br>Writer/Composer: Alfonzo Udell  <br>Publisher: Go Fonz Music/BMI  <br>ISRC: USCGJ 1889675         <br>UPC: 1929 1445 0771","AlfonzoUdell,I&#039;llMakeIt,AlfonzoUdell&amp;ChosenChorale","upload/photos/2020/06/PYkkTtEZNntiBfMPMqDe_26_f18750691789c43e7717ba11fee26f22_image.jpg","0","0","1593150782","0","0","0","0","4:30","0:0","upload/audio/2020/06/SwJMF1ohVK1qiCen2ThZ_26_75024bed0f3816991aa5baea98a7f03c_audio.mp3","","2","2020/6","8632162","upload/waves/2020/06/iG4KzyE8FtMuR4cdUx6oxd7KBJxvcS6grHKxkmhD_dark.png","upload/waves/2020/06/Q5rxwHLNKdUa7MoAGUtclUzcLs8lRBLp9YMEqY2U_light.png","0","0","0","","1","1","0","","",""),
("102","32","aHDIZqcdTSt64EI","Alvin Darling - Leave It Alone","“Leave It Alone” Alvin Darling From the Emtro Gospel CD Leave It Alone (release date: September 20, 2019) By Bob Marovich From singer, songwriter, pastor, and Stellar and Dove Award nominee Alvin Darling comes “Leave It Alone,” a quartet-flavored roof-raiser. The verses string together familiar church aphorisms while the chorus sums it up: “God can fix it better than you can.” An agreeable lead vocal with ample support from background vocalists and musicians evoke Darling’s zesty 2003 album, Waiting Right Here. His foundation in quartet and Pentecostal music is evident throughout the single, which is short and simple but effective.  <br>  <br>Artist: Alvin Darling  <br>Digital Album Title: Leave It Alone  <br>UPC#: 801193154965  <br>Label: Emtro Gospel  <br>Alvin Darling (BMI CAE/IPI#: 72453778)  <br>ISRC: USEV91900012","AlvinDarling,LeaveItAlone","upload/photos/2020/06/sr3f6HCHTnBqIDCAA8dF_26_080947082e57acd6fddde65c4ab81827_image.jpg","0","0","1593151137","0","0","0","0","2:39","0:0","upload/audio/2020/06/kYUAUUXHgb66N91bG5CQ_26_1719e7d78af55462846b6c65e17c4fc0_audio.mp3","","2","2020/6","6376016","upload/waves/2020/06/ncc8c7bgfEf2IuM5eYIIpMASVcqLRIf446QWhS1r_dark.png","upload/waves/2020/06/s437K3817riWAt97qLZk5ntQ8nVcxy8IdSffetcu_light.png","0","0","0","","1","1","0","","",""),
("103","32","bCSH6RzpZSsdRoN","Bemeche   I&#039;m Determined","First, I would like to thank God for His revelation of his Word and spirit in me, and of his words in the single release entitled, &quot;I&#039;m Determined&quot;. It speaks to the situation in your life be it Spiritual or Natural, hearing the voice and call of God and purposing in your heart, mind and soul to run... your race of life with determination. Fighting the good fight of faith all the way. &quot;The race is not given to the swift or strong but to those who endure until the end.&quot;  <br>Single Song Title: I&#039;m Determined  <br>Label: Bemeche Gospel Music   <br>Writer: Roger Culberson  <br>Publisher: Roy Decoy/BMI  <br>ISRC Code:  USCGJ 1875031  <br>Barcode: 192914131625","Bemeche,I&#039;mDetermined","upload/photos/2020/06/gmmEeXsoaowdm1JGn5px_26_d6dcf4352720bab58799e2ae4c457a90_image.jpg","0","0","1593151458","0","0","0","0","3:37","0:0","upload/audio/2020/06/4dbTHRkGhFHgVnPttt1F_26_c7c5f94fa409148b3dc9d41f18e3ea7f_audio.mp3","","2","2020/6","8669504","upload/waves/2020/07/UohjBA5YRLVT9BL8xQzN2qcl8Rtmtwpz2zdNdB6M_dark.png","upload/waves/2020/07/mYCL7jN1rnzsvOeIzDgz9vY8OCJ47mfRSABhOAop_light.png","0","0","0","","1","1","0","","",""),
("104","32","JyQBJEApvOawENM","Bill Moss Jr. Change My Heart","With yet another dynamic contribution from his sophomore album &quot;Songbook of Praise and Worship-Live w/Third New Hope&quot;, esteemed Gospel Psalmist, Bill Moss, Jr., ushers the Worshiper into an introspective space of prayer for total transformation and healing of Sin, that is a truly relatable place for many Believers. With this latest chart-rising single, &quot;Change My Heart&quot;, is a follow-up to his recent Nielsen/BDS Charted songs to include fan favorites: &quot;Celebrate Our King&quot;,  &quot;Your Will&quot; (featuring duet with brother- J Moss), and &quot;I&#039;m Changed&quot;. Bill fervently cries out to the Lord to perform a work of correction, straight from the heart. The fusion of Bill&#039;s signature musical arrangements, along with powerful lyrics like, &quot;...God&#039;s Well of love will never run dry...all you have to simply say is Lord...change...my heart&quot;, makes this new single a simple, yet impactful plea to be stored in the heart of every Believer who feels like they have lost their way along the journey, and may have fallen too deep into Sin....that God&#039;s Love, Mercy, &amp; Grace is still always there waiting with open arms for restoration!  Available at ALL Digital Outlets.  <br>  <br>Artist: Bill Moss, Jr.  <br>Single: &quot;Change My Heart&quot;  <br>ISRC Code: USVS 31553894  <br>UPC Code: 688981149729  <br>Label: Salathiel Records  <br>Publishing: Dunns River/BMI  <br>Words &amp; Music By: Bill Moss, Jr. &amp; Glyne Griffth  <br>CD Title: &quot;Songbook of Praise &amp; Worship&quot; LIVE with Third New Hope","BillMoss,Jr,ChangeMyHeart","upload/photos/2020/06/Wz7aGozKNLjT9LRcBfmZ_26_a9a30e05ff782b768237fa55f4105b5e_image.jpg","0","0","1593152242","0","0","0","0","4:31","0:0","upload/audio/2020/06/XjM5q7R6CrYGTryOJxJ9_26_2f6ca7fa6def55895cb1ba2b4fd40378_audio.mp3","","8","2020/6","10858811","upload/waves/2020/06/2gGU5yONNOd7qHyx6neMVwWT85ar6LR3TN7Jh6EC_dark.png","upload/waves/2020/06/XCcRbJ9bUaCcfx4yyKzD1n4oDAKdBCe1VbgbLdST_light.png","0","0","0","","1","1","0","","",""),
("105","32","beQotK91QNpOwAI","The Fantastic Bibletones I&#039;ll Do It For You","The Fantastic Bibletones have released their debut single entitled, &quot;I&#039;ll Do It For You.&quot; The song describes how hard  it is to walk the path that God wants us to. We make a lot of mistakes until one day we finally get it right.  We trust God to guide us on this path. So whatever he wants us to do we will do it.  <br>  <br>   <br>  <br>   <br>  <br>Artist:  The Fantastic Bibletones  <br>Song: &quot;I&#039;ll Do It For You&quot;  <br>Written by: Stephen Butler  <br>  <br>Publishing: Steven Butler Music/ASCAP   <br>ISRC Code: QM9AA1899745  <br>  <br>UPC Code: 888295160704","TheFantasticBibletones,I&#039;llDoItForYou","upload/photos/2020/06/PJsLuhzsqjREBjS7v6cL_26_f52a8dc9567b52cd4ce9ab1eb716fe2e_image.jpg","0","0","1593152460","0","0","0","0","4:00","0:0","upload/audio/2020/06/335DOmfvhtsgw6VyU1G3_26_8e0cfcc9ef757a06e6baf0057b480de1_audio.mp3","","7","2020/6","3838536","upload/waves/2020/07/G8P2t8Z5zA1GO1nyohdgdHO42fJx5NWdCsQE5pG4_dark.png","upload/waves/2020/07/Nl7UILkt5GHyTUvY5GBJctz3fiXBb3D9IzSIUxDK_light.png","0","0","0","","1","1","0","","",""),
("106","32","gdudJFxLCCSHLwa","Bishop J.D. Means, Sr.  Tell The World","The single release of Bishop J.D. Means, Sr. entitled, &quot;Tell The World&quot; is a classic song that is now considered the Resurrection Anthem, is one of the greatest compositions that has ever been produced. It is a traditional, churchy and worship song for the entire gospel music genre.   <br>   <br>Taken from &quot;A Tribute To Sunday Morning Gospel&quot;, it demonstrates the anointing that GOD has placed on Bishop Means and through the musical gifts and talents of this Anointed Pastor, Evangelist, Composer, Musician, and Psalmist. This project is destined to encourage and electrify you spiritually through songs of Hope, Assurance, and Deliverance   <br>   <br>&quot;A Tribute To Sunday Morning Gospel&quot; is the latest composition of Bishop J.D. Means, Sr., which was recorded at The Travelers Rest Missionary Baptist Church where Pastor Arthur L. Powell serves as Pastor.   <br>   <br>This project features some of the greatest and anointed vocalists of Gospel in the likes of Bishop J.D. Means, Sr., Monica Lisa Stevenson, Dorothy Norwood, Roxanne Broadnax, David Hammond, Voncille Belcher, Ashley Perrymond, Rev. Arthur L. Powell, Dr. R.L. White, Jr. and the electrifying background vocals of Joseph Robinson &amp; Chosen Aggregation.    <br>   <br>If you are in search of a traditional church music project to add to your music collection, A Tribute To Sunday Morning Gospel Vol 1 (Part 1) is available for download TODAY!   <br>   <br>Artist: Bishop J.D. Means, Sr.   <br>   <br>Song Title: Tell The World   <br>   <br>Label: Mean Means Music Group   <br>   <br>Writer:  Bishop J.D. Means, Sr.   <br>   <br>Publisher: MM Music Group  ( BMI )   <br>   <br>ISRC Code: HSDY 41556277   <br>   <br>Barcode: 804837087322","BishopJDMeansSr,TellTheWorld,SundayMorningGospel","upload/photos/2020/06/3s7EuOBRF2wS73L4nwL2_26_0e8e56d7c4998d57e66ea798931005fe_image.png","0","0","1593153191","0","0","0","0","4:47","0:0","upload/audio/2020/06/RTV7oine3m84lmeu4n1v_26_f572a04ce2adfd0f1e4b7dd11d151107_audio.mp3","","2","2020/6","5734531","upload/waves/2020/06/GrGh8rEDsW18T2Shpqa3cBommTRsWP1Y8oxy5ORT_dark.png","upload/waves/2020/06/p3BiBloBHolxQsIlrma3Qy4QiiHaHH9FiYLTt4tR_light.png","0","0","0","","1","1","0","","",""),
("107","32","mPK8g3f8ySMiO6B","Evangelist Lorraine Punch Baldwin  I Have So Much To Thank God For","DNMG (Dorothy Norwood Music Group) is proud to release the new single entitled, &quot;I Have So Much To Thank God For,&quot; by Bishop Jonathan Greer featuring the legendary Evangelist Lorraine &quot;Punch&quot; Baldwin. This debut single from the soon to be released gospel compilation album &quot;Pastor Jonathan Greer, Embraces the Legends.&quot;  <br>   <br>This single is a song of gratitude, as there has never been a time that I needed God and he&#039;s never let me down. He never gave up on me and never said no. That&#039;s enough of a reason that every morning when I get up I have so say thank you. When I lay down at night I have to say thank You. Look at what thank you really means, every test, every trial, even though my loved one passed away, even in my bereavement I didn&#039;t lose my mind by just saying thank you, I have realized that I have too much to be thankful for.  <br>  <br>Evangelist Lorraine &quot;Punch&quot; Baldwin, is no stranger to the gospel music industry, Punch Baldwin is best known for her work with the Georgia Mass Choir. She has placed her footprint in the gospel music industry and has paved the way for so many gospel artists. Her powerful vocals and anointed delivery have inspired so many artists and has deemed her as a Gospel Legend.  <br>  <br>One of Punch&#039;s most memorable accomplishments was to record her first live recording entitled &quot;Step Back and Let God Do It,&quot; featuring the world renown, Rev. Milton Biggham, founder and director of the Georgia Mass Choir (Grammy Award Winners).  During the year of 2002, Punch became a member of the Georgia Mass Choir and has performed in several live recordings with the choir.  On her first recording with the choir, she was the featured lead vocalist on two selections on their album, one of which is entitled &quot;Bye and Bye.&quot;  This song grew in popularity to be the #1 hit song on the album.  The music of Bishop Jonathan Greer is available online at your favorite digital outlets.   <br>  <br>  <br>Artist: Bishop Jonathan Greer feat Evangelist Lorraine Punch Baldwin  <br>Song Title:  &quot; I Have So Much To Thank God For&quot;  <br>  <br>Label: Dorothy Norwood Music Group   <br>Publishing:  KG Productions/BMI  <br>Writers: Eric Ayers &amp; Dorothy Norwood  <br>UPC Code: 194171233593  <br>ISRC Code: USDY41999412","EvangelistLorrainePunchBaldwin,IHaveSoMuchToThankGodFor","upload/photos/2020/06/i6uKqLFGuodXwX8oqtlQ_26_cfd6239614e235cb05065143083f27c0_image.jpg","0","0","1593153614","0","0","0","0","5:18","0:0","upload/audio/2020/06/5ohKG9XrSiNo5huoFa8f_26_79ea0d10c9662804ce1a502e13f5c84a_audio.mp3","","2","2020/6","12723311","upload/waves/2020/06/loFVzAWOiadRf3GbojLr4I4O5noqjXyjoZeGR4jM_dark.png","upload/waves/2020/06/BB46z5EfnBK2QLSsbFXwYfEu5v1O5B3KWxv5CcsN_light.png","1","0","0","","1","1","0","","",""),
("108","32","hNhUrHi88snxzMc","Pastor David Wright &amp; N. Y. Fellowship Mass Choir, Clap Your Hands","In 1993, &quot;The Godfather Of Gospel Music&quot; Rev. Timothy Wright was able to get a large portion of NYC&#039;s choirs together for a 1 night live recording that would change the face of Gospel Music forever. That night Bishop Hezekiah Walker, Pastor Donnie McClurkin, Bishop Albert Jamison, Bishop Eric McDaniel, Evangelist Valerie Boyd and many others joined forces to show the world that New York City was united in praise to Almighty God. In June of 2013, 20 years later Pastor David Wright &quot;the son of late Rev. Timothy Wright&quot; picked up the Legacy that his father started.  <br>  <br>Upwards of 180 voices came together from all over NYC to form The New York Fellowship Mass Choir (Next Generation). On this Live Recording &amp; Video taping several artists participated such as Rubinstein McClure, Bruce Parham, Chrystal Rucker, Shawn Bigby, Doobie Powell, Pastor Kevin Bond, Jay Nixon, Craig Hayes and many others. The Live Recording consisted of 25 live songs. With Part 1 being released in 2014 and now here comes the much anticipated release of Part 2 which is titled Clap Your Hands.  <br>  <br> With special guest featured soloist such as Monique Walker, Tiffany Woodside, Timiney Figeroa, Roger Hambrick, Leon Lacey, Craig Hayes, and Doobie Powell this project will truly be a blessing to the Body of Christ.  <br>  <br>The highly anticipated radio single &quot;Clap Your Hands&quot; from Godfather Records own Pastor David Wright &amp; The NY Fellowship Mass Choir has been serviced to radio stations across the country and has now climbed into the Top 100 on the Nielsen/BDS Gospel Charts. The CD is available at ALL Digital Platforms and New Day Christian Distribution  <br>Artist: Pastor David Wright &amp; The NY Fellowship Mass Choir  <br>Song Title: &quot;Clap Hour Hands&quot;      <br>ISRC Code:  QMAAK1601981   <br>Barcode: 888295389549  <br>Label: Godfather Records  <br>Written by: Craig Hayes  <br>Publishing:  Hay-Nay Music /ASCAP","PastorDavidWright&amp;The NewYorkFellowshipMassChoir,ClapYourHands","upload/photos/2020/06/SV3VMehoTN5Tr8Cz9fjf_26_2136a9e0922f6492a5d5e7b5a44f8d8e_image.jpg","0","0","1593155033","0","0","0","0","4:30","0:0","upload/audio/2020/06/o8z1KSOa4Kywd3j8z1Sg_26_bca91d911857c43acaba9236036712df_audio.mp3","","2","2020/6","10785647","upload/waves/2020/06/ISpoWNzOKYIWYMIZOAIl95KjHSSwJKIOUZJuJAP8_dark.png","upload/waves/2020/06/wY5JIRQmHV7nvAnXA4f3QgsTPCLyrAnLDRpxmuCb_light.png","0","0","0","","1","1","0","","",""),
("109","21","QOZKbkDS8GG7kW2","Jesus Always With Me (online-audio-converter.com).mp3","Smooth jam","Urban,Contemporary,Soulful","upload/photos/2020/06/YrioXD3rrjCbtLTHaPUD_26_737de75b7ac12b8fc648a49fa7318dc2_image.jpg","0","0","1593201175","0","0","0","0","4:18","0:0","upload/audio/2020/06/OeBsmysZoLIxVfiHrKCN_26_f98fb0dde2d3937fd439ac0023da2f77_audio.mp3","","12","2020/6","4130606","upload/waves/2020/06/R5e4dlYQn1C6KLtUStJZXdhYRq4Jw5zXcdNesrOH_dark.png","upload/waves/2020/06/ZgyNeEmJJdqwmWnLuu3Rq7gXtUVHNJyIqxsbm9d2_light.png","2","1","0","There were times when I was feeling low <br>No one to talk to, no other place to go <br>Then, I remembered what my mama told me <br>Look to Jesus he&#039;s the greatest friend you need <br>I&#039;m so glad that the Lord is in my life <br>All the little battles give them to him to fight <br>Victory in Jesus is the sweetest thing <br>Words cannot express all the joy he bring <br> <br>Every move I make <br>Every step I take <br>He is with me <br>Always with me <br>Jesus always with me <br>(x2) <br> <br>I want to speak happiness into your life <br>Whatever the problem Jesus can make it right <br>I&#039;m not telling you things that I don&#039;t even know <br>I know that he loves me because the Bible tells me so <br>Got some good news the Lord loves you too <br>Any situation the Lord will see you through <br>He&#039;s so amazing I just can&#039;t tell you enough <br>Each and every day  <br>Jesus shows you his love <br> <br>Every move I make <br>Every step I take <br>He is with me <br>Always with me <br>Jesus always with me <br>(x4) <br> <br>Jesus always with me (loop)","1","1","0","","",""),
("110","21","LtHb2XF1UwyhF79","This Far by Faith.mp3","A good choir number...","Traditional,Modern,Gospel","upload/photos/2020/06/cIl9jmHCj5PLu5QyKBmh_26_793c1207b43face79c511de18cdccdbf_image.jpg","0","0","1593201712","0","0","0","0","4:39","0:0","upload/audio/2020/06/8CseUs8eA3WBaNPa3sCZ_26_343acc9ba3f1013a29969376fdba131e_audio.mp3","","12","2020/6","6694745","upload/waves/2020/06/amSisUqTIEscCXiXUA2XkCefbshtYpou2KhShAHa_dark.png","upload/waves/2020/06/ZSAiHXxPPM2GltjllP6Bj2YRu8qTeP9r9J1N1KLx_light.png","3","1","0","We have come this far by faith <br>Leaning on the Lord <br>Trusting in his Holy word <br>He has never failed me yet <br>Oh oh can&#039;t turn around <br>We have come (this far by faith) <br>We have come (this far by faith) <br>We have come this far by faith <br>(x2) <br> <br>I remember a day I heard someone say they could not believe in God&#039;s word <br>I can truly say that God has made a way and he&#039;s never failed me yet <br> <br>We have come this far by faith <br>Leaning on the Lord <br>Trusting in his Holy word <br>He has never failed me yet <br>Oh oh can&#039;t turn around <br>We have come (this far by faith) <br>We have come (this far by faith) <br>We have come this far by faith <br> <br>We have come  <br>This far by faith <br>(loop til)","1","1","0","","",""),
("111","34","k6GAiRNiBQS9ZmJ","Church Medley.mp3","Church Medley will appeal to both a seasoned generation who grew up on traditional church hymns and to Millineals wanting to know gospel music roots and songs.","Dave Shirley,Church medley,traditional gospel,old school,daveshirleymusic","upload/photos/2020/06/A8WqMJkXlKOwN2TGR1Rz_27_32a388dfd61be414c92103df431bbba3_image.jpg","0","0","1593234274","0","0","0","0","7:15","0:0","upload/audio/2020/06/IfRkmjCxUvkKnuQTUYOQ_27_90925bacaaaf97e974932ba47fd5b0de_audio.mp3","","12","2020/6","10441408","upload/waves/2020/07/XeE259CsUHRLpqaiVfWCiyxQYyEQojCJcC6xywC5_dark.png","upload/waves/2020/07/YPvLQKODxGPqTaVTTbYyFL9ArpfD7tvEt93KWh3M_light.png","0","1","0","","1","1","0","","",""),
("112","34","Vrx7BbbarNFPq6e","Lift Him Up.mp3","Lift Him Up provides a strong toe-tapping quartet sound with an upbeat tempo.","Lift Him Up,traditional,toe-tapping,Quartet,Dave Shirley,daveshirleymusic","upload/photos/2020/06/A8WqMJkXlKOwN2TGR1Rz_27_32a388dfd61be414c92103df431bbba3_image.jpg","0","0","1593234275","0","0","0","0","5:14","0:0","upload/audio/2020/06/4hdhSzqL97hFvuXCAlN4_27_4e954a395ea126b6879c4bd91e5946fa_audio.mp3","","12","2020/6","7538042","upload/waves/2020/07/TtWMDH5MEkg8FB8jV1Y2CQrvE4UlR9lOpxhof2JF_dark.png","upload/waves/2020/07/eRvP9IHdtviuBwU1zlPFc4126qIsmGHoh895B41U_light.png","0","1","0","","1","1","0","","",""),
("113","34","s2zN1usvfEEy9Oy","Lets Help One Another.mp3","Let’s Help One Another appeals to both contemporary gospel sound and the Urban sound with its powerful lyrics and upbeat music tempo. With cross-over appeal it ministers to a global community.","Lets Help One Another,Dave Shirley,Contemporary gospel,Gospel cross-over,R&amp;B gospel,Urban,daveshirleymusic","upload/photos/2020/06/A8WqMJkXlKOwN2TGR1Rz_27_32a388dfd61be414c92103df431bbba3_image.jpg","0","0","1593234275","0","0","0","0","4:45","0:0","upload/audio/2020/06/sK6NaBf4mDZP5m1SsBEs_27_e94c890e2e756cae424a662275ad7bb6_audio.mp3","","12","2020/6","6830859","","","0","1","0","","1","1","0","","",""),
("114","34","nhUuE4QeaR4Jcqm","Beams of Heaven.mp3","Beams of Heaven with a choir background is the old traditional song arranged with an up-tempo beat.","Beams of Heaven,Traditional","upload/photos/2020/06/aQoTG3wUoTO9tStBzw5x_27_17eac4b14aea7b3d360072494c7afbc7_image.jpg","0","0","1593235538","0","0","0","0","3:18","0:0","upload/audio/2020/06/fTdYH2tg8g5qISJNgwHM_27_3c1ccc80dc6c79e9846f3115de961ffb_audio.mp3","","2","2020/6","4760712","upload/waves/2020/06/sqUsX2N4Q6mXXRaeBkRkzGtHbsnGQfNeLwe67nlu_dark.png","upload/waves/2020/06/ccpcjhfKeUkrBfPOI42XwlXWuV3nNAVzbHgeAUqf_light.png","0","1","0","","1","1","0","","",""),
("115","34","GjiClmdOvagay2k","Prayer Changes Things.mp3","Prayer Changes Things will appeal to lovers of reggae music and has genre crossover appeal.","reggae music,Dave Shirley,Island gospel","upload/photos/2020/06/kbcitRJRLy1yZply7tLx_27_52df446161f662e38b673e3db4b6bdd9_image.jpg","0","0","1593235984","0","0","0","0","4:18","0:0","upload/audio/2020/06/ADrnvw9DsR2cyxDSFgb2_27_296cd0187638498f879e8a46ee704694_audio.mp3","","12","2020/6","6199543","upload/waves/2020/06/3Zz4oFM89JXS1fboQVdlIyU2cRSVmwxxanOm29y4_dark.png","upload/waves/2020/06/YvCBgJwFZbaGCqVSCsICC8JouC9OsSTbKsbyr1IN_light.png","0","1","0","","1","1","0","","",""),
("116","25","VyqVf47E5Netgz1","01 - I&#039;ll Praise You.mp3","The Man Behind the Music <br> <br>One of the most extraordinary things about Contemporary Christian Music is that it allows each praise and worship experience to explore outside of the box. A true example of that experience can be explored through the music ministry of Louisiana born and bred, Varieon Owens aka Chosen. He is the Founding Pastor/ Apostle of Spiritual Warriors for Christ, Inc.  and who’s Overseer is Apostle DeWanda Owens of DOMinistries, Inc. Forney, TX.  <br>Through Chosen’s prophetic dais, many have experienced God’s deliverance and healing. His first single was released in January 2014 entitled, “I’ll Praise You” co-written by national recording artist, Stellar Awards Nominee, Rhythm of Gospel Awards winner, Gilbert Wilson.  Chosen’s single has allowed him the opportunity to work with GilWil Music, TraeWayMusic and CRAZY Faith Entertainment. Also, it has landed him on the International T.V. Show KDAY Live T.V., The Ernie Miles Show and It Had to Be God on UA Network. He has appeared on many stages, such as, The Blockbuster Pre-Stellar Showcase and The Rhythm of Gospel Awards Show Midnight Musical. He has interviewed with radio announcers, such as, Jacki of KDAY Live ETC. He was the Artist of the Month on the Dorinda Clark Cole Radio Show website for the month of September 2014. Chosen has appeared in several issues of ICONIC Magazine along with some of Gospel Music’s top artist such as, Jessica Reedy, J Moss, Motown Gospel’s Brian Courtney Wilson and more. <br> He was recently nominated in 3 categories for the 2015 NOLA Gospel Awards in which he won one of the three, “Best New Artist.” He is currently a nominee in 4 categories for the 7th Annual Rhythm of Gospel Awards Show for 2015, and 4 categories in the Second round for The Prayze Factor People Choice Awards in Montego Bay, Jamaica. Also, just to name a few more awards and nominations. Music Love Awards 2016 Nominee, S&amp;M Gospel Indie Awards winner in 2016 &amp; 2017 &amp; DJ of The Year (Chosen) The Prayze Factor People Choice Awards 2017 in Atlanta Georgia etc. With the support of Christ, his beautiful wife (personal music manager), children, Pastor, Spiritual Fathers, leaders and fans, Chosen’s desire is to pursue his passion of being an international psalmist, minister and being a part of the Unreached Group Project focusing on the 10/40 window, bordering Africa, the Middle East and Asia.","I&#039;ll Praise You In Advance I&#039;ll Praise You in A Dance","upload/photos/2020/07/CcX2bdAeh5fRy89KXykL_01_c9f15add61caaa62c9e285a75548cd15_image.jpg","0","0","1593577304","0","0","0","0","4:26","0:0","upload/audio/2020/07/56ZMzPGjWCDiutvSSq1A_01_a9ff7eac13a98e44bc88ed68594902f6_audio.mp3","","4","2020/7","6972064","upload/waves/2020/07/BvOeglbrANU9HT5oxV87XB5g4GXJleZL1ONbcLRG_dark.png","upload/waves/2020/07/TBRGd9px7jr9TlKPfAofc5xfPQrH6Irlq85d5Wzq_light.png","0","0","0","","0","1","0","","",""),
("117","17","KkaseylVQRtiD27","I&#039;m Alive","6 x Antigua Barbuda Gospel Music Award Winner, Lisa Harris..&quot;I&#039;m Alive&quot; from her EP &quot;Take Me Out Of Me&quot;.","I&#039;m Alive,Praise &amp; Worship,Caribbean Gospel,Lisa Harris,So Arise Radio,Antigua Barbuda","upload/photos/2020/07/hXrrehcKKLm2Lx68V1M1_02_18b3aec534935bf4cd48892106e9cdc4_image.jpg","0","0","1593719657","0","0","0","0","3:52","0:0","upload/audio/2020/07/sR2oriyw625a1jjjOKZh_02_ce1a23a8f7995e99f1db8dcde1169038_audio.mp3","","8","2020/7","9282959","upload/waves/2020/07/VVPjCY6gSM6jsMgSQUrHRwlY8uLZS4ZnhAY4vKy3_dark.png","upload/waves/2020/07/l7wn865jyjqCXyldngWErnPG1QUtRopWTfEWQsCr_light.png","0","1","0","","0","0","0","","",""),
("118","41","9scDtp4MfBQtwTu","Because of Who You Are (Radio Mix)","Because of Who You Are is a contemporary gospel worship song with a jazzy feel that puts your in the mood of spiritual reflection of what God has done for your and thanking him for being who He is.","contemporary gospel,praise and worship,gospel jazz,sounds of imani,duo,group,indie","upload/photos/2020/07/QwpWTHZJPcKpgvHM2djB_03_785b35ea0b71dd9a7dea4159e05a1260_image.jpg","0","0","1593745630","0","0","0","0.99","00:00","0:0","upload/audio/2020/07/oO7Y2j1ta4i5lylZBk8W_03_aca86f67bf0cd104c69a524bb17a9a0a_audio.mp3","","12","2020/7","10949455","upload/waves/2020/07/fdZgnvKZMgucFo72ncj1xMUscohOgvkgQoHnqwnq_dark.png","upload/waves/2020/07/lqWf9UfHSvUxKxGu2emFOmepw13OkDJnyIQoLGox_light.png","1","1","0","","0","0","0","","",""),
("119","43","LWp2XoES6OAM1EE","On And On  (256Mastered 4-3-19) 1.mp3","Gospel Inspirational","Matthew 5:41 Jesus will go the extra mile","upload/photos/2020/07/NXmhjzAsKi9yJHVXs4EA_04_10e6942c649a7da6d177537c6155a95d_image.jpg","0","0","1593823289","0","0","0","0","4:14","0:0","upload/audio/2020/07/dVXtcX4g3A9rezv9mIg5_04_7e356bae4b3f5d83cd82f08c480eea4d_audio.mp3","","5","2020/7","8125961","upload/waves/2020/07/hg1cdBBWRTM4jjilEKzdvd4MrqdnmITTJ8N75EhC_dark.png","upload/waves/2020/07/uk852ofLAZsdAxet5oj8omw8smOukN3F2GFEk78r_light.png","3","0","0","","1","1","0","","",""),
("120","43","T2PrJEd86zSIFHE","stay free.mp3","Gospel Inspirational","John 8:36 So if the Son sets you free,you will be free indeed.","upload/photos/2020/07/dP9byqf9BFpeMufzwo12_06_d6ee7ca4db3c595b2fe2b50bb3957b71_image.jpg","0","0","1594073399","0","0","0","0","3:28","0:0","upload/audio/2020/07/m3sT7vq3tNOQDf7WmVgE_06_c5c26ca0aeb3165367aae0b62d547a6f_audio.mp3","","5","2020/7","6653073","upload/waves/2020/07/9d3MsaK1f6eJGddJP1yGyKGy8NQmKM3yf1Etz2Ze_dark.png","upload/waves/2020/07/nYzASLhxrzERbEEvxNpTSwIFrknrh2SzIQOahSkA_light.png","0","0","0","","1","1","0","","",""),
("121","43","Z3yie6xvpkrP99W","Live To Worship MP3.mp3","Worship is sacrifice","Romans 12:1 Worship is not just a lifting of hands,it is the lifestyle in which we live.","upload/photos/2020/07/KdQuztQTbAiVKeMyATTS_06_78c11d928bb6482afa13f3dc038986ab_image.jpg","0","0","1594078908","0","0","0","0","3:43","0:0","upload/audio/2020/07/b7WU5w4s9HkOYK8zwyrn_06_c3ce0e7c4e67f16cc997738d49512c73_audio.mp3","","8","2020/7","7122859","upload/waves/2020/07/rY9nYM3dGXRo31jUd6hfqpIJGlqBOW1FzFHdqrtD_dark.png","upload/waves/2020/07/TxLnG96bQG3IAIYR1CtsZ7KhCakl1437aSxlWFNF_light.png","0","0","0","","1","1","0","","","");

-- ---------------------------------------------------------
--
-- Table structure for table : `terms`
--
-- ---------------------------------------------------------

CREATE TABLE `terms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
  `content` longtext CHARACTER SET utf8mb4,
  PRIMARY KEY (`id`),
  KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `terms`
--

INSERT INTO terms VALUES
("1","terms","&lt;h4&gt;1- Write your Terms Of Use here.&lt;/h4&gt; &lt;br&gt;                &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adisdpisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis sdnostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;/p&gt; &lt;br&gt;    &lt;h4&gt;2- Random title&lt;/h4&gt; &lt;br&gt;                &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;/p&gt;"),
("2","about","Welcome to UGN Artists Radio Network , The Urban Gospel Music Sharing portion of the UGN Broadcasting Network, our purpose is to allow Artists to  &lt;br&gt;upload music , and create a profile pages that show  their brand as well as their music, for listeners that sign up., and also gives UGN Radio Charting stations the opportunity to hear their music and add it , if it fits their playlist, giving them the opportunity to gain access to the UGN Global urban Gospel Top 50 Chart...."),
("3","privacy","Privacy Policy  <br>   <br>Protecting your private information is our priority. This Statement of Privacy applies to ugnbroadcasting.com/pools and United Gospel Network and governs data collection and usage. For the purposes of this Privacy Policy, unless otherwise noted, all references to United Gospel Network include ugnbroadcasting.com/pools, UGN, United Gospel Radio Nework and ugnbroadcasting.com. The UGN website is a Music Sharing , Ecommerce, site. By using the UGN website, you consent to the data practices described in this statement.  <br>   <br>Collection of your Personal Information  <br>In order to better provide you with products and services offered on our Site, UGN may collect personally identifiable information, such as your:  <br>   <br> -	First and Last Name  <br> -	E-mail Address  <br>   <br>If you purchase UGN&#039;s products and services, we collect billing and credit card information. This information is used to complete the purchase transaction.  <br>   <br>Please keep in mind that if you directly disclose personally identifiable information or personally sensitive data through UGN&#039;s public message boards, this information may be collected and used by others.  <br>   <br>We do not collect any personal information about you unless you voluntarily provide it to us. However, you may be required to provide certain personal information to us when you elect to use certain products or services available on the Site. These may include: (a) registering for an account on our Site; (b) entering a sweepstakes or contest sponsored by us or one of our partners; (c) signing up for special offers from selected third parties; (d) sending us an email message; (e) submitting your credit card or other payment information when ordering and purchasing products and services on our Site. To wit, we will use your information for, but not limited to, communicating with you in relation to services and/or products you have requested from us. We also may gather additional personal or non-personal information in the future.  <br>   <br>Use of your Personal Information  <br>UGN collects and uses your personal information to operate its website(s) and deliver the services you have requested.  <br>   <br>UGN may also use your personally identifiable information to inform you of other products or services available from UGN and its affiliates.  <br>   <br>Sharing Information with Third Parties  <br>UGN does not sell, rent or lease its customer lists to third parties.  <br>   <br>UGN may share data with trusted partners to help perform statistical analysis, send you email or postal mail, provide customer support, or arrange for deliveries. All such third parties are prohibited from using your personal information except to provide these services to UGN, and they are required to maintain the confidentiality of your information.  <br>   <br>UGN may disclose your personal information, without notice, if required to do so by law or in the good faith belief that such action is necessary to: (a) conform to the edicts of the law or comply with legal process served on UGN or the site; (b) protect and defend the rights or property of UGN; and/or (c) act under exigent circumstances to protect the personal safety of users of UGN, or the public.  <br>   <br>Tracking User Behavior  <br>UGN may keep track of the websites and pages our users visit within UGN, in order to determine what UGN services are the most popular. This data is used to deliver customized content and advertising within UGN to customers whose behavior indicates that they are interested in a particular subject area.  <br>   <br>Automatically Collected Information  <br>Information about your computer hardware and software may be automatically collected by UGN. This information can include: your IP address, browser type, domain names, access times and referring website addresses. This information is used for the operation of the service, to maintain quality of the service, and to provide general statistics regarding use of the UGN website.  <br>   <br>Links  <br>This website contains links to other sites. Please be aware that we are not responsible for the content or privacy practices of such other sites. We encourage our users to be aware when they leave our site and to read the privacy statements of any other site that collects personally identifiable information.  <br>   <br>Security of your Personal Information  <br>UGN secures your personal information from unauthorized access, use, or disclosure. UGN uses the following methods for this purpose:  <br>   <br> -	SSL Protocol  <br>   <br>When personal information (such as a credit card number) is transmitted to other websites, it is protected through the use of encryption, such as the Secure Sockets Layer (SSL) protocol.  <br>   <br>We strive to take appropriate security measures to protect against unauthorized access to or alteration of your personal information. Unfortunately, no data transmission over the Internet or any wireless network can be guaranteed to be 100% secure. As a result, while we strive to protect your personal information, you acknowledge that: (a) there are security and privacy limitations inherent to the Internet which are beyond our control; and (b) security, integrity, and privacy of any and all information and data exchanged between you and us through this Site cannot be guaranteed.  <br>   <br>Right to Deletion  <br>Subject to certain exceptions set out below, on receipt of a verifiable request from you, we will:  <br>•	Delete your personal information from our records; and  <br>•	Direct any service providers to delete your personal information from their records.  <br>   <br>Please note that we may not be able to comply with requests to delete your personal information if it is necessary to:  <br>•	Complete the transaction for which the personal information was collected, fulfill the terms of a written warranty or product recall conducted in accordance with federal law, provide a good or service requested by you, or reasonably anticipated within the context of our ongoing business relationship with you, or otherwise perform a contract between you and us;  <br>•	Detect security incidents, protect against malicious, deceptive, fraudulent, or illegal activity; or prosecute those responsible for that activity;  <br>•	Debug to identify and repair errors that impair existing intended functionality;  <br>•	Exercise free speech, ensure the right of another consumer to exercise his or her right of free speech, or exercise another right provided for by law;  <br>•	Comply with the California Electronic Communications Privacy Act;  <br>•	Engage in public or peer-reviewed scientific, historical, or statistical research in the public interest that adheres to all other applicable ethics and privacy laws, when our deletion of the information is likely to render impossible or seriously impair the achievement of such research, provided we have obtained your informed consent;  <br>•	Enable solely internal uses that are reasonably aligned with your expectations based on your relationship with us;  <br>•	Comply with an existing legal obligation; or  <br>•	Otherwise use your personal information, internally, in a lawful manner that is compatible with the context in which you provided the information.  <br>   <br>Children Under Thirteen  <br>UGN does not knowingly collect personally identifiable information from children under the age of thirteen. If you are under the age of thirteen, you must ask your parent or guardian for permission to use this website.  <br>   <br>E-mail Communications  <br>From time to time, UGN may contact you via email for the purpose of providing announcements, promotional offers, alerts, confirmations, surveys, and/or other general communication.  <br>   <br>Changes to this Statement  <br>UGN reserves the right to change this Privacy Policy from time to time. We will notify you about significant changes in the way we treat personal information by sending a notice to the primary email address specified in your account, by placing a prominent notice on our site, and/or by updating any privacy information on this page. Your continued use of the Site and/or Services available through this Site after such modifications will constitute your: (a) acknowledgment of the modified Privacy Policy; and (b) agreement to abide and be bound by that Policy.  <br>   <br>Contact Information  <br>UGN welcomes your questions or comments regarding this Statement of Privacy. If you believe that UGN has not adhered to this Statement, please contact UGN at:  <br>   <br>United Gospel Network  <br>610 East Desoto St."),
("4","dmca","The Digital Millennium Copyright Act is a 1998 United States copyright law that implements two 1996 treaties of the World Intellectual Property Organization. It criminalizes production and dissemination of technology, devices, or services intended to circumvent measures that control access to copyrighted works. It also criminalizes the act of circumventing an access control, whether or not there is actual infringement of copyright itself. In addition, the DMCA heightens the penalties for copyright infringement on the Internet. Passed on October 12, 1998, by a unanimous vote in the United States Senate and signed into law by President Bill Clinton on October 28, 1998, the DMCA amended Title 17 of the United States Code to extend the reach of copyright, while limiting the liability of the providers of online services for copyright infringement by their users.");

-- ---------------------------------------------------------
--
-- Table structure for table : `user_ads`
--
-- ---------------------------------------------------------

CREATE TABLE `user_ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL DEFAULT '',
  `results` int(11) NOT NULL DEFAULT '0',
  `spent` varchar(20) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1',
  `audience` text,
  `category` varchar(50) NOT NULL DEFAULT '',
  `media` varchar(1000) NOT NULL DEFAULT '',
  `audio_media` varchar(1000) NOT NULL DEFAULT '',
  `url` varchar(3000) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `placement` varchar(50) NOT NULL DEFAULT '',
  `posted` varchar(50) NOT NULL DEFAULT '0',
  `headline` varchar(1000) NOT NULL DEFAULT '',
  `description` varchar(1000) NOT NULL DEFAULT '',
  `location` varchar(1000) NOT NULL DEFAULT '',
  `type` varchar(50) NOT NULL DEFAULT '',
  `day_limit` varchar(11) NOT NULL DEFAULT '0',
  `day` varchar(50) NOT NULL DEFAULT '',
  `day_spend` varchar(11) NOT NULL DEFAULT '0',
  `ad_type` varchar(50) NOT NULL DEFAULT 'image',
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `location` (`location`(255)),
  KEY `placement` (`placement`),
  KEY `user_id` (`user_id`),
  KEY `category` (`category`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_ads`
--

INSERT INTO user_ads VALUES
("2","Tendeep Randle","101","10.09999999999998","1","1,2,11","image","upload/photos/2020/06/nlhE5zlCuBmY8dxaSJd6_23_6cf8a875d9801671fb0c8f3766dfd873_image.jpg","","https%3A%2F%2Fwww.digitalradiotracker.com%2Fdrt-dev%2Flogin.php","3","1","1592946158","ARTISTS JOIN DIGITAL RADIO TRACKER FOR FREE SIGN UP TODAY.......","Artists can sign up to track your music through over 300 Gospel Stations , and be able to locate more stations to submit your music to, its free to sign up, Do It Today!!","","2","5","2020-06-30","5","image");

-- ---------------------------------------------------------
--
-- Table structure for table : `user_fields`
--
-- ---------------------------------------------------------

CREATE TABLE `user_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ---------------------------------------------------------
--
-- Table structure for table : `user_interest`
--
-- ---------------------------------------------------------

CREATE TABLE `user_interest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `category_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=158 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_interest`
--

INSERT INTO user_interest VALUES
("1","3","2"),
("2","1","12"),
("6","8","8"),
("7","8","5"),
("8","8","4"),
("9","8","3"),
("15","10","6"),
("18","11","12"),
("19","11","10"),
("20","11","9"),
("21","11","8"),
("22","11","7"),
("23","11","5"),
("24","11","4"),
("25","11","2"),
("26","13","6"),
("27","13","5"),
("28","13","1"),
("29","14","12"),
("30","14","6"),
("31","14","4"),
("32","14","1"),
("36","16","12"),
("37","16","9"),
("38","16","8"),
("39","16","6"),
("40","16","5"),
("41","16","4"),
("42","16","3"),
("43","16","2"),
("44","17","10"),
("45","17","9"),
("46","17","8"),
("47","17","6"),
("48","17","5"),
("49","17","4"),
("50","19","9"),
("51","19","8"),
("52","19","5"),
("53","19","4"),
("54","19","2"),
("55","18","12"),
("56","18","9"),
("57","18","8"),
("58","18","7"),
("59","18","5"),
("60","18","4"),
("61","18","2"),
("62","20","6"),
("63","21","12"),
("64","21","10"),
("65","21","9"),
("66","21","8"),
("67","21","5"),
("68","21","4"),
("69","21","3"),
("70","21","2"),
("71","22","4"),
("74","24","10"),
("75","24","9"),
("76","24","8"),
("77","24","7"),
("78","24","5"),
("79","24","4"),
("80","24","2"),
("81","25","9"),
("82","25","8"),
("83","25","5"),
("84","25","4"),
("85","27","12"),
("86","27","10"),
("87","27","9"),
("88","27","8"),
("89","27","5"),
("90","27","4"),
("91","27","2"),
("92","30","12"),
("93","30","10"),
("94","30","9"),
("95","30","8"),
("96","30","5"),
("97","30","3"),
("98","31","4"),
("99","32","12"),
("100","32","8"),
("101","32","7"),
("102","32","5"),
("103","32","4"),
("104","32","2"),
("105","33","6"),
("106","28","12"),
("107","28","10"),
("108","28","9"),
("109","28","8"),
("110","28","7"),
("111","28","6"),
("112","28","5"),
("113","28","4"),
("114","28","3"),
("115","28","2");

--
-- Dumping data for table `user_interest`
--

INSERT INTO user_interest VALUES
("116","28","1"),
("118","35","12"),
("119","35","9"),
("120","35","4"),
("121","34","12"),
("122","34","9"),
("123","37","9"),
("124","37","8"),
("125","37","7"),
("126","37","5"),
("127","37","4"),
("128","37","2"),
("129","38","8"),
("130","40","9"),
("131","41","5"),
("132","41","4"),
("133","43","8"),
("134","43","5"),
("135","45","12"),
("152","46","9"),
("153","46","8"),
("154","46","7"),
("155","46","6"),
("156","46","5"),
("157","46","2");

-- ---------------------------------------------------------
--
-- Table structure for table : `users`
--
-- ---------------------------------------------------------

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `ip_address` varchar(150) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `password` varchar(150) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `gender` varchar(10) CHARACTER SET latin1 NOT NULL DEFAULT 'male',
  `email_code` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `language` varchar(22) CHARACTER SET latin1 NOT NULL DEFAULT 'english',
  `avatar` varchar(100) CHARACTER SET latin1 NOT NULL DEFAULT 'upload/photos/d-avatar.jpg',
  `cover` varchar(100) CHARACTER SET latin1 NOT NULL DEFAULT 'upload/photos/d-cover.jpg',
  `src` varchar(22) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `country_id` int(11) NOT NULL DEFAULT '0',
  `age` int(11) NOT NULL DEFAULT '0',
  `about` text COLLATE utf8_unicode_ci,
  `google` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `facebook` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `twitter` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `instagram` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `active` int(11) NOT NULL DEFAULT '0',
  `admin` int(11) NOT NULL DEFAULT '0',
  `verified` int(11) NOT NULL DEFAULT '0',
  `last_active` int(11) NOT NULL DEFAULT '0',
  `registered` varchar(40) CHARACTER SET latin1 NOT NULL DEFAULT '0000/00',
  `uploads` float NOT NULL DEFAULT '0',
  `wallet` varchar(200) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `balance` float unsigned NOT NULL DEFAULT '0',
  `website` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `artist` int(11) NOT NULL DEFAULT '0',
  `is_pro` int(11) NOT NULL DEFAULT '0',
  `pro_time` int(11) NOT NULL DEFAULT '0',
  `last_follow_id` int(11) unsigned NOT NULL DEFAULT '0',
  `ios_device_id` varchar(100) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
  `android_device_id` varchar(100) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
  `web_device_id` varchar(100) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
  `email_on_follow_user` int(11) unsigned DEFAULT '0',
  `email_on_liked_track` int(11) unsigned DEFAULT '0',
  `email_on_liked_comment` int(11) unsigned DEFAULT '0',
  `email_on_artist_status_changed` int(11) unsigned DEFAULT '0',
  `email_on_receipt_status_changed` int(11) unsigned DEFAULT '0',
  `email_on_new_track` int(11) unsigned DEFAULT '0',
  `email_on_reviewed_track` int(11) unsigned DEFAULT '0',
  `two_factor` int(11) unsigned NOT NULL DEFAULT '0',
  `new_email` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
  `two_factor_verified` int(11) unsigned NOT NULL DEFAULT '0',
  `new_phone` varchar(32) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
  `phone_number` varchar(32) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
  `last_login_data` text CHARACTER SET utf8mb4,
  `referrer` int(11) NOT NULL DEFAULT '0',
  `ref_user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `username` (`username`),
  KEY `email` (`email`),
  KEY `password` (`password`),
  KEY `last_active` (`last_active`),
  KEY `admin` (`admin`),
  KEY `active` (`active`),
  KEY `registered` (`registered`),
  KEY `wallet` (`wallet`),
  KEY `balance` (`balance`),
  KEY `pro_time` (`pro_time`),
  KEY `country_id` (`country_id`),
  KEY `verified` (`verified`),
  KEY `artist` (`artist`),
  KEY `is_pro` (`is_pro`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `users`
--

INSERT INTO users VALUES
("1","BudBell","gwyddell@gmail.com","","$2y$10$L1rHyc5ETyjE8rAjVRRoLuH5cfLGc5hha3UThHdLSGZ9VIjKGcCJS","","male","cf5362a2a1adb228f542c7068b8e3a30237bb5f3","english","upload/photos/2020/06/TFu2w9vOwYJot7bkxb8Y_23_43d1f7b67d9fcf6bfb059d10d3746142_image.png","upload/photos/2020/06/aJpCEk3ZbGrSx8FnCZfU_23_065c2ce159163ab868ac87625e3f89d5_image.png","","1","0","","","","","","1","1","1","1592619393","2020/6","0","5000","0","","1","1","1592619393","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("3","UBNGospelRadio","ugnceo1@gmail.com","24.32.89.221","$2y$10$IMf95BslcseNPVCacYiUXeO5nIBLEEDbwsYlEkqJFy1QlU9LyszU.","United Gospel Radio Network","male","9f21864c5eeac2fa1eb36747f0fd4905af3cfbcf","english","upload/photos/2020/06/BHThzxSkOlLn75QRi9QZ_21_f07db1f6417956973259fe31f12ca297_image.jpg","upload/photos/2020/06/wqRSRgTHTRvAzO364bkq_20_65b5afe1d631b11ceb6374acf7a93262_image.jpg","","1","0","Our World has changed to the point that we have an opportunity to gain in this world is a major way. The Internet has launched an opportunity for those of us that live in small communities, and also in larger metro areas.  At United Gospel Network Media, our goal is to reach all viewers from all nations and walks of life through global internet Television, Radio, and Music, following the message that the creator of the heavens and the earth bestowed on us of reaching the entire world in a manner that we would be able to show the diversity of the message of the Gospel, not only through preaching the word, but also living the entire life that we were supposed to live from the beginning, The life of enjoyment. Our goal is to provide video and audio programming that will inform you, relax you and make you praise and shout, no matter what walk of life you are from, so search through our pages and find what will make your day whether its a Movie, Talk Show, listening to one of our Radio Stations /TV Channels here on the site or even on our mobile app, &quot; United Gospel Radio Network.&quot; <br> <br>We also have one of the newest Global Gospel Charting Systems in the world, and our amazing annual Top 50 Global Urban Gospel Award shows, celebrating the accomplishments of those in the music industry.....","","Tendeep Randle","","","1","1","1","1592620176","2020/6","0","4989.899999999963","1","https://ugnbroadcasting.com","1","1","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("8","rowanchapman","wallacemusicmedia@gmail.com","71.221.250.10","$2y$10$Iq5UCa76Qs6n.uFtI3t/eeqsydNyQrUBNXFqKm.uWUYLVuahGeEe.","Rowan Chapman","male","a2dee6144d1321eacde9ba311be7d34b348f8e2a","english","upload/photos/2020/06/yRgjE6aGYO3jVDWHVdIY_21_223228911c261a712078af0c040bd43e_image.jpg","upload/photos/2020/06/HD1OHk76YEeTdxIJEUui_21_9ffffdaadaa528802358e842190297a5_image.jpg","","1","53","Rowan Chapman is a native of Baltimore, Maryland. As a young boy, Rowan had a love for music and found that he had a musical gift.  His father was a musician as well.  Rowan grew up in the church and was taught the importance and value of music to a church service.   <br> <br>Rowan’s ministry was birth in his early 20’s out of some of life&#039;s most difficult hurts and pains in which he became a new and surrendered vessel. Rowan miraculous recovery from a brain tumor in the late 90&#039;s afforded him much time in the presence of the Lord. During this dark season, Rowan acknowledges having learned a very valuable lesson of stepping out of arrogance and into a new creation.  He understood that he was created to worship.  It was during that time that he began writing songs that he believed were inspired by God.","","","","","1","0","1","1592711773","2020/6","0","0.00","0","http://www.rowanchapmanmusic.com","0","1","0","0","","","","1","1","1","0","0","1","1","0","","0","","","","0","0"),
("10","ImEturnul","eturnul@gmail.com","76.176.17.171","$2y$10$l6BmQQKYGKYLB0Xj2GDuYOOndS/NMvxy1IvqlsXIECTILYURb7oV6","ETURNUL","female","17006ebc7b4c9189995dd6b5e4200adc4a9ea92d","english","upload/photos/2020/06/ReFdbXdRs5826dH94IsW_21_8b130068cc21d5e733484cc7328f4206_image.jpg","upload/photos/2020/06/xgN2T5BCgc2XzGdcgz8i_21_f75db7cc5f35f84a63877722ff61a6ab_image.jpg","","1","0","La&#039;tesha Nicole &quot;Eturnul    <br>    <br>Born and rooted from the urban communities of Southeast San Diego located in Southern California. From living a fast Street life to being delivered and blessed with a second chance to live life again. Giving all glory to GOD. Living to spread the gospel of Jesus Christ. Her music is proven outreach which bridges a gap long desired between the wise and youthful generation. Reaching and inspiring the masses.    <br>    <br>    <br>    <br>Eturnul has chose to live her life educating, encouraging and empowering others to live a positive Life Style. Having graced the stage with local and global Legends she is now on a mission to impact the world in a positive way with a catchy twist to her flow and a classic lyrical delivery. We asked her to describe her lifestyle and music in 3 words, she replied. &quot;God Or Nothing&quot;    <br>    <br>*New Music Alert By- Eturnul &quot;Blessings&quot; Feat Fre&#039;yonni La&#039;mari  Available On All Digital Outlets     <br>    <br>***Video &quot; Blessings &quot;    <br>    <br>https://youtu.be/yPYKaxhsEpI   <br>    <br>    <br>Upcoming Projects- Mission Possible Ep    <br>    <br>Follow &quot;ETURNUL&quot; on all social media.    <br>    <br>www.youtube.com/ImEturnul    <br>www.facebook.com/ImEturnul    <br>www.instagram.com/ImEturnul    <br>www.twitter.com/ImEturnul    <br>    <br>    <br>    <br>Awards    <br>    <br>2016&#039; Prestige Award Winner- &quot;Gospel Rap Artist Of The Year&quot;    <br>    <br>2018&#039; P.O.W.E.R Award (805SOUTH) &quot; Young Black &amp; Business    <br>    <br>2019&#039; &quot;National &quot;Artist Of The Year&quot; 21st Texas Gospel Excellence Awards 2019    <br>    <br>2019&#039; National &quot;Gospel Rap Artist Of The Year&quot; 21st Texas Gospel Excellence Awards     <br>    <br>2019’ VIGA “Hip Hop Video Of The Year    <br>    <br>Thank You Kindly","","ImEturnul","","","1","0","1","1592714309","2020/6","0","0.00","0","http://linktr.ee/imeturnul","0","1","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("11","LadyD","fontenotdeetra@gmail.com","24.32.89.221","$2y$10$/qlAOki/9vCgb1LSSDfUlujDhrLWqFX4PK5/ogISQthqfBY12Odji","Deetra Fontenot","female","951a7c07f7294a67a9200f805f0ba219fd72d639","english","upload/photos/2020/06/3UgQEvCm1iMY9mqkISMS_21_423383f447a5f8446ec14fa53f60ea54_image.jpg","upload/photos/2020/06/F6473BM53mHsjAu6aR5D_23_c3b0c7fb340f73d0a366220c62d2c19c_image.jpg","","1","52","","","","","","1","0","0","1592716149","2020/6","0","0.00","0","","0","1","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("13","KenClear","sonzofredemption@gmail.com","12.37.34.162","$2y$10$xJ4JAit0LsWZ9bq.9pB.rOOmdyM6oq9x2nGTxWqjLCmaQvwttl.MG","Ken Cockheran","male","566b0e195c03cd31db62c1849ae57b8d12c955c3","english","upload/photos/2020/06/PSQozr9e4rPTcYlvPxTP_21_86280aba7ec78321e4c94283b7b91842_image.jpg","upload/photos/2020/06/qjdGDnPMvsdlmPNpmAf3_21_f9c1338be271234dacb7185d1ded3331_image.jpg","","1","28","Allow us to introduce the pop/alternative artist Ken Clear, is Carley5K Production most recent signing and compelling new talent.  On his debut self-title &quot;FRESH&quot;, this new Orleans native tells his story with lyric vulnerability and raw passion. His music draws from real life milestones, victories, struggles, and the fresh parts of his life.   <br> <br>Quote  - To motivate the massess","","","","","1","0","0","1592717375","2020/6","0","0.00","0","https://www.kenclear.com/","0","0","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("14","GospelUnderground","jen.wilson@roadrunner.com","174.104.202.66","$2y$10$6qRuvTpZ/jk3gOHVXH9gpOgX8Pz/pivAKxyxXtUupFOfC7/Ij947W","Officially LadyJ","female","eb334d913642cf86c193588be15d57bd4f1667b1","english","upload/photos/2020/06/7IXrAMguyklMXHfPwDJU_22_b4ca5c50c036a25ea71cdbd8f7c68ae0_image.jpg","upload/photos/2020/06/c9ah1q4dnOPLjppfUVLr_22_a7b52e52e4a2c956a2b21f3cb898f01e_image.jpg","","227","0","Gospel Contemporary Artist, Officially LadyJ, uses an eclectic fusion of techno, funk, R&amp;B and pop to deliver the message of Jesus Christ through song.  With the soulful ‘whistle register’ of singer, Minnie Riperton and Mariah Carey, Officially LadyJ’s five-octave coloratura soprano range floats over heavy repetitious bass lines and drum beats. The results uniquely create a  new sound that is unlike any other.  Singer, songwriter and producer, Officially LadyJ, has music lovers across genres listening and taking notice.  Her lyrics are raw and transparent.  Officially LadyJ orates unspoken truths about Christian struggles to the beat of her own drum as the HolySpirit gives utterance.","","OfficiallyLadyJ","","","1","0","0","1592836952","2020/6","0","0.00","0","https://www.officiallyladyj.com","1","1","0","0","","","","1","1","1","0","0","1","1","0","","0","","","","0","0"),
("16","Rightnowpraizzradio","rightnowpraizzradio@gmail.com","50.4.240.10","$2y$10$Dpwia54cJerNG8sS4AghDetEr1eKWT1hNAtO3uSyR8OzGKQLWX87K","Right Now Praizz Radio","male","928f92c70190074083007372ea72aa9acf740708","english","upload/photos/2020/06/159BGam288OLUGwnmekv_23_7d87f0360bec86806c892eba9dc47827_image.jpg","upload/photos/2020/06/JrN6r1qmXuUo6Sq6Pv4z_24_0046a6fc7ccc4ed71c1820b7e895e1e2_image.png","","1","0","","","","","","1","0","0","1592931515","2020/6","0","0.00","0","","0","1","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("17","SoAriseRadio","soariseradio@gmail.com","208.83.87.72","$2y$10$mvFRE3ZTYvuqBKjTM20MU.LBPlwjAcVIgEgy4z3eevyZT5xfDI82y","So Arise Radio","male","3fd4a527d85c1582ee997b7563546c31cf70b9ef","english","upload/photos/2020/06/JnOtzJ24F32aGMTfrYoo_23_dbd49262e1bac29c726124135816c808_image.jpg","upload/photos/2020/07/ME9OpvtHARvJKqdR3Hz6_03_d89743d092636dbe1851beaed6f84c3e_image.jpg","","0","0","A Multi-Award Nominated Gospel Internet Radio Station established in 2014 on the island of Antigua, West Indies. We play all genres of Gospel music + broadcast a wide variety of programs from around the world!","","SoAriseRadio","","","1","0","0","1592931862","2020/6","0","0","0","https://www.soarisemusic.com","0","1","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("18","XtremePraise","Xtremepraise6@gmail.com","107.77.253.15","$2y$10$3Kexz9BBcI7l5eapowjs2Ol9CuSvAFyZjwRVnBfm.pSNXyuxy.lQe","Jonathan Stevenson","male","53478a33cb150b6a5d3b8efcb047d25c45ec13d0","english","upload/photos/d-avatar.jpg","upload/photos/d-cover.jpg","","0","0","","","","","","1","0","0","1592964849","2020/6","0","0","0","","0","1","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("19","Ladycjwebb","lcjwradio.2511@gmail.com","70.238.40.136","$2y$10$mqyf.mV8AU5DKI4eIB2YpuRjsa2u0UpwCUBEY67D7vepeOO2.dWWG","LCJW RADIO","female","6b3a4c1471f05cdccd5d83c1e22106e75380437a","english","upload/photos/2020/06/2kZUsDkf3Wdp9TkRz3bI_24_470a0bfb9e68a7a9cc968d85b3100868_image.jpg","upload/photos/2020/06/AAYFAt3zqJEWYRVdJ2H6_24_98dd7502bb90756b89cb50466e9ec6cf_image.jpg","","0","51","","","","","","1","0","0","1592966240","2020/6","0","0.00","0","","0","1","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("20","thacarpentur","thacarpentur1@gmail.com","107.77.200.210","$2y$10$6Cfblqf9N1vfcxs0j5lBIuVKQRUgkW/2WHwOnjuzzauZsXc.98DTS","Nicholas Hoof","male","a4e430a95102d362c957daa542b764ed8f133798","english","upload/photos/2020/06/TxP15sBUXLLaK3E35mOq_24_a94801d3ec7d0b717ab969fe61149495_image.jpeg","upload/photos/2020/06/9x2q2bAEFr2ajMNDTGnS_24_7d66e7f15188ce3fb864bfa19456b898_image.jpeg","","1","0","","","","","","1","0","0","1593025396","2020/6","0","0.00","0","","0","1","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("21","Chaddybear","chaddybearentertainment@gmail.com","98.196.84.109","$2y$10$fFTse1Og5/cHNTGTMWuW5edzIZZErxvPgoECWnCpklnlaDMI8fL9q","Chad Howard","male","aaa740f14022af06ef802fa64f9d097f1fda1dfd","english","upload/photos/2020/06/j4lSADZWUav7w39ok6G3_24_d2426fce606eeebc2e8a2a5f89f7c8bd_image.jpg","upload/photos/2020/06/ylipKryE9lzdac8UjMRc_24_f152d0e9200ab1e3c67df55804d614b2_image.jpg","","1","38","Singer/Songwriter","","Chad Weslyn Howard","","","1","0","0","1593030781","2020/6","0","0.00","0","","0","1","1593139497","0","","","","1","1","1","0","0","1","1","0","","0","","","","0","0"),
("22","jasonpnation","jasonpnations@gmail.com","67.10.36.102","$2y$10$VyWBf0dkC/Pk03SqbL/Bq.a699KCHpO1gEniEP2GcJBFxB1ttBB5m","Jason Nation","male","290185a7dd43d4787f3ea26b6b0aa0391fe3f5ce","english","upload/photos/d-avatar.jpg","upload/photos/d-cover.jpg","","0","0","","","","","","1","0","0","1593032440","2020/6","0","0","0","","0","1","1593032837","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("24","Lulu","atwaterlouise14@gmail.com","66.115.181.73","$2y$10$khVwlYlPRBCQr.R2uVZRa.jD25J/S2fIliw.Pmvx6vzkbe5hTSS6C","Priscilla Atwater","female","1caa7ebf3570a49a2c6dfdd58881976b1da343d7","english","upload/photos/2020/06/PJaLi3KmPjSSBEXbSCLV_25_e35c6ae21ec5ffa001ecdc3793e7cb88_image.jpg","upload/photos/2020/06/2UwALqSFmKyKgEzhFbEY_25_58032c99c303be0fff0702561bdcffa9_image.jpg","","1","63","I am the oldest of 3 children. I have 1 son and 2 grandchildren. I have over 50 years experience  as church musician.","","Priscilla Garner Atwater","","","1","0","0","1593039378","2020/6","0","0.00","0","http://facebook.com/pra63","0","1","1593062869","0","","","","0","1","0","0","0","0","0","0","","0","","","","0","0"),
("25","Chosen","iwaschosenbygod@gmail.com","45.20.139.57","$2y$10$dt8KYTJwmyUJRcKLLkOt8OTxa7cwgOK43BoCSOTQQwj3i/QLXdlKS","Varieon Owens","male","b1e559e627235a81ca25306d2f0e2d10e326d3f8","english","upload/photos/2020/07/xL6jO1GSuzrsVS9gDgrZ_01_6c8422a543a2e01e0fb042871e0bc228_image.jpg","upload/photos/2020/07/jWXlh4tgUXtwjgqmUND7_01_332efc099e0f9d7b3720d93817fa43fd_image.jpg","","1","32","","","www.facebook.com/chosen.owens.7/","","","1","0","0","1593059376","2020/6","0","0.00","0","","1","0","0","0","","","","0","1","1","0","0","1","1","0","","0","","","","0","0"),
("27","hawkinsbhu","bettyhawkinsandunited@gmail.com","73.252.63.23","$2y$10$g6QncoJ.9vwiCW0o0gjPDunfjYeEf84lKo0jUSUjtAVdC2a7hwuTO","Betty Hawkins","female","5eda056c9ba6779d17b3454c95d4954c5cae9851","english","upload/photos/2020/06/ZdaZEXYMVPrK77SFplDR_25_de2aa64b14142a7658c481b887da9d9b_image.jpg","upload/photos/2020/06/gLRCHvr3HiPfBSEjaXF4_25_80eb3ea784075f406bd3b181e0a2f8e5_image.jpg","","1","0","Singer and Christian worship leader, Betty McCoy Hawkins grew up in Monroe, Louisiana, the youngest of 10 children. Reared up in the church with strong family roots in Christ. This soulful, anointed Woman of God has been active in ministry most of her life, youth leader, youth director, praise and worship leader, and sung in many choirs throughout Northeast Louisiana often as a soloist. <br> <br>Gospel singer, chief songwriter, Betty McCoy Hawkins, leader of United, who’s background singers are all from different backgrounds but united together in Christ. Betty Hawkins and United released their first singles, “You Are” with its praise and worship sound and “All I Need” with its Contemporary, soulful gospel sound, in 2015.  <br> <br>God gave her a vision in 2015 to write all her songs, Betty McCoy Hawkins wrote these songs and didn’t realize that God was giving her songs of comfort to prepare for the death of her daughter who was a member of United. She never lost her praise and never lost her faith. She continued to pursue God’s vision in the midst of her trials and recorded her next two singles, “Faithful God” with its up-tempo Latin gospel sound, and “A Change is Coming” an anointed song of encouragement, which was released in June 2016. <br> <br>  <br> <br>In 2016, Betty Hawkins and United became a member of the PrayzeFactor Inspired Artists Movement and The International Music Association TIMA where she became an International Recording Artist performing on the Islands of Nassau, Bahamas. Betty Hawkins and United has won numerous of gospel music awards including, Best Praise and Worship, Traditional Artists, Best CD for Group/Duo and many more. In the last 3 years, they have performed on many platforms, such as the Texas Gospel Music Excellence Awards (Pastor Larry Davies), Prayze Factor Inspired Artists Awards (Pastor Teresa Jordan), Free Style Worship Movement (Annilia Wright- Mosley), and many more. <br> <br>​ <br> <br>Recently, Min. Hawkins has released a new single, “I Believe” which was written and produced by Joey Oscar of JOMOCO Music.","","bettyhawkinsandunited","","","1","0","0","1593111930","2020/6","0","0.00","0","","1","1","1593114694","0","","","","1","1","1","0","0","1","1","0","","0","","","","0","0"),
("28","gharris817","gharris817@hotmail.com","107.77.200.59","$2y$10$txR0cpoPnlLl9mtbdjJczeFesku33bOAMdsDm/T7l2WhDkAKhBXhq","G.HARRIS817","male","302b9887d9e76916038806d2b09fc96c23ade5ac","english","upload/photos/2020/06/ieq33vykiOOFj6XLhBLx_26_2363c65a96cc0d188017ae327710289c_image.jpeg","upload/photos/2020/06/hMq1qqQaoqAXcbMuL5l2_26_3788ba75083aff500c1f161bece9a9b8_image.jpeg","","1","0","Artist: G.HARRIS817 <br>Cellbloccheaven International LLC  <br>Kingdom Rapper <br>Kingdom Rapper,Singer,Best Selling Book Author,Philanthropist, Preacher, Spoken Word Artist, Prison Ministry, Several Projects on major distributions worldwide","","Www.facebook.com/g.harris817","","","1","0","0","1593113362","2020/6","0","0.00","0","https://gharris817.org/","1","1","1593181713","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("29","DJSermon","iamkaranichole@gmail.com","99.203.213.202","$2y$10$snmlvifsWBx9ZNGG7dNbdeUOd38.CcuX6gtHz4xcRgNB1qQj43kpm","DJ Sermon","male","e7d6d6b79a7b96922339b3ce85d5f728a1564545","english","upload/photos/d-avatar.jpg","upload/photos/d-cover.jpg","","0","0","","","","","","1","0","0","1593113878","2020/6","0","0","0","","0","0","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("30","Loujams","louisemsmith7@outlook.com","174.111.6.141","$2y$10$AG8Q4RYw5pCSo9jcR0hvEeJbvyljvNM49H78mfaNbY9.f.yorl6zq","Louise Smith","female","58e8175112f5fe832d6ef314226e1e3df98d73f5","english","upload/photos/2020/06/BHVPoC9pY5p4Fh9MUMNK_25_a2031bdb6c32337849d6dda727d1b98f_image.jpg","upload/photos/d-cover.jpg","","1","65","Louise Smith was meant to sing and play the piano! She was born to write a different kind of song. Her mother declared she used to sing in the crib, and when she was a toddler to a young child, she would bang on flat surfaces as if it were a piano.  By age 5, she was given a 22-key organ by her parents.  Before she turned six years old, she would make up little songs.  Then one day after kindergarten, she played her 1st song which was a hymn she heard in church Sunday after Sunday, Just as I Am.  Her skill grew by leaps and bounds.  By the time she was seven, she was writing songs and singing them in her home and church.  Her talent was strong enough for her parents to upgrade her small starter organ to a spinet 88 key piano. After turning seven, she started playing for her Sunday School choir.  Then the unimaginable happened.  She was asked to teach the senior choir vocal parts and to play for the church. <br>She knows she was called by God to sing, write, and play piano.  Music is her life and music saved her life. At the most tumultuous times with sickness, clinical depression, and low self-esteem, God soothed her with some of her music mentors such as Danibelle Hall, Andre Crouch, Tramaine Davis-Hawkins, Roberta Flack, Aretha Franklin, Patty Page, Karen Carpenter, and Ann Murray. God cultivated her voice through all the men and women who ministered to her. <br>Louise loves gospel music most but has a jazzy accent; of what she calls JAZZ-PEL.  She is typically a balladeer, romantic God songwriter, and vocal stylist. She is a hymnologist and arranger. <br>Louise has been writing songs well over 50 years.  She has recorded three original albums of her own, as she has recorded with countless recording artists, to include The New Jersey Mass Choir with Donnie Harper, Derrick Brown and W.O.R. (Weapons of Righteousness), trumpeter extraordinaire Husband Linton Smith, and Alfonzo Udell and the Chosen Chorale.  She also has a modest school of music of which she teaches voice and piano.  She is married to trumpeter sensation Linton Smith, JR.  <br>The purpose of the Songs Louise Smith writes is Inspirational God music for listeners to enjoy, and gain a message of hope, peace, love, and healing. <br>The mission of this music ministry is to spread the Gospel through original music and rearrangements with vocal and instrumental expression in person and recorded music. <br>Louise’s music can be found on the world wide web and personal appearances with hard copies of her music. She is available to minister in any capacity of music or as a speaker.","","louisemsmith","","","1","0","0","1593123218","2020/6","0","0.00","0","http://www.seasonedmusic.org","0","0","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("31","Momperousse","taniasings19@gmail.com","139.138.83.37","$2y$10$a39u4aGTeFcfvvPLW0jZTOoA4vmhq6TmRE/c6.xMm2RICvTDzgvlS","Tania","male","74176899290693fa815e36a6e56654a522fd051b","english","upload/photos/2020/06/8XUNpIQf9ckH3TtvgMdl_26_787de66357b254bbef0f032173358992_image.jpg","upload/photos/d-cover.jpg","","0","0","","","","","","1","0","0","1593130622","2020/6","0","0.00","0","","0","0","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("32","onegoodknight","pushitmarketingandpromotions@gmail.com","65.27.140.238","$2y$10$lGs1gUX5OE/iyZsnsT8iQu0z2Qc7toCOuaeSThBuPOsLGFGk.XfLm","Bruce Knight","male","6e1917924ca17d7870657e84d1d0855b5f6d774e","english","upload/photos/2020/06/pHBGoFGA7NLYBLcAWpal_26_14378ac4827a8e227a6d6f45017f5062_image.jpg","upload/photos/2020/06/OlyBjHCYPgEiQTVLGQ9N_26_88b7b82270c8ac28e066040e7b68301f_image.jpg","","1","0","","","","","","1","0","0","1593146980","2020/6","0","0.00","0","","0","1","1593182368","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("33","tear2life23","matthewcg7@outlook.com","207.47.175.34","$2y$10$LoIc3moEa2x3ptBnZbJf6urSdurirm7XKQCHhN5nhJgKfAOxcO.vq","matthew cody guenther","male","2a3534b1f2576b94368c456ae56bc15427b66526","english","upload/photos/d-avatar.jpg","upload/photos/d-cover.jpg","","0","0","","","","","","1","0","0","1593151795","2020/6","0","0","0","","0","0","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("34","JLogan","joycea.logan@yahoo.com","75.142.97.251","$2y$10$VwN3fwjN5Z8Hdopj0vSLVOZoVcH5fFY5ejP8KgRgz9L961P4.lSye","Dave Shirley","male","512efff89b001c16f871d62c45646120789cad82","english","upload/photos/2020/06/SmmPtqoeHSlDdojGHqHC_27_01b2037eeed2fccda575ba619b7477b6_image.jpg","upload/photos/d-cover.jpg","","0","0","His powerful singing voice and acting ability earned him top billing in plays touring throughout the United States and in his travels,  he shared the stage and performed with renowned acts and artists such as Aretha Franklin, The Temptations, Stevie Wonder,  The Chi-Lights, The Mighty Clouds of Joy, Shirley Caesar, and Kirk Franklin to name a few.  Dave was a cast member of the production of “Precious Lord – The Untold Story of Gospel Music”, and has performed leading roles in numerous stage plays, films and television shows.  Some of his credits are “God’s Trying to Tell You Something”, “Inquest of Sam Cooke”, &quot; Is There a Preacher in The House”, and &quot;Uncle Fletchers Money&quot; Dave’s compassion for humanity inspire him to write. When asked what inspires him he speaks of the current conditions that exist in the world today with mass murders, homelessness, the decline in  human kindness and the toll it has taken on families and communities. His focus in the lyrics is to inspire and encourage others to show more love; to show that they care with a simple smile or an act of kindness, or simply saying “hello”. Billboard magazine once wrote that Dave Shirley’s music bridges the gap and inspires not just one genre but many genres of music and culture.","","Dave Shirley","","","1","0","0","1593225500","2020/6","0","0","0","http://www.daveshirleymusic.com","0","1","1593235202","0","","","","1","1","1","0","0","1","1","0","","0","","","","0","0"),
("35","Endtymemusicgroup","endtymemusicgroup@gmail.com","107.77.216.227","$2y$10$k51RlSNjrXx6MjmqFnvaSu6no4cQIFgjNL5aEXjwrZCdf41IcHFDa","Janes Jay Smith","male","49476c5a9c83201f11853e2c4343c97c42588092","english","upload/photos/d-avatar.jpg","upload/photos/d-cover.jpg","","0","0","","","","","","1","0","0","1593226174","2020/6","0","0","0","","0","0","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("36","gospelkid1230am","jonathan1stevenson@gmail.com","64.132.141.220","$2y$10$KG5WMsg7ygWSIujVoR5wUO1LKJoDjxaGfxP4Yd6ls81tWaM9V/gMK","Jonathan l Stevenson","male","5b9d94f47cfe6b301699d698c666537d22480298","english","upload/photos/d-avatar.jpg","upload/photos/d-cover.jpg","","0","0","","","","","","1","0","0","1593267074","2020/6","0","0","0","","0","0","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("37","Soulcompassradio","soulcompassradio@gmail.com","66.207.241.15","$2y$10$ouikdeTscpbcjio65VVmGu.0YP6s2vPnbu.Z/9VOLs5iHFWi6OO32","Tomeka","male","22d422c58efe9229d344f3cacd8ad23938b44025","english","upload/photos/d-avatar.jpg","upload/photos/d-cover.jpg","","0","0","","","","","","1","0","0","1593294170","2020/6","0","0","0","","0","0","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("38","Neva","ellanation85@gmail.com","67.10.36.102","$2y$10$wnaMcUFM.jqTItzmR1fKl.JtRlvUGEQTDXHOzRJ4kcHckIrFDvNLi","Neva Ford Nation","male","ea7a9c4199ea9e0031f2097322122c2d01d4e902","english","upload/photos/2020/07/1rYIuOarc33BAXPZtZ7d_03_ec5b9847d228501ad42b9b3fc998e5c5_image.jpeg","upload/photos/d-cover.jpg","","0","0","","","","","","1","0","0","1593391983","2020/6","0","0","0","","0","0","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("39","Hill","Shunice@sbcglobal.net","107.77.208.93","$2y$10$TsrieJxkEpZNtakunQVsEuVcH9.TzHquJwa50BuCwI7fgJA4P.ufy","Shunice","male","79f6c3350d3122de20e1ee49007e8152c2e77fb1","english","upload/photos/d-avatar.jpg","upload/photos/d-cover.jpg","","0","0","","","","","","0","0","0","1593432805","2020/6","0","0","0","","0","0","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("40","Thelma2020","rltaylorpss@yahoo.com","71.221.250.10","$2y$10$MMsv2..IXZB9LS1RJ75Ji.hgIQCjHFoY2MF/gJqUMQQQNWDt2xBXG","Rodney Taylor","male","6d69f9e177b22a0c95ce20c75d53a05316a20151","english","upload/photos/d-avatar.jpg","upload/photos/d-cover.jpg","","0","0","","","","","","1","0","0","1593578304","2020/7","0","0","0","","0","0","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("41","Soundsofimani","imanigroove@gmail.com","50.4.240.10","$2y$10$11qniQyCLmSmm.B3C7bgRuZGPy6mU6vcOhBzIpqK0gqwsaPeap2jC","Sounds of Imani","female","3665b8b857ead4d3f4a17832cde8099c1cb590b4","english","upload/photos/2020/07/1IfbRWN1Imq8DqG4e3Lk_03_baa24b08a497fa221abc04ebc5753d2c_image.jpg","upload/photos/2020/07/1z3VjuxqwKfcEGv4gKoe_03_b1bc61859b21c3a449b4487658dc4e78_image.png","","1","0","Sounds of Imani is a contemporary gospel recording artist","","@soundsofimanimusic","","","1","0","0","1593580112","2020/7","0","0.00","0","https://soundsofimani.com/","1","1","1593741082","0","","","","1","0","0","0","0","0","1","0","","0","","","","0","0"),
("42","eb51bec09","fa_386600555632757@facebook.com","","bd79983b21c773ddf09f7b0ecb7280ab8a1e09ff","Junaid Raza","male","6bab230f8ccb1c43126b53da93af3f81115ef3a1","english","upload/photos/d-avatar.jpg","upload/photos/d-cover.jpg","Facebook","0","0","","","","","","1","0","0","0","0000/00","0","0","0","","0","0","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("43","Sboyd","boydsal572@gmail.com","100.34.162.49","$2y$10$4qYj3Kwyqu7fKRrLePCqeOElaMD6.3Xji2FQ9l1dzWt6eDAATCHNC","So&#039;lo","male","0bfb9e57ac339969c7dbbfe969bc1424d938767d","english","upload/photos/2020/07/AzY3IVvOYUGjCnUq4NUC_06_02880e54750ef07e2b354b10ee495628_image.jpg","upload/photos/d-cover.jpg","","0","0","Gospel Inspirational Artist","","","","","1","0","0","1593822249","2020/7","0","0.00","0","http://www.solosalmusic.net","0","0","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("44","mr_AGRT","evereverett@yahoo.com","174.196.7.32","$2y$10$IUVe3tlV9GxRGFH7hbRA8.GVsqBGloUtIyFGIKxWT4ge3ZyOYQoqC","Everett Drake","male","91c5fc0040e4aadd2ea32486a886ab5098e2e9b6","english","upload/photos/d-avatar.jpg","upload/photos/d-cover.jpg","","0","0","","","","","","1","0","0","1593897739","2020/7","0","0","0","","0","0","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("45","hoops","chris.gilge@gmail.com","45.52.197.79","$2y$10$9V4zmWnQ/3Una.FSzy5L7..hkDxGS5pofZkSmLItpPGKApey57mfa","Chris Gilge","male","2af57f6515a657023205e63752a184b6068620fe","english","upload/photos/d-avatar.jpg","upload/photos/d-cover.jpg","","0","0","","","","","","1","0","0","1593903894","2020/7","0","0","0","","0","0","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("46","513db937d","bookmiztiffany@gmail.com","","22236f36ea4b69aa48b550540c7360b13eef3700","Tiffany Coleman-McGee","female","40722c4a170a2596a1526aeaf5da24aad59b68cd","english","upload/photos/2020/07/ilM5iMpmUH2vvtIWe9yj_05_1494046c5ce07cffead2fe9edd0ce097_image.jpeg","upload/photos/d-cover.jpg","Facebook","1","36","Multi-Award Winning / Stellar Award Nominated Music Artist, Model, Actress, Author","","@officialmiztiffany","","","1","0","0","0","0000/00","0","0.00","0","https://www.miztiffany.com","1","1","1593967790","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("47","4redmusic","4redmusic@gmail.com","35.133.215.156","$2y$10$7PvF4lvvE9XUAgJC67EvLe65MW6J.YGQGcJYUBlaKziHH5LFktiwe","Stephen Redmond","male","53ad7e231a6cda69aafd102023f79592f24b9f26","english","upload/photos/d-avatar.jpg","upload/photos/d-cover.jpg","","0","0","","","","","","0","0","0","1594410760","2020/7","0","0","0","","0","0","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("48","Samted10","believerslifestylenetwork@gmail.com","208.86.136.9","$2y$10$B8D79JXrNzm67zv6f4fC8eeN0BCnxB4JW8PogTVXe5IGE0kjOJ1Em","John Smith","male","114713a572e3b742cb871fab112ccc6e9794a182","english","upload/photos/d-avatar.jpg","upload/photos/d-cover.jpg","","0","0","","","","","","0","0","0","1594423647","2020/7","0","0","0","","0","0","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("49","Mandingo7","Unitedgospelnetwork@gmail.com","208.86.136.9","$2y$10$CaYq1J/k9.AyRn7znfKFueBpORs8syIR/9ot0J1qnhQZWPb8/FbMq","John Smith","male","46ff49e9fa6afefa4d90d4401f66fbd267868288","english","upload/photos/d-avatar.jpg","upload/photos/d-cover.jpg","","0","0","","","","","","0","0","0","1594423778","2020/7","0","0","0","","0","0","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("50","price","dureemusic@gmail.com","24.125.40.108","$2y$10$ZGCE7aQrhMJV0LDpv9PruuhX0kDVlKJvV5nzFMTEwcbvudLtSjexS","shanrae","male","020fe1f3fa86f3e1e86b0baf9a6c0abaeee40796","english","upload/photos/d-avatar.jpg","upload/photos/d-cover.jpg","","0","0","","","","","","0","0","0","1594426503","2020/7","0","0","0","","0","0","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0"),
("51","Calandra1","princessofpraise3@gmail.com","73.43.29.125","$2y$10$Wk/k8mVYltP6hd.CEiA/reQl292oAKBc3Rzs8LWuEcpkBpubwQEU2","Calandra Gantt","male","d0f74d4295040189f4d8d4f3fe5b5030d2165dd1","english","upload/photos/d-avatar.jpg","upload/photos/d-cover.jpg","","0","0","","","","","","0","0","0","1594515687","2020/7","0","0","0","","0","0","0","0","","","","0","0","0","0","0","0","0","0","","0","","","","0","0");

-- ---------------------------------------------------------
--
-- Table structure for table : `views`
--
-- ---------------------------------------------------------

CREATE TABLE `views` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `track_id` int(11) NOT NULL DEFAULT '0',
  `album_id` int(11) NOT NULL DEFAULT '0',
  `fingerprint` varchar(50) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `track_id` (`track_id`),
  KEY `fingerprint` (`fingerprint`),
  KEY `user_id` (`user_id`),
  KEY `album_id` (`album_id`)
) ENGINE=InnoDB AUTO_INCREMENT=821 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `views`
--

INSERT INTO views VALUES
("35","2","0","470a489574f18c33813e49553c8a4e779172aba1","0","1592713669"),
("36","2","0","6b7f5fb779186e9d7bc899f7fa08fe98d0fd41f8","0","1592713672"),
("38","3","0","96e07b2cc331f2d1fa591b1ec56b8d932714a52c","8","1592715676"),
("40","3","0","0d7af6a6b2d14903c42999b3903a8ed31adb49a3","3","1592715705"),
("50","7","0","96e07b2cc331f2d1fa591b1ec56b8d932714a52c","8","1592716121"),
("70","5","0","f9aa0df0344dd085f29b44afda085cba7fb961b3","0","1592721848"),
("71","5","0","56745dc667c9b9945b667a7e7a286bace84868da","0","1592721850"),
("72","4","0","287c13ec8c8a8d0a8282ac578dd23102224bb72d","0","1592721862"),
("106","4","0","6b7f5fb779186e9d7bc899f7fa08fe98d0fd41f8","10","1592726261"),
("115","5","0","532334cde73af78a409bca7b8e19af85b2aecbda","10","1592727118"),
("117","9","0","6b7f5fb779186e9d7bc899f7fa08fe98d0fd41f8","10","1592727143"),
("126","9","0","74fd71a5d9e82c1ed0dd07c8f4ca9c441c7c35ad","8","1592748885"),
("167","3","0","92dd032f022c7a2423e0969bb2dc32702a6087c3","0","1592819921"),
("168","3","0","b9d0251d1da6af34ecd04a40eb17e2208f242e80","0","1592819922"),
("169","3","0","bfce4c4576dfc45e76f7beb0059609aaa9db2f56","0","1592819964"),
("176","27","0","79930a35ae2075e90b85c8ba3a712680be6529bf","3","1592858529"),
("180","8","0","b19279b4e6317ae99adbc572c80ef8be4308b35b","3","1592862086"),
("182","4","0","1f32201e4b8c05c86ade2fad8a6c4c69fdb77e4f","3","1592864788"),
("183","2","0","edc8559099c60edb73c55d6136ff0d646ae90f73","3","1592865081"),
("193","2","0","a734fb470b81baa04f963c4806d1625497f75329","1","1592867365"),
("205","27","0","a734fb470b81baa04f963c4806d1625497f75329","1","1592883011"),
("217","3","0","a734fb470b81baa04f963c4806d1625497f75329","1","1592884569"),
("221","5","0","0d7af6a6b2d14903c42999b3903a8ed31adb49a3","3","1592884713"),
("243","3","0","47ba6d821e51d2dd737e4bc16e01966b98115934","0","1592914429"),
("244","3","0","32cd0bd027f63375729585b50a6dc9598d1fc4c3","0","1592914430"),
("249","3","0","5e76001104cdfa014b5651280b28bac009957185","0","1592928684"),
("250","3","0","a67d4b4f623a5ec96f751667e74362255852c18c","0","1592928687"),
("251","5","0","4ad5c29c7cdbf92fb46cc6cd7f98df7fd7edc524","0","1592928701"),
("252","5","0","a67d4b4f623a5ec96f751667e74362255852c18c","0","1592928703"),
("253","25","0","cebfc307eada0ef32bee356e74439493acf07825","0","1592931537"),
("254","25","0","a67d4b4f623a5ec96f751667e74362255852c18c","0","1592931539"),
("258","43","0","3582b621b5300dbf3ea5bc5c61754f5841d83017","16","1592938844"),
("262","9","0","0d7af6a6b2d14903c42999b3903a8ed31adb49a3","3","1592942949"),
("264","5","0","2ad3356db2630b664ec6ee108dbed6dea598319e","17","1592945877"),
("267","2","0","2ad3356db2630b664ec6ee108dbed6dea598319e","17","1592945902"),
("275","49","0","a67d4b4f623a5ec96f751667e74362255852c18c","16","1592963187"),
("277","54","0","7ba142dc3f157daa9ec9c1cd62e91accab2c8e42","3","1592969432"),
("283","27","0","82798b9a35a7ef276f85de8b1fb19c886b99602f","14","1593023615"),
("284","3","0","f3a4570162aa2d39537171bd5bf53ab4ed3b171c","0","1593024958"),
("285","3","0","f297b39ab3b056a856c2d87b822949ffcea06e73","0","1593024961"),
("289","58","0","727bfd174b3effb512843caddfb7c406dc84adf1","20","1593027132"),
("290","60","0","4912067ad1bea763b3001b73d5a66ce33221c5c0","0","1593028536"),
("291","60","0","2249bb6dd3179c7d9a02692bf27182e582dc51f6","0","1593028538"),
("292","4","0","e02f63485193bf0eaab7b5d6efffae1750f1266a","0","1593031608"),
("293","4","0","f297b39ab3b056a856c2d87b822949ffcea06e73","0","1593031610"),
("294","3","0","b6dc5b0951e2720a5d3a4b8dc51c3ade3b337db2","0","1593031874"),
("295","3","0","7aa89b71929f40f26198cbccf2ec6c624344c91e","0","1593031875"),
("300","60","0","28439c27d68c15ac420a6b8f8a8a7638061e3ccb","0","1593034832"),
("301","60","0","b257aab8dd1475dae6542e37b5e32497344c5a85","0","1593035048"),
("302","60","0","19158b18036fff9c7f5c94a3e7136cc11cf124bb","0","1593035051"),
("303","45","0","77de7cfe9ecc108c6e9d5d6c3dc47cbd629f024f","0","1593035922"),
("304","45","0","71eb98acd93bff46315a031263e9941e1a514f10","0","1593035923"),
("305","4","0","8c7f7ba450304c62a9812b23019986e6b8301386","0","1593035944"),
("306","4","0","71eb98acd93bff46315a031263e9941e1a514f10","0","1593035946"),
("307","2","0","89c8ca3eae632466ffad7b51adc17f0492202bd5","0","1593035982"),
("308","2","0","71eb98acd93bff46315a031263e9941e1a514f10","0","1593035983"),
("309","45","0","4389dc31847b5ea2525d244ec354c1dc65064afc","0","1593035986"),
("310","45","0","cf528460f48f31dbf8cb0995d2d254bf385fa3fd","0","1593036002"),
("311","59","0","ffce7949eaee136464ddffdbb6ef42ddc35890ba","0","1593037022"),
("312","59","0","08f78ffee7dc503a519f7eb229c526b652822170","0","1593037025"),
("314","60","0","0d7af6a6b2d14903c42999b3903a8ed31adb49a3","3","1593037180"),
("315","21","0","4956751075167fa4220488f16b4d9dd227806059","0","1593037196"),
("316","21","0","08f78ffee7dc503a519f7eb229c526b652822170","0","1593037199"),
("317","27","0","b477143f71f29c90318e89e64654ca16dd836350","0","1593037272"),
("318","27","0","08f78ffee7dc503a519f7eb229c526b652822170","0","1593037274"),
("319","8","0","be7f5e8a57de3ace7c7ba5fc2029900b61ec9387","0","1593045723"),
("320","8","0","b33a944119b027286319e7cc3abf59edc08621a6","0","1593045724"),
("321","8","0","2413a69d50e003f43b3b0d253ce485e6be7d670c","0","1593045727"),
("322","48","0","4ad731ce36160ceed447f460822faf6575fc4406","0","1593045811"),
("323","48","0","2413a69d50e003f43b3b0d253ce485e6be7d670c","0","1593045814"),
("324","60","0","61f6dc6aefee77699409a051b66567cf77a57023","0","1593046672"),
("325","60","0","67f8c00b30d5629051cd46f42a40aa817c61ca01","0","1593046674"),
("329","61","2","7b33edc921c6d9342f434ad7861d82b6f2216637","0","1593058361"),
("330","61","2","7ba142dc3f157daa9ec9c1cd62e91accab2c8e42","0","1593058363"),
("332","67","2","b9d0251d1da6af34ecd04a40eb17e2208f242e80","14","1593058734"),
("338","68","2","da0626d85f42d1003dafb6341f5bde23be9ddec2","14","1593059054"),
("339","69","2","da0626d85f42d1003dafb6341f5bde23be9ddec2","14","1593059054"),
("340","64","2","aeef5b2d0a79b7172d44ccf1a46ec4de228a3715","14","1593059056"),
("341","69","2","b9d0251d1da6af34ecd04a40eb17e2208f242e80","0","1593059056"),
("342","64","2","e098d72c23d78ac4858f24dfd12d1af4a4c56b98","0","1593059058"),
("343","68","2","bccb52e940cdf6b1a72ed168e1a1fd8264165f0e","0","1593059074"),
("344","68","2","b9d0251d1da6af34ecd04a40eb17e2208f242e80","0","1593059075"),
("345","67","2","a19cf179e5bae36ca19fe21b08673a6e3bbe76e3","0","1593059376"),
("348","69","2","ce91586df90dcf8516b4d69292e841e91b9b48c8","0","1593061040"),
("349","69","2","7ba142dc3f157daa9ec9c1cd62e91accab2c8e42","0","1593061128"),
("350","71","0","a8528292d49a1e2fb1b1f1db8c9246694273dd0a","0","1593062359"),
("351","71","0","7ba142dc3f157daa9ec9c1cd62e91accab2c8e42","0","1593062416"),
("355","45","0","eb51c980c82441f794ee885dc99a5f1292854977","3","1593064325"),
("356","60","0","82ffdd008e42cd91b7eebbd4b0dfbfa3efdf1559","0","1593072965"),
("357","60","0","be4881c44219306b567d49c99130713a6c95dfae","0","1593072967"),
("358","3","0","42ff6e4880dc97e952b0f7c479211a6da7521ac0","0","1593089760"),
("359","3","0","01f09e0a647a50e055073433f60cf165395223f8","0","1593089761"),
("361","5","0","b9d0251d1da6af34ecd04a40eb17e2208f242e80","14","1593097768"),
("364","45","0","09fa652954d861258dea2d5af5ace4a9f5035fe8","14","1593098278"),
("368","71","0","00152f683a5ee537a4378fa51430eb1332e3c777","0","1593104008"),
("369","71","0","ac06084992e804b10aace9382d65faab345da1dc","0","1593104010"),
("381","71","0","7185c1014b1d5d710ab4a3e7f684e6aea8ab263d","24","1593114395"),
("382","71","0","279da8ccd08813aa48412f5987d70a788c844db0","0","1593118400"),
("383","71","0","b3e290828b06ff16dd0e3aa8d2900a5f3eb2093e","0","1593118403"),
("384","59","0","9ea3510b55e6abae264aaa95f43c3861c9b7d625","0","1593119069");

--
-- Dumping data for table `views`
--

INSERT INTO views VALUES
("385","59","0","682dc68f2075c82d6d5a9449e260fc9f0689df2a","0","1593119819"),
("386","58","0","97739950d2ee19848571a2ee8e7a229d4cddad10","0","1593120578"),
("387","43","0","08b0fb2f13f0f89cf6d60fe6e3837ee206701382","0","1593120642"),
("388","26","0","4664b110cd47cffe841c2f01f831eb88682c874d","0","1593120715"),
("390","45","0","13752aa6096412ba325c6142836b0089d2b60a6c","30","1593124619"),
("394","61","2","13752aa6096412ba325c6142836b0089d2b60a6c","30","1593124746"),
("396","44","0","13752aa6096412ba325c6142836b0089d2b60a6c","30","1593124787"),
("398","9","0","13752aa6096412ba325c6142836b0089d2b60a6c","30","1593125109"),
("400","71","0","13752aa6096412ba325c6142836b0089d2b60a6c","30","1593125126"),
("402","3","0","13752aa6096412ba325c6142836b0089d2b60a6c","30","1593125157"),
("404","60","0","21485b8f28c299c842227ff470e87a2e0d52768e","31","1593130746"),
("405","48","0","ec1898f09f89f48075466d4f049019b3c8ffcdcc","0","1593130968"),
("406","48","0","1fa1def719abd905b5d7926e15f9f1e70c5f3a2e","0","1593130972"),
("408","78","3","13752aa6096412ba325c6142836b0089d2b60a6c","30","1593132249"),
("410","79","3","13752aa6096412ba325c6142836b0089d2b60a6c","30","1593132253"),
("412","82","3","13752aa6096412ba325c6142836b0089d2b60a6c","30","1593132257"),
("417","76","3","13752aa6096412ba325c6142836b0089d2b60a6c","30","1593132313"),
("418","83","3","209b941ffce4bbc33e059181337073c1fda98ea9","30","1593132314"),
("420","80","3","13752aa6096412ba325c6142836b0089d2b60a6c","30","1593132498"),
("422","87","4","13752aa6096412ba325c6142836b0089d2b60a6c","30","1593138121"),
("423","74","0","16df17f9c0707e46c2dc38473a4c25897da78b99","0","1593141111"),
("424","74","0","4836315e6b7716105e35f244df0691f8d0b10804","0","1593141112"),
("426","75","0","6214a826a787b349aa6b5bd6ea5422d4bc6c132f","3","1593142401"),
("428","74","0","7ba142dc3f157daa9ec9c1cd62e91accab2c8e42","3","1593142854"),
("429","59","0","496315af12f9271ab60d0eac3187b372e7fb12f6","0","1593143845"),
("439","96","0","c6340237362864459decd3d50da98361001310ab","30","1593146554"),
("440","75","0","53833bb8a6179fa31e5dd442535311a04e564c6e","0","1593146556"),
("441","75","0","4836315e6b7716105e35f244df0691f8d0b10804","0","1593146558"),
("442","26","0","5c1c0657c166cc420c67027c843593be123c825c","0","1593151566"),
("443","26","0","0d6fa83b985306d0a7e4756dd257a4cc83fb63b7","0","1593151569"),
("444","25","0","197a035ce4b94f7d4542aee6e868bcd6f939bb88","0","1593151575"),
("445","59","0","56b302edfc8d3d3b5b11b9464d868581d92a612d","0","1593151743"),
("446","59","0","750377f7594b40e21356389fbd76241efc57a66c","0","1593151746"),
("447","97","0","c902e702bc6804e34c2005d699306f2c4f0ef441","33","1593151926"),
("455","74","0","4a6b5137cf09a623c859403b445cad03863231d6","0","1593156425"),
("456","74","0","6190b13b0b09fab1b92f414e302e3f97978cfd23","0","1593156429"),
("459","107","0","676219990249b89bdef3e6375bcd9af3913aed5b","32","1593157027"),
("461","106","0","7ba142dc3f157daa9ec9c1cd62e91accab2c8e42","3","1593183249"),
("462","45","0","0e1e483d02f5f745a6f174544b88c41d61c6a7c0","0","1593184492"),
("463","45","0","1bf2133874311e13792a0b8c8fbd858fb3ed4bdb","0","1593184494"),
("474","72","0","c6a5a2bfcff3f457e9d68798faf1beb69a6b55ef","0","1593255903"),
("475","72","0","bab7212d66ca1b0f1543907a2bf7370080c48963","0","1593255905"),
("476","48","0","a113614db0ad6e54949c41b2346f411a8679b9a4","0","1593258678"),
("477","48","0","6cef9c20c80379f3b3961f42b525c2e37495f69f","0","1593258680"),
("478","2","0","7a70506cdb311a1085386281dbbf5a1011da09ee","0","1593258704"),
("479","2","0","6cef9c20c80379f3b3961f42b525c2e37495f69f","0","1593258705"),
("480","22","0","5c58e5e16a93be7a4c4d349989df336a1b2cf0c9","0","1593290330"),
("481","22","0","a40acb27d266a0cc7d078d0338cf789b98cbcafc","0","1593290334"),
("482","3","0","3e22b62e86fa7960e38f0c033634218e9084e97d","0","1593308714"),
("483","3","0","1747f3836819a21c759b2a1e5cf2e0fd4f2560b3","0","1593308716"),
("484","74","0","b8dc573e06f5069ce16f3e5e1ab57fcef7c384cc","0","1593308749"),
("485","74","0","1747f3836819a21c759b2a1e5cf2e0fd4f2560b3","0","1593308752"),
("486","60","0","0d87719d1d27efae1806d045726dc1f9377b7756","0","1593348727"),
("487","60","0","727bfd174b3effb512843caddfb7c406dc84adf1","0","1593348728"),
("488","71","0","1be2a9548050960276d4222ff72e1aab0eca2a1d","0","1593379774"),
("489","71","0","cc3b5c759dc1beb2bc9677749d5abacbf431273a","0","1593379775"),
("490","75","0","2d5202127e6962de7b5c1d0478fbf2f820284aa7","0","1593380074"),
("491","75","0","eda441ca3fa701476e8d00bd1c0dc6168dea0b03","0","1593380078"),
("492","3","0","8239840104276c4075e070a6d6e969de2edae83a","0","1593391916"),
("494","45","0","5cdeb8fab40f6f5434faf90dccd92d251f465077","0","1593393746"),
("495","45","0","97ed659d74bffb1ef02baf1e761b5a3991f0b6c6","0","1593393747"),
("496","111","0","1c91b53850cd93b9a931e849d4285ae57cd12e28","0","1593393850"),
("497","111","0","97ed659d74bffb1ef02baf1e761b5a3991f0b6c6","0","1593393851"),
("498","47","0","e561deb5e3afff18a30144ce2b86a811d3efd414","0","1593394114"),
("499","47","0","97ed659d74bffb1ef02baf1e761b5a3991f0b6c6","0","1593394115"),
("500","99","0","edacccd3e28f710a58143bafe1f8c25d6687b5df","0","1593397998"),
("501","99","0","6cef9c20c80379f3b3961f42b525c2e37495f69f","0","1593397999"),
("502","45","0","d809de69029bc004b9af6706a1c19e6f07100dfc","0","1593398025"),
("503","45","0","6cef9c20c80379f3b3961f42b525c2e37495f69f","0","1593398026"),
("504","2","0","75897f67874a733877c891cd48ca96f4fd235275","0","1593398048"),
("505","2","0","a2158b554da23ccbf18ab9bd86eeaaa73d3b12c9","0","1593398114"),
("506","5","0","cdb392934ecbee0d4a797d3256d30a2a3f35e43b","0","1593398127"),
("507","5","0","6cef9c20c80379f3b3961f42b525c2e37495f69f","0","1593398128"),
("508","5","0","b13479f4e0d02b43a67d8a7d915993c4a75eba35","0","1593398140"),
("509","2","0","a079a91dc20aded84f5526974fc7e751dc7b7325","0","1593398146"),
("510","2","0","f0668e6f37f390811c78b08dec996fa97ae58a19","0","1593398189"),
("511","2","0","1931145afc5a57130ee0aa64d4ccfea95e9360e2","0","1593406638"),
("512","2","0","082fac6c406f19a848f20d4f3ff37920c5e204e6","0","1593406639"),
("513","110","0","2f7c4fb641f1a62389605225719f9fd92291f9ba","0","1593471814"),
("514","110","0","6bdad3a5b89357c15d911ca40b3b9d63db839267","0","1593471816"),
("515","2","0","a057278da0478416325f5483e83caa3b7d3b6e83","0","1593476978"),
("516","2","0","905b08ddab78a9e75d33d7aefd61f771d7605744","0","1593476981"),
("517","99","0","563c1ed52b233b340cac2e437e1abda1d3fd2602","0","1593485173"),
("518","99","0","4836315e6b7716105e35f244df0691f8d0b10804","0","1593485175"),
("519","110","0","5462e538856f404f79da4f05e1f09f0876d12e73","0","1593572705"),
("520","110","0","187ede980ed756d0d9023bdf9d8dde5ed73b1a1d","0","1593572706"),
("522","6","0","96e07b2cc331f2d1fa591b1ec56b8d932714a52c","8","1593576397"),
("525","8","0","9d97214eaf4d3b75b1062a82e7838a7eafe60c31","8","1593577685"),
("527","107","0","a67d4b4f623a5ec96f751667e74362255852c18c","16","1593578883"),
("528","116","0","2b1200496a2d1985682c0dd16fe1fe3f8424e1cf","0","1593580210"),
("529","116","0","5455ebf0c8a76df7b6b7714d823f5c9cb00e069e","0","1593580212"),
("531","70","2","85102b955565f1fd928121a1acad31b475e92f95","14","1593641746"),
("533","62","2","bab7212d66ca1b0f1543907a2bf7370080c48963","14","1593641758"),
("534","107","0","2fe3f020babe8d0f57a05437c38ecd2531d46cdb","0","1593655000"),
("535","107","0","55095ac9faaf56053ab86c5658185b9bb92f8070","0","1593655004"),
("536","103","0","4977b673667567006052d96804b071cd28c69259","0","1593655101"),
("537","103","0","55095ac9faaf56053ab86c5658185b9bb92f8070","0","1593655103"),
("538","103","0","0cbb653076d167f34f51fce207a4cf6760070f99","0","1593655299"),
("539","102","0","db037b5c910ea0b95ef985cb8efe607aee6db8d3","0","1593655341"),
("540","102","0","55095ac9faaf56053ab86c5658185b9bb92f8070","0","1593655343");

--
-- Dumping data for table `views`
--

INSERT INTO views VALUES
("541","103","0","9a1bf8e68343142b00bff80d45fe82f2e8ab33d3","0","1593655446"),
("542","98","0","9453b9e68c41ac415dddbebca322308b2607d197","0","1593655484"),
("543","98","0","55095ac9faaf56053ab86c5658185b9bb92f8070","0","1593655488"),
("544","98","0","676316efca76604e21ce4818ad1a120a6d5a9152","0","1593655527"),
("545","103","0","975276f9219bc6c3d4efb3eb13207486eb4ccf64","0","1593655698"),
("546","98","0","f3d897bfb30d395c3ed5f77aeb5c29873f7a092e","0","1593655722"),
("547","103","0","64d44be6039dc28599bf9d267a2033e31f4b09b2","0","1593655730"),
("548","103","0","49f7dd3119e7ac7cc9464c8932b7d2c6fcf6611a","0","1593655739"),
("549","103","0","e12c4a520d0eb78d30e4e925f5f10fb68570bd29","0","1593655762"),
("550","103","0","c5c8322935988ee6a0c4c30b08c32a1507ccbde5","0","1593655791"),
("551","103","0","745761d79bbdb9080b72793778ec3f7f85d32682","0","1593655808"),
("552","103","0","8f96a70f087ede713e1c3263c947620f6e3f0afc","0","1593655829"),
("553","103","0","87d83db3131c82b49fb4495d0e0c2d32fbb6b8c2","0","1593655841"),
("554","103","0","bb59155affa71b82c19c9b3d42c25bd5441133ee","0","1593655852"),
("555","103","0","9b014595a9103eebb4cd8f9e068234b682f08b49","0","1593655864"),
("556","103","0","e1b0cfb41b76d52f446c3ee4f633abc308e9ed38","0","1593655874"),
("557","103","0","c1519e8c0e55e221e495ebfcb364c79333d1694f","0","1593655884"),
("558","103","0","a8c2aa4852e901bc9d56d3655d3c2939120fb944","0","1593655892"),
("559","103","0","c6978827f9ea56cb38e4026a264cb9cfebafb692","0","1593655915"),
("560","103","0","a5c8437cbe807260b282e417309bb2962b08545c","0","1593655930"),
("561","103","0","62ff8d7136dc4815cf689a6389701402a1cbae65","0","1593655939"),
("562","103","0","e1b393265de90aa400325572fa3a66883f46a98f","0","1593655949"),
("563","103","0","0bcd37d7c009fa9923c33c934d69978eb1941cd4","0","1593655956"),
("564","103","0","61db77a372eac17959609e5424697fef43071d4d","0","1593655963"),
("565","103","0","219be10a8996aa27edeba86fd5454e7b22d1a86a","0","1593655970"),
("566","102","0","d6327bad6933415abe585c5e66e3a1d503e279c7","0","1593655995"),
("567","103","0","496f0b533005811133c33505f10a94ae904066f0","0","1593656008"),
("568","103","0","5e14654b8b11b462bca57533cefb91609e1c1071","0","1593656012"),
("569","103","0","6080a9d7383e7d9d6aa6001d95726fce686beee4","0","1593656029"),
("570","103","0","74aa8e556cf041255a79cb616dcf411057c0497d","0","1593656039"),
("571","103","0","9cf54451aed1554784ec9aea68aa46053b60fc9d","0","1593656047"),
("572","103","0","29b052278528758add08d6d0669032a0107dcfa0","0","1593656057"),
("573","103","0","a432f710fc8ba93408d40c7f13b56be918cfddc4","0","1593656069"),
("574","103","0","d6a983d051da803a1bb982c647d21980c4e3d670","0","1593656076"),
("575","103","0","c95528c23f1c551fd50af4ceba180f290b223d5f","0","1593656175"),
("576","103","0","5d5cd4678d9038ef5cf7f350f02e50f8be20b52a","0","1593656180"),
("577","103","0","d4603ee2fe297c304137c6403d40f9cfac832425","0","1593656191"),
("578","103","0","d93f198d5a86f447e8740e7618ecfbac62cc6795","0","1593656202"),
("579","103","0","7de0a80ba47875769e313387913e7abd818da240","0","1593656209"),
("580","103","0","d5e170e5ebd4666cb8e0885c28242aaabf335e61","0","1593656353"),
("581","103","0","932d83f6f4cdf44a622b095398522cfc43399589","0","1593656364"),
("582","103","0","0b00b5b5056c6ac777c2c3fa23ca7de486922723","0","1593656376"),
("583","103","0","adecf188dc6c7676f748d0d1d9553eac755cf488","0","1593656384"),
("584","103","0","5f5ee1541c39a2ba05f922eaa080ea9143c49121","0","1593656391"),
("585","103","0","c88791b1cacb23f78883c86f5315a20e0c69518f","0","1593656400"),
("587","117","0","8f555bcb41bcf2fa4680cfdfd6a362afc878e8ed","17","1593719690"),
("591","118","0","7c3b28a75b28588c01cdf9127c249d59e23185ba","41","1593747578"),
("593","3","0","e2c433dce1564bd425885612efcebfbb2b15401d","38","1593748634"),
("595","110","0","9358e52052136a9888c61ea5b716418c471602ca","21","1593783942"),
("597","109","0","9358e52052136a9888c61ea5b716418c471602ca","21","1593784292"),
("598","60","0","d008978a5325dfaf5ab47b4688fec35d2c29df2b","0","1593796815"),
("599","60","0","14a7d47e16657a5251d8755f3d59786260a09653","0","1593796816"),
("600","71","0","c398c055861584782cc910585fdf389e818cd881","0","1593817916"),
("601","71","0","e1838c0bcd28515bd369d0284d9e8618c775b990","0","1593817918"),
("604","45","0","95a2e6084cb50335428a254e20960cab9118d48e","43","1593825734"),
("606","4","0","2f0265808599b95b31f696f50ffbc7706e20923f","43","1593826790"),
("607","3","0","8076468d234f91f2e1fe26ef9ab933edbb190367","43","1593826824"),
("608","5","0","1accaa857f6d86c83c3160ffa51d95d3ad86ae13","43","1593826852"),
("610","74","0","b7ffb7bbd9bb4920564dc40e33b906608a419b79","43","1593826882"),
("612","117","0","f749252bf6c4077814fa75bc641f85b0c293af3e","43","1593856780"),
("614","118","0","f749252bf6c4077814fa75bc641f85b0c293af3e","43","1593856804"),
("623","103","0","7ba142dc3f157daa9ec9c1cd62e91accab2c8e42","3","1593913715"),
("624","116","0","25c21d332a7bd8ca1a22f772fecb5057464beb17","0","1593996685"),
("625","116","0","4e1a25f36eb771adfe9667009759de8ff51f6aec","0","1593996686"),
("626","27","0","ea8852b37cbc20bba9eae08c6cb554f422bd6a82","0","1593997141"),
("627","27","0","4e1a25f36eb771adfe9667009759de8ff51f6aec","0","1593997143"),
("628","99","0","2ac21219ccb44c248f4efc8937076ddaa537fe3f","0","1594005432"),
("629","99","0","95a71e230c614963c11bc355481fc452402d157b","0","1594005437"),
("630","103","0","f4603d25ee90b34c8ee455b51904054805e7be33","0","1594009656"),
("631","103","0","5da871b52b48a2b635cbd9dce1438edf7f3de983","0","1594009659"),
("632","117","0","3265fa2865ca3b4cf4b947f0fb500306b6e78d73","0","1594009859"),
("633","117","0","5da871b52b48a2b635cbd9dce1438edf7f3de983","0","1594009868"),
("634","117","0","cab18907dcbf5cc77148b801cab6010f79bd905a","0","1594010128"),
("638","23","0","aa100d7ab6a6ce6ae546d89f6477c1eb57826e98","10","1594014932"),
("640","21","0","aa100d7ab6a6ce6ae546d89f6477c1eb57826e98","10","1594014937"),
("644","22","0","aa100d7ab6a6ce6ae546d89f6477c1eb57826e98","10","1594014964"),
("646","26","0","aa100d7ab6a6ce6ae546d89f6477c1eb57826e98","10","1594014973"),
("648","20","0","ea748d32c97f5e8210c331e246bae1074b2b344c","10","1594014997"),
("649","25","0","29bbd49b2e476302aa8336035fdefae9ea9a2fb8","10","1594015025"),
("665","116","0","f749252bf6c4077814fa75bc641f85b0c293af3e","43","1594092327"),
("667","119","0","f749252bf6c4077814fa75bc641f85b0c293af3e","43","1594092332"),
("669","120","0","f749252bf6c4077814fa75bc641f85b0c293af3e","43","1594092339"),
("671","121","0","f749252bf6c4077814fa75bc641f85b0c293af3e","43","1594092344"),
("673","118","0","60e3096a3a8d7b3e35b9d726b2ecf55272aaa696","1","1594197922"),
("675","119","0","60e3096a3a8d7b3e35b9d726b2ecf55272aaa696","1","1594197927"),
("678","119","0","1b564f018e7ce56f46bdf994b954c00871d836fe","0","1594324006"),
("679","119","0","a443cf473311cdbc7b1777e96af27f292982e945","0","1594324007"),
("680","117","0","2b1162f0679be1b7f7b36554e8fdf8902b37a8ed","0","1594324113"),
("681","117","0","5046de6402c4b72f6a72b52bdb5c0f91ecadf114","0","1594324115"),
("682","103","0","2896f97af39a7237f22d6e18a6fbf47a4f1ce99a","0","1594324177"),
("683","103","0","5046de6402c4b72f6a72b52bdb5c0f91ecadf114","0","1594324178"),
("684","116","0","6fbd03c6e813c50d0f66f4c7ccd301592189793d","0","1594324226"),
("685","116","0","5046de6402c4b72f6a72b52bdb5c0f91ecadf114","0","1594324227"),
("686","60","0","3bede885536c409f1e22ac3b7946a95a476be6a9","0","1594327928"),
("687","60","0","7ba142dc3f157daa9ec9c1cd62e91accab2c8e42","0","1594327949"),
("688","120","0","d004b0337a788f1f9b3e90ceed99280d857afc9f","0","1594328343"),
("689","120","0","29754df346e2c059a1bd96c254682d83e17bfe3c","0","1594328344"),
("690","117","0","2a8c2a3470b99cd53d750eeea7c4408737c02f2a","0","1594353247"),
("691","117","0","10335a973f9b8366bfcd71647df829c3374655f1","0","1594353249"),
("692","117","0","65362be54b40b9eb973716fb35506cc531b0428e","0","1594372996");

--
-- Dumping data for table `views`
--

INSERT INTO views VALUES
("693","117","0","bdd4facb5888d6d8937e4f0852f6c0b49c9bd052","0","1594372998"),
("694","117","0","2372f2e5c0cb6d4964a63ac6237886142909d0f9","0","1594373045"),
("695","117","0","7ec7a1ccbbc6cf59b9c76e1ad64438c81f963f37","0","1594377414"),
("696","117","0","89fb48624171bca72e0bc5a45d7e366cbb675be4","0","1594377416"),
("697","60","0","94847404ecff800d158e81395d5c31f201e75ca3","0","1594377546"),
("698","60","0","89fb48624171bca72e0bc5a45d7e366cbb675be4","0","1594377547"),
("699","120","0","079d896198e2055c10feb4bb3b88587edb36f74f","0","1594377680"),
("700","120","0","89fb48624171bca72e0bc5a45d7e366cbb675be4","0","1594377685"),
("701","71","0","e52ad8bb656c8d08c739e75329e2b5266e2117a3","0","1594377738"),
("702","71","0","89fb48624171bca72e0bc5a45d7e366cbb675be4","0","1594377739"),
("703","103","0","abf949d8e6eb3ddad00b3e2866252c07b975e4c4","0","1594377930"),
("704","103","0","89fb48624171bca72e0bc5a45d7e366cbb675be4","0","1594377936"),
("705","119","0","ef9a2b9f6083657ee627724c62ada3111cae9f19","0","1594420158"),
("706","119","0","6624d113e637ca61066bba8f1d94134854be1c2f","0","1594420160"),
("707","119","0","a985a9363d3ea01754f7d62f896c0de48cd82889","0","1594426792"),
("708","119","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594426793"),
("709","120","0","7bc2124b39336f9b74ffb49e0212715d6a487c43","0","1594426878"),
("710","120","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594426879"),
("711","100","0","fa1b83798b8dbee06ac7610d6749aca473cb6051","0","1594426919"),
("712","100","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594426920"),
("713","99","0","7abbeab8dd777d0bc25d954eef9c930299f68567","0","1594426948"),
("714","99","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594426950"),
("715","121","0","984ac33a4863e28a58357b9a546135ef6d8bc8a0","0","1594427027"),
("716","121","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594427028"),
("717","104","0","9d8226424434cbc45d6d29075a282d7814fe54a1","0","1594427083"),
("718","104","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594427084"),
("719","117","0","a5599c18704d570832f46c227d031bb12381f627","0","1594427108"),
("720","117","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594427109"),
("721","83","3","3f699de80691ad047d4d822de7134ad0f77dfc0b","0","1594427147"),
("722","83","3","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594427148"),
("723","80","3","40f515a3cf07b6f322e7b323460e3af7006ced34","0","1594427154"),
("724","80","3","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594427155"),
("725","64","2","d94300181441f3677249aa6664ef122e80cab09d","0","1594427179"),
("726","64","2","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594427180"),
("727","65","2","7e7a5707ea9fbced1b7675eb337a79b40d8a593b","0","1594427231"),
("728","65","2","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594427232"),
("729","66","2","bc007d453db0072b5427e9830a82b96976382be4","0","1594427246"),
("730","66","2","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594427247"),
("731","61","2","1a9dcfd2421affcb93598bf6ca085ca63edb233f","0","1594427260"),
("732","61","2","116fe4e317edc39619bb85411c04b58483d5df91","0","1594427261"),
("733","61","2","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594427261"),
("734","3","0","578876ca9f66fdca528790720f39b05f4c3152ec","0","1594427308"),
("735","3","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594427309"),
("736","44","0","5c199e1a2adf941375dbfd0caa639540e4136bfd","0","1594427353"),
("737","44","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594427354"),
("738","5","0","51618964c88712aeb4f124963f84390b22b0a7a0","0","1594427375"),
("739","5","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594427376"),
("740","46","0","f26bf66191a8e8880c1af5fe71216dcb6bfa5f83","0","1594427402"),
("741","46","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594427403"),
("742","114","0","1b411ad0a4a114616167c3e2ed5eca29be219299","0","1594427447"),
("743","114","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594427448"),
("744","103","0","5d21eead370b1bdd7c702f8618ae64827c90be54","0","1594427453"),
("745","103","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594427454"),
("746","107","0","343cbd655e4a10189d69249062dffb90b93f997e","0","1594427496"),
("747","107","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594427497"),
("748","47","0","69485c7a99aa8eeefd65c37fadda8acc514b21a9","0","1594427545"),
("749","47","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594427546"),
("750","101","0","6d83885ed5c8295bbabbff051c95658192fa74cc","0","1594427576"),
("751","101","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594427577"),
("752","98","0","86a0b8f7e26f2d85d3e632205a9e8bcbd8cb4778","0","1594427619"),
("753","98","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594427620"),
("754","106","0","f3b8c107d1528965d1b56f9a7cfc409530f3d0c5","0","1594427685"),
("755","106","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594427686"),
("756","102","0","fa5f996b1c1cb57e490ee8125156e6c3b5cdacc7","0","1594427726"),
("757","102","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594427727"),
("758","48","0","586d2dab427047ab7eda973491dd0775411629a3","0","1594427759"),
("759","48","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594427760"),
("760","102","0","d48430ec2f3fd09b0b2c95b5ccbcdaa0a4474ed1","0","1594427897"),
("761","105","0","c7166fec359c33a309d9605a73cddbde22de2502","0","1594428090"),
("762","105","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594428091"),
("763","107","0","6fce5dc24f341d87922485bf8cfa61ba86131c6c","0","1594430698"),
("764","101","0","39f27be989b4ddfbbcd7472adf2722d7375bb128","0","1594431105"),
("765","106","0","debbdeb275506c352065cf7a0464278a685aed2d","0","1594431116"),
("766","98","0","c340a7ea99c7fa14fa5a8981c4a84643a82eb63e","0","1594431418"),
("767","98","0","90d1fd20870aabb197dbe9aa6010ba28d5838b7b","0","1594431480"),
("768","101","0","4667df3520f8e3deabcb0f9dae87427db9c3a5d7","0","1594431830"),
("769","102","0","9fc9b1f74ccefd6be9bd6a6f3206dda154a3df61","0","1594432157"),
("770","48","0","d8c5979b00cad0c37d7c7b4960fef5c92e453e17","0","1594432333"),
("771","48","0","abb682c36dbba7df70c5e95e04dfe6842979c4dc","0","1594432343"),
("772","114","0","12e4c2c5eff28e23a49918e39ffc0d9dddd8f079","0","1594432618"),
("773","47","0","437d9f219bf3ce674ff3f839d321ef041484aeb3","0","1594432639"),
("774","103","0","ba20c51b9252d3b4f154d3aece2eceda0a88f49b","0","1594432707"),
("775","117","0","84a693b4a08c9397b0596176851ed544c43044dd","0","1594432991"),
("776","111","0","85d4e9b4ff712aed51256f6b690f11afc4c36b92","0","1594433005"),
("777","111","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594433007"),
("778","20","0","34a672d3520df0aa334f18f691585b314e833316","0","1594433031"),
("779","20","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594433032"),
("780","3","0","4ef6b5e08e91008b462cc7db9b5af0c2edea4c81","0","1594433065"),
("781","3","0","3589c7750e6e5cd730c79e10098083e0518a5aae","0","1594433094"),
("782","74","0","ef4b820031fd0dfeb5fd7176d298a583d36d0656","0","1594433905"),
("783","74","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594433906"),
("784","2","0","74db8698270232cf4345b3bdf4a0c6efaf693ee3","0","1594433937"),
("785","2","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594433938"),
("786","5","0","07c2b29f4d274f097084a446583a5dae9095c6e7","0","1594434014"),
("787","120","0","08b1c9243c7c9f4597d3b8dfbdf2a8a9e0d1352c","0","1594434031"),
("788","119","0","571c24246f0ad12bb77ce8ccf39ea0e8be95ae2b","0","1594434319"),
("789","45","0","3094cd294795a83af0663cc0b99aad9f24324664","0","1594434360"),
("790","45","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594434361"),
("791","45","0","79478a741e65b53c41380703b3d9a54e424622a7","0","1594434363"),
("792","97","0","f3906385cc4b42e7fb71dbee160e4c827a70a39e","0","1594434401");

--
-- Dumping data for table `views`
--

INSERT INTO views VALUES
("793","97","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594434402"),
("794","121","0","834e8dfe05908e7f9c327e18d13da2e68b6ec3f3","0","1594434422"),
("795","49","0","e821bf53d26d5e0b2fee0b51cf255a6dc736a18b","0","1594434428"),
("796","49","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594434429"),
("797","117","0","43dd35cbdb7f9f40159b3fd61bca37aa411cc6f4","0","1594434445"),
("798","7","0","5a018011e7edbf9bfcd228e3aba63d72a48ecbc0","0","1594434448"),
("799","7","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594434449"),
("800","109","0","2aa97a7a38745deb905928d877aa5c59f084d70e","0","1594434472"),
("801","109","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594434473"),
("802","27","0","b93e6c0393ece3edccdeaddfc50aef2a9146fed5","0","1594434478"),
("803","27","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594434479"),
("804","105","0","64d32a1bbb07983e0345c2f8fd9343331f282f78","0","1594434494"),
("805","119","0","6330d70f4c354903203b7650408ac64235a2b683","0","1594434781"),
("806","75","0","4d236283707bd7a2f2dfb47a6fb2071ee09ba9d6","0","1594435090"),
("807","75","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594435091"),
("808","75","0","0f2954dcac0ebb102e15d679a3f1fd6bba6c3a02","0","1594435127"),
("809","54","0","e69010d9f883d865c34b4e3fa7d17e4319714080","0","1594435500"),
("810","54","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594435501"),
("811","96","0","a7eee5d4787a642ece3abbcb005ffcc2828114c0","0","1594436140"),
("812","96","0","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594436140"),
("813","88","4","01d39b38ed18512dfaa6853e3da97e0fb9c6624b","0","1594436179"),
("814","88","4","b6068dd85a285dc83cd44d4c9619e592dd44c93f","0","1594436180"),
("815","7","0","d7ea879e31da6fc7bdf904e93b4f8ce8b2e83104","0","1594436218"),
("816","7","0","bc4c2453941d22e14f276d39b017f26cc5fc1fc7","0","1594436245"),
("817","118","0","45f73e3c2fe444b37ecce24cedac286573097c29","0","1594490316"),
("818","118","0","9360b46f3c74a77c2b058048a38b40286d61b372","0","1594490318"),
("820","119","0","7ba142dc3f157daa9ec9c1cd62e91accab2c8e42","3","1594498628");

-- ---------------------------------------------------------
--
-- Table structure for table : `withdrawal_requests`
--
-- ---------------------------------------------------------

CREATE TABLE `withdrawal_requests` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `email` varchar(200) NOT NULL DEFAULT '',
  `amount` varchar(100) NOT NULL DEFAULT '0',
  `currency` varchar(20) NOT NULL DEFAULT '',
  `requested` varchar(100) NOT NULL DEFAULT '',
  `status` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;