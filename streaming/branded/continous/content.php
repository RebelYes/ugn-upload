<!-- player code -->   
        <div id="hap-wrapper">

            <div class="hap-player-outer">

                <div class="hap-player-holder">

                    <div class="hap-player-wrapper-left">

                        <div class="hap-playback-toggle-ex"><i class="fa fa-play hap-contr-btn-i hap-icon-color"></i></div>
                        <div class="hap-player-toggle"><i class="fa fa-chevron-down hap-contr-btn-i hap-icon-color"></i></div>
                     
                        <div class="hap-seekbar-inner hap-tooltip-item"> 
                            <div class="hap-progress-bg"></div>
                            <div class="hap-load-level"></div>
                            <div class="hap-progress-level"></div>
                        </div>

                        <div class="hap-player-thumb"></div>

                        <div class="hap-info">
                            <p class="hap-player-title"></p>
                            <p class="hap-player-artist"></p>
                        </div>

                        <div class="hap-center-controls">
                            <div class="hap-prev-toggle"><i class="fa fa-backward hap-contr-btn-i hap-icon-color"></i></div>
                            <div class="hap-playback-toggle"><i class="fa fa-play hap-contr-btn-i hap-icon-color"></i></div>
                            <div class="hap-next-toggle"><i class="fa fa-forward hap-contr-btn-i hap-icon-color"></i></div>
                        </div>

                    </div> 

                </div>    

                <div class="hap-player-wrapper-right">

                    <div class="hap-right-controls">
                   
                        <div class="hap-random-toggle hap-tooltip-item" data-tooltip="Shuffle"><i class="fa fa-random hap-contr-btn-i hap-icon-color"></i></div>
                        <div class="hap-playlist-toggle hap-tooltip-item" data-tooltip="Playlist"><i class="fa fa-list hap-contr-btn-i hap-icon-color"></i></div>

                        <div class="hap-volume-seekbar hap-tooltip-item">
                            <div class="hap-volume-bg"></div>
                            <div class="hap-volume-level"></div>
                        </div>
                        <div class="hap-player-volume hap-toggle-mute"><i class="fa fa-volume-up hap-contr-btn-i hap-icon-color"></i></div>

                    </div>

                </div>

                <div style="clear: both;"></div>
              
            </div> 

            <div class="hap-playlist-holder">
                <div class="hap-playlist-filter-msg"><p>NOTHING FOUND!</p></div>

                <div class="hap-playlist-inner">
                    <div class="hap-playlist-content">
                        <!-- playlist items are appended here! --> 
                    </div>
                </div>
                
                <div class="hap-preloader"></div>

                <div class="hap-bottom-bar">

                    <input class="hap-search-filter" type="text" value="filter...">
                    <div class="hap-sort-alpha hap-tooltip-item" data-tooltip="Alphabetic sort"><i class="fa fa-sort-alpha-desc hap-sr-bar-i hap-icon-color"></i></div>

                    <div class="hap-share-item hap-tooltip-item" data-type="googleplus" data-tooltip="Share on Google+"><i class="fa fa-google-plus hap-sr-bar-i hap-icon-color"></i></div>
                    <div class="hap-share-item hap-tooltip-item" data-type="tumblr" data-tooltip="Share on Tumblr"><i class="fa fa-tumblr hap-sr-bar-i hap-icon-color"></i></div>
                    <div class="hap-share-item hap-tooltip-item" data-type="twitter" data-tooltip="Share on Twitter"><i class="fa fa-twitter hap-sr-bar-i hap-icon-color"></i></div>
                    <div class="hap-share-item hap-tooltip-item" data-type="facebook" data-tooltip="Share on Facebook"><i class="fa fa-facebook hap-sr-bar-i hap-icon-color"></i></div> 
                    
                </div>

            </div>
			
			<div class="hap-tooltip"><p></p></div>  
       
        </div>  
           
        <!-- PLAYLIST LIST -->
        <div id="hap-playlist-list">

             <ul id="playlist-audio2">

                <li class="hap-playlist-item" data-type="audio" data-mp3="media/audio/2/main/Soundroll - Driving In My Car.mp3" data-artist="Soundroll" data-title="Driving In My Car" data-thumb="media/audio/2/hap_thumb3/01.jpg" data-share="http://audiojungle.net/item/driving-in-my-car/826142?ref=Tean"></li>

                <li class="hap-playlist-item" data-type="audio" data-mp3="media/audio/2/main/Soundroll - Speed Of Fire.mp3" data-artist="Soundroll" data-title="Speed Of Fire" data-thumb="media/audio/2/hap_thumb3/02.jpg" data-share="http://audiojungle.net/item/speed-of-fire/400088?ref=Tean"></li>

                <li class="hap-playlist-item" data-type="audio" data-mp3="media/audio/2/main/01.mp3" data-artist="Soundroll" data-title="A Way To The Top" data-thumb="media/audio/2/hap_thumb3/03.jpg" data-share="http://audiojungle.net/item/a-way-to-the-top/162200?ref=Tean"></li>

                <li class="hap-playlist-item" data-type="audio" data-mp3="media/audio/2/main/02.mp3" data-artist="Soundroll" data-title="Feel Good Journey" data-thumb="media/audio/2/hap_thumb3/04.jpg" data-share="http://audiojungle.net/item/feel-good-journey/119565?ref=Tean"></li>

                <li class="hap-playlist-item" data-type="audio" data-mp3="media/audio/2/main/03.mp3" data-artist="Soundroll" data-title="Fight No More" data-thumb="media/audio/2/hap_thumb3/05.jpg" data-share="http://audiojungle.net/item/fight-no-more/143561?ref=Tean"></li>

                <li class="hap-playlist-item" data-type="audio" data-mp3="media/audio/2/main/04.mp3" data-artist="Soundroll" data-title="Funky Boom" data-thumb="media/audio/2/hap_thumb3/06.jpg" data-share="http://audiojungle.net/item/funky-boom/127833?ref=Tean"></li>

                <li class="hap-playlist-item" data-type="audio" data-mp3="media/audio/2/main/05.mp3" data-artist="Soundroll" data-title="Pump The Club" data-thumb="media/audio/2/hap_thumb3/07.jpg" data-share="http://audiojungle.net/item/pump-the-club/166358?ref=Tean"></li>

             </ul>

        </div> 
        
    </body>
</html>