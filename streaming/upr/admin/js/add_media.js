$(document).ready(function() {

    var is_submit;
    $('#fgform').on('submit', function (){
       if(is_submit)return false;//prevent double submit
       $('#type').prop('disabled', false);//Disabled form inputs do not appear in the request
       $('#message-modal').modal('show');//preloader
       is_submit = true;
    });

    var path = $('#path'),
    path_field = $('#path_field'),
    path_upload = $('#path_upload'),
    title_field = $('#title_field'),
    artist_field = $('#artist_field'),
    thumb_field = $('#thumb_field'),
    thumb = $('#thumb'),
    thumb_upload = $('#thumb_upload'),
    thumb_default_field = $('#thumb_default_field'),
    thumb_default = $('#thumb_default'),
    thumb_default_upload = $('#thumb_default_upload'),
    thumb_quality_field = $('#thumb_quality_field'),
    download_field = $('#download_field'),
    download = $('#download'),
    download_upload = $('#download_upload'),
    link_field = $('#link_field'),
    target_field = $('#target_field'),
    share_field = $('#share_field'),
    limit_field = $('#limit_field'),
    id3_field = $('#id3_field'),
    start_field = $('#start_field'),
    end_field = $('#end_field'),
    rate_field = $('#rate_field'),
    custom_content_field = $('#custom_content_field'),

    gdinfo = $('#gdinfo'),
    folderinfo = $('#folderinfo'),
    ytsinfo = $('#ytsinfo'),
    ytpinfo = $('#ytpinfo'),
    shareinfo = $('#shareinfo'),
    dlinfo = $('#dlinfo');


    //file uploads
    path_upload.on('change',function(){
        if($(this).val()){
            var filename = $(this).val().split('\\').pop();
            path.val(filename);
        }else{
            path.val('');
        }
    });
    thumb_upload.on('change',function(){
        if($(this).val()){
            var filename = $(this).val().split('\\').pop();
            thumb.val(filename);
        }else{
            thumb.val('');
        }
    });
    thumb_default_upload.on('change',function(){
        if($(this).val()){
            var filename = $(this).val().split('\\').pop();
            thumb_default.val(filename);
        }else{
            thumb_default.val('');
        }
    });
    download_upload.on('change',function(){
        if($(this).val()){
            var filename = $(this).val().split('\\').pop();
            download.val(filename);
        }else{
            download.val('');
        }
    });



    $('#type').on('change',function(){

      var type = $(this).val();

      //hide
      path_upload.hide();
      title_field.hide();
      artist_field.hide();
      thumb_field.hide();
      thumb_default_field.hide();
      thumb_quality_field.hide();
      download_field.hide();
      link_field.hide();
      target_field.hide();
      share_field.hide();
      limit_field.hide();
      id3_field.hide();

      gdinfo.hide();
      folderinfo.hide();
      ytsinfo.hide();
      ytpinfo.hide();
      shareinfo.hide();
      dlinfo.hide();


      //show for all
      path_field.show();
      start_field.show();
      end_field.show();
      rate_field.show();
      custom_content_field.show();


      $('#fgform')[0].reset();
      $(this).val(type);//reset on type change so we dont get not used vars for different media in db table

      
      if(type == 'audio'){

        path.attr("placeholder", "Enter mp3 url");

        path_upload.show();
        title_field.show();
        artist_field.show();
        thumb_field.show();
        download_field.show();
        link_field.show();
        target_field.show();
        share_field.show();

    }else if(type == 'soundcloud'){

        path.attr("placeholder", "Enter Soundcloud url");
        
        limit_field.show();
        thumb_default_field.show();
        thumb_quality_field.show();
        download_field.show();
        share_field.show();

        shareinfo.show();
        dlinfo.show();

    }else if(type == 'podcast'){

        path.attr("placeholder", "Enter Podcast url");
        
        limit_field.show();
        thumb_default_field.show();
        download_field.show();
        share_field.show();

        shareinfo.show();
        dlinfo.show();

    }else if(type == 'youtube_single'){

        path.attr("placeholder", "Enter Video ID");

        title_field.show();
        artist_field.show();
        thumb_field.show();
        share_field.show();

        ytsinfo.show();
        shareinfo.show();
        
    }else if(type == 'youtube_playlist'){

        path.attr("placeholder", "Enter Playlist ID");

        limit_field.show();
        thumb_default_field.show();
        share_field.show();

        ytpinfo.show();
        shareinfo.show();
        
    }else if(type == 'folder'){

        path.attr("placeholder", "Enter Folder name");

        id3_field.show();
        download_field.show();
        share_field.show();

        folderinfo.show();
        shareinfo.show();
        dlinfo.show();

    }else if(type == 'gdrive-folder'){

        path.attr("placeholder", "Enter Google Drive folder ID");

        download_field.show();
        share_field.show();

        gdinfo.show();
        shareinfo.show();
        dlinfo.show();

    }

    }).change();


});

