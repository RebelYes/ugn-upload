<?php

include "libs/config.php";
include "libs/database.php";

$db = new database();

$query = "SELECT * FROM playlists";
$playlists = $db->select($query);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin panel</title>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="media/css/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="resources/syntax/shCore.css">

    <style type="text/css">

        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            vertical-align:baseline;
        }

        .table > thead > tr > th[class*="sort"]:after{
            content: "" !important;
        }

        .title{
            border: 0;
            background-color: rgba(255, 255, 255, 0);
            width: 100%;
        }

    </style>


    <script src="media/js/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="media/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="resources/syntax/shCore.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/rowreorder/1.2.0/js/dataTables.rowReorder.min.js"></script>

    <script>

        $(document).ready(function() {

            var table = $("#media-table"),
            tableObj;

            $("#media-table tbody tr").each(function(i, tr) {
                $(tr).attr('id', 'id'+i).prepend('<td>'+parseInt(i+1)+'</td>');    
            });  

            tableObj = table.DataTable({
                columnDefs: [
                    {
                        "targets": 0,
                        "width": "10px"
                    },
                    {
                        "targets": -4,
                        "width": "150px"
                    },
                    {
                        "targets": [-1,-2,-3],
                        "width": "10px"
                    },
                ],
                stateSave: true,
                columns: [
                    { name: 'index' },
                    { name: 'title', "orderDataType": "dom-text", type: 'string' },
                    { name: 'created' },
                    { name: 'edit' },
                    { name: 'duplicate' },
                    { name: 'delete' }
                ]
                
            });

            //https://datatables.net/examples/plug-ins/dom_sort.html (order from input fields)
            $.fn.dataTable.ext.order['dom-text'] = function  ( settings, col ){
                return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
                    return $('input', td).val();
                } );
            }

            table.on('click', '.delete', function(e) {//delete individual

                var parent = $(this).parents().eq(1),
                playlist_id = parent.attr('data-id'),
                index = tableObj.row(parent[0]).index(),
                title = $(tableObj.column( 'title:name' ).data()[index]).val();

                $('#delete-form').attr('action', 'delete_playlist.php?playlist_id='+playlist_id);

                $('#delete-modal').find('.modal-body').html('<p>Are you sure you want to delete playlist "'+title+'" ?</p>');

            });

            table.on('click', '.duplicate', function(e) {//duplicate individual

                var parent = $(this).parents().eq(1),
                playlist_id = parent.attr('data-id'),
                index = tableObj.row(parent[0] ).index(),
                title = $(tableObj.column( 'title:name' ).data()[index]).val();

                $('#duplicate-title').val(title);

                $('#duplicate-form').attr('action', 'duplicate_playlist.php?playlist_id='+playlist_id);

            });

            $('.title').on('blur', function () {//edit title

                var input = $(this), title = input.val();
                if(title == ''){
                    input.val(input.attr('data-title'));
                    $('#message-modal').modal('show');
                    return false;
                }

                var playlist_id = input.parents().eq(1).attr('data-id'),
                url = 'edit_playlist_title.php', data = { title: title, id: playlist_id};

                $.ajax({
                    type: 'POST',
                    url: url,
                    data: data,
                    dataType: "json"
                }).done(function(response) {
                    input.attr('data-title', response);
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR, textStatus, errorThrown);
                }); 

            });



        });

</script>


</head>

<body>

    <div class="blog-masthead">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="http://codecanyon.net/item/audio-player-with-playlist-v2/16698456?ref=Tean" target="_blank">
            <img alt="Brand" src="media/images/logo.png">
        </a>
    </div>
    <nav class="blog-nav">
      <a class="blog-nav-item active" href="playlists.php">Playlists</a>
      <a id="nav-item-generator" class="blog-nav-item" href="generator.php">HTML generator</a>
  </nav>
</div>
</div>


<div class="container-fluid">

  <div class="row">

    <div class="col-sm-12 blog-main">

        <?php 
        if(isset($_GET['msg']))echo("<div class='alert alert-success'>".$_GET['msg']."</div>"); 
        elseif(isset($_GET['msgf']))echo("<div class='alert alert-danger'>".$_GET['msgf']."</div>"); 
        ?>

        <table id="media-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>

              <tr>  
                <th>#</td>
                    <th>Playlist title</th>
                    <th>Created</th>
                    <th>Edit</th>
                    <th>Duplicate</th>
                    <th>Delete</th>
                </tr>

            </thead>

            <tbody>

              <?php if($playlists):?>

                  <?php while ($row = $playlists->fetch_array()) : ?>

                     <tr data-id="<?php echo($row['id']); ?>">  
                        <td><input type="text" name="title" class="title" data-title="<?php echo(htmlspecialchars($row['title'])); ?>" value="<?php echo(htmlspecialchars($row['title'])); ?>"/></td>
                        <td><?php echo($row['date']); ?></td>
                        <td><a href="edit_playlist.php?playlist_id=<?php echo($row['id']); ?>" class="btn btn-success"><span class="glyphicon glyphicon-edit"></span></a></td>
                        <td><button type="input" class="btn btn-primary duplicate" data-toggle="modal" data-target="#duplicate-modal"><span class="glyphicon glyphicon-duplicate"></span></button></td>
                        <td><button type="input" class="btn btn-danger delete" data-toggle="modal" data-target="#delete-modal"><span class="glyphicon glyphicon-trash"></span></button></td>
                    </tr>

                <?php endwhile;  ?>

            <?php endif;  ?>

        </tbody>
    </table>

    <button type="input" class="btn btn-primary" data-toggle="modal" data-target="#add-modal">Create new playlist</button>


</div><!-- /.blog-main -->

</div><!-- /.row -->

</div><!-- /.container -->

<div id="delete-modal" class="modal fade" tabindex="-1" role="dialog">
   <form id="delete-form" action="" method="post">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Delete playlist</h4>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
            <button type="submit" name="submit" class="btn btn-danger">Ok</button>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</form>
</div><!-- /.modal -->

<div id="duplicate-modal" class="modal fade" tabindex="-1" role="dialog">
   <form id="duplicate-form" action="" method="post">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Duplicate playlist</h4>
        </div>
        <div class="modal-body">

          <div class="form-group" style="max-width:500px;">
               <p>Enter new title for the playlist:</p>
               <input type="text" id="duplicate-title" name="title" class="form-control" required>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" name="submit" class="btn btn-primary">Ok</button>
    </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</form>
</div><!-- /.modal -->

<div id="add-modal" class="modal fade" tabindex="-1" role="dialog">
   <form action="add_playlist.php" method="post">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Create new playlist</h4>
        </div>
        <div class="modal-body">

          <div class="form-group" style="max-width:500px;">
              <input type="text" name="title" class="form-control" required placeholder="Enter new title for the playlist">
          </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
            <button type="submit" name="submit" class="btn btn-primary">Ok</button>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</form>
</div><!-- /.modal -->

<div id="message-modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
       <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Title cannot be empty!</h4>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
    </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


</body>
</html>