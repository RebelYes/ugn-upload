<?php

include "libs/config.php";
include "libs/database.php";

$db = new database();

$query = "SELECT * FROM playlists";
$playlists = $db->select($query);

$markup='';


if(isset($_POST['submit']) && isset($_GET['playlist_id'])){

    $playlist_id = intval($_GET['playlist_id']);
    $mediaArr = explode(",", $playlist_id);

    $groupArr = array();

    for($i=0; $i<count($mediaArr); $i++){

        $stmt = $db->prepare("SELECT * FROM media WHERE playlist_id=? ORDER BY order_id");
        $stmt->bind_param('i', $mediaArr[$i]);
        $stmt->execute();
        $media_query_result = $stmt->get_result();
        $stmt->close();

        $groupArr[] = $media_query_result;

    }

    $playlist_prefix = 'hap-playlist-';

    $markup = '<div id="hap-playlist-list" style="display:none;">'.PHP_EOL;

        for($i=0; $i<count($groupArr); $i++){

            $markup .= str_repeat('&nbsp;', 5).'<ul id="'.$playlist_prefix.$mediaArr[$i].'">'.PHP_EOL;

                while($media = $groupArr[$i]->fetch_array()) {
                        
                    $type = $media["type"];

                    $track = str_repeat('&nbsp;', 10).'<li class="hap-playlist-item" data-type="'.$type.'" ';
                
                    if($type == "audio"){
                        $track .= 'data-mp3="'.$media["path"].'" ';
                    }else{
                        $prefix='';
                        if($type == "folder"){
                            $prefix = '../mp3-dir/';
                        }
                        $track .= 'data-path="'.$prefix.$media["path"].'" ';
                    }
                    if(!empty($media["title"])){
                        $track .= 'data-title="'.$media["title"].'" ';
                    }
                    if(!empty($media["artist"])){
                        $track .= 'data-artist="'.$media["artist"].'" ';
                    }
                    if(!empty($media["thumb"])){
                        $track .= 'data-thumb="'.$media["thumb"].'" ';
                    }
                    if(!empty($media["thumb_default"])){
                        $track .= 'data-thumb-default="'.$media["thumb_default"].'" ';
                    }
                    if(!empty($media["thumb_quality"])){
                        $track .= 'data-thumb-quality="'.$media["thumb_quality"].'" ';
                    }
                    if(!empty($media["download"])){
                        $track .= 'data-download="'.$media["download"].'" ';
                    }
                    if(!empty($media["share"])){
                        $track .= 'data-share="'.$media["share"].'" ';
                    }
                    if(!empty($media["link"])){
                        $track .= 'data-link="'.$media["link"].'" ';
                    }
                    if(!empty($media["target"])){
                        $track .= 'data-target="'.$media["target"].'" ';
                    }
                    if(!empty($media["limit"])){
                        $track .= 'data-limit="'.$media["limit"].'" ';
                    }
                    if(!empty($media["id3"])){
                        $track .= 'data-id3="'.$media["id3"].'" ';
                    }
                    if(!empty($media["start"])){
                        $track .= 'data-start="'.$media["start"].'" ';
                    }
                    if(!empty($media["end"])){
                        $track .= 'data-end="'.$media["end"].'" ';
                    }
                    if(!empty($media["playback_rate"])){
                        $track .= 'data-playback-rate="'.$media["playback_rate"].'" ';
                    }

                    $track .= '>';

                    if(!empty($media["custom_content"])){
                        $track .= $media["custom_content"];
                    }
                    
                    $track .= '</li>';//end li
                    
                    $markup .= $track.PHP_EOL;

                }
                    
            $markup .= str_repeat('&nbsp;', 5).'</ul>'.PHP_EOL;//end ul

        }//end for  
            
    $markup .= '</div>';//end playlist list

}


?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin panel</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap-select.css">

    <style type="text/css">


      #type_field .btn-group{
          width: 250px;
      }
      .form-group label{
          display: block;
      }
      .form-group{
          margin-bottom: 25px;
          clear: both;
      }



    </style>

    <script src="media/js/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="js/bootstrap-select.js"></script>

<script>

  $(document).ready(function() {

        var type = $('#type'),
        generate_code_form = $('#generate_code_form'),
        playlist_code = $('#playlist_code');

        $('#generate_code').on('click', function(){
             var arr = type.val();  
             if(arr)generate_code_form.attr('action', 'generator.php?playlist_id='+arr.toString());
        });


    });

</script>

</head>
<body>

    <div class="blog-masthead">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="http://codecanyon.net/item/audio-player-with-playlist-v2/16698456?ref=Tean" target="_blank">
            <img alt="Brand" src="media/images/logo.png">
        </a>
    </div>
    <nav class="blog-nav">
      <a class="blog-nav-item" href="playlists.php">Playlists</a>
      <a id="nav-item-generator" class="blog-nav-item active" href="generator.php">HTML generator</a>
  </nav>
</div>
</div>

<div class="container">

  <div class="row">

    <div class="col-sm-12 blog-main">

    <form id="generate_code_form" action="" method="post">

        <div class="form-group" id="type_field">
            <label>Select playlists</label>
            <select id="type" name="type" class="selectpicker" data-live-search="true" title="Select one or more playlists..." multiple required>
                <?php while ($row = $playlists->fetch_array()) : ?>
                    <option value="<?php echo($row['id']); ?>" <?php if(isset($mediaArr) && in_array($row['id'], $mediaArr)) echo "selected" ?>><?php echo(htmlspecialchars($row['title'])); ?></option>
                <?php endwhile;  ?>
            </select>
        </div>

        <div class="form-group" id="playlist_code_field">
          <label>Code</label>
          <textarea id="playlist_code" class="form-control" name="custom_content" rows="20" placeholder="Select playlists you wish to be included in your webpage, click Generate code button, and playlist code will be generated here which you can copy inside your webpage."><?php echo $markup; ?></textarea>
        </div>

        <button id="generate_code" type="submit" name="submit" class="btn btn-success">Generate code</button> 

    </form>

<br><br><br>



</div><!-- /.blog-main -->

</div><!-- /.row -->

</div><!-- /.container -->


</body>
</html>


