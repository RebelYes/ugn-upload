<?php

class database{

	public $host = DB_HOST;
	public $user = DB_USER;
	public $pass = DB_PASS;
	public $db_name = DB_NAME;

	public $mysqli;

	public function __construct(){
		$this->init();
	}

	private function init(){

		$this->mysqli = new mysqli($this->host, $this->user, $this->pass, $this->db_name) or die($this->mysqli->error);

		//create tables
		$playlists = "CREATE TABLE IF NOT EXISTS `playlists` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `title` varchar(255) NOT NULL,
		  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

		$media = "CREATE TABLE IF NOT EXISTS `media` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `title` varchar(255) DEFAULT NULL,
		  `type` varchar(255) NOT NULL,
		  `path` varchar(1000) NOT NULL,
		  `limit` int(11) DEFAULT NULL,
		  `artist` varchar(255) DEFAULT NULL,
		  `share` varchar(1000) DEFAULT NULL,
		  `download` varchar(1000) DEFAULT NULL,
		  `link` varchar(1000) DEFAULT NULL,
		  `target` varchar(10) DEFAULT NULL,
		  `thumb` varchar(1000) DEFAULT NULL,
		  `start` int(11) DEFAULT NULL,
		  `end` int(11) DEFAULT NULL,
		  `playback_rate` decimal(3,2) DEFAULT NULL,
		  `thumb_default` varchar(1000) DEFAULT NULL,
		  `thumb_quality` varchar(50) DEFAULT NULL,
		  `id3` tinyint(1) DEFAULT NULL,
		  `custom_content` text,
		  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `order_id` int(11) DEFAULT NULL,
		  `playlist_id` int(11) NOT NULL,
		  PRIMARY KEY (`id`),
		  KEY `playlist_id` (`playlist_id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

		$this->mysqli->query($playlists) or die($this->mysqli->error);
		$this->mysqli->query($media) or die($this->mysqli->error);
		
		
	}

	public function prepare($query){

		$result = $this->mysqli->prepare($query);	
		return $result;

	}

	public function select($query){

		$result = $this->mysqli->query($query);	

		if($result->num_rows > 0){
			return $result;
		}else{
			return false;
		}
		
	}

	public function insert($query){

		$result = $this->mysqli->query($query);	
		$insert_id = $this->mysqli->insert_id;

		if($result)	{
			return $insert_id;
			header('location: index.php?msg=Success!');
		}else{
			printf("Errormessage: %s\n", $this->mysqli->error);
		}
		
	}

	public function multi($query){

		$result = $this->mysqli->multi_query($query);	

		if($result)	{
			header('location: index.php?msg=Success!');
		}else{
			printf("Errormessage: %s\n", $this->mysqli->error);
		}
	}

	public function delete($query){

		$result = $this->mysqli->query($query);	

		if($result)	{
			header('location: index.php?msg=Success!');
		}else{
			printf("Errormessage: %s\n", $this->mysqli->error);
		}
		
	}

	public function update($query){

		$result = $this->mysqli->query($query);	
		
	}

}

