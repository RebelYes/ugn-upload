<?php

if(isset($_REQUEST['title']) && isset($_REQUEST['id'])){

    include "libs/config.php";
    include "libs/database.php";

    $db = new database();

    $id = $_REQUEST['id'];
    $title = $_REQUEST['title'];

    $stmt = $db->prepare("UPDATE playlists SET title=? WHERE id=?");
    $stmt->bind_param('si', $title, $id);
    $stmt->execute();
    $stmt->close();

    echo json_encode($title);

}



?>

