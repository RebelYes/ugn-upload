<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>HTML5 Audio Player with Playlist by Tean</title>
       
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"><!-- icons -->
        <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" media="all" /><!-- playlist scroll -->
        <link rel="stylesheet" type="text/css" href="css/index_fixed_bottom6_continous.css" />
        
        <script type="text/javascript" src="js/jquery-1.12.0.min.js"></script>
        <script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script><!-- playlist scroll -->
        <script type="text/javascript" src="js/new_cb.js"></script>
        <script type="text/javascript" src="js/new.js"></script>            
        <script type="text/javascript">

            /* opens index_fixed_popup.html */


            /* START PLAYER TOGGLE CODE */
            
            var playerOpened = true, 
            playlistOpened,
            wrapper,
            playerOuter,
            playlistHolder,
            playbackBtn,
            hasLocalStorage = HAPUtils.hasLocalStorage(),
            css = 'css/hap_fixed_bottom6.css';

            function togglePlayer(target){

                if(playerOpened){
                    wrapper.stop().animate({'bottom': -playerOuter.height()-playlistHolder.height()+'px'}, {duration: 300, complete: function(){
                        playbackBtn.show();   
                    }});
                    target.find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up'); 
                    playlistOpened=false;
                }else{
                    wrapper.stop().animate({'bottom': -playlistHolder.height()+'px'}, {duration: 300});  
                    target.find('i').removeClass('fa-chevron-up').addClass('fa-chevron-down'); 
                    playbackBtn.hide();
                }   
                playerOpened = !playerOpened;
            }

            function togglePlaylist(){

                if(playlistOpened){
                    wrapper.stop().animate({'bottom': -playlistHolder.height()+'px'}, {duration: 400});
                }else{
                    wrapper.stop().animate({'bottom': 0+'px'}, {duration: 400});
                }   
                playlistOpened = !playlistOpened;
            }

            function setData(playing){
                if(playing) playbackBtn.find('i').removeClass('fa-play').addClass('fa-pause');
                else playbackBtn.find('i').removeClass('fa-pause').addClass('fa-play');
            }

            function openPopup(instance, sett){
                if(hasLocalStorage && localStorage.getItem('hap_popup_fixed'))return;
                if(hap_player || instance){
                    var player = hap_player || instance,
                    sourcePath = player.getSettings().sourcePath || '',
                    settings = {
                        popupUrl: sourcePath+'index_fixed_popup.html?3456863',
                        popupWidth: 900,
                        popupHeight: 350
                    };
                    hapOpenPopup(player, settings);
                }
            }

            /* END PLAYER TOGGLE CODE */



            var hap_player; 

            jQuery(document).ready(function($){
                
                var settings = {
                    instanceName:"fixed_bottom6",
                    sourcePath:"",
                    activePlaylist:"playlist-audio2",
                    activeItem:0,
                    volume:0.5,
                    autoPlay:true,
                    preload:"auto",
                    randomPlay:false,
                    loopingOn:true,
                    autoAdvanceToNextMedia:true,
                    youtubeAppId:"",
                    soundCloudAppId:"",
                    usePlaylistScroll:true,
                    playlistScrollOrientation:"vertical",
                    playlistScrollTheme:"minimal",
                    useTooltips:true,
                    useKeyboardNavigationForPlayback:true,
                    useDownload:false,
                    autoReuseDownloadMail:true,
                    useShare:true,
                    facebookAppId:"",
                    useNumbersInPlaylist: true,
                    numberTitleSeparator: ".  ",
                    artistTitleSeparator: " - ",
                    sortableTracks: false,
                    playlistItemContent:"title",
                    continousPlayback:true,
					continousKey:'hap_continous'
                };

                /* START PLAYER TOGGLE CODE */

                wrapper = $('#hap-wrapper'),
                playerOuter = $('.hap-player-outer'),
                playlistHolder = $('.hap-playlist-holder'),
                playbackBtn = $('.hap-playback-toggle-ex').on('click',function(e){
                    e.preventDefault();
                    hap_player.togglePlayback();
                });  

                /* END PLAYER TOGGLE CODE */

                if(hasLocalStorage){
                    //localStorage.removeItem('hap_popup_fixed');
                    if(!localStorage.getItem('hap_popup_fixed')){
                        hap_player = $("#hap-wrapper").hap(settings); 
                    }
                }else{
                   hap_player = $("#hap-wrapper").hap(settings);  
                }

            });

        </script>
        
    </head>
    <body>