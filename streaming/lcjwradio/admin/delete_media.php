<?php

include "libs/config.php";
include "libs/database.php";

if(isset($_GET['playlist_id']) && isset($_GET['media_id'])){

	$db = new database();

	$playlist_id = intval($_GET['playlist_id']);
	$media_id = intval($_GET['media_id']);
	$media = explode(",", $media_id);

	$qMarks = str_repeat('?,', count($media) - 1) . '?';
	$stmt = $db->prepare("DELETE FROM media WHERE playlist_id=? AND id IN ($qMarks)");

	if($stmt){

		array_unshift($media, $playlist_id);
		$params = str_repeat('i', count($media));
		$stmt->bind_param($params, ...$media);
		$stmt->execute();
		$stmt->close();

		header('location: edit_playlist.php?msg=Success!&playlist_id='.$playlist_id);

	}else{
		printf("Errormessage: %s\n", $stmt->error);

		header('location: edit_playlist.php?msgf=Failed!&playlist_id='.$playlist_id);
	}

}

?>

