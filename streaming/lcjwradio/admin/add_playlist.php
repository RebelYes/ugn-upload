<?php

  include "libs/config.php";
  include "libs/database.php";

if(isset($_POST['submit'])){

  $title = $_POST['title'];

  $db = new database();

  $stmt = $db->prepare("INSERT INTO playlists (title) VALUES (?)");

  if($stmt){

    $stmt->bind_param('s', $title);
    $stmt->execute();
    $stmt->close();

    header('location: index.php?msg=Success!');

  }else{

    header('location: index.php?msg=Failed!');

  }

}

?>





