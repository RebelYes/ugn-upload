<?php

include "libs/config.php";
include "libs/database.php";

if(isset($_GET['playlist_id'])){

	$db = new database();

	$playlist_id = intval($_GET['playlist_id']);

	//delete playlist
	$stmt = $db->prepare("DELETE FROM playlists WHERE id=?");
	$stmt->bind_param('i', $playlist_id);
	$stmt->execute();
	$stmt->close();


	//delete media
	$stmt = $db->prepare("DELETE FROM media WHERE playlist_id=?");
	$stmt->bind_param('i', $playlist_id);
	$stmt->execute();
	$stmt->close();

	if($stmt){

		header('location: index.php?msg=Success!');

	}else{
		printf("Errormessage: %s\n", $stmt->error);

		header('location: index.php?msgf=Failed!');
	}

}

?>

