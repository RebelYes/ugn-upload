<?php  
  
	require("db_data.php");
	
	if(!isset($_REQUEST['database']) || !isset($_REQUEST['table']) || !isset($_REQUEST['type']) || IsNullOrEmpty($username) || IsNullOrEmpty($password) || IsNullOrEmpty($hostname)) exit("Database information missing!");
	
	$database = $_REQUEST['database'];
	$table = $_REQUEST['table'];
	$type = $_REQUEST['type'];

	if(isset($_REQUEST['limit']))$limit = $_REQUEST['limit'];
	if(isset($_REQUEST['range_from']))$range_from = $_REQUEST['range_from'];
	if(isset($_REQUEST['range_to']))$range_to = $_REQUEST['range_to'];
	
	function IsNullOrEmpty($v){
		return (!isset($v) || trim($v)==='');
	}
	function int_to_bool( $value ) {
		return empty($value) ? false : true;
    }
	function true_or_custom( $value ) {
		//detect 1 (true) or custom download path
		if(strlen($value) == 1 && is_numeric($value) && $value === "1")return "";
		else return $value;
    }
  
	$connection = mysqli_connect($hostname,$username,$password);
	if(!$connection){
		die('Db connection failed: ' . mysqli_connect_errno());
	}
				
	$db_select = mysqli_select_db($connection, $database);
	if(!$db_select){
		die('Db selection failed: ' . mysqli_error($connection));
	}
	
	if(!IsNullOrEmpty($limit)){
		$result = mysqli_query($connection, "SELECT * FROM $table LIMIT $limit");
	}else if(!IsNullOrEmpty($range_from) && !IsNullOrEmpty($range_to)){
		$result = mysqli_query($connection, "SELECT * FROM $table LIMIT $range_from, $range_to");
	}else{
		$result = mysqli_query($connection, "SELECT * FROM $table");
	}
	
	if(!$result){
		die('Db query failed: ' . mysqli_error($connection));
	}

	while($row = mysqli_fetch_array($result)){
		if($type == 'database_data'){
			$new = array();
			
			if(isset($row['type']) && !empty($row['type']))$new['type'] = $row['type'];
			if(isset($row['mp3']) && !empty($row['mp3']))$new['mp3'] = $row['mp3'];
			if(isset($row['artist']) && !empty($row['artist']))$new['artist'] = $row['artist'];
			if(isset($row['title']) && !empty($row['title']))$new['title'] = $row['title'];
			if(isset($row['thumb']) && !empty($row['thumb']))$new['thumb'] = $row['thumb'];
			if(isset($row['link']) && !empty($row['link']))$new['link'] = $row['link'];
			if(isset($row['target']) && !empty($row['target']))$new['target'] = $row['target'];
			if(isset($row['download']) && !empty($row['download']))$new['download'] = true_or_custom($row['download']);
			if(isset($row['playbackRate']) && !empty($row['playbackRate']))$new['playbackRate'] = floatval($row['playbackRate']);
			if(isset($row['start']) && !empty($row['start']))$new['start'] = intval($row['start']);
			if(isset($row['end']) && !empty($row['end']))$new['end'] = intval($row['end']);
			
			if(isset($row['path']) && !empty($row['path']))$new['path'] = $row['path'];
			if(isset($row['limit']) && !empty($row['limit']))$new['limit'] = intval($row['limit']);
			if(isset($row['subdirs']) && !empty($row['subdirs']))$new['subdirs'] = int_to_bool($row['subdirs']);
			if(isset($row['id3']) && !empty($row['id3']))$new['id3'] = int_to_bool($row['id3']);
			if(isset($row['thumbDefault']) && !empty($row['thumbDefault']))$new['thumbDefault'] = $row['thumbDefault'];
			if(isset($row['noapi']) && !empty($row['noapi']))$new['noapi'] = int_to_bool($row['noapi']);
			if(isset($row['share']) && !empty($row['share']))$new['shareUrl'] = true_or_custom($row['share']);
			
		}else{//html
			$new = array(
				'path' => $row['path']
			);
		}
        $encode[] = $new;
	}
	echo json_encode( $encode );
		
	mysqli_close($connection);

?>