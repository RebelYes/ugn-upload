<?php

if(!isset($_GET['playlist_id'])) die("Unexpected error occured!");

include "libs/config.php";
include "libs/database.php";

$db = new database();

$playlist_id = intval($_GET['playlist_id']);

if(isset($_POST['update_playlist'])){

    $title = $_POST['title'];

    $stmt = $db->prepare("UPDATE playlists SET title=? WHERE id=?");
    $stmt->bind_param('si', $title, $playlist_id);
    $stmt->execute();
    $stmt->close();

    //update order
    $arr = explode(",",$_POST["media_order"]);

    $stmt = $db->prepare("UPDATE media SET order_id=? WHERE playlist_id=? AND id=?");

    for($i=0;$i<count($arr);$i++) {

        $stmt->bind_param('iii', $i, $playlist_id, $arr[$i]);
        $stmt->execute();
        
    }
    $stmt->close();

    $msg = 'Success!';

    

}

$stmt = $db->prepare("SELECT * FROM media WHERE playlist_id=? ORDER BY order_id");
$stmt->bind_param('i', $playlist_id);
$stmt->execute();
$media_query_result = $stmt->get_result();
$stmt->close();



$columns = array(
    'index' => 'Index',
    'checkbox' => 'Checkbox',
    'type' => 'Type',
    'path' => 'Path',
    'title' => 'Title',
    'author' => 'Author',
    'thumb' => 'Thumb',
    'thumb_quality' => 'Thumb quality',
    'thumb_default' => 'Thumb default',
    'share' => 'Share',
    'download' => 'Download',
    'link' => 'Link',
    'target' => 'Target',
    'start' => 'Start',
    'end' => 'End',
    'playback_rate' => 'Playback rate',
    'id3' => 'ID3',
    'limit' => 'Limit',
    'custom_content' => 'Custom content',
    'created' => 'Created',
    'edit' => 'Edit',
    'delete' => 'Delete'
);


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin panel</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="media/css/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="resources/syntax/shCore.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.2.0/css/rowReorder.dataTables.min.css">
    <link rel="stylesheet" href="css/bootstrap-select.css">

    <style type="text/css">

        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            vertical-align:baseline;
        }

        .table > thead > tr > th[class*="sort"]:after{
            content: "" !important;
        }

        .dataTables_scroll{
            overflow:auto;
        }

        .c-width{
            overflow: hidden; 
            text-overflow: ellipsis; 
            white-space: nowrap;
            max-width:300px;
        }

        .icounter{
            cursor: move;
        }

        .thumb_img{
            max-height:50px;
            width: auto;
        }

    </style>



    <script src="media/js/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="js/bootstrap-select.js"></script>

    <script type="text/javascript" src="media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="media/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="resources/syntax/shCore.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/rowreorder/1.2.0/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="js/edit_playlist.js"></script>


</head>

<body>

    <div class="blog-masthead">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="http://codecanyon.net/item/audio-player-with-playlist-v2/16698456?ref=Tean" target="_blank">
            <img alt="Brand" src="media/images/logo.png">
        </a>
    </div>
    <nav class="blog-nav">
      <a class="blog-nav-item" href="playlists.php">Playlists</a>
      <a class="blog-nav-item active" href="edit_playlist.php?playlist_id=<?php echo $playlist_id ?>">Edit playlist</a>
      <a id="nav-item-generator" class="blog-nav-item" href="generator.php">HTML generator</a>
  </nav>
</div>
</div>

<div class="container-fluid">

    <span id="column-select-wrap">
        <select id="column-select" name="column-select" class="selectpicker" data-live-search="true" data-count-selected-text="Select visible columns" data-selected-text-format="count>0" data-actions-box="true" multiple>
            <?php foreach ($columns as $key => $value) : ?>
                <option value="<?php echo($key); ?>"><?php echo($value); ?></option>
            <?php endforeach; ?>
        </select>
    </span>

  <div class="row">

    <div class="col-sm-12 blog-main">

      <?php 
      if(isset($_GET['msg']))echo("<div class='alert alert-success'>".$_GET['msg']."</div>"); 
      elseif(isset($msg))echo("<div class='alert alert-success'>".$msg."</div>");
      elseif(isset($_GET['msgf']))echo("<div class='alert alert-danger'>".$_GET['msgf']."</div>"); 
      ?>

      <form action="edit_playlist.php?playlist_id=<?php echo($playlist_id);?>" method="post">

      <table id="media-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>

            <tr>  
                <th>#</th>
                <th><label class="btn"><input id="sall" type="checkbox"></label></th>
                <th>Type</th>
                <th class="chwidth">Path</th>
                <th class="chwidth">Title</th>
                <th class="chwidth">Author</th>
                <th class="chwidth">Thumb</th>
                <th>Thumb quality</th>
                <th>Thumb default</th>
                <th class="chwidth">Share</th>
                <th class="chwidth">Download</th>
                <th class="chwidth">Link</th>
                <th>Target</th>
                <th>Start</th>
                <th>End</th>
                <th>Playback rate</th>
                <th>ID3</th>
                <th>Limit</th>
                <th class="chwidth">Custom content</th>
                <th>Created</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>

        </thead>

        <tbody>

          <?php if($media_query_result): ?>

              <?php while($media = $media_query_result->fetch_array()): ?>

                 <tr data-id="<?php echo($media['id']); ?>">  
                    <td><label class="btn"><input class="sind" type="checkbox"></label></td>
                    <td><?php echo($media['type']); ?></td>
                    <td><?php echo(htmlspecialchars($media['path'])); ?></td>
                    <td><?php echo(htmlspecialchars($media['title'])); ?></td>
                    <td><?php echo(htmlspecialchars($media['artist'])); ?></td>
                    <td><?php if(!empty($media['thumb'])) echo('<img class="thumb_img" src="../'.htmlspecialchars($media['thumb']).'"/>'); ?></td>
                    <td><?php echo($media['thumb_quality']); ?></td>
                    <td><?php if(!empty($media['thumb_default'])) echo('<img class="thumb_img" src="../'.htmlspecialchars($media['thumb_default']).'"/>'); ?></td>
                    <td><?php echo(htmlspecialchars($media['share'])); ?></td>
                    <td><?php echo(htmlspecialchars($media['download'])); ?></td>
                    <td><?php echo(htmlspecialchars($media['link'])); ?></td>
                    <td><?php echo($media['target']); ?></td>
                    <td><?php echo($media['start']); ?></td>
                    <td><?php echo($media['end']); ?></td>
                    <td><?php echo($media['playback_rate']); ?></td>
                    <td><?php echo($media['id3']); ?></td>
                    <td><?php echo($media['limit']); ?></td>
                    <td><?php echo(htmlspecialchars($media['custom_content'])); ?></td>
                    <td><?php echo($media['date']); ?></td>
                    <td><a href="add_media.php?playlist_id=<?php echo($playlist_id); ?>&media_id=<?php echo($media['id']); ?>" class="btn btn-success"><span class="glyphicon glyphicon-edit"></span></a></td>
                    <td><button data-playlist-id="<?php echo($playlist_id); ?>" type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#delete-modal"><span class="glyphicon glyphicon-trash"></span></button></td>
                </tr>

            <?php endwhile;  ?>

        <?php endif;  ?>

    </tbody>
</table>



<button type="submit" id="update_playlist" name="update_playlist" class="btn btn-primary">Save</button> 

<a href="index.php" class="btn btn-success">Cancel / Back</a>
<a href="#" id="delete-selected" data-playlist-id="<?php echo($playlist_id);?>" class="btn btn-danger" data-toggle="modal" data-target="#delete-selected-modal">Delete selected</a>

<a href="add_media.php?playlist_id=<?php echo($playlist_id);?>" class="btn btn-primary">Add media</a>

<input type="hidden" name="media_order" id="media_order" /> 

</form>

</div><!-- /.blog-main -->

</div><!-- /.row -->

</div><!-- /.container -->



<div id="delete-selected-modal" class="modal fade" tabindex="-1" role="dialog">
   <form id="delete-selected-form" action="edit_playlist.php?playlist_id=<?php echo($playlist_id); ?>" method="post">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Delete media</h4>
        </div>
        <div class="modal-body">

            <div class="form-group" style="max-width:500px;">
              <p>Are you sure you want to delete selected media?</p>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
        <button type="submit" name="submit" id="delete-modal-confirm" class="btn btn-danger">Ok</button>
    </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</form>
</div><!-- /.modal -->


<div id="delete-modal" class="modal fade" tabindex="-1" role="dialog">
    <form id="delete-media-form" action="" method="post">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Delete media</h4>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
            <button type="submit" name="submit" id="delete-modal-confirm" class="btn btn-danger">Ok</button>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</form>
</div><!-- /.modal -->

<div id="message-modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
       <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">No media selected!</h4>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
    </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->



</body>
</html>




