<?php

function handleUpload($file, $type){

    //var_dump($file);

    if($type == 'image'){

        $uploadsDirectory = '../uploads/thumb/'; 
        $max_file_size = 1024*1024;

    }else if($type == 'audio'){

        $uploadsDirectory = '../uploads/audio/'; 
        $max_file_size = 1024*1024*100;

    }

    if($file['error'] == UPLOAD_ERR_OK){ 

        if($file['size'] > 0 && $file['size'] < $max_file_size){

            $tmp_file = $file['tmp_name'];

            if(is_valid_type($type, $tmp_file)){

                //safe filename
                $name = preg_replace("/[^A-Za-z0-9._-]/i", "_", $file["name"]);

                //unique name
                $i = 0;
                $parts = pathinfo($name);
                while (file_exists($uploadsDirectory.$name)) {
                    $i++;
                    $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
                }

                $dest = $uploadsDirectory.$name;

                if (move_uploaded_file($tmp_file, $dest)) {

                    chmod($dest, 0644);//permission

                    return substr($dest, 3);
                    
                } 
            }
        }

        return null;
        
    }else{

        switch ($file['error']) { 
            case UPLOAD_ERR_INI_SIZE: 
                $message = "Value: 1; The uploaded file exceeds the upload_max_filesize directive in php.ini"; 
                break; 
            case UPLOAD_ERR_FORM_SIZE: 
                $message = "Value: 2; The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
                break; 
            case UPLOAD_ERR_PARTIAL: 
                $message = "Value: 3; The uploaded file was only partially uploaded"; 
                break; 
            case UPLOAD_ERR_NO_FILE: 
                $message = "Value: 4; No file was uploaded"; 
                break; 
            case UPLOAD_ERR_NO_TMP_DIR: 
                $message = "Value: 6; Missing a temporary folder"; 
                break; 
            case UPLOAD_ERR_CANT_WRITE: 
                $message = "Value: 7; Failed to write file to disk"; 
                break; 
            case UPLOAD_ERR_EXTENSION: 
                $message = "Value: 8; File upload stopped by extension"; 
                break; 

            default: 
                $message = "Unknown upload error"; 
                break; 
        } 

        //return $message;
        return null; 

    }

}

function validChars($str) {
    $str = preg_replace('/\s+/', '_', $str);
    $str = preg_replace('/[^A-Za-z0-9_-]/', '', $str);
    return $str;
}

function is_valid_type($type, $file){

    if($type == 'image'){

        $size = getimagesize($file);
        if(!$size) {
            return 0;
        }

        $valid_types = array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG, IMAGETYPE_BMP);

        if(in_array($size[2],  $valid_types)) {
            return 1;
        } else {
            return 0;
        }

    }
    if($type == 'audio'){

        $valid_types = array('audio/mpeg','audio/x-mpeg','audio/mp3','audio/x-mp3','audio/mpeg3','audio/x-mpeg3','audio/mpg','audio/x-mpg','audio/x-mpegaudio');

        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($finfo, $file);
        finfo_close($finfo);

        if(in_array($mime,  $valid_types)) {
            return 1;
        } else {
            return 0;
        }


    }
}


?>