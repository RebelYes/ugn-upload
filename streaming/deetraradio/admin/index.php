<?php

//check extensions
$req = array();

if (!extension_loaded('mysqlnd')){
   $req[] = 'mysqlnd';
}
if (!extension_loaded('fileinfo')){
   $req[] = 'fileinfo';
}
if(count($req)){
    $ex = implode(', ', $req);
    die("Required PHP extensions missing or not enabled: " . $ex);
}   

include "playlists.php";

