$(document).ready(function() {

    var table = $("#media-table"),
    tableObj;

    $("#media-table tbody tr").each(function(i, tr) {
        $(tr).attr('id', 'id'+i).prepend('<td class="icounter">'+parseInt(i+1)+'</td>');//for index column   
    }); 

    var columns_arr = ["index", "checkbox", "type", "path", "title", "author", "thumb", "thumb_quality", "thumb_default", "share", "download", "link", "target", "start", "end", "playback_rate", "id3", "limit", "custom_content", "created", "edit", "delete"];

    var i, len = columns_arr.length, data_columns = [];
    for(i=0;i<len;i++){
        data_columns.push({"name":columns_arr[i]});
    } 

    tableObj = table.on( 'init.dt', function () {

       table.wrap('<div class="dataTables_scroll" />');//fix for missaligned table header / rows in scroll mode

       $("#column-select-wrap").appendTo('#media-table_filter');//append column selector

    }).on( 'page.dt', function () {

        //var info = tableObj.page.info();
        //console.log( 'Showing page: '+info.page+' of '+info.pages );

        $("#sall").prop('checked', false);//reset main checkbox

    }).DataTable({
         
        rowReorder: true,
        columnDefs: [
            
            {
                "targets": 1,//checkbox select 
                "orderable": false
            },
            {
                "targets": [0,1],
                "width": "10px"
            },
            {
                render: function (data, type, full, meta) {
                    return "<div class='c-width'>" + data + "</div>";//custom width for some columns 
                },
                targets: ['chwidth']
            },
            
        ],
        stateSave: true,
        columns: data_columns

    });


    //column visiblity selector
    var columnSelector = $('#column-select').on('change',function(){

        var columns = $(this).val(),
        diff = $(columns_arr).not(columns).get();

        tableObj.columns().visible(true);//show all

        var i, len = diff.length;
        for(i=0;i<len;i++){
            tableObj.column( diff[i]+':name' ).visible(false);//hide selected
        }
        
    });

    function checkColumnsVisiblility(){
        var i, len = columns_arr.length, visible = [];
        for(i=0;i<len;i++){
            if(tableObj.column( columns_arr[i]+':name' ).visible()) visible.push(columns_arr[i]);
        }
        columnSelector.selectpicker('val',visible); 
        columnSelector.selectpicker('refresh');
    }
    checkColumnsVisiblility();//from stateSave




    $("#sall").on('click',function(e) {//select all checkbox on current page
        var checked = this.checked, data = [];

        var start = tableObj.page.info().start,
        i = start,
        rows = tableObj.rows({ page: 'current' }).nodes();

        $(rows).each(function() {
            $(this).find("input:checkbox[class=sind]").each(function(){
                this.checked = checked;
            });
        }); 

    });


    $('#update_playlist').on('click',function(){//save id for media order_id

        var arr = [];
        $(tableObj.rows().nodes()).each(function(){
            arr.push($(this).attr("data-id"));
        });
        document.getElementById("media_order").value = arr;

    });

    table.on('click', '.delete', function(e) {//delete individual

        var playlist_id = parseInt($(this).attr('data-playlist-id'),10), 
        media_id = parseInt($(this).parents().eq(1).attr('data-id'),10);

        var index = tableObj.row( $(this).parents().eq(1)[0] ).index(),
        title = tableObj.column( 'title:name' ).data()[index],
        path = tableObj.column( 'path:name' ).data()[index], 
        t = title != '' ? title : path;

        $('#delete-media-form').attr('action', 'delete_media.php?playlist_id='+playlist_id+'&media_id='+media_id);

        $('#delete-modal').find('.modal-body').html('<p>Are you sure you want to delete "'+t+'" ?</p>');

    });

    $('#delete-selected').on('click', function(e) {//delete selected

        var rows = $(tableObj.rows().nodes()),
        media_id = '';

        $(tableObj.rows().nodes()).find("input:checkbox[class=sind]:checked").each(function() {

            activeRow = parseInt($(this).parents().eq(2).attr('data-id'),10);
            media_id += activeRow + ',';
            
        }); 

        if(media_id.length == 0){
            $('#message-modal').modal('show');
            return false;
        }
        media_id = media_id.slice(0,-1);//remove last comma

        var playlist_id = parseInt($(this).attr('data-playlist-id'),10);

        $('#delete-selected-form').attr('action', 'delete_media.php?playlist_id='+playlist_id+'&media_id='+media_id);
        console.log('delete_media.php?playlist_id='+playlist_id+'&media_id='+media_id)

    });


});

