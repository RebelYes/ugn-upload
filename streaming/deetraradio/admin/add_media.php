<?php

if(!isset($_GET['playlist_id'])) die("Unexpected error occured!");

include "libs/config.php";
include "libs/database.php";
include "libs/functions.php";
include "file_upload.php";

$db = new database();

$playlist_id = intval($_GET['playlist_id']);
$media_id;

if(isset($_POST['submit_add']) || isset($_POST['submit_edit'])){

    $type = $_POST['type'];

    if(!empty($_FILES['path_upload']['name'])){
        $path = handleUpload($_FILES['path_upload'],'audio');
    }else{
        $path = trim($_POST['path']);
    }

    if(!empty($_FILES['thumb_upload']['name'])){
        $thumb = handleUpload($_FILES['thumb_upload'],'image');
    }else{
        $thumb = !IsNullOrEmpty($_POST['thumb']) ? $_POST['thumb'] : NULL;
    }

    if(!empty($_FILES['thumb_default_upload']['name'])){
        $thumb_default = handleUpload($_FILES['thumb_default_upload'],'image');
    }else{
        $thumb_default = !IsNullOrEmpty($_POST['thumb_default']) ? $_POST['thumb_default'] : NULL;
    }

    if(!empty($_FILES['download_upload']['name'])){
        $download = handleUpload($_FILES['download_upload'],'audio');
    }else{
        $download = !IsNullOrEmpty($_POST['download']) ? $_POST['download'] : NULL;
    }

    $title = !IsNullOrEmpty($_POST['title']) ? $_POST['title'] : NULL;
    $artist = !IsNullOrEmpty($_POST['artist']) ? $_POST['artist'] : NULL;
    $link = !IsNullOrEmpty($_POST['link']) ? $_POST['link'] : NULL;
    $target = ($link != NULL) ? (!IsNullOrEmpty($_POST['target']) ? $_POST['target'] : NULL) : NULL;
    $custom_content = !IsNullOrEmpty($_POST['custom_content']) ? $_POST['custom_content'] : NULL;
    $id3 = ($type == 'folder') ? (!IsNullOrEmpty($_POST['id3']) ? $_POST['id3'] : NULL) : NULL;
    $start = !IsNullOrEmpty($_POST['start']) ? $_POST['start'] : NULL;
    $end = !IsNullOrEmpty($_POST['end']) ? $_POST['end'] : NULL;
    $playback_rate = !IsNullOrEmpty($_POST['playback_rate']) ? $_POST['playback_rate'] : NULL;
    $limit = !IsNullOrEmpty($_POST['limit']) ? $_POST['limit'] : NULL;
    $thumb_quality = ($type == 'soundcloud') ? (!IsNullOrEmpty($_POST['thumb_quality']) ? $_POST['thumb_quality'] : NULL) : NULL;
    $share = !IsNullOrEmpty($_POST['share']) ? $_POST['share'] : NULL;




    if(isset($_POST['submit_add'])){ 

        $stmt = $db->prepare("SELECT MAX(order_id) AS order_id FROM media WHERE playlist_id=?");
        $stmt -> bind_param("i", $playlist_id);
        $stmt -> execute();
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();
        $max_order_id = intval($data['order_id']);
        if($max_order_id>0)$max_order_id+=1;

        $stmt = $db->prepare("
            INSERT INTO media (type, path, title, artist, thumb, thumb_default, thumb_quality, share, download, link, target, custom_content, id3, start, end, playback_rate, `limit`, playlist_id, order_id)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

        $stmt -> bind_param("ssssssssssssiiidiii",$type, $path, $title, $artist, $thumb, $thumb_default, $thumb_quality, $share, $download, $link, $target, $custom_content, $id3, $start, $end, $playback_rate, $limit, $playlist_id, $max_order_id);

    }elseif (isset($_POST['submit_edit'])){

        $media_id = intval($_GET['media_id']);

        $stmt = $db->prepare("
            UPDATE media SET type=?, path=?, title=?, artist=?, thumb=?, thumb_default=?, thumb_quality=?, share=?, download=?, link=?, target=?, custom_content=?, id3=?, start=?, end=?, playback_rate=?, `limit`=?
            WHERE id=? AND playlist_id=?");

        $stmt -> bind_param("ssssssssssssiiidiii",$type, $path, $title, $artist, $thumb, $thumb_default, $thumb_quality, $share, $download, $link, $target, $custom_content, $id3, $start, $end, $playback_rate, $limit, $media_id, $playlist_id);

    }

    if($stmt){

        $stmt -> execute();
        $stmt -> close();

        header('location: edit_playlist.php?playlist_id='.$playlist_id.'&msg=Success!');

    }else{

        header('location: edit_playlist.php?playlist_id='.$playlist_id.'&msg='.$stmt->error);
    }

    exit;

}else{

    $data = '';   
    $menu_title = 'Add media';  
    $submit_action = 'submit_add';  

    $type = array(
        'audio' => 'audio', 
        'soundcloud' => 'soundcloud', 
        'podcast' => 'podcast', 
        'folder' => 'folder', 
        'gdrive-folder' => 'google drive folder', 
        'youtube_single' => 'youtube single', 
        'youtube_playlist' => 'youtube playlist' 
        );

    $thumb_quality = array(
        't67x67.jpg' => 't67x67 (67×67px)', 
        'large.jpg' => 'large (100×100px)', 
        't300x300.jpg' => 't300x300 (300×300px)', 
        'crop.jpg' => 'crop (400×400px)', 
        't500x500.jpg' => 't500x500 (500x500px)',
        );

    if(isset($_GET['media_id'])){

      $media_id = intval($_GET['media_id']);

      $stmt = $db->prepare("SELECT * FROM media WHERE id=?");
      $stmt->bind_param('i', $media_id);
      $stmt->execute();
      $result = $stmt->get_result();
      $data = $result->fetch_assoc();
      $stmt->close();

      $menu_title = 'Edit media';  
      $submit_action = 'submit_edit';  

  }


}





?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin panel</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap-select.css">

    <style type="text/css">

      .form-group label{
          display: block;
      }
      .form-group input[type=file]{
          margin-top:5px;
          margin-left:30px;
          float: left;
      }
      .form-group{
          margin-bottom: 25px;
          overflow: hidden;
          clear: both;
      }
      .fgdata{
          float: left;
          width:50%;
      }

      #type_field{
        overflow: visible;
      }

      .thumb_img{
        float: left;
        clear: both;
        margin-top: 5px;
        max-width:120px;
        height: auto;
      }

      .desc{
         position: relative;
         clear:both;
         font-size: 12px;
         top: 5px;
         left:12px;
         width: 50%;
      }
      .desc span.glyphicon{
          color:#428bca;
      }

      .info-light{
          color:#aaa;
      }
      .info-light2{
          color:#ccc;
      }
      .info-highlight{
          color:#d9534f;
      }

</style>

<script src="media/js/jquery.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/add_media.js"></script>


</head>
<body>

    <div class="blog-masthead">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="http://codecanyon.net/item/audio-player-with-playlist-v2/16698456?ref=Tean" target="_blank">
            <img alt="Brand" src="media/images/logo.png">
        </a>
    </div>
    <nav class="blog-nav">
      <a class="blog-nav-item" href="index.php">Playlists</a>
      <a class="blog-nav-item active" href="#"><?php echo($menu_title); ?></a>
  </nav>
</div>
</div>

<div class="container">

  <div class="row">

    <div class="col-sm-12 blog-main">

      <form id="fgform" action="add_media.php?playlist_id=<?php echo($playlist_id); if(isset($media_id))echo('&media_id='.$media_id); ?>" method="post" enctype="multipart/form-data">

        <div class="form-group" id="type_field">
            <label>Select media type</label>
            <select id="type" name="type" class="selectpicker" data-live-search="true" required <?php if(isset($data['type'])) echo 'disabled' ?>>
                <?php foreach ($type as $key => $value) : ?>
                    <option value="<?php echo($key); ?>" <?php if(isset($data['type']) && $data['type'] == $key) echo 'selected' ?>><?php echo($value); ?></option>
                <?php endforeach; ?>
            </select>
        </div>

        <div class="form-group" id="path_field">
          <label>Path *</label>
          <input type="text" id="path" name="path" class="form-control fgdata" placeholder="" required pattern=".*\S+.*" value="<?php if(isset($data['path'])) echo (htmlspecialchars($data['path'])); ?>">
          <input type="hidden" name="MAX_FILE_SIZE" value="100000000" />
          <input type="file" id="path_upload" name="path_upload" accept="audio/mp3,audio/mpeg">

          <p id="gdinfo" class="desc"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;Enter folder ID which needs to be public, for example (part in red): <br><span class="info-light2">https://drive.google.com/drive/folders/</span><span class="info-highlight">0ByzcNpNrQNpWbjJGY19NSFF0R3M</span></p>
          <p id="folderinfo" class="desc"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;Place your folder with mp3 files in 'mp3-dir' directory and enter folder name here.</p>
          <p id="ytsinfo" class="desc"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;Enter Youtube single video ID, for example (part in red): <br><span class="info-light2">https://www.youtube.com/watch?v=</span><span class="info-highlight">lWA2pjMjpBs</span></p>
          <p id="ytpinfo" class="desc"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;Enter Youtube playlist ID, for example (part in red): <br><span class="info-light2">https://www.youtube.com/playlist?list=</span><span class="info-highlight">LL2pmfLm7iq6Ov1UwYrWYkZA</span></p>
      </div>

      <div class="form-group" id="id3_field">
          <label>Read ID3 tags</label>
          <select name="id3" class="form-control fgdata">
            <option value="0" <?php if(isset($data['id3']) && $data['id3'] == "0") echo 'selected' ?>>no</option>
            <option value="1" <?php if(isset($data['id3']) && $data['id3'] == "1") echo 'selected' ?>>yes</option>
            </select>
        </div>

        <div class="form-group" id="limit_field">
              <label>Results limit</label>
              <input type="number" name="limit" min="1" step="1" class="form-control fgdata" placeholder="Number of results to retrive (default:all)" value="<?php if(isset($data['limit'])) echo ($data['limit']); ?>">
          </div>

          <div class="form-group" id="title_field">
              <label>Title</label>
              <input type="text" name="title" class="form-control fgdata" placeholder="Enter title" value="<?php if(isset($data['title'])) echo (htmlspecialchars($data['title'])); ?>">
          </div>

          <div class="form-group" id="artist_field">
              <label>Artist</label>
              <input type="text" name="artist" class="form-control fgdata" placeholder="Enter artist" value="<?php if(isset($data['artist'])) echo (htmlspecialchars($data['artist'])); ?>">
          </div>

          <div class="form-group" id="thumb_field">
              <label>Thumbnail</label>
              <input type="text" id="thumb" name="thumb" class="form-control fgdata" placeholder="Enter thumbnail path" value="<?php if(isset($data['thumb'])) echo (htmlspecialchars($data['thumb'])); ?>">
              <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
              <input type="file" id="thumb_upload" name="thumb_upload" accept="image/x-png,image/gif,image/jpeg">
              <?php if(isset($data['thumb'])): ?>
                <img class="thumb_img" src="../<?php echo(htmlspecialchars($data['thumb'])); ?>"/>
              <?php endif; ?>
          </div>

          <div class="form-group" id="thumb_default_field">
              <label>Default thumbnail</label>
              <input type="text" id="thumb_default" name="thumb_default" class="form-control fgdata" placeholder="Enter default thumbnail path for songs that do not have thumbnail available with api" value="<?php if(isset($data['thumb_default'])) echo (htmlspecialchars($data['thumb_default'])); ?>">
              <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
              <input type="file" id="thumb_default_upload" name="thumb_default_upload" accept="image/x-png,image/gif,image/jpeg">
              <?php if(isset($data['thumb_default'])): ?>
                <img class="thumb_img" src="../<?php echo(htmlspecialchars($data['thumb_default'])); ?>"/>
              <?php endif; ?>
          </div>

          <div class="form-group" id="thumb_quality_field">
              <label>Thumbnail quality</label>
              <select name="thumb_quality" class="form-control fgdata">
                <?php foreach ($thumb_quality as $key => $value) : ?>
                    <option value="<?php echo($key); ?>" <?php if(isset($data['thumb_quality']) && $data['thumb_quality'] == $key) echo 'selected' ?>><?php echo($value); ?></option>
                <?php endforeach; ?>
            </select>
        </div>

        <div class="form-group" id="share_field">
          <label>Share url</label>
          <input type="text" name="share" class="form-control fgdata" placeholder="Enter share link" value="<?php if(isset($data['share'])) echo (htmlspecialchars($data['share'])); ?>">

          <p id="shareinfo" class="desc"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;Enter custom share link or enter "1" which will share playing song link.</p>
        </div>

        <div class="form-group" id="download_field">
          <label>Download path</label> 
          <input type="text" id="download" name="download" class="form-control fgdata" placeholder="Enter download path" value="<?php if(isset($data['download'])) echo (htmlspecialchars($data['download'])); ?>">
          <input type="hidden" name="MAX_FILE_SIZE" value="100000000" />
          <input type="file" id="download_upload" name="download_upload" accept="audio/*">

          <p id="dlinfo" class="desc"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;Enter custom download link or enter "1" which will download playing song link (if download is possible for specific media type).</p>
        </div>

        <div class="form-group" id="link_field">
          <label>Web Url link</label>
          <input type="text" name="link" class="form-control fgdata" placeholder="Enter web url link for playlist" value="<?php if(isset($data['link'])) echo (htmlspecialchars($data['link'])); ?>">
        </div>

        <div class="form-group" id="target_field">
          <label>Web Url target</label>
          <select name="target" class="form-control fgdata">
            <option value="_blank" <?php if(isset($data['target']) && $data['target'] == "_blank") echo 'selected' ?>>blank</option>
            <option value="_parent" <?php if(isset($data['target']) && $data['target'] == "_parent") echo 'selected' ?>>parent</option>
        </select>
        </div>

        <div class="form-group" id="start_field">
          <label>Start time</label>
          <input type="number" name="start" min="0" step="1" class="form-control fgdata" placeholder="Enter start time in seconds" value="<?php if(isset($data['start'])) echo ($data['start']); ?>">
        </div>

        <div class="form-group" id="end_field">
          <label>End time</label>
          <input type="number" name="end" min="0" step="1" class="form-control fgdata" placeholder="Enter end time in seconds" value="<?php if(isset($data['end'])) echo ($data['end']); ?>">
        </div>

        <div class="form-group" id="rate_field">
          <label>Playback rate</label>
          <input type="number" name="playback_rate" step="0.1" class="form-control fgdata" placeholder="Enter playback rate" value="<?php if(isset($data['playback_rate'])) echo ($data['playback_rate']); ?>">
        </div>

        <div class="form-group" id="custom_content_field">
          <label>Custom content</label>
          <textarea class="form-control fgdata" name="custom_content" rows="3" placeholder="Enter Custom content for playlist items (HTML allowed)"><?php if(isset($data['custom_content'])) echo (htmlspecialchars($data['custom_content'])); ?></textarea>

          <p id="ccontentinfo" class="desc"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;Example, add shopping cart icon in playlist:
          <br><span class="info-light">&lt;a href="http://www.google.com" title="buy"&gt;&lt;i class="fa fa-cart-arrow-down"&gt;&lt;/i&gt;&lt;/a&gt;</span><br>
          (Note: will require additional css)</p>
        </div>

        <a href="edit_playlist.php?playlist_id=<?php echo($playlist_id);?>" class="btn btn-danger">Cancel</a>
        <button type="submit" name="<?php echo($submit_action); ?>" class="btn btn-success">Submit</button> 

        </form>

<br><br><br>



</div><!-- /.blog-main -->

</div><!-- /.row -->

</div><!-- /.container -->


<div id="message-modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
       <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Upload progress</h4>
    </div>
    <div class="modal-body">
        <div class="form-group">
            <p>Please wait while the files are being uploaded...</p>
        </div>
        <div class="progress">
          <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
          </div>
        </div>
      </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
    </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


</body>
</html>


